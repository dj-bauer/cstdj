#include "queue_impl.h"

QUEUE_IMPL_FULL(i, int, "%d")
QUEUE_IMPL_FULL(f, float, "%f")
QUEUE_IMPL_FULL(s, char*, "%s")
QUEUE_IMPL_FULL(u64, uint64_t, "%"PRIu64)
QUEUE_IMPL_FULL(v, void*, "%p")
