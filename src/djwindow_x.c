#include "djwindow.h"
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <assert.h>
#include <X11/Xatom.h>
#include "logger.h"

#define SUBSCRIBE_MASK (ExposureMask|ButtonPressMask|KeyPressMask|ButtonReleaseMask|KeyReleaseMask|FocusChangeMask|PointerMotionMask)

#include "x_window.h"

// We need this to create the OpenGL 3.0+ context
typedef GLXContext (*glXCreateContextAttribsARBProc)(Display*, GLXFBConfig, GLXContext, Bool, const int*);
static glXCreateContextAttribsARBProc glXCreateContextAttribsARB = 0;

static bool init_gl(DJWindow* win);
static bool init_x_draw(DJWindow* win);
static void dj_win_keyevent_to_char(char ch[4], const DJEvent* event);

static void clear_subscribers(DJWindow* win, DJEventType type)
{
	if(!win) return;
	if(!win->callbacks[type]) return;

	while(win->callbacks[type]->next) {
		EventCallback* parent = win->callbacks[type];
		while(parent->next->next) {
			parent = parent->next;
		}
		free(parent->next);
		parent->next = NULL;
	}
	free(win->callbacks[type]);
	win->callbacks[type] = NULL;
}

static void create_close_event(DJWindow* window, XEvent* e) {
	DJEvent event = {.close = {.type = WinCloseEvent, .win = window}};
	dispatch_event(window, WinCloseEvent, &event);
}
static void create_motion_event(DJWindow* window, XEvent* e) {
	DJEvent event;
	XMotionEvent* ev = (XMotionEvent*) e;
	event.mousemove.x = ev->x;
	event.mousemove.y = ev->y;
	dispatch_event(window, MouseMotionEvent, &event);
}
static void create_buttonpress_event(DJWindow* window, XEvent* e){
	DJEvent event;
	XButtonEvent* ev = (XButtonEvent*) e;
	if(ev->button == 4 || ev->button == 5) {
		event.mousescroll.direction = ev->button == 4 ? 1 : -1;
		dispatch_event(window, MouseScrollEvent, &event);
	} else {
		event.buttonpress.button = ev->button;
		event.buttonpress.x = ev->x;
		event.buttonpress.y = ev->y;
		dispatch_event(window, MousePressEvent, &event);
	}
}
static void create_buttonrelase_event(DJWindow* window, XEvent* ev){
	DJEvent event;
	XButtonEvent* e = (XButtonEvent*) ev;
	// We don't care about the mousewheel scrolling release
	if(e->button == 4 || e->button == 5) return;
	event.buttonrelease.button = e->button;
	event.buttonrelease.x = e->x;
	event.buttonrelease.y = e->y;
	dispatch_event(window, MouseReleaseEvent, &event);
}
static void create_keypress_event(DJWindow* window, XEvent* ev){
	XKeyPressedEvent* e = (XKeyPressedEvent*) ev;

	DJEvent event = {.keypress = {
		.key = e->keycode,
		.win = window,
		.modifier = 0,
		.utf8_name = NULL, // TODO: Obtain a friendly name for these names
	}};
	if(e->state & ShiftMask)
		event.keypress.modifier |= ShiftModifier;
	if(e->state & ControlMask)
		event.keypress.modifier |= CtrlModifier;
	if(e->state & Mod1Mask)
		event.keypress.modifier |= AltModifier;
	if(e->state & Mod4Mask)
		event.keypress.modifier |= WindowsModifier;
	if(e->state & Mod5Mask)
		event.keypress.modifier |= AltGrModifier;
	event.keypress.utf8_name = NULL;
	char buf[4];
	dj_win_keyevent_to_char(buf, &event);
	event.keypress.utf8_ch = buf;
	
	dispatch_event(window, KeyPressEvent, &event);
}
static void create_keyrelease_event(DJWindow* window, XEvent* ev){
	XKeyReleasedEvent* e = (XKeyReleasedEvent*) ev;
	DJEvent event = {.keyrelease = {.type = KeyRelease, .key = e->keycode}};
	dispatch_event(window, KeyReleaseEvent, &event);
}
static void create_resize_event(DJWindow* window, XEvent* ev) {
	DJEvent event;
	XExposeEvent* e = (XExposeEvent*) ev;
	window->size.x = e->width;
	window->size.y = e->height;

	event.resize.type = WinResizeEvent;
	event.resize.width = e->width;
	event.resize.height = e->height;
	dispatch_event(window, WinResizeEvent, &event);
}
static void create_focus_event(DJWindow* window, XEvent* ev) {
    (void) ev;
	DJEvent event;
	dispatch_event(window, WinFocusEvent, &event);
}
static void create_unfocus_event(DJWindow* window, XEvent* ev) {
    (void) ev;
	DJEvent event;
	dispatch_event(window, WinUnfocusEvent, &event);
}

DJWindow* dj_win_init(DrawType drawType)
{
	DJWindow* win = calloc(1, sizeof(DJWindow));
	if(win == NULL) return NULL;
	win->drawtype = drawType;
	win->is_fullscreen = false;

	win->disp = XOpenDisplay(NULL);
	if(!win->disp) {
		log_error("Could not open X display");
		free(win);
		return NULL;
	}
	win->screen = DefaultScreen(win->disp);

	win->xim = XOpenIM(win->disp, 0, 0, 0);
  	win->xic = XCreateIC(win->xim, XNInputStyle, XIMPreeditNothing | XIMStatusNothing, NULL);

	bool ctx = false;
	switch(drawType) {
		case(DRW_GL):
			ctx = init_gl(win);
			break;
		case(DRW_X11):
			ctx = init_x_draw(win);
			break;
		default:
			log_error("This rendering type is not supported");
			break;
	}
	if(!win->win || !ctx) {
		XCloseDisplay(win->disp);
		log_error("Could not open simple window");
		free(win);
		return NULL;
	}

	// Prevent the X killing of the window
	//Atom WM_DELET_WINDOW = XInternAtom(win->disp, "WM_DELETE_WINDOW", False);
	//XSetWMProtocols(win->disp, win->win, &WM_DELET_WINDOW, 1);
	XSetStandardProperties(win->disp, win->win, "My Window!", "HI!", None, NULL, 0, NULL);
	XSelectInput(win->disp, win->win, SUBSCRIBE_MASK);
	XClearWindow(win->disp, win->win);
	// Actually show the window
	XMapWindow(win->disp, win->win);

	// Window protocols
	win->atom_protocols[0] = XInternAtom(win->disp, "WM_DELETE_WINDOW", False);
	XSetWMProtocols(win->disp, win->win, win->atom_protocols, sizeof(win->atom_protocols)/sizeof(Atom));

	return win;
}

void dj_win_close(DJWindow** win) 
{
	DJWindow* obj = *win;
	if(!obj) return;

	for(size_t i=0; i<EventCount; i++) 
		clear_subscribers(*win, i);

	/*if(obj->visual) {
		XFree(obj->visual);
	}*/
	if(obj->gl_context) {
		glXDestroyContext(obj->disp, obj->gl_context);
	}

	XDestroyWindow(obj->disp, obj->win);
	XCloseDisplay(obj->disp);
	free(obj);
	*win = NULL;
}

void dj_win_update(DJWindow* win) 
{
	if(!win) return;
	XEvent e;
	while(XPending(win->disp)) {
		XNextEvent(win->disp, &e);
		switch(e.type) {
			case(MotionNotify):
				create_motion_event(win, &e); break;
			case(ButtonPress):
				create_buttonpress_event(win, &e); break;
			case(ButtonRelease):
				create_buttonrelase_event(win, &e); break;
			case(KeyPress):
				create_keypress_event(win, &e); break;
			case(KeyRelease):
				create_keyrelease_event(win, &e); break;
			case(FocusIn):
				create_focus_event(win, &e); break;
			case(FocusOut):
				create_unfocus_event(win, &e); break;
			case(Expose):
				create_resize_event(win, &e); break;
			case(ClientMessage): {
				if((Atom)e.xclient.data.l[0] == win->atom_protocols[0]) {
					create_close_event(win, &e);
				} else {
					log_error("Unknown client message received");
				}
			} break;
			default:
				log_trace("The XEvent %d has not been handled", e.type);
				break;
		}
	}
}

void dj_win_set_title(DJWindow *win, const char* str) {
	if(!win) return;
	XStoreName(win->disp, win->win, str);
}

bool init_gl(DJWindow* win)
{
	if(win == NULL) return false;

	// Framebuffer config query
	int attributes[] = {GLX_X_RENDERABLE, True,
						GLX_DRAWABLE_TYPE, GLX_WINDOW_BIT,
						GLX_RENDER_TYPE, GLX_RGBA_BIT,
		                GLX_X_VISUAL_TYPE, GLX_TRUE_COLOR, 
						GLX_DOUBLEBUFFER, True,
						GLX_DEPTH_SIZE, 24,
						None};
	int fbcount;
	GLXFBConfig* fbconfs = glXChooseFBConfig(win->disp, win->screen, attributes, &fbcount);
	GLXFBConfig fbconf_choice;
	if(!fbcount) {
		log_error("could not setup OpenGL context. No appropiate fbconfig found.");
		return false;
	}
	fbconf_choice = fbconfs[0];
	XFree(fbconfs);

	// Context creation
	glXCreateContextAttribsARB = (glXCreateContextAttribsARBProc) glXGetProcAddressARB((const GLubyte *) "glXCreateContextAttribsARB");
	int version[2] = {4,3};
	int context_attribs[] =
      {
        GLX_CONTEXT_MAJOR_VERSION_ARB, version[0],
        GLX_CONTEXT_MINOR_VERSION_ARB, version[1],
        GLX_CONTEXT_PROFILE_MASK_ARB, GLX_CONTEXT_CORE_PROFILE_BIT_ARB,
        None
      };
    win->gl_context = glXCreateContextAttribsARB( win->disp, fbconf_choice, NULL, True, context_attribs );

	if(!win->gl_context) {
		log_error("Error while trying to create GL context. Tried minimum version %d.%d", version[0], version[1]);
		return false;
	}

	// Window creation
	XVisualInfo* visual = glXGetVisualFromFBConfig(win->disp, fbconf_choice);
	Colormap cmap = XCreateColormap(win->disp, RootWindow(win->disp, win->screen), visual->visual, AllocNone);
	win->attributes.colormap = cmap;
	win->attributes.event_mask = SUBSCRIBE_MASK;

	static const int border_width = 0;
	win->win = XCreateWindow(win->disp, RootWindow(win->disp, win->screen),
			0, 0, 600, 600,
			border_width, visual->depth, InputOutput, visual->visual,
			CWColormap | CWEventMask, &(win->attributes));
	win->size.x = 600; win->size.y = 600;

	XFree(visual);

	dj_win_bind_gl(win);

	return true;
}

void dj_win_bind_gl(DJWindow *win) {
	if(!win) return;
	if(!win->gl_context) return;

	glXMakeCurrent(win->disp, win->win, win->gl_context);
}

void dj_win_flip(DJWindow* win) {
	if(!win) return;
	switch(win->drawtype) {
		case DRW_GL:
			if(!win->gl_context) return;
			glXSwapBuffers(win->disp, win->win);
			break;
		case DRW_X11:
			XFlush(win->disp);
			break;
		default:
			break;
	}
}

bool init_x_draw(DJWindow* win) {
	unsigned long black,white;
	black = BlackPixel(win->disp, win->screen);
	white = WhitePixel(win->disp, win->screen);

	win->win = XCreateSimpleWindow(win->disp, RootWindow(win->disp, win->screen), 0, 0,
			200, 300, 5, white, black);
	if(!win->win) return false;
	win->size.x = 200;
	win->size.y = 300;

	win->gc = DefaultGC(win->disp, win->screen);
	XSetBackground(win->disp, win->gc, black);
	XSetForeground(win->disp, win->gc, white);

	return true;
}

unsigned int dj_win_width(const DJWindow *win)
{
	return win->size.x;
}

unsigned int dj_win_height(const DJWindow *win)
{
	return win->size.y;
}

void dj_win_keyevent_to_char(char ch[4], const DJEvent* event)
{
	KeySym ignore;
	Status ret_status;
	XKeyPressedEvent ev = {
		.type = KeyPress,
		.display = event->keypress.win->disp,
		.state = 0,
		.keycode = event->keypress.key,
	};
	if(event->keypress.modifier & ShiftModifier)
		ev.state |= ShiftMask;
	if(event->keypress.modifier & AltGrModifier)
		ev.state |= Mod5Mask;
	Xutf8LookupString(event->keypress.win->xic, &ev, ch, 4, &ignore, &ret_status);
}

void dj_win_set_fullscreen(DJWindow* window, bool value)
{
	Atom atom_state = XInternAtom(window->disp, "_NET_WM_STATE", False);
	Atom atom_fullscreen = XInternAtom(window->disp, "_NET_WM_STATE_FULLSCREEN", False);
	XEvent event = {ClientMessage};
	event.xclient.window = window->win;
	event.xclient.format = 32;
	event.xclient.message_type = atom_state;
	event.xclient.data.l[0] = value ? 0 : 1; // 0 is add, 1 is remove
	event.xclient.data.l[1] = atom_fullscreen;
	event.xclient.data.l[2] = 0;
	event.xclient.data.l[3] = 1;
	event.xclient.data.l[4] = 0;
	XSendEvent(window->disp, RootWindow(window->disp, window->screen), False, SubstructureNotifyMask | SubstructureRedirectMask, &event);

	window->is_fullscreen = value;
}
bool dj_win_get_fullscreen(const DJWindow* window)
{
	return window->is_fullscreen;
}

void dj_win_set_icon(DJWindow* window, int width, int height, const unsigned char* icon_rgba, const char* icon_title)
{
	Pixmap pixmap = XCreateBitmapFromData(window->disp, window->win, (const char*) icon_rgba, width, height);
	if(!pixmap) {
		log_error("Couldn't create pixmap");
		return;
	}
	XWMHints* hints = XAllocWMHints();
	hints->flags = IconPixmapHint;
	hints->icon_pixmap = pixmap;
	hints->icon_x = 0;
	hints->icon_y = 0;
	XSetWMHints(window->disp, window->win, hints);

	if(icon_title) {
		XTextProperty icon_name_prop;
		char* strings[] = {(char*) icon_title, NULL};
		int rc = XStringListToTextProperty(strings, 1, &icon_name_prop);
		assert(rc);
		XSetWMIconName(window->disp, window->win, &icon_name_prop);
	}
	XFree(hints);
}

#include "djwindow_common.h"
