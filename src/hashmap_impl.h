#ifndef DJHASHMAP_IMPL_H
#define DJHASHMAP_IMPL_H
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include "hashmap.h"
#include "util.h"
#include "logger.h"

uint64_t next_pow2(uint64_t x)
{
	x -= 1;
	x |= (x >> 1);
	x |= (x >> 2);
	x |= (x >> 4);
	x |= (x >> 8);
	x |= (x >> 16);
	x |= (x >> 32);
	
	return x + 1;
}

#define HASHMAP_IMPL_FMT(suffix, T, fmt)

#define HASHMAP_IMPL(suffix, T) \
	static void __hashmap_resize_##suffix(Hashmap_##suffix* hm, size_t dest_cap) \
	{ \
		assert(hm->capacity < dest_cap); \
		size_t old_cap = hm->capacity; \
		T* old_data = hm->data; \
		bool* old_probing = hm->cell_probed; \
		bool* old_filled = hm->cell_filled; \
		\
		hm->capacity = dest_cap; \
		hm->data = malloc(sizeof(T) * hm->capacity); \
		hm->cell_probed = malloc(sizeof(bool) * hm->capacity); \
		hm->cell_filled = malloc(sizeof(bool) * hm->capacity); \
		memset(hm->cell_filled, false, sizeof(bool) * hm->capacity); \
		memset(hm->cell_probed, false, sizeof(bool) * hm->capacity); \
		assert(hm->data != NULL); \
		hm->num_elements = 0; \
		for(size_t i=0; i<old_cap; i++) { \
			if(old_filled[i]) \
				hashmap_insert_##suffix(hm, old_data[i]); \
		} \
		free(old_data); \
		free(old_probing); \
		free(old_filled); \
	} \
	void init_hashmap_##suffix(Hashmap_##suffix* hm, bool (*cmp_func)(T*,T*),size_t (*hash_func)(T*)) \
	{ \
		hm->cmp_func = cmp_func; \
		hm->hash_func = hash_func; \
		hm->data = NULL; \
		hm->cell_probed = NULL; \
		hm->cell_filled = NULL; \
		hm->capacity = 0; \
		hm->num_elements = 0; \
		/* We need to have a minimum size, so we dont resize again on the insert of the first resize caused by inserting the second element. Boah, that was a sentence!*/ \
		__hashmap_resize_##suffix(hm, 2); \
	} \
	void deinit_hashmap_##suffix(Hashmap_##suffix* hm) \
	{ \
		free(hm->data); \
		free(hm->cell_probed); \
		free(hm->cell_filled); \
	} \
	void hashmap_insert_##suffix(Hashmap_##suffix* hm, T element) \
	{ \
		if(hm->num_elements * 2 >= hm->capacity) { \
			__hashmap_resize_##suffix(hm, hm->capacity * 2); \
		} \
		const size_t hash_pos = hm->hash_func(&element) % hm->capacity; \
		bool inserted = false; \
		for(int i=0; i<hm->capacity && !inserted; i++) { \
			size_t pos = hash_pos + i * i; \
			if(!hm->cell_filled[pos]) { \
				hm->cell_filled[pos] = true; \
				hm->cell_probed[pos] = i == 0; \
				hm->data[pos] = element; \
				hm->num_elements++; \
				inserted = true; \
			} \
		} \
		assert(inserted); \
	} \
	void hashmap_remove_##suffix(Hashmap_##suffix* hm, T* element); \
	void hashmap_reserve_##suffix(Hashmap_##suffix* hm, size_t min_capacity) \
	{ \
		__hashmap_resize_##suffix(hm, MIN(hm->capacity, next_pow2(min_capacity))); \
	} \
	T* hashmap_get_##suffix(Hashmap_##suffix* hm, T* cmp) \
	{\
		const size_t hash_pos = hm->hash_func(cmp) % hm->capacity; \
		for(int i=0; i<hm->capacity; i++) { \
			size_t pos = hash_pos + i * i; \
			if(hm->cell_filled[pos] && hm->cmp_func(&hm->data[pos], cmp)) { \
				return &hm->data[pos]; \
			} \
		} \
		return NULL; \
	} \
	bool hashmap_contains_##suffix(Hashmap_##suffix* hm, T* cmp) \
	{ \
		return hashmap_get_##suffix(hm, cmp) != NULL; \
	}


#define HASHMAP_IMPL_FULL(suffix, T, fmt) \
	HASHMAP_IMPL_FMT(suffix, T, fmt) \
	HASHMAP_IMPL(suffix, T)

#endif
