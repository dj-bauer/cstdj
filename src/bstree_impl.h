/*
    Copyright (C) 2023 Daniel Bauer

    This program is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation, either version 3 of the License,
    or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty
    of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include <stdio.h>

#include "bstree.h"
#include "bst_util.h"

/** 
 * @brief Implements the bst functions for a given type
 * @param suffix suffix for all the functions
 * @param T datatype
 **/
#define BST_IMPL(suffix, T) \
	void destroy_node_##suffix(struct s_bstnode_##suffix* n); \
	struct s_bstnode_##suffix* create_node_##suffix(T value, struct s_bstnode_##suffix* parent) MALLOC(destroy_node_##suffix); \
	BSTree_##suffix* create_bst_##suffix(void) {\
		BSTree_##suffix* t = malloc(sizeof(BSTree_##suffix)); \
		init_bst_##suffix(t); \
		return t; \
	} \
	void init_bst_##suffix(BSTree_##suffix* t) {\
		t->size = 0; \
		t->root = NULL; \
	} \
	size_t bst_size_##suffix(const BSTree_##suffix* t) { \
		return t->size; \
	} \
	void destroy_bst_##suffix(BSTree_##suffix* t) { \
		deinit_bst_##suffix(t); \
		free(t); \
	} \
	void destroy_node_##suffix(struct s_bstnode_##suffix* n) { \
		if(n == NULL) return; \
		destroy_node_##suffix(n->left); \
		destroy_node_##suffix(n->right); \
		free(n); \
	} \
	void deinit_bst_##suffix(BSTree_##suffix* t) { \
		destroy_node_##suffix(t->root); \
		init_bst_##suffix(t); \
	} \
	struct s_bstnode_##suffix* create_node_##suffix(T value, struct s_bstnode_##suffix* parent) { \
		struct s_bstnode_##suffix* n = malloc(sizeof(struct s_bstnode_##suffix)); \
		if(n == NULL) return NULL; \
		n->left = NULL; \
		n->right = NULL; \
		n->data = value; \
		n->parent = parent; \
		n->balance = 0; \
		return n; \
	} \
	struct s_bstnode_##suffix* node_insert_##suffix(struct s_bstnode_##suffix* n, T value) { \
		assert(n != NULL); \
		if(dj_eq_##suffix(value, n->data)) return NULL; \
		if(dj_lt_##suffix(value, n->data)) { \
			if(n->left == NULL) { \
				n->left = create_node_##suffix(value, n); \
				return n->left; \
			} else { \
				return node_insert_##suffix(n->left, value); \
			} \
		} else { \
			if(n->right == NULL) { \
				n->right = create_node_##suffix(value, n); \
				return n->right; \
			} else { \
				return node_insert_##suffix(n->right, value); \
			} \
		} \
	} \
	bool bst_insert_##suffix(BSTree_##suffix* tree, T value) { \
		bool success; \
		if(tree->root == NULL) { \
			assert(tree->size == 0); \
			tree->root = create_node_##suffix(value, NULL); \
			if(tree->root == NULL) return false; \
			success = true; \
		} else { \
			success = node_insert_##suffix(tree->root, value) != NULL; \
		} \
		if(success) tree->size++; \
		return success; \
	} \
	static inline void node_replace_at_parent_##suffix(BSTree_##suffix* t, struct s_bstnode_##suffix* old, struct s_bstnode_##suffix* new) { \
		if(old->parent != NULL) { \
			if(old->parent->left == old)  \
				old->parent->left = new; \
			else \
				old->parent->right = new; \
		} else { t->root = new; } \
		if(old->left != NULL) old->left->parent = new; \
		if(old->right != NULL) old->right->parent = new; \
		if(new != NULL) new->parent = old->parent; \
	} \
	/** @brief Returns the parent of the removed node. **/ \
	struct s_bstnode_##suffix* node_remove_##suffix(BSTree_##suffix* t, struct s_bstnode_##suffix* n, T value) { \
		if(t == NULL || n == NULL) return NULL; \
		if(dj_eq_##suffix(value, n->data)) { \
			struct s_bstnode_##suffix* replacement; \
			if(n->left == NULL) { \
				replacement = n->right; \
			} else if(n->right == NULL) { \
				replacement = n->left; \
			} else { \
				/* We find the inorder-Predecessor */ \
				replacement = n->left; \
				while(replacement->right != NULL) replacement = replacement->right; \
				replacement->right = n->right; \
				if(replacement != n->left) { \
					replacement->parent->right = replacement->left; \
					if(replacement->left != NULL) \
						replacement->left->parent = replacement->parent; \
					replacement->left = n->left; \
				} \
			} \
			node_replace_at_parent_##suffix(t, n, replacement); \
			free(n); \
			t->size--; \
			return replacement == NULL ? NULL : replacement->parent; \
		} else if(dj_lt_##suffix(value, n->data)) { \
			return node_remove_##suffix(t, n->left, value); \
		} else { \
			return node_remove_##suffix(t, n->right, value); \
		} \
	} \
	bool bst_remove_##suffix(BSTree_##suffix* tree, T value) { \
		if(tree->root == NULL) return false; \
		size_t old_size = tree->size; \
		node_remove_##suffix(tree, tree->root, value); \
		return old_size != tree->size; \
	} \
	struct s_bstnode_##suffix* node_search_##suffix(struct s_bstnode_##suffix* n, T value) { \
		if(n == NULL) return NULL; \
		if(dj_eq_##suffix(n->data, value)) return n; \
		if(dj_lt_##suffix(value, n->data)) \
			return node_search_##suffix(n->left, value); \
		else \
			return node_search_##suffix(n->right, value); \
	} \
	bool bst_contains_##suffix(const BSTree_##suffix* tree, T value) { \
		if(tree == NULL) return NULL; \
		return node_search_##suffix(tree->root, value) != NULL; \
	} \
	T* bst_search_##suffix(BSTree_##suffix* tree, T value) { \
		struct s_bstnode_##suffix* n = node_search_##suffix(tree->root, value); \
		if(n == NULL) return NULL; \
		return &n->data; \
	} \
	void node_dfs_##suffix(struct s_bstnode_##suffix* n, void(*func)(T* v,void*), void* payload) { \
		if(n == NULL) return; \
		node_dfs_##suffix(n->left, func, payload); \
		func(&n->data, payload); \
		node_dfs_##suffix(n->right, func, payload); \
	} \
	void bst_foreach_##suffix(BSTree_##suffix* tree, void(*func)(T*, void*), void* payload) { \
		if(tree == NULL || func == NULL) return; \
		node_dfs_##suffix(tree->root, func, payload); \
	}

#define BST_IMPL_PRNT(suffix, T, fmt) \
	static void node_print_dot_##suffix(const struct s_bstnode_##suffix* n, FILE* f) { \
		if(n == NULL) return; \
		fprintf(f, "\""fmt"\" [label=\""fmt"\\n%d\", color=\"%s\"];\n", n->data, n->data, n->balance, n->parent == NULL ? "orange" : "black"); \
		if(n->left != NULL) fprintf(f, "\""fmt"\" -> \""fmt"\" [color=green]\n", n->data, n->left->data); \
		if(n->right != NULL) fprintf(f, "\""fmt"\" -> \""fmt"\" [color=red]\n", n->data, n->right->data); \
		if(n->parent != NULL) fprintf(f, "\""fmt"\" -> \""fmt"\" [color=blue]\n", n->data, n->parent->data);\
		node_print_dot_##suffix(n->left, f); \
		node_print_dot_##suffix(n->right, f); \
	} \
	void bst_generate_graph_##suffix(const BSTree_##suffix* tree, const char* filename) { \
		if(tree == NULL) return; \
		FILE* f = fopen(filename, "w"); \
		if(f == NULL) return; \
		fprintf(f, "digraph bst {\n"); \
		node_print_dot_##suffix(tree->root, f); \
		fprintf(f, "}"); \
		fclose(f); \
	}

#define BST_IMPL_ALL(suffix, T, fmt) \
	BST_IMPL(suffix, T) \
	BST_IMPL_PRNT(suffix, T, fmt)
