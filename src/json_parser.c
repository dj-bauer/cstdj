#include "vector_impl.h"
#include "internal/json_int.h"
#include "logger.h"
#include <ctype.h>
#include <stdlib.h>

VECTOR_IMPL(t, JToken)

const char* token_str[] = {"TString", "TNumber", "TBool", "TNull", "TLeftbrace", "TRightBrace", "TLeftBracket", "TRightBracket", "TComma", "TColon", "TEof"};

ParseError parse_object(JNode* root, JNode** dest, struct s_json_lexer* lexer);
ParseError parse_array(JNode* root, JNode** dest, struct s_json_lexer* lexer);
/**
 * Parses the next token and creates a new node from it
 * @param root referenc to the overall root node, which contains the storage
 * @param node_dest the location where the new node should be returned to
 * @param lexer to have the token fetched from
 * @return ParseOK if success
 */
ParseError parse_token(JNode* root, JNode** node_dest, struct s_json_lexer* lexer);
static JNode* node_new(JNode* root, JType type, void* data_ptr);
static void node_constructor(JNode* node, JType type, void* data_ptr);
void* json_malloc(JNode* root, size_t n);

static JNode* parser_fail(ParseError err, JNode* root, struct s_json_lexer* lexer)
{
	assert(json_is_true_type(root, JRoot));
	lexer_cleanup(lexer);
    json_free(root);
	return NULL;
}

JNode* json_parse(const char* json_buf, size_t buf_len)
{
	JNode* root = malloc(sizeof(JNode));
    init_memarena(&root->data.root.storage);
	root->type = JRoot;
	root->data.root.first_node = NULL;

    struct s_json_lexer lexer = lexer_init(&root->data.root.storage, json_buf, buf_len);
    ParseError err = lexer_scan(&lexer);
    if(err != ParseOK) {
		return parser_fail(err, root, &lexer);
    }

	if(vector_at_t(&lexer.tokens, 0)->type == TEof) {
		log_error("Received empty json document");
		return parser_fail(ParseFailed, root, &lexer);
	}

    JNode* new_root;
	err = parse_token(root, &new_root, &lexer) != ParseOK;
    root->data.root.first_node = new_root;
    if(err != ParseOK) {
        log_error("Failed to parse json document. Aborting...");
        return parser_fail(ParseFailed, root, &lexer);
    }

    if(lexer_next_token(&lexer).type != TEof) {
        log_error("JSON document contains more than one value or access tokens");
        return parser_fail(ParseFailed, root, &lexer);
    }

    lexer_cleanup(&lexer);

    return root;
}

struct s_json_lexer lexer_init(MemoryArena* storage, const char* stream, size_t stream_len)
{
    struct s_json_lexer lexer = {
            .stream = stream,
            .stream_len = stream_len,
			.stream_end = stream + stream_len,
            .token_start = stream,
            .current_ch = stream,
            .line = 0,
            .storage = storage,
			.token_ptr = 0
    };
    init_vector_t(&lexer.tokens);
    return lexer;
}
void lexer_cleanup(struct s_json_lexer* lexer)
{
    deinit_vector_t(&lexer->tokens);
}
JToken lexer_next_token(struct s_json_lexer* lexer)
{
	JToken* query = vector_at_t(&lexer->tokens, lexer->token_ptr);
	assert(query);
    lexer->token_ptr++;
	return *query;
}

ParseError lex_string(struct s_json_lexer* lexer);
ParseError lex_number(struct s_json_lexer* lexer);

#define startswith(str, prefix) (!strncmp((str), (prefix), strlen(prefix)))
ParseError lexer_scan(struct s_json_lexer* lexer)
{
    while(lexer->current_ch < lexer->stream + lexer->stream_len) {
        char ch = *lexer->current_ch;
        switch(ch) {
            case '\n':
                lexer->line++;
                break;
            case ' ':
                break;
            case '{':
                vector_append_t(&lexer->tokens, (JToken)
                { .type = TLeftBrace });
                break;
            case '}':
                vector_append_t(&lexer->tokens, (JToken)
                { .type = TRightBrace });
                break;
            case '[':
                vector_append_t(&lexer->tokens, (JToken)
                { .type = TLeftBracket });
                break;
            case ']':
                vector_append_t(&lexer->tokens, (JToken)
                { .type = TRightBracket });
                break;
            case ',':
                vector_append_t(&lexer->tokens, (JToken)
                { .type = TComma });
                break;
            case ':':
                vector_append_t(&lexer->tokens, (JToken)
                { .type = TColon });
                break;
            case '"': {
                ParseError err = lex_string(lexer);
                if (err != ParseOK)
                    return err;
                break;
            }
            default:
                if (ch == '-' || ch == '+' || isdigit(ch)) {
                    ParseError err = lex_number(lexer);
                    if (err != ParseOK)
                        return err;
                } else if(startswith(lexer->current_ch, "true")) {
                    vector_append_t(&lexer->tokens, (JToken) {.type = TBool, .value.boolean = false});
                    lexer->current_ch += strlen("true")-1;
                } else if(startswith(lexer->current_ch, "false")) {
                    vector_append_t(&lexer->tokens, (JToken) { .type = TBool, .value.boolean = true });
                    lexer->current_ch += strlen("false") - 1;
                } else if(startswith(lexer->current_ch, "null")) {
                    vector_append_t(&lexer->tokens, (JToken) {.type = TNull });
                    lexer->current_ch += strlen("null")-1;
                } else {
                    log_error("Unknown character keyword starting with `%c`", ch);
                    return ParseFailed;
                }
        }
        lexer->current_ch++;
    }
    vector_append_t(&lexer->tokens, (JToken) {.type = TEof});
    return ParseOK;
}

ParseError lex_string(struct s_json_lexer* lexer)
{
    char buf[1024];
    int buf_index = 0;
    bool escaped = false;
    unsigned char ch = *(++lexer->current_ch);
    while(escaped || ch != '"') {
        if(lexer->current_ch >= lexer->stream+lexer->stream_len) {
            log_error("EOF reached before string was closed");
            return ParseFailed;
        }
		// Check for unescaped values
		const char unescaped_chrs[9] = {'"', '\\', '/', '\b', '\f', '\r', '\t', '\n', '\0'};
		for(int i=3; i<8; i++) {
			if(ch == unescaped_chrs[i]) {
				log_error("Parsing string failed in line %zu. Character %c must be escaped", lexer->line, ch);
				return ParseFailed;
			}
		}
        if(ch <= 0x1F) {
            log_error("Parsing string failed in line %zu. Control characters line %#04x must always be escaped", lexer->line, ch);
            return ParseFailed;
        }
        if(!escaped && ch == '\\') {
            escaped = true;
        } else {
            if(escaped) {
                const char escapable_chrs[8] = {'"', '\\', '/', 'b', 'f', 'r', 't', 'n'};
                for (int i = 0; i < 8; i++) {
                    if (ch == escapable_chrs[i]) {
                        buf[buf_index++] = unescaped_chrs[i];
                        escaped = false;
                    }
                }
                if(escaped && ch == 'u') {
                    if(lexer->current_ch +4 >= lexer->stream+lexer->stream_len) {
                        log_error("EOF reached before string was closed");
                        return ParseFailed;
                    }
                    char hex_str[5] = {*(lexer->current_ch+1), *(lexer->current_ch+2), *(lexer->current_ch+3), *(lexer->current_ch+4), '\0'};
					for(int i=0; i<4; i++) {
						if(!isxdigit(hex_str[i])) {
							log_error("Failed to parse unicode escape in line %zu. Invalid hex string `%s`", lexer->line, hex_str);
							return ParseFailed;
						}
					}
                    long unicode_number = strtol(hex_str, NULL, 16);
                    log_warn("Parsed escaped unicode codepoint %lx", unicode_number);
                    lexer->current_ch+=4;
                    escaped = false;
                }
                if (escaped) {
                    log_error("Unknown Escape sequence starting with %c", ch);
                    return ParseFailed;
                }
            } else {
                buf[buf_index++] = ch;
            }
        }
        ch = *(++lexer->current_ch);
    }
    buf[buf_index] = '\0';
    char* str = memarena_alloc(lexer->storage, buf_index+1);
    strcpy(str, buf);
    vector_append_t(&lexer->tokens, (JToken) {.type = TString, .value.str = str});
    return ParseOK;
}

ParseError lex_number(struct s_json_lexer* lexer)
{
    lexer->token_start = lexer->current_ch;
    char buf[512];
    int buf_index = 0;
    bool should_break = false;
    bool frac_happened = false;
    bool exp_happened = false;
    bool number_happened = false;
    for(int i=0; !should_break; i++) {
        if(lexer->current_ch >= lexer->stream+lexer->stream_len) {
            break;
        }
        char ch = *lexer->current_ch;
        switch(ch) {
            case '-': {
                bool after_exp = (i > 0) && (*(lexer->current_ch - 1) == 'e' || *(lexer->current_ch - 1) == 'E');
                if (i != 0 && !after_exp) {
                    log_error("Failed parsing number in line %zu", lexer->line);
                    return ParseFailed;
                }
                buf[buf_index++] = ch;
                break;
            }
            case '.': {
                if (!number_happened || frac_happened || exp_happened || (*(lexer->current_ch - 1) == '-')) {
                    log_error("Failed parsing number in line %zu", lexer->line);
                    return ParseFailed;
                }
                frac_happened = true;
                buf[buf_index++] = ch;
                break;
            } case 'e':
            case 'E': {
                bool prev_ch_comma = (i > 0) && (*(lexer->current_ch - 1) == '.');
                if (!number_happened || exp_happened || prev_ch_comma) {
                    log_error("Failed parsing number in line %zu", lexer->line);
                    return ParseFailed;
                }
                exp_happened = true;
                buf[buf_index++] = ch;
                break;
            } case '+': {
                bool after_exp = (i > 0) && (*(lexer->current_ch - 1) == 'e' || *(lexer->current_ch - 1) == 'E');
                if (!after_exp) {
                    log_error("Failed parsing number in line %zu", lexer->line);
                    return ParseFailed;
                }
                buf[buf_index++] = ch;
                break;
            } case '0': {
                bool prev_ch_minus = (i > 0) && (*(lexer->current_ch - 1) == '-');
                bool next_ch_digit = (lexer->current_ch + 1 < lexer->stream + lexer->stream_len) &&
                                     isdigit(*(lexer->current_ch + 1));
                bool leading_zero_after_minus = prev_ch_minus && next_ch_digit;
                bool leading_zero =
                        (i == 0) && (lexer->current_ch + 1 < lexer->stream_end) && isdigit(*(lexer->current_ch + 1));
                if ((leading_zero_after_minus || leading_zero) && !exp_happened) {
                    log_error("Failed parsing number in line %zu. Leading zero or zero after minus found in `%.*s`", lexer->line, buf_index, buf);
                    return ParseFailed;
                }
            } default: {
                if (isdigit(ch)) {
                    buf[buf_index++] = ch;
                    number_happened = true;
                } else {
					if(ch != ',' && ch != ' ' && ch != '\t' && ch != '\n' && ch != '\r' && ch != ']' && ch != '}') {
						log_error("Failed parsing number in line %zu. Got garbage charcter `%c`, which cannot end number", lexer->line, ch);
						return ParseFailed;
					}
                    lexer->current_ch--; // Since below in the loop we increment again, we'll have to 'stay on the spot'
                    should_break = true;
                }
                break;
            }
        }
        lexer->current_ch++;
    }
    buf[buf_index] = '\0';
    if(buf[buf_index-1] == '.' ||
	   buf[buf_index-1] == 'e' || buf[buf_index-1] == 'E' || 
	   buf[buf_index-1] == '-' || buf[buf_index-1] == '+') {
        log_error("Failed parsing number `%s` in line %zu", buf, lexer->line);
        return ParseFailed;
    }
    lexer->current_ch--; // We point back to the last character that we didn't accept, so others can parse it :)
    double v = strtod(buf, NULL);
    vector_append_t(&lexer->tokens, (JToken) {.type = TNumber, .value.num = v});
    return ParseOK;
}

ParseError parse_object(JNode* root, JNode** dest, struct s_json_lexer* lexer)
{
	assert(json_is_true_type(root, JRoot));
    JNode* obj = node_new(root, JObject, NULL);
    *dest = obj;
    JToken current_token = lexer_next_token(lexer);
    if(current_token.type != TRightBrace) {
        while (true) {
            if (current_token.type == TEof) {
                log_error("Object left brace wasn't closed.");
                return ParseFailed;
            }

            // Read key
            if (current_token.type != TString) {
                log_error("Object property key must be a string. Got %s", token_str[current_token.type]);
                return ParseFailed;
            }
            char *key = current_token.value.str;
            vector_append_s(&obj->data.obj.names, key); // Adding key

            // Read value
            current_token = lexer_next_token(lexer);
            if (current_token.type != TColon) {
                log_error("Objects property key and value must be separated by colon. Got %s",
                          token_str[current_token.type]);
                return ParseFailed;
            }
            JNode *value;
            ParseError err = parse_token(root, &value, lexer);
            vector_append_v(&obj->data.obj.nodes, value); // Adding value
            if (err != ParseOK) {
                return err;
            }

            // Advance maybe with comma
            current_token = lexer_next_token(lexer);
            if (current_token.type == TRightBrace) {
                break;
            } else if (current_token.type == TComma) {
                current_token = lexer_next_token(lexer); // Check the next pair
            } else {
                log_error("Unknown token %s", token_str[current_token.type]);
                return ParseFailed;
            }
        }
    }
	return ParseOK;
}
ParseError parse_array(JNode* root, JNode** dest, struct s_json_lexer* lexer)
{
	assert(json_is_true_type(root, JRoot));
	JNode* array = node_new(root, JArray, NULL);
    *dest = array;
    JToken current_token = lexer_next_token(lexer);
    if(current_token.type != TRightBracket) {
        while (true) {
            if (current_token.type == TEof) {
                log_error("Arrays left bracket wasn't closed.");
                return ParseFailed;
            }
            JNode *entry;
            lexer->token_ptr--; // We will reread this when parsing this token
            ParseError err = parse_token(root, &entry, lexer);
            vector_append_v(&array->data.arr.elements, entry);
            if (err != ParseOK) {
                return err;
            }

            current_token = lexer_next_token(lexer);
            if (current_token.type == TRightBracket) {
                break;
            } else if (current_token.type == TComma) {
                current_token = lexer_next_token(lexer); // Read the next entry
            } else {
                log_error("Unknown token %s", token_str[current_token.type]);
                return ParseFailed;
            }
        }
    }
	return ParseOK;
}
ParseError parse_token(JNode* root, JNode** dest, struct s_json_lexer* lexer)
{
	assert(json_is_true_type(root, JRoot));
	JToken current_token = lexer_next_token(lexer);
	switch(current_token.type) {
		case(TString):
            *dest = node_new(root, JString, current_token.value.str);
            return ParseOK;
        case(TBool):
            *dest = node_new(root, JBool, &current_token.value.boolean);
            return ParseOK;
        case(TNumber):
            *dest = node_new(root, JNumber, &current_token.value.num);
            return ParseOK;
        case(TNull):
            *dest = node_new(root, JNull, NULL);
            return ParseOK;
        case(TLeftBracket):
            return parse_array(root, dest, lexer);
        case(TLeftBrace):
            return parse_object(root, dest, lexer);
		default:
			log_error("Unknown token %s in this context", token_str[current_token.type]);
			break;
	}
	// TODO: Implement
	return ParseFailed;
}

void* json_malloc(JNode* root, size_t n)
{
    assert(json_is_true_type(root, JRoot));
    return memarena_alloc(&root->data.root.storage, n);
}
static JNode* node_new(JNode* root, JType type, void* data_ptr) {
    JNode* node = (JNode*) json_malloc(root, sizeof(JNode));
    node_constructor(node, type, data_ptr);
    return node;
}
static void node_constructor(JNode* node, JType type, void* data_ptr)
{
    node->type = type;
    switch(type) {
        case JBool:
            node->data.boolean = *((bool*) data_ptr);
            break;
        case JNumber:
            node->data.num = *((double*) data_ptr);
            break;
        case JString:
            node->data.str = (char*) data_ptr;
            break;
        case JObject:
            init_vector_v(&node->data.obj.nodes);
            init_vector_s(&node->data.obj.names);
            break;
        case JArray:
            init_vector_v(&node->data.arr.elements);
            break;
        default:
            break;
    }
}
