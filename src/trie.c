#include "trie.h"
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <stdio.h>

#include "util.h"

struct s_trie_node {
	// Points to the first of the nodes children
	struct s_trie_node* children;
	// Points to the next node, which is a sibling.
	// The next sibling has to be lexicographically larger than the previous one.
	// This leads to the effect where we can abort if our character to check is an A, but the first child is a B
	// Then we know, it won't contain the character anymore
	struct s_trie_node* next;
	char chr;
	bool is_final;
};

struct s_trie {
	struct s_trie_node* root;
	size_t min_length;
	size_t max_length;
};

static struct s_trie_node* create_node(const char ch);

Trie* create_trie(void)
{
	Trie* t = malloc(sizeof(Trie));
	if(t == NULL) return NULL;
	t->min_length = 0;
	t->max_length = SIZE_MAX;
	t->root = NULL;

	return t;
}
static struct s_trie_node* create_node(const char ch) {
	struct s_trie_node* n = malloc(sizeof(struct s_trie_node));
	if(n == NULL) return NULL;
	n->chr = ch;
	n->next = NULL;
	n->children = NULL;
	n->is_final = false;
	return n;
}
static void destroy_node(struct s_trie_node* n) {
	if(n == NULL) return;

	if(n->children != NULL) {
		destroy_node(n->children);
	}
	if(n->next != NULL) {
		destroy_node(n->next);
	}
	free(n);
}
void destroy_trie(Trie* t)
{
	destroy_node(t->root);
	free(t);
}
static bool tree_contains_str(const struct s_trie_node* n, const char* str, size_t len)
{
	assert(n != NULL);
	if(len == 0) return false;

	if(n->chr == str[0]) {
		if(len == 1) {
			return n->is_final;
		} else {
			if(n->children == NULL) return false;
			return tree_contains_str(n->children, str+1, len-1);
		}
	} else if(n->next != NULL) {
		return tree_contains_str(n->next, str, len);
	} else  {
		return false;
	}
}
bool trie_contains_string_n(const Trie* t, const char *str, size_t n)
{
	if(t->root == NULL) return false;
	return tree_contains_str(t->root, str, n);
}
bool trie_contains_string(const Trie* t, const char *str)
{
	return trie_contains_string_n(t, str, strlen(str));
}

TrieError trie_insert(Trie *t, const char *str)
{
	return trie_insert_n(t, str, strlen(str));
}
static TrieError tree_append(struct s_trie_node* n, const char* str, size_t len)
{
	if(len == 0) return TrieSuccess;
	struct s_trie_node* new_parent = n->children;
	if(new_parent == NULL) {
		// We need to create the first new child
		n->children = create_node(str[0]);
		if(n->children == NULL)
			return TrieMalloc;
		new_parent = n->children;
	} else {
		// We need to find or create the sibling
		while(new_parent->next != NULL && new_parent->chr != str[0]) 
			new_parent = new_parent->next;
		if(new_parent->chr != str[0]) {
			new_parent->next = create_node(str[0]);
			if(new_parent == NULL)
				return TrieMalloc;
			new_parent = new_parent->next;
		}
	}
	if(len == 1) new_parent->is_final = true;
	return tree_append(new_parent, str+1, len-1);
}
TrieError trie_insert_n(Trie* t, const char* str, size_t len)
{
	if(len == 0) return TrieSuccess;

	struct s_trie_node* root = t->root;
	if(t->root == NULL) {
		t->root = create_node(str[0]);
		if(t->root == NULL) 
			return TrieMalloc;
		t->root->is_final = (len == 1);
		root = t->root;
	} else {
		while(root->next != NULL && root->chr != str[0]) 
			root = root->next;
		if(root->chr != str[0]) {
			root->next = create_node(str[0]);
			if(root->next == NULL) 
				return TrieMalloc;
			root = root->next;
		}
	}
	return tree_append(root, str+1, len-1);
}

static void print_rek(struct s_trie_node* n, size_t space) {
	if(n == NULL)
		return;
	for(size_t i=0; i<space; i++) printf(" ");
	printf("%s%c\033[0m\n", n->is_final ? "\033[1m" : "", n->chr);
	print_rek(n->children, space+1);
	print_rek(n->next, space);
}
void trie_print(const Trie* t)
{
	if(t == NULL) return;
	print_rek(t->root, 0);
}
