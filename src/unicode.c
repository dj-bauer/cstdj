#include "unicode.h"
#include <assert.h>

int u_to_utf8(char buf[5], uint32_t codepoint)
{
	if(codepoint <= 0x7F) {
		buf[0] = codepoint;
		buf[1] = '\0';
		return 1;
	} else if (codepoint <= 0x07FF) {
		buf[0] = (char) (((codepoint >> 6) & 0x1F) | 0xC0);
		buf[1] = (char) (((codepoint >> 0) & 0x3F) | 0x80);
		buf[2] = '\0';
		return 2;
	} else if (codepoint <= 0xFFFF) {
		buf[0] = (char) (((codepoint >> 12)& 0x0F) | 0xE0);
		buf[1] = (char) (((codepoint >> 6) & 0x3F) | 0x80);
		buf[2] = (char) (((codepoint >> 0) & 0x3F) | 0x80);
		buf[3] = '\0';
		return 3;
	} else if (codepoint <= 0x10FFFF) {
		buf[0] = (char) (((codepoint >>18) & 0x07) | 0xF0);
		buf[1] = (char) (((codepoint >>12) & 0x3F) | 0x80);
		buf[2] = (char) (((codepoint >> 6) & 0x3F) | 0x80);
		buf[3] = (char) (((codepoint >> 0) & 0x3F) | 0x80);
		buf[4] = '\0';
		return 4;
	}
	buf[0] = '\0';
	assert(0);
	return 0;
}
