#include <assert.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#include "huffman.h"
#include "zlib.h"
#include "logger.h"
#include "util.h"

//#define OUTPUT_INFGEN

enum e_treeType {LitTree, DistTree};
static void read_uncompressed_block(unsigned char** dst, Bitreader* reader, size_t* size)
	NONNULL(1, 2, 3);
static void read_fixed_huffman_block(unsigned char** dst, Bitreader* bitstream, size_t* size)
	NONNULL(1, 2, 3);
static void read_dynamic_huffman_block(unsigned char** dst, Bitreader* bitstream, size_t* size)
	NONNULL(1, 2, 3);

uint16_t read_dyn_symbol(Bitreader* reader, const HuffmanNode* code_tree)
	NONNULL(1, 2);

static void read_dyn_huffman_lit_dist_len(size_t n, uint8_t bit_length[n], Bitreader* reader, HuffmanNode* code_tree, enum e_treeType type)
NONNULL(3, 4);
/**
 * Loads up a symbol tree
 * @param num_codes the number of codes in the tree. Maximum of 512
 **/
void load_symbol_tree(HuffmanNode* code_tree, uint16_t num_codes, Bitreader* reader, HuffmanNode* length_tree, enum e_treeType type)
	NONNULL(1, 3, 4);

#define CM_MASK 0x0F
#define CINFO_MASK 0xF0

#define FCHECK_MASK 0x1F
#define FDICT_MASK 0x20
#define FLEVEL_MASK 0xC0

unsigned char* zlib_decode(const unsigned char* stream, size_t length, size_t * out_length)
{
	Bitreader reader;
	init_bitreader(&reader, stream, length);

	// Figure out compression method and flags
	unsigned char CMF = bitreader_read_byte(&reader);
	unsigned char FLG = bitreader_read_byte(&reader);

	unsigned char CM = CMF & CM_MASK;
	if(CM != 8) {
		log_error("Compression mode %d is not supported. Only DEFLATE compression (mode 8) is supported.", CM);
		return NULL;
	}
#ifndef CINFO
    signed char CINFO = CMF >> 4;
#endif
	assert(CINFO <= 7);
	//uint32_t window_size = 1 << ( CINFO + 8 );
	
	//unsigned char FCHECK = FLG & FCHECK_MASK;
	//unsigned char FLEVEL = FLG >> 6;
	uint16_t checksum = CMF << 8 | FLG;
	if(checksum % 31 != 0) {
		log_error("Checksum in the Zlib header failed");
		return NULL;
	}

	unsigned char FDICT = (FLG >> 5) & 0x1;
	uint32_t preset_dictionary;
	if(FDICT) {
		preset_dictionary = bitreader_read_quadbyte(&reader, true);
		log_warn("Preset dictionary %d is not supported", preset_dictionary);
	}

#ifdef OUTPUT_INFGEN
    printf("! infgen 3.0 output\n!\nzlib\n!\n");
#endif

	unsigned char* data = NULL;
	size_t size = 0;
	// Reads one deflate block each time
	while(read_deflate_block(&data, &reader, &size)) {
        /*log_error("Temporarily printing only one block");
        break;*/
    }

	*out_length = size;
	return data;
}


bool read_deflate_block(unsigned char** dst, Bitreader* reader, size_t* size) {
	uint8_t BFINAL = read_single_bit(reader);
	uint8_t BTYPE = read_bit(reader, 2, false);

#ifdef OUTPUT_INFGEN
    if(BFINAL)
        print("last\n");
#endif

	switch(BTYPE) {
		case(0x0):
			bitreader_skip_bits(reader);
			read_uncompressed_block(dst, reader, size);
			break;
		case(0x1):
			read_fixed_huffman_block(dst, reader, size);
			break;
		case(0x2):
            read_dynamic_huffman_block(dst, reader, size);
			break;
		case(0x3):
			assert(0 && "Deflate: invalid compression type found: 0b11.");
		default:
			assert(0 && "Parsing error");
	}

	return !BFINAL;
}

static void read_uncompressed_block(unsigned char** dst, Bitreader* reader, size_t* size) {
	uint16_t len = bitreader_read_doublebyte(reader, false);
	uint16_t len_comp = bitreader_read_doublebyte(reader, false);
	if(len != (~len_comp & 0xffff)) {
		log_error("Failed to read checksum for uncompressed block");
		*dst = NULL;
	}

	*dst = realloc(*dst, *size + len);

	memcpy(*dst+*size, bitreader_stream(reader), len);
	bitreader_skip_bytes(reader, len);

	*size += len;
}

// The literal/length alphabet for fixed huffman code
static const uint16_t fixed_codes_limits[4][2] = {
	{48, 191}, // 0   - 143 (8-bit) [0]
	{400, 511},// 144 - 255 (9-bit) [1] 
	{0, 23},   // 256 - 279 (7-bit) [2]
	{192, 199} // 280 - 287 (8-bit) [3]
}; 
static bool fixed_fits_in_range(uint16_t value, uint8_t range) {
	assert(range < 4);
	return value >= fixed_codes_limits[range][0] && value <= fixed_codes_limits[range][1];
}
static const uint8_t fixed_length_bits[29] = {
	0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 2, 2, 2, 2, 3, 3, 3, 3, 4, 4, 4, 4, 5, 5, 5, 5, 0 };
static const uint16_t  fixed_length_start[29] = {
	3, 4, 5, 6 ,7 ,8 ,9, 10, 11, 13, 15, 17, 19, 23, 27, 31, 35, 43, 51, 59, 67, 83, 99, 115, 131, 163, 195, 227, 258};
static const uint16_t fixed_dist_bits[30] = {
	0, 0, 0, 0, 1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6, 7, 7, 8, 8, 9, 9, 10, 10, 11, 11, 12, 12, 13, 13};
static const uint16_t fixed_dist_start[30] = {
	1, 2, 3, 4, 5, 7, 9, 13, 17, 25, 33, 49, 65, 97, 129, 193, 257, 385, 513, 769, 1025, 1537, 2049, 3073, 4097, 6145, 8193, 12289, 16385, 24577};


// We have a 1k work buffer
#define FIXED_STATIC_BUFSIZE 1024
// Once we push this work buffer, we reallocate the destination to fit the content of the work buffer again
static void fixed_push_buf(unsigned char** dst, size_t* size, uint8_t buf[FIXED_STATIC_BUFSIZE], size_t* buf_used) {
	if(*buf_used == 0) return;
	*dst = realloc(*dst, *size + *buf_used);
	memcpy(*dst + *size, buf, *buf_used);
	*size += *buf_used;
    // Reset the size of the buffer
	*buf_used = 0;
}

uint16_t read_symbol(Bitreader* reader) {
	uint16_t symbol = read_most_significant_bit_first(reader, 7);
	if(fixed_fits_in_range(symbol, 2)) {
		return symbol - fixed_codes_limits[2][0] + 256;
	} else {
		symbol <<= 1;
		symbol |= read_single_bit(reader);
		if(fixed_fits_in_range(symbol, 0)) {
			uint16_t adjusted = symbol - fixed_codes_limits[0][0] + 0;
			return adjusted;
		} else if(fixed_fits_in_range(symbol, 3)) {
			return symbol - fixed_codes_limits[3][0] + 280;
		} else {
			symbol <<= 1;
			symbol |= read_single_bit(reader);
			assert(fixed_fits_in_range(symbol, 1) && "The read symbol did not fit in any of the fixed code ranges");
			return symbol - fixed_codes_limits[1][0] + 144;
		}
	}
}

static void unfold_length_distance(unsigned char** dst, size_t* size, uint16_t length, uint16_t distance) {
	assert(distance <= *size);
	size_t new_size = *size + length;

	*dst = realloc(*dst, new_size);

	const unsigned char* read_ptr = *dst + *size - distance;
	unsigned char* write_ptr = *dst + *size;
	assert(read_ptr < write_ptr);
	assert(new_size > *size);

	for(uint16_t i=0; i<length; i++) {
		write_ptr[i] = read_ptr[i];
	}
	*size = new_size;
}

static void read_fixed_huffman_block(unsigned char** dst, Bitreader* reader, size_t* size) {

	uint8_t buf[FIXED_STATIC_BUFSIZE];
	size_t buf_used = 0;

	while(bitreader_has_data(reader)) {
		uint16_t symbol = read_symbol(reader);
		if(symbol == 256) break;

		if(symbol < 256) {
			buf[buf_used] = (uint8_t) symbol;
			buf_used++;
			if(buf_used == FIXED_STATIC_BUFSIZE)
				fixed_push_buf(dst, size, buf, &buf_used);
		} else { 
			// We need to flush the buffer so that we can reference back on everything that we've read before
			fixed_push_buf(dst, size, buf, &buf_used);

			uint16_t length_code = symbol - 257;
			uint16_t length = read_least_significant_bit_first(reader, fixed_length_bits[length_code]);
			length += fixed_length_start[length_code];

			uint16_t distance_code = read_most_significant_bit_first(reader, 5);
			assert(distance_code < 30 && "Distance code 30 and 31 are never used");

			uint16_t distance = read_least_significant_bit_first(reader, fixed_dist_bits[distance_code]);
			distance += fixed_dist_start[distance_code];
			unfold_length_distance(dst, size, length, distance);
		}
	}
	fixed_push_buf(dst, size, buf, &buf_used);
}


static void read_dynamic_huffman_block(unsigned char** dst, Bitreader* reader, size_t* size) {
#ifdef OUTPUT_INFGEN
    printf("dynamic\n");
#endif
	uint16_t HLIT = ((uint16_t) read_bit(reader, 5, false)) + 257;
	uint8_t HDIST = read_bit(reader, 5, false) + 1;
	uint8_t HCLEN = read_bit(reader, 4, false) + 4;
	//log_debug("#literals %d #distances %d #code length %d", HLIT, HDIST, HCLEN);

#define N_LENGTH_SYMBOLS 19
	assert(HCLEN <= N_LENGTH_SYMBOLS);
	// The order in which the length codes are read from the stream
	static const uint16_t code_length_mapping[N_LENGTH_SYMBOLS] = {16, 17, 18, 0, 8, 7, 9, 6, 10, 5, 11, 4, 12, 3, 13, 2, 14, 1, 15};
	// Maps the symbol to its lexicographical correct spot
	uint8_t code_length_bit_len[N_LENGTH_SYMBOLS];
	memset(code_length_bit_len, 0, sizeof(uint8_t)*N_LENGTH_SYMBOLS);
	for(uint8_t i=0; i<HCLEN; i++)
	{
		code_length_bit_len[code_length_mapping[i]] = read_bit(reader, 3, false);
	}

	uint16_t code_length_codes[N_LENGTH_SYMBOLS];
	generate_huffman_codes(N_LENGTH_SYMBOLS, code_length_codes, code_length_bit_len);

	// Build the actual code length huffman tree
	HuffmanNode code_length_tree_array[2*HCLEN-1];
	memset(code_length_tree_array, 0, sizeof(code_length_tree_array));
	size_t code_length_tree_next = 0;
	for(uint8_t i=0; i<N_LENGTH_SYMBOLS; i++) {
		if(code_length_bit_len[i] == 0)
			continue;
		huffman_add_node(2*HCLEN-1, code_length_tree_array, &code_length_tree_next, i, code_length_codes[i], code_length_bit_len[i]);
	}

	HuffmanNode code_literal_tree_array[2*HLIT-1];
	load_symbol_tree(code_literal_tree_array, HLIT, reader, code_length_tree_array, LitTree);

	HuffmanNode code_dist_tree_array[2*HDIST-1];
	load_symbol_tree(code_dist_tree_array, HDIST, reader, code_length_tree_array, DistTree);

	uint8_t buf[FIXED_STATIC_BUFSIZE];
	size_t buf_used = 0;
#ifdef OUTPUT_INFGEN
    enum {Print_ch, Print_sym, Print_start} print_mode = Print_start;
#endif
	while(bitreader_has_data(reader)) {
		uint16_t symbol = read_dyn_symbol(reader, code_literal_tree_array);
		if(symbol == 256) {
#ifdef OUTPUT_INFGEN
            if(print_mode == Print_sym || print_mode == Print_ch)
                printf("\nend\n!\n");
            else
                printf("end\n!\n");
#endif
            break;
        };
		if(symbol < 256) {
			buf[buf_used] = (uint8_t) symbol;
			buf_used++;
			if(buf_used == FIXED_STATIC_BUFSIZE)
				fixed_push_buf(dst, size, buf, &buf_used);
#ifdef OUTPUT_INFGEN
            if(print_mode == Print_ch) {
                if(isprint(symbol))
                    printf("%c", symbol);
                else {
                    printf("\nliteral %d", symbol);
                    print_mode = Print_sym;
                }
            } else if(print_mode == Print_sym) {
                if(!isprint(symbol))
                    printf(" %d", symbol);
                else {
                    printf(" '%c", symbol);
                    print_mode = Print_ch;
                }
            } else {
                if(isprint(symbol)) {
                    printf("literal '%c", symbol);
                    print_mode = Print_ch;
                } else {
                    printf("literal %d", symbol);
                    print_mode = Print_sym;
                }
            }
#endif
		} else { 
			// We need to flush the buffer
			fixed_push_buf(dst, size, buf, &buf_used);

			uint16_t length_code = symbol - 257;
			uint16_t length = read_least_significant_bit_first(reader, fixed_length_bits[length_code]);
			length += fixed_length_start[length_code];

			uint16_t distance_code = read_dyn_symbol(reader, code_dist_tree_array);
			uint16_t distance = read_least_significant_bit_first(reader, fixed_dist_bits[distance_code]);
			distance += fixed_dist_start[distance_code];
			unfold_length_distance(dst, size, length, distance);
#ifdef OUTPUT_INFGEN
            if(print_mode == Print_ch || print_mode == Print_sym) {
                printf("\n");
                print_mode = Print_start;
            }
            printf("match %d %d\n", length, distance);
#endif
		}
	}
	fixed_push_buf(dst, size, buf, &buf_used);
}

void generate_huffman_codes(uint16_t n, uint16_t codes[n], const uint8_t bit_lengths[n])
{
	/* STEP 1. Count the number of codes for each code length. */
	uint8_t max_bits = 0;
	for(size_t i=0; i<n; i++) {
		if(max_bits < bit_lengths[i]) max_bits = bit_lengths[i];
	}
	assert(max_bits <= 16);
	uint16_t bl_count[max_bits+1];
	memset(bl_count, 0, sizeof(bl_count));
	for(size_t i=0; i<n; i++) {
		assert(bit_lengths[i] < max_bits+1);
		bl_count[bit_lengths[i]]++;
	}

	/* STEP 2. Find the numerical value of the smallest code for each code length */
	uint16_t next_code[max_bits+1];
	uint16_t code = 0;
	bl_count[0] = 0;
	for(uint32_t bits = 1; bits <= max_bits; bits++) {
		code = (code + bl_count[bits-1]) << 1;
		next_code[bits] = code;
	}

	/* STEP 3. Assign numerical values to all codes */
	for(uint16_t i=0; i<n; i++) {
		uint8_t len = bit_lengths[i];
		if(len != 0) {
			codes[i] = next_code[len];
			next_code[len]++;
		} else {
			codes[i] = -1;
		}
	}
}

// Reads the literal & distance symbol length from the stream with a given code tree
static void read_dyn_huffman_lit_dist_len(size_t n, uint8_t bit_length[n], Bitreader* reader, HuffmanNode* code_tree, enum e_treeType type)
{
	for(uint16_t i=0; i<n; i++) {
		uint16_t symbol = read_dyn_symbol(reader, code_tree);
        if(0 <= symbol && symbol <= 15) {
            bit_length[i] = symbol;
#ifdef OUTPUT_INFGEN
            if(symbol && type == LitTree)
                printf("litlen %d %d\n", i, symbol);
            if(symbol && type == DistTree)
                printf("dist %d %d\n", i, symbol);
#endif
        } else if (symbol == 16) {
            assert(i > 0 && "We cannot repeat the last distance code if we haven't read any other");
            uint8_t repeat_count = 3 + read_least_significant_bit_first(reader, 2);
            for(uint16_t j=0; j<repeat_count; j++) {
                bit_length[i+j] = bit_length[i-1]; // We repeat the previous distance code n times
#ifdef OUTPUT_INFGEN
                if(bit_length[i+j]) {
                    if(type == LitTree)
                        printf("litlen %d %d\n", i + j, bit_length[i + j]);
                    if(type == DistTree)
                        printf("dist %d %d\n", i + j, bit_length[i + j]);
                }
#endif
            }
            i += repeat_count-1;
        } else {
            assert(symbol == 17 || symbol == 18);
            uint8_t repeat_count = 3;
            uint8_t read_bits = 3;
            if(symbol == 18) {
                repeat_count = 11;
                read_bits = 7;
            }
            repeat_count += read_least_significant_bit_first(reader, read_bits);
            for(uint16_t j=0; j<repeat_count; j++) {
                bit_length[i+j] = 0;
            }
            i += repeat_count - 1;
        }
	}
}
uint16_t read_dyn_symbol(Bitreader* reader, const HuffmanNode* code_tree)
{
	uint16_t current_node = 0;
	do {
		if(read_single_bit(reader)) {
			current_node = code_tree[current_node].one_child;
		} else {
			current_node = code_tree[current_node].zero_child;
		}
		if(code_tree[current_node].has_symbol) {
			return code_tree[current_node].symbol;
		}
	} while(current_node != 0);
	return -1;
}
// The tree-type is just for debugging
void load_symbol_tree(HuffmanNode* code_tree, uint16_t num_codes, Bitreader* reader, HuffmanNode* length_tree, enum e_treeType type) {
	// This assertion is simply to skip compiler warning, since this function is not meant to be called with insanely large trees
	assert(num_codes <= 512);
	uint8_t bit_len[num_codes];
	read_dyn_huffman_lit_dist_len(num_codes, bit_len, reader, length_tree, type);
	uint16_t codes[num_codes];
	generate_huffman_codes(num_codes, codes, bit_len);

	memset(code_tree, 0, sizeof(HuffmanNode) * (2*num_codes-1));
	size_t next_node = 0;
	for(uint16_t i=0; i<num_codes; i++) {
		if(bit_len[i] == 0)
			continue;
		huffman_add_node(2*num_codes-1, code_tree, &next_node,
				i, codes[i], bit_len[i]);
	}
}
