#include "cas.h"
#include "util.h"
#include <math.h>
#include <complex.h>

#define EPSILON 1e-7

typedef double complex cmplx;

void cas_solve_quadratic(double buf[2], double a, double b, double c)
{
	buf[0] = (-b + sqrt(b*b - 4*a*c)) / (2*a);
	buf[1] = (-b - sqrt(b*b - 4*a*c)) / (2*a);
}

void cas_solve_cubic(double solutions[3], double a, double b, double c, double d)
{
	cmplx p = (9*a*c - 3*b*b) / (9*a*a);
	cmplx q = (-9*a*b*c + 27*a*a*d + 2*b*b*b) / (27*a*a*a);

	cmplx pref = -b/(3*a);
	cmplx factor = csqrt(-4.0f/3*p);
	cmplx acos_part = cacos(-q/2.0f * csqrt(-27.0f/(p*p*p)));
	cmplx sol[3];
	sol[0] = pref + factor * ccos(1.0f/3.0f * acos_part);
	sol[1] = pref - factor * ccos(1.0f/3.0f * acos_part + M_PI_3);
	sol[2] = pref - factor * ccos(1.0f/3.0f * acos_part - M_PI_3);
	if(fabs(cimag(sol[0])) < EPSILON) 
		solutions[0] = creal(sol[0]);
	else
		solutions[0] = NAN;
	if(fabs(cimag(sol[1])) < EPSILON) 
		solutions[1] = creal(sol[1]);
	else
		solutions[1] = NAN;
	if(fabs(cimag(sol[2])) < EPSILON) 
		solutions[2] = creal(sol[2]);
	else
		solutions[2] = NAN;
	return;

	FAIL_NOT_IMPLEMENTED
}
