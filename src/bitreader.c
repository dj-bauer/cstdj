#include <assert.h>

#include "bitreader.h"
#include "util.h"

void init_bitreader(Bitreader* reader, const unsigned char* stream, size_t stream_length)
{
	reader->stream = stream;
	reader->stream_length = stream_length;
	reader->bit_offset = 0;
	reader->byte_offset = 0;
}

uint8_t bitreader_read_byte(Bitreader* reader)
{
	assert(reader->bit_offset == 0);
	uint8_t value = reader->stream[reader->byte_offset];
	reader->byte_offset++;
	return value;
}

uint32_t read_most_significant_bit_first(Bitreader* reader, uint16_t num_bits)
{
	assert(num_bits <= 32);
	uint32_t res = 0;
	for(uint16_t i=0; i<num_bits; i++) {
		assert(reader->bit_offset < 8);
		assert(reader->byte_offset < reader->stream_length);
		res <<= 1;
		res |= (reader->stream[reader->byte_offset] >> reader->bit_offset & 1);
		reader->bit_offset ++;
		if(reader->bit_offset == 8) {
			reader->bit_offset = 0;
			reader->byte_offset++;
		}
	}
	return res;
}

uint32_t read_least_significant_bit_first(Bitreader* reader, uint16_t num_bits)
{
	assert(num_bits <= 32);
	uint32_t res = 0;
	for(uint16_t i=0; i<num_bits; i++) {
		assert(reader->bit_offset < 8);
		assert(reader->byte_offset < reader->stream_length);
		res |= (reader->stream[reader->byte_offset] >> reader->bit_offset & 1) << i;
		reader->bit_offset ++;
		if(reader->bit_offset == 8) {
			reader->bit_offset = 0;
			reader->byte_offset++;
		}
	}
	return res;
}

uint8_t read_single_bit(Bitreader* reader)
{
	uint8_t bit = reader->stream[reader->byte_offset] >> reader->bit_offset & 0x1;
	reader->bit_offset++;
	if(reader->bit_offset == 8) {
		reader->bit_offset = 0;
		reader->byte_offset++;
	}
	return bit;
}

static const uint8_t amount_masks[8] = {0x1, 0x3, 0x7, 0xf, 0x1f, 0x3f, 0x7f, 0xff};
uint8_t read_bit(Bitreader *reader, uint8_t num_bits, bool MSB) 
{
	assert(num_bits <= 8);
	assert(reader->bit_offset < 8);
	assert(reader->byte_offset < reader->stream_length);
	// The trivial case where everything can be read from a singe byte
	if(num_bits + reader->bit_offset <= 8) {
		uint8_t value = reader->stream[reader->byte_offset] >> reader->bit_offset & amount_masks[num_bits-1];
		reader->bit_offset += num_bits;
		if(reader->bit_offset == 8) {
			reader->bit_offset = 0;
			reader->byte_offset++;
		}
		return value;
	}
	assert(reader->byte_offset < reader->stream_length - 1);

	const uint8_t first_byte_amount = 8 - reader->bit_offset;
	const uint8_t second_byte_amount = num_bits - first_byte_amount;
	assert(first_byte_amount + second_byte_amount == num_bits);

	uint8_t value = reader->stream[reader->byte_offset] >> reader->bit_offset;
	uint8_t second_value = reader->stream[reader->byte_offset+1] & amount_masks[second_byte_amount-1];
	/*uint8_t second_value = 0;
	uint8_t mask = 1;
	for(uint8_t i=0; i<second_byte_amount; i++) {
		second_value |= (reader->stream[reader->byte_offset+1] & mask) >> i;
		mask <<= 1;
	}*/
	if(MSB) {
		value = value << second_byte_amount | second_value;
	} else {
		value = value | (second_value << first_byte_amount);
	}
	reader->bit_offset += num_bits;
	reader->bit_offset -= 8;
	reader->byte_offset++;
	if(reader->bit_offset >= 8) {
		assert(0);
	}
	return value;
}

bool bitreader_has_data(const Bitreader* reader) 
{
	return reader->byte_offset < reader->stream_length;
}
uint32_t bitreader_read_quadbyte(Bitreader* reader, bool big_endian)
{
	assert(reader->bit_offset == 0);
	assert(reader->byte_offset + 3 < reader->stream_length);
	if(!big_endian) {
		FAIL_NOT_IMPLEMENTED
	}
	uint32_t v = (reader->stream[reader->byte_offset] << 24)
			   | (reader->stream[reader->byte_offset+1] << 16)
			   | (reader->stream[reader->byte_offset+2] << 8)
			   | reader->stream[reader->byte_offset+3];
	reader->byte_offset += 4;
	return v;
}
uint16_t bitreader_read_doublebyte(Bitreader* reader, bool big_endian)
{
	assert(reader->bit_offset == 0);
	assert(reader->byte_offset + 3 < reader->stream_length);
	if(big_endian) {
		FAIL_NOT_IMPLEMENTED
	}
	uint16_t v =  reader->stream[reader->byte_offset]
			   | (reader->stream[reader->byte_offset+1] << 8);
	reader->byte_offset += 2;
	return v;
}
void bitreader_skip_bits(Bitreader* reader)
{
	reader->bit_offset = 0;
	reader->byte_offset++;
}
const unsigned char* bitreader_stream(Bitreader* reader)
{
	assert(reader->byte_offset < reader->stream_length);
	return &reader->stream[reader->byte_offset];
}
void bitreader_skip_bytes(Bitreader* reader, size_t n)
{
	reader->byte_offset += n;
}
