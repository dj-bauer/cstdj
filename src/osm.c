#include "osm.h"
#include <assert.h>
#include <string.h>
#include <stdlib.h>
#include "logger.h"
#include "bstree_impl.h"
#include "avltree_impl.h"
#include "util.h"
#include "polygon.h"

coord coord_minus(const coord a, const coord b)
{
	return (coord){.lat = a.lat - b.lat, 
		  		   .lon = a.lon - b.lon};
}
coord coord_plus(const coord a, const coord b)
{
	return (coord){.lat = a.lat + b.lat, 
		  		   .lon = a.lon + b.lon};
}
coord coord_sca_mult(const coord a, double scal)
{
	return (coord) {.lat = a.lat * scal, 
		            .lon = a.lon * scal};
}

static inline bool dj_lt_osmnodes(struct s_osmnode a, struct s_osmnode b) { return a.id < b.id; }
static inline bool dj_eq_osmnodes(struct s_osmnode a, struct s_osmnode b) { return a.id == b.id; }
static inline bool dj_lt_osmways(struct s_osmway a, struct s_osmway b) { return a.id < b.id; }
static inline bool dj_eq_osmways(struct s_osmway a, struct s_osmway b) { return a.id == b.id; }
static inline bool dj_lt_osmrel(struct s_osmrelation a, struct s_osmrelation b) {return a.id < b.id; }
static inline bool dj_eq_osmrel(struct s_osmrelation a, struct s_osmrelation b) {return a.id < b.id; }

BST_IMPL(osmnodes, struct s_osmnode)
AVL_IMPL(osmnodes, struct s_osmnode)
BST_IMPL(osmways, struct s_osmway)
AVL_IMPL(osmways, struct s_osmway)
BST_IMPL(osmrel, struct s_osmrelation)
AVL_IMPL(osmrel, struct s_osmrelation)

static void read_bound_coord(struct s_osm* map, xml_attr* coord) {
	assert(coord);
	if(!strcmp(coord->tag, "minlat")) {
		map->min_coord.lat = atof(coord->content);
	} else if (!strcmp(coord->tag, "minlon")) {
		map->min_coord.lon = atof(coord->content);
	} else if (!strcmp(coord->tag, "maxlat")) {
		map->max_coord.lat = atof(coord->content);
	} else if (!strcmp(coord->tag, "maxlon")) {
		map->max_coord.lon = atof(coord->content);
	} else assert(0);
}
static void read_bounds(struct s_osm* map, xml_node* bounds) {
	assert(!strcmp(bounds->tag, "bounds"));
	xml_attr* a = bounds->attributes;
	read_bound_coord(map, a);
	xml_attr* b = a->next;
	read_bound_coord(map, b);
	xml_attr* c = b->next;
	read_bound_coord(map, c);
	xml_attr* d = c->next;
	read_bound_coord(map, d);
}

static void read_node_attr(struct s_osmnode* node, xml_node* root) {
	assert(node != NULL && root != NULL);
	node->data.type = UnknownType;

	const xml_node* tag = root->children;
	while(tag && !strcmp(tag->tag, "tag")) {
		xml_attr* attr = tag->attributes;
		assert(!strcmp(attr->tag, "k"));

		if(!strcmp(attr->content, "natural")) {
			node->data.type = Natural;
			node->data.subtypes.natural = UnclassifiedNatural;
			xml_attr* subtype = attr->next;
			assert(!strcmp(subtype->tag, "v"));
#define X(id, str, c) else if(!strcmp(subtype->content, str)) node->data.subtypes.natural = id;
			if(0) {} OSMNATURE_FOREACH(X)
#undef X
		}
#define X(en, str) else if(!strcmp(attr->content, str)) node->data.type = en;
		OSMTYPES_FOREACH(X)
#undef X
		tag = tag->next;
	}
}

/**
 * @param way reference to be modified
 * @param tag the first tag in the linked list to be parsed
 **/
static enum e_osmtype read_way_attr(struct s_osmway* way, xml_node* tag) {
	assert(way != NULL);

	if(tag == NULL) {
		way->data.type = EmptyType;
		return way->data.type;
	}

	way->data.type = UnknownType;
	while(tag && !strcmp(tag->tag, "tag")) {
		xml_attr* attr = tag->attributes;
		assert(!strcmp(attr->tag, "k"));
		if(!strcmp(attr->content, "area") && !strcmp(attr->next->content, "yes")) {
			way->area = true;
		} else {// if(way->data.type == UnknownType || true) {
			if(!strcmp(attr->content, "indoor")) {
				 if(!strcmp(attr->next->content, "yes") ||
					!strcmp(attr->next->content, "room") ||
					!strcmp(attr->next->content, "area") ||
					!strcmp(attr->next->content, "corridor") ||
					!strcmp(attr->next->content, "level") ||
					!strcmp(attr->next->content, "wall") ||
					!strcmp(attr->next->content, "door")) {
					way->data.type = Indoor;
					// TODO: Handle indoor types
				}
			} else if(!strcmp(attr->content, "highway")) {
				way->data.type = Highway;
				way->data.subtypes.highway = UnclassifiedHighway;
				xml_attr* subtype = attr->next;
				#define X(id, str, c) else if(!strcmp(subtype->content, str)) way->data.subtypes.highway = id;
				if(0) {}OSMROADS_FOREACH(X)
				#undef X
			} else if(!strcmp(attr->content, "natural")) {
				way->data.type = Natural;
				way->data.subtypes.natural = UnclassifiedNatural;
				xml_attr* subtype = attr->next;
				#define X(id, str, c) else if(!strcmp(subtype->content, str)) way->data.subtypes.natural = id;
				if(0) {}OSMNATURE_FOREACH(X)
				#undef X
			} else if(!strcmp(attr->content, "landuse")) {
				way->data.type = Landuse;
				way->data.subtypes.landuse = UnknownLanduse;
				xml_attr* subtype = attr->next;
				#define X(id, str, c) else if(!strcmp(subtype->content, str)) way->data.subtypes.landuse = id;
				if(0) {}OSMLANDUSE_FOREACH(X)
				#undef X
			} else if(!strcmp(attr->content, "leisure")) {
				way->data.type = Leisure;
				way->data.subtypes.leisure = UnknownLeisure;
				xml_attr* subtype = attr->next;
				#define X(id, str, c) else if(!strcmp(subtype->content, str)) way->data.subtypes.leisure = id;
				if(0) {} OSMLEISURE_FOREACH(X)
				#undef X
			}
			#define X(en, str) else if(way->data.type == UnknownType && str_startswith(attr->content, str)) way->data.type = en;
			OSMTYPES_FOREACH(X)
			#undef X
		}

		tag = tag->next;
	}

	return way->data.type;
}

void osm_load(const char* mapfile, struct s_osm* map)
{
	init_bst_osmnodes(&map->nodes);
	init_bst_osmways(&map->ways);
	init_bst_osmrel(&map->relations);
	map->dom = xml_read_file(mapfile);
	assert(map->dom);
	xml_node* root = map->dom->root;
	xml_node* bounds = root->children;
	read_bounds(map, bounds);

	xml_node* osm_node = bounds->next;
	assert(osm_node != NULL);
	assert(!strcmp(osm_node->tag, "node"));

	while(osm_node != NULL && !strcmp(osm_node->tag, "node")) {
		xml_attr* id_tag = osm_node->attributes;
		assert(!strcmp(id_tag->tag, "id"));
		xml_attr* lat_tag = id_tag->next->next->next->next->next->next->next;
		xml_attr* lon_tag = lat_tag->next;
		struct s_osmnode new_node = {.id = atol(id_tag->content), 
			                         .coords.lat= atof(lat_tag->content),
									 .coords.lon= atof(lon_tag->content)};
		read_node_attr(&new_node, osm_node);

		avl_insert_osmnodes(&map->nodes, new_node);
		osm_node = osm_node->next;
	}
	assert(!strcmp(osm_node->tag, "way"));
	
	size_t way_unknown_types = 0;
	while(osm_node != NULL && !strcmp(osm_node->tag, "way")) {
		xml_attr* id_tag = osm_node->attributes;
		assert(!strcmp(id_tag->tag, "id"));

		xml_node* way_node = osm_node->children;
		size_t nodecount = 0;
		while(way_node != NULL && !strcmp(way_node->tag, "nd")) {
			nodecount++;
			way_node = way_node->next;
		}


		struct s_osmway new_way = {.id = atoi(id_tag->content),
								   .nodes = (struct s_osmnode**) malloc(sizeof(struct s_osmnode*)*nodecount),
								   .nodecount = nodecount,
								   .area = false
		};
		read_way_attr(&new_way, way_node);
		if(new_way.data.type == UnknownType) {
			log_warn("Way %lu has no known type", new_way.id);
			way_unknown_types++;
		}

		way_node = osm_node->children;
		for(size_t i=0; i<new_way.nodecount; i++) {
			xml_attr* ref = way_node->attributes;
			assert(!strcmp(ref->tag, "ref"));

			struct s_osmnode cmp_node = {.id = atol(ref->content) };
			struct s_osmnode* node = bst_search_osmnodes(&map->nodes, cmp_node);
			assert(node);
			new_way.nodes[i] = node;
			way_node = way_node->next;
		}
		// TODO: actually check area tag and also consider closed vs open ways
		if(new_way.data.type == Highway || new_way.data.type == Barrier) {
		} else {
			new_way.area = new_way.nodes[0] == new_way.nodes[nodecount-1];
		}
		if(new_way.area) {
			vec2 coords[new_way.nodecount];
			for(size_t i=0; i<nodecount; i++) {
				// Multiply the coords with 1000 to avoid double unprecision
				coords[i] = (vec2) {.x = new_way.nodes[i]->coords.lon*1000, 
					                .y = -new_way.nodes[i]->coords.lat*1000};
			}
			new_way.area_value = polygon_area(new_way.nodecount, coords);
			assert(new_way.area_value >= 0);
		} else {
			new_way.area_value = -1;
		}

		avl_insert_osmways(&map->ways, new_way);

		osm_node = osm_node->next;
	}
	assert(!osm_node || !strcmp(osm_node->tag, "relation"));
	
	while(osm_node != NULL && !strcmp(osm_node->tag, "relation")) {
		xml_attr* id_tag = osm_node->attributes;
		assert(!strcmp(id_tag->tag, "id"));

		struct s_osmrelation new_rel = {
			.id = atoi(id_tag->content),
			.type = UnknownRel,
			.members = NULL,
			.membercount = 0
		};
		xml_node* member = osm_node->children;
		while(member != NULL && !strcmp(member->tag, "member")) {
			assert(!(strcmp(member->attributes->tag, "type")));
			new_rel.membercount++;
			member = member->next;
		}
		xml_node* tag = member;
		while(tag != NULL && !strcmp(tag->tag, "tag")) {
			xml_attr* tag_type = tag->attributes;
			assert(!strcmp(tag_type->tag, "k"));

			if(!strcmp(tag_type->content, "type")) {
				xml_attr* tag_content = tag_type->next;
				assert(!strcmp(tag_content->tag, "v"));

#define X(id, str) else if(!strcmp(tag_content->content, str)) new_rel.type = id;
				if(0){} RELATION_FOREACH(X)
#undef X
			}

			tag = tag->next;
		}
		new_rel.members= malloc(sizeof(struct s_osmrel_member)*new_rel.membercount);

		member = osm_node->children;
		size_t member_index = 0;
		while(member != NULL && !strcmp(member->tag, "member")) {
			xml_attr* type_tag = member->attributes;
			assert(!(strcmp(type_tag->tag, "type")));
			xml_attr* ref_tag = type_tag->next;
			assert(!(strcmp(ref_tag->tag, "ref")));
			xml_attr* role_tag = ref_tag->next;
			assert(!(strcmp(role_tag->tag, "role")));

			new_rel.members[member_index].role = UnknownRole;
			#define X(id, str) \
			else if(!strcmp(role_tag->content, str)) { \
				new_rel.members[member_index].role = id; \
			}
			if(0) {} RELATIONROLE_FOREACH(X)
#undef X

			#define X(Enum, str, T, suffix, avl) \
			else if (!strcmp(type_tag->content, str)) { \
				T looking_for = {.id = atol(ref_tag->content)}; \
				T* found = bst_search_##suffix(&map->avl, looking_for); \
				if(found != NULL) { \
					new_rel.members[member_index].ptr = found; \
					new_rel.members[member_index].type = Enum; \
					member_index++; \
				} else { \
					new_rel.membercount--; \
				} \
			}
			if(0) {} RELATIONMEMBER_FOREACH(X)
			else {
				log_error("Relation member type %s not known", type_tag->content);
				assert(0);
			}
#undef X
			member = member->next;
		}
		assert(new_rel.membercount == member_index);

		tag = member;
		while(tag != NULL && !strcmp(tag->tag, "tag")) {
			xml_attr* attr = tag->attributes;
			assert(!strcmp(attr->tag, "k"));
			assert(!strcmp(attr->next->tag, "v"));

#define X(en, str) else if(str_startswith(attr->content, str)) { new_rel.data.type = en; }
		if(0) {} OSMTYPES_FOREACH(X)
#undef X
			tag = tag->next;
		}

		avl_insert_osmrel(&map->relations, new_rel);

		osm_node = osm_node->next;
	}
	assert(osm_node == NULL);

	log_debug("%zu ways with unknown type parsed", way_unknown_types);
}

static void deinit_ways(struct s_osmway* way, void* ptr) {
	free(way->nodes);
}
static void deinit_rels(struct s_osmrelation* rel, void* ptr) {
	free(rel->members);
}

void osm_free(struct s_osm* map)
{
	bst_foreach_osmways(&map->ways, deinit_ways, NULL);
	bst_foreach_osmrel(&map->relations, deinit_rels, NULL);
	deinit_bst_osmnodes(&map->nodes);
	deinit_bst_osmways(&map->ways);
	deinit_bst_osmrel(&map->relations);
	xml_free(map->dom);
}

struct s_osmnode* osm_add_node(struct s_osm* map, long id, coord coords, struct s_wayattr attributes)
{
	struct s_osmnode new_node = {.id = id, .coords = coords, .data = attributes };

	avl_insert_osmnodes(&map->nodes, new_node);
	return bst_search_osmnodes(&map->nodes, new_node);
}
struct s_osmway* osm_add_way(struct s_osm* map, long id, size_t num_nodes, bool area, struct s_wayattr attributes)
{
	struct s_osmway new_way = {.id = id, .nodecount = num_nodes, .area = area, .data = attributes,
								   .nodes = (struct s_osmnode**) malloc(sizeof(struct s_osmnode*)*num_nodes) };
	avl_insert_osmways(&map->ways, new_way);
	return bst_search_osmways(&map->ways, new_way);
}
