#include "osm.h"
#include "svg.h"
#include "heap.h"
#include <assert.h>
#include <string.h>
#include "attributes.h"

#define SCALING 10000
#define LINE_COLOR HEX_BLACK
#define BG_COLOR HEX_GRAY
#define ERROR_COLOR "#ff00ff"

struct s_lineinfo {
	FILE* svg;
	coord min;
	coord max;
};

static bool should_render_way(const struct s_osmway* way) {
	if(way->data.type == Indoor) return false;
	if(way->data.type == EmptyType) return false;
	if(way->data.type == Boundary) return false;
	if(way->data.type == Razed) return false;
	return true;
}

static int way_cmp(const void** a_ptr_ptr, const void** b_ptr_ptr) {
	const struct s_osmway* a = *((const struct s_osmway**) a_ptr_ptr);
	const struct s_osmway* b = *((const struct s_osmway**) b_ptr_ptr);

	if(a->area && b->area) {
		long double diff = b->area_value - a->area_value;
		if(diff < 0) return -1;
		if(diff == 0) return 0;
		if(diff > 0) return 1;
		assert(0);
	} else if(a->area == b->area) { // both paths
		return a->id - b->id;
	}
	return ((int) b->area) - ((int) a->area);
}

static vec2 coord_to_svg_vec(coord coords, coord min, coord max) {
	coords = coord_sca_mult(coords, SCALING);
	return (vec2) {.x = coords.lon, .y = -coords.lat};
}

static void add_to_heap(const struct s_osmway* way, heap_v* heap) 
{
	if(!should_render_way(way)) return;
	heap_insert_v(heap, (void*) way);
}

static const AdjNode* get_prims_start(AdjGraph* g, const AdjNode* n) {
    while(n->algorithms.prim.parent != -1) n = graph_node(g, n->algorithms.prim.parent);
    return n;
}

static void drw_graph(AdjGraph* g, struct s_lineinfo* params)
{
	assert(g != NULL && params != NULL);
	for(size_t i=0; i<graph_node_count(g); i++) {
		AdjNode* node = vector_at_nodes(&g->nodes, i);
		assert(node != NULL);
		struct s_osmnode* start_node = node->data;
		assert(start_node != NULL);
		vec2 start = coord_to_svg_vec(start_node->coords, params->min, params->max);

		size_t id = get_prims_start(g, node)->id;
		char class_buf[64];
		sprintf(class_buf, "spanntree spanntree-%lu", id);

		for(size_t i=0; i<graph_node_edge_count(node); i++) {
			AdjEdge* edge = graph_edge(g, *vector_at_l(&node->edges, i));
			AdjNode* start_n = graph_node(g, edge->start);
			AdjNode* end_n = graph_node(g, edge->end);
			assert(start_n == node);

			if(start_n->id > end_n->id) {
				continue;
			}
			struct s_osmnode* end_node = end_n->data;

			vec2 end = coord_to_svg_vec(end_node->coords, params->min, params->max);

			svg_line_class(params->svg, 
					 edge->highlight ? HEX_DARKRED : HEX_BLACK,
                     start.x, start.y, end.x, end.y,
                     edge->highlight ? 0.2 : 0.1, 
					 class_buf);

		}
		sprintf(class_buf, "%sspanntree spanntree-%lu", 
				node->algorithms.prim.parent == -1 ? "root_node " : "", 
				id);
		svg_circle_class(params->svg, NONE, HEX_DARKRED, 0, start, 0.2, class_buf);
	}
}

UNUSED static void draw_way(const struct s_osmway* way, void* payload) {
	

	struct s_lineinfo* params = (struct s_lineinfo*) payload;
	if(way == NULL || way->nodecount == 0) return;

	vec2 coords[way->nodecount];
	for(size_t i=0; i<way->nodecount; i++) {
		coords[i] = coord_to_svg_vec(way->nodes[i]->coords, params->min, params->max);
	}
	char id_buf[64];
	sprintf(id_buf, "%lu", way->id);
	char class_buf[256];
	if(way->data.type == Natural) {
		sprintf(class_buf, "natural:%s", osmnature_str(way->data.subtypes.natural));
	} else if (way->data.type == Landuse) {
		sprintf(class_buf, "landuse:%s", osmlanduse_str(way->data.subtypes.landuse));
	} else if (way->data.type == Leisure) {
		sprintf(class_buf, "leisure:%s", osmleisure_str(way->data.subtypes.leisure));
	} else if (way->data.type == CustomType) {
		sprintf(class_buf, "custom:%s", way->data.subtypes.custom.name);
	} else {
		sprintf(class_buf, "%s", osmtype_str(way->data.type));
	}
	if(way->area) {
		svg_polygon_id_class(params->svg, LINE_COLOR, way_color(way), 0.05, way->nodecount, coords, id_buf, class_buf);
	} else {
		double w = 0.25;
		if(way->data.type == Barrier) {
			w = 0.1;
		}
		svg_polyline_class_id(params->svg, way_color(way), w, way->nodecount, coords, class_buf, id_buf);
		// Railway special styling
		// We draw a white dasharray over the black line
		if(way->data.type == Railway) {
			const char* style = "stroke-dasharray=\"2, 2\"";
			svg_polyline_style(params->svg, HEX_WHITE, w, way->nodecount, coords, style);
		}
	}
}

UNUSED static void draw_node(const struct s_osmnode* node, void* payload) {
	struct s_lineinfo* params = (struct s_lineinfo*) payload;
	static char id_buf[64];
	static char class_buf[128];
	vec2 coords = coord_to_svg_vec(node->coords, params->min, params->max);

	switch(node->data.type) {
		case Natural:
#define X(name, str, c) \
			else if(node->data.subtypes.natural == name) { \
				sprintf(id_buf, "%ld", node->id); \
				svg_circle_id(params->svg, NONE, c, 0, coords, 0.2, id_buf); \
			}
			if(0) {} OSMNATURE_FOREACH(X)
#undef X
			break;
		case CustomType:
			strcpy(class_buf, "custom");
			if(!strcmp(node->data.subtypes.custom.name, "trackpoint")) {
				sprintf(id_buf, "%ld", node->id);
				strcat(class_buf, ":trackpoint");
				svg_circle_class_id(params->svg, NONE, node->data.subtypes.custom.color, 0, coords, 0.4, class_buf, id_buf);
			}
			break;
		default: break;
	}
}

UNUSED static void draw_relation(const struct s_osmrelation* relation, void* payload) {
	struct s_lineinfo* params = payload;
	if(relation->data.type == Boundary) return;
	
	if(relation->type == MultipolygonRel) {
		char id_buf[64];
		sprintf(id_buf, "%ld", relation->id);
		svg_begin_path_id(params->svg, id_buf);

		for(size_t member_i=0; member_i<relation->membercount; member_i++) {
			const struct s_osmrel_member* member = &relation->members[member_i];
			if(member->type == WayMember) {
				const struct s_osmway* way = member->ptr;
				vec2 coords[way->nodecount];
				for(size_t i=0; i<way->nodecount; i++) {
					coords[i] = coord_to_svg_vec(way->nodes[i]->coords, params->min, params->max);
                }
				double winding_order = vertex_winding(way->nodecount, coords);
				if((member->role == OuterRole && winding_order < 0) ||
				   (member->role == InnerRole && winding_order > 0)) {
                    flip_winding_order(way->nodecount, coords);
                }
				winding_order = vertex_winding(way->nodecount, coords);

				svg_subpath(params->svg, way->nodecount, coords);
			}
		}
		svg_end_path(params->svg, HEX_DARKBLUE, NONE, 0.05);
	} else if(relation->type == BoundaryRel) {
		fprintf(params->svg, "<g id=\"%lu\">\n", relation->id);
		for(size_t member_i=0; member_i<relation->membercount; member_i++) {
			const struct s_osmrel_member* member = &relation->members[member_i];
			if(member->type == WayMember) {
				const struct s_osmway* way = member->ptr;
				vec2 coords[way->nodecount];
				for(size_t i=0; i<way->nodecount; i++) {
					coords[i] = coord_to_svg_vec(way->nodes[i]->coords, params->min, params->max);
				}
				svg_polyline(params->svg, HEX_PURPLE, 0.25, way->nodecount, coords);
			}
		}
		fprintf(params->svg, "</g>\n");
	}
}

void osm_graph(const struct s_osm* map, const char* out_file, AdjGraph* g)
{
	coord min = map->min_coord;
	coord max = map->max_coord;
	min.lat *= -1;
	max.lat *= -1;
	double tmp = min.lat;
	min.lat = max.lat;
	max.lat = tmp;

	viewport viewBox = {
		.min = (vec2)  {.x = min.lon,         .y = min.lat},
		.dims = (vec2) {.x = max.lon-min.lon, .y = max.lat-min.lat}
	};
	viewBox.min.x *= SCALING;
	viewBox.min.y *= SCALING;
	viewBox.dims.x *= SCALING;
	viewBox.dims.y *= SCALING;
	FILE* out_svg = svg_open(out_file, viewBox, BG_COLOR);

	static const char* style = " \
		polygon:hover, path:hover, circle:hover, polyline:hover {\n \
			filter: brightness(1.25);\n \
		}\n \
		.root_node {\n \
			fill: yellow;\n \
		}\n \
		line.spanntree-0:hover, line.spanntree-0:hover ~ line.spanntree-0 {\n \
			stroke: white;\n \
		}\n \
		line.spanntree-1:hover, line.spanntree-1:hover ~ line.spanntree-1 {\n \
			stroke: white;\n \
		}\n \
		line.spanntree-2:hover, line.spanntree-2:hover ~ line.spanntree-2 {\n \
			stroke: white;\n \
		}\n \
		line.spanntree-3:hover, line.spanntree-3:hover ~ line.spanntree-3 {\n \
			stroke: white;\n \
		}\n \
	";

	svg_add_style(out_svg, style);
	struct s_lineinfo func_params = {.svg = out_svg, .min=map->min_coord, .max=map->max_coord};

	// Fill heap
	heap_v render_ways;
	heap_init_v(&render_ways, way_cmp);
	heap_reserve_v(&render_ways, map->ways.size);
	bst_foreach_osmways((BSTree_osmways*) &map->ways, (bst_it_func_osmways) add_to_heap, &render_ways);
	while(!heap_is_empty(&render_ways)) {
		struct s_osmway** w_p = (struct s_osmway**) heap_front_v(&render_ways);
		struct s_osmway* w = *w_p;
		draw_way(w, &func_params);
		heap_pop_v(&render_ways);
	}

	//bst_foreach_osmways((BSTree_osmways*) &map->ways, (bst_it_func_osmways) draw_way, &func_params);
	bst_foreach_osmrel((BSTree_osmrel*) &map->relations, (bst_it_func_osmrel) draw_relation, &func_params);
	bst_foreach_osmnodes((BSTree_osmnodes*) &map->nodes, (bst_it_func_osmnodes) draw_node, &func_params);

	if(g != NULL)
		drw_graph(g, &func_params);

	svg_close(out_svg);

	heap_cleanup_v(&render_ways);
}
