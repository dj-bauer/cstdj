#ifndef DJWINDOW_COMMON_H
#define DJWINDOW_COMMON_H
#include <assert.h>
#include "util.h"
void dj_win_sub_event(DJWindow* win, DJEventType event, void (*callback_func)(DJEvent *, void *), void *obj)
{
	if(!win) return;
	assert(event < EventCount);
	EventCallback* callback = win->callbacks[event];
	if(callback) {
		while(callback->next)
			callback = callback->next;
		callback->next = calloc(1, sizeof(EventCallback));
		callback = callback->next;
	} else {
		win->callbacks[event] = calloc(1, sizeof(EventCallback));
		assert(win->callbacks[event]);
		callback = win->callbacks[event];
	}
	callback->callback_func = callback_func;
	callback->obj = obj;
}
void dj_win_unsub_event(DJWindow *win, DJEventType event, void (*func)(DJEvent *, void *), void *obj) {
	if(!win) return;
	EventCallback* callback = win->callbacks[event];
	while(callback && (callback->callback_func != func || callback->obj != obj)) {
		callback = callback->next;
	}
	if(callback) {
		EventCallback* prev = win->callbacks[event];
		if(prev == callback) {
			// If this subscriber is the first element. We just gotta clear the list entry
			win->callbacks[event] = NULL;
		} else {
			// Find the parent and skip this subscriber
			while(prev->next != callback) {
				prev = prev->next;
			}
			prev->next = callback->next;
		}

		free(callback);
	}
}
void dj_win_toggle_fullscreen(DJWindow* window)
{
	dj_win_set_fullscreen(window, !dj_win_get_fullscreen(window));
}
void dispatch_event(DJWindow* win, DJEventType type, DJEvent* e)
{
	EventCallback* callback = win->callbacks[type];
	while(callback) {
		// We cannot be sure, that the object will still exist after invocation
		// So we already update the reference to the next object
		// and simply use the old value for invocation
		EventCallback* to_invoke = callback;
		callback = callback->next;
		to_invoke->callback_func(e, to_invoke->obj);
	}
}
const char* dj_mousebutton_str(MouseButton btn)
{
	switch(btn) {
		case(MouseLeft): return "Left Button";
		case(MouseRight): return "Right Button";
		case(MouseMiddle): return "Middle Button";
		default: return "UnknownButton";
	}
	ASSERT_NOT_REACHED;
}
#endif
