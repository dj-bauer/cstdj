#include "svg.h"

#define TAB "\t"

FILE* svg_open(const char* filename, viewport viewport, const char* bg_color)
{
	FILE* f = fopen(filename, "w");
	if(f == NULL) return NULL;

	fprintf(f, "<svg viewBox=\"%Lf %Lf %Lf %Lf\" xmlns=\"http://www.w3.org/2000/svg\">\n", viewport.min.x, viewport.min.y, viewport.dims.x, viewport.dims.y);
	svg_rect(f, bg_color, HEX_BLACK, viewport.min.x, viewport.min.y, viewport.dims.x, viewport.dims.y, 0);

	return f;
}

void svg_close(FILE* f) 
{
	fprintf(f, "</svg>\n");
	fclose(f);
}

void svg_rect(FILE* f, const char* color, const char* stroke_color, double x, double y, double width, double height, double stroke_width)
{
	fprintf(f, TAB"<rect fill=\"%s\" stroke=\"%s\" x=\"%f\" y=\"%f\" width=\"%f\" height=\"%f\" stroke-width=\"%f\"/>\n",
			color, stroke_color, x, y, width, height, stroke_width);

}

void svg_line(FILE* f, const char* color, double x0, double y0, double x1, double y1, double stroke_width)
{
	fprintf(f, TAB"<line x1=\"%f\" y1=\"%f\" x2=\"%f\" y2=\"%f\" stroke=\"%s\" stroke-width=\"%f\"/>\n",
			x0, y0, x1, y1, color, stroke_width);
}
void svg_line_id(FILE* f, const char* color, double x0, double y0, double x1, double y1, double stroke_width,
                 const char* id)
{
    fprintf(f, TAB"<line id=\"%s\" x1=\"%f\" y1=\"%f\" x2=\"%f\" y2=\"%f\" stroke=\"%s\" stroke-width=\"%f\"/>\n",
            id, x0, y0, x1, y1, color, stroke_width);
}
void svg_line_class(FILE* f, const char* color, double x0, double y0, double x1, double y1, double stroke_width,
                 const char* class)
{
    fprintf(f, TAB"<line class=\"%s\" x1=\"%f\" y1=\"%f\" x2=\"%f\" y2=\"%f\" stroke=\"%s\" stroke-width=\"%f\"/>\n",
            class, x0, y0, x1, y1, color, stroke_width);
}

inline void svg_polyline(FILE* f, const char* stroke_color, double stroke_width, size_t node_count, vec2 points[node_count])
{
	svg_polyline_id(f, stroke_color, stroke_width, node_count, points, "");
}
void svg_polyline_class_id(FILE* f, const char* stroke_color, double stroke_width, 
		size_t node_count, vec2 points[node_count], 
		const char* class, const char* id)
{
	svg_polyline_class_id_style(f, stroke_color, stroke_width,
			node_count, points,
			class, id, "");
}
void svg_polyline_id(FILE* f, const char* stroke_color, double stroke_width, size_t node_count, vec2 points[node_count], const char* id)
{
	svg_polyline_style_id(f, stroke_color, stroke_width,
			node_count, points,
			id, "");
}

void svg_polyline_style(FILE* f, 
		const char* stroke_color, double stroke_width, 
		size_t node_count, vec2 points[node_count], 
		const char* style)
{
	svg_polyline_style_id(f,
			stroke_color, stroke_width,
			node_count, points,
			"", style);
}
void svg_polyline_style_id(FILE* f, 
		const char* stroke_color, double stroke_width, 
		size_t node_count, vec2 points[node_count], 
		const char* id, const char* style)
{
	svg_polyline_class_id_style(f, stroke_color, stroke_width,
			node_count, points,
			"", id,
			style);
}
void svg_polyline_class_id_style(FILE* f, 
		const char* stroke_color, double stroke_width, 
		size_t node_count, vec2 points[node_count], 
		const char* class, const char* id, 
		const char* style)
{
	if(node_count == 0) return;
	fprintf(f, TAB"<polyline fill=\"none\" stroke=\"%s\" stroke-width=\"%f\" ", stroke_color, stroke_width);
	if(style[0] != '\0')
		fprintf(f, " %s ", style);
	if(class[0] != '\0')
		fprintf(f, "class=\"%s\" ", class);
	if(id[0] != '\0')
		fprintf(f, "id=\"%s\" ", id);
	fprintf(f, "points=\"");
	for(size_t i=0; i<node_count; i++) {
		fprintf(f, "%Lf,%Lf,", points[i].x, points[i].y);
	}
	fprintf(f, "\" />\n");
}
void svg_circle_id(FILE* f, const char* stroke_color, 
		                    const char* fill_color, 
							double stroke_width, vec2 coords, double radius, 
							const char* id)
{
	fprintf(f, TAB"<circle cx=\"%Lf\" cy=\"%Lf\" r=\"%f\" fill=\"%s\" stroke=\"%s\" stroke-width=\"%f\" ",
			coords.x, coords.y, radius, fill_color, stroke_color, stroke_width);
	if(id[0] == '\0')
		fprintf(f, "/>\n");
	else
		fprintf(f, "id=\"%s\" />\n", id);
}
void svg_circle_class(FILE* f, const char* stroke_color, 
		                    const char* fill_color, 
							double stroke_width, vec2 coords, double radius, 
							const char* class)
{
	fprintf(f, TAB"<circle cx=\"%Lf\" cy=\"%Lf\" r=\"%f\" fill=\"%s\" stroke=\"%s\" stroke-width=\"%f\" ",
			coords.x, coords.y, radius, fill_color, stroke_color, stroke_width);
	if(class[0] == '\0')
		fprintf(f, "/>\n");
	else
		fprintf(f, "class=\"%s\" />\n", class);
}
void svg_circle_class_id(FILE* f, const char* stroke_color, 
		                    const char* fill_color, 
							double stroke_width, vec2 coords, double radius, 
							const char* class, const char* id)
{
	fprintf(f, TAB"<circle cx=\"%Lf\" cy=\"%Lf\" r=\"%f\" fill=\"%s\" stroke=\"%s\" stroke-width=\"%f\" ",
			coords.x, coords.y, radius, fill_color, stroke_color, stroke_width);
	if(class[0] != '\0')
		fprintf(f, "class=\"%s\" ", class);
	if(id[0] != '\0')
		fprintf(f, "id=\"%s\" ", id);
	fprintf(f, "/>\n");
}

void svg_add_style(FILE* f, const char* def) {
	fprintf(f, TAB"<defs>\n"TAB TAB"<style>\n%s\n"TAB TAB"</style>\n"TAB"</defs>\n", def);
}

inline void svg_polygon(FILE* f, const char* stroke_color, const char* fill_color, double stroke_width, size_t node_count, const vec2 points[node_count]) {
	svg_polygon_id_class(f, stroke_color, fill_color, stroke_width, node_count, points, "", "");
}
inline void svg_polygon_id(FILE* f, const char* stroke_color, const char* fill_color, double stroke_width, size_t node_count, const vec2 points[node_count], const char* id) {
	svg_polygon_id_class(f, stroke_color, fill_color, stroke_width, node_count, points, id, "");
}

void svg_polygon_id_class(FILE* f, const char* stroke_color, const char* fill_color, double stroke_width, size_t node_count, const vec2 points[node_count], const char* id, const char* class)
{
	if(node_count == 0) return;
	fprintf(f, "<polygon");
	if(id[0] != '\0') fprintf(f, " id=\"%s\"", id);
	if(class[0] != '\0') fprintf(f, " class=\"%s\"", class);
	fprintf(f, " points=\"");
	for(size_t i=0; i<node_count; i++) {
		fprintf(f, "%Lf %Lf ", points[i].x, points[i].y);
	}
	fprintf(f, "\" fill=\"%s\" stroke=\"%s\" stroke-width=\"%f\"/>\n", fill_color, stroke_color, stroke_width);
}

void svg_path_id(FILE* f, const char* stroke_color, const char* fill_color, double stroke_width, size_t node_count, const vec2 points[node_count], const char* id)
{
	if(node_count == 0) return;
	svg_begin_path_id(f, id);
	svg_subpath(f, node_count, points);
	svg_end_path(f, fill_color, stroke_color, stroke_width);
}

inline void svg_path(FILE* f, const char* stroke_color, const char* fill_color, double stroke_width, size_t node_count, const vec2 points[node_count]) {
	svg_path_id(f, stroke_color, fill_color, stroke_width, node_count, points, "");
}
void svg_begin_path(FILE* f)
{
	svg_begin_path_id(f, "");
}
void svg_begin_path_id(FILE* f, const char* id)
{
	if(id[0] != '\0')
		fprintf(f, "<path id=\"%s\" d=\"", id);
	else 
		fprintf(f, "<path d=\"");
}
void svg_end_path(FILE* f, const char* fill_color, const char* stroke_color, double stroke_width)
{
	fprintf(f, "\" fill=\"%s\" stroke=\"%s\" stroke-width=\"%f\"  />\n", fill_color, stroke_color, stroke_width);
}
void svg_subpath(FILE* f, size_t num_points, const vec2 points[num_points])
{
	fprintf(f, "M%Lf %Lf ", points[0].x, points[0].y);
	for(size_t i=1; i<num_points; i++) {
		fprintf(f, "L%Lf %Lf ", points[i].x, points[i].y);
	}
	fprintf(f, "z");
}
