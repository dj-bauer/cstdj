#include "polygon.h"
#include <assert.h>
#include <stdbool.h>
#include <math.h>
#include "dlist_impl.h"
#include "util.h"

long double vertex_winding(size_t num_points, const vec2 p[num_points])
{
	vec2* points = (vec2*) p;
	if(num_points < 2) return 0;

	// Make sure all vertices are positive
	// If not we transform all vertices up into the positive sector
	vec2 min_values = {0, 0};
	for(size_t i=0; i<num_points; i++) {
		vec2 p = points[i];
		if(p.x < min_values.x)
			min_values.x = p.x;
		if(p.y < min_values.y)
			min_values.y = p.y;
	}
	bool should_adjust_points = min_values.x < 0 || min_values.y < 0;
	if(should_adjust_points) {
		for(size_t i=0; i<num_points; i++) {
			points[i] = vec2_sub(points[i], min_values);
		}
	}

	long double circ = 0;
	
	for(size_t i=0; i<num_points-1; i++) {
		vec2 a = points[i+1];
		vec2 b = points[i];
		circ += (a.x-b.x)*(a.y+b.y);
	}
	circ += (points[0].x-points[num_points-1].x)*(points[0].y+points[num_points-1].y);

	if(should_adjust_points) {
		for(size_t i=0; i<num_points; i++) {
			points[i] = vec2_add(points[i], min_values);
		}
	}

	return circ;
}

void flip_winding_order(const size_t num_points, vec2 points[num_points]) {
#ifndef NDEBUG
	vec2 first = points[0];
#endif
	for(size_t i=0; i<(num_points/2); i++) {
		vec2 tmp = points[i];
		points[i] = points[num_points-i-1];
		points[num_points-i-1] = tmp;
	}
#ifndef NDEBUG
	assert(points[num_points-1].x == first.x && points[num_points-1].y == first.y);
#endif
}

bool dj_eq_p(vec2 a, vec2 b) { return a.x==b.x && a.y==b.y; }
DLIST_DECL(p, vec2)
DLIST_IMPL(p, vec2)
long double polygon_area(size_t n, const vec2 c[n])
{
	if(n == 0) return 0;

	if(dj_eq_p(c[0], c[n-1])) {
		n--;
	}

	vec2 coords[n];
	memcpy(coords, c, sizeof(vec2)*n);
	if(vertex_winding(n, coords) < 0)
		flip_winding_order(n, coords);
	assert(vertex_winding(n, coords) >= 0 && "Clockwise winding order");

	dlist_p points;
	init_dlist_p(&points);
	for(size_t i=0; i<n; i++) {
		dlist_append_p(&points, coords[i]);
	}

    ASSERT_NONNULL(dlist_first_p(&points));
    ASSERT_NONNULL(dlist_last_p(&points));
	assert(!dj_eq_p(*dlist_first_p(&points), *dlist_last_p(&points)));

	long double area = 0;
	while(dlist_size_p(&points) > 2) {
		bool trimmed = false;
		dlistnode_p* p = points.head;
		while(!trimmed) {
			assert(p != NULL);

			dlistnode_p* left_p = p->prev == NULL ? points.tail : p->prev;
			dlistnode_p* right_p = p->next == NULL ? points.head : p->next;
			assert(left_p != NULL);
			assert(right_p != NULL);

			vec2 a = vec2_sub(left_p->data, p->data);
			vec2 b = vec2_sub(right_p->data, p->data);
			long double angle = vec2_angle(a, b);
			if(0 <= angle && angle <= M_PI*2) {
				vec2 p1 = right_p->data;
				vec2 p2 = p->data;
				vec2 p3 = left_p->data;
				long double A = 0.5 * fabsl(p1.x*(p2.y-p3.y) + p2.x*(p3.y-p1.y) + p3.x*(p1.y-p2.y));
				area += A;
				trimmed = true;
				dlist_remove_p(&points, p);
			} else {
				p = p->next;
			}
		}
		assert(trimmed);
	}
	dlist_clear_p(&points);
	return area;
}

vec2 vec2_add(vec2 a, vec2 b) {
	return (vec2) {.x = a.x + b.x,
		           .y = a.y + b.y};
}
vec2 vec2_sub(vec2 a, vec2 b) {
	return (vec2) {.x = a.x - b.x,
		           .y = a.y - b.y};
}
vec2 vec2_sca_mult(vec2 a, long double sca)
{
	return (vec2) {.x = a.x * sca,
		           .y = a.y * sca};
}
inline long double vec2_dist(vec2 a) {
	return sqrt(a.x*a.x + a.y*a.y);
}
inline long double vec2_sca(vec2 a, vec2 b) {
	return a.x*b.x + a.y*b.y;
}
inline vec2 vec2_norm(vec2 v) {
	long double d = vec2_dist(v);
	return vec2_sca_mult(v, 1/d);
}
inline long double vec2_angle(vec2 a, vec2 b) {
	long double cos = vec2_sca(vec2_norm(a), vec2_norm(b));
	long double angle = acosl(cos);
	if(cos >= 1) return 0.0f;
	if(cos <= -1) return 2*M_PI;
	return angle;
}
