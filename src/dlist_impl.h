#include "dlist.h"
#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include "./cmp.h"

#ifdef NDEBUG
#define CHECK_CONTAINMENT(i) (void)(0);
#else
#define CHECK_CONTAINMENT(i) \
	bool contains_node = false; \
	struct s_dlist_node_##i* cn = l->head; \
	while(cn != NULL) { \
		if(cn == n) { contains_node = true; break; } \
		cn = cn->next; \
	} \
	assert(contains_node); \
	if(!contains_node) { \
		return; \
	}
#endif

#define DLIST_IMPL(i, T) \
	dlist_##i* create_dlist_##i(void) { \
		dlist_##i* l = malloc(sizeof(dlist_##i)); \
		init_dlist_##i(l); \
		return l; \
	} \
	void init_dlist_##i(dlist_##i* l) { \
		if(l == NULL) return; \
		l->head = NULL; \
		l->tail = NULL; \
		l->size = 0; \
	} \
	void destroy_dlist_##i(dlist_##i** list_ptr) { \
		if(list_ptr == NULL || *list_ptr == NULL) return; \
		deinit_dlist_##i(*list_ptr); \
		free(*list_ptr); \
		*list_ptr = NULL; \
	} \
	void deinit_dlist_##i(dlist_##i* l) { \
		dlist_clear_##i(l); \
	} \
	void dlist_clear_##i(dlist_##i* l) { \
		if(l == NULL) return; \
		struct s_dlist_node_##i* to_delete = l->head; \
		while(to_delete != NULL) { \
			struct s_dlist_node_##i* next = to_delete->next; \
			free(to_delete); \
			to_delete = next; \
		} \
		l->size = 0; \
	} \
	size_t dlist_size_##i(const dlist_##i* l) { \
		if(l == NULL) return 0; \
		return l->size; \
	} \
	static inline dlistnode_##i* create_node_##i(T value, dlistnode_##i* p, dlistnode_##i* next) { \
		dlistnode_##i* n = malloc(sizeof(dlistnode_##i)); \
		if(n == NULL) return NULL; \
		n->prev = p; \
		n->next = next; \
		n->data = value; \
		return n; \
	} \
	void dlist_append_##i(dlist_##i* l, T value) { \
		if(l == NULL) return; \
		if(l->head == NULL) { \
			assert(l->tail == NULL); \
			l->head = create_node_##i(value, NULL, NULL); \
			l->tail = l->head; \
		} else { \
			assert(l->tail != NULL); \
			dlistnode_##i* n = create_node_##i(value, l->tail, NULL); \
			l->tail->next = n; \
			l->tail = n; \
		} \
		assert(l->tail != NULL); \
		l->size++; \
	} \
	void dlist_prepend_##i(dlist_##i* l, T value) { \
		if(l == NULL) return; \
		if(l->head == NULL) { \
			assert(l->tail == NULL); \
			l->head = create_node_##i(value, NULL, NULL); \
			l->tail = l->head; \
		} else { \
			dlistnode_##i* n = create_node_##i(value, NULL, l->head); \
			l->head->prev = n; \
			l->head = n; \
		} \
		l->size++; \
	} \
	void dlist_insert_##i(dlist_##i* l, struct s_dlist_node_##i* n, T value) { \
		if(l == NULL || n == NULL) return; \
		CHECK_CONTAINMENT(i) \
		if(n->next == NULL) { \
			dlist_append_##i(l, value); \
			return; \
		} \
		struct s_dlist_node_##i* new_node = create_node_##i(value, n, n->next); \
		n->next = new_node; \
		l->size++; \
	} \
	void dlist_insert_before_##i(dlist_##i* l, struct s_dlist_node_##i* n, T value) { \
		if(l == NULL || n == NULL) return; \
		CHECK_CONTAINMENT(i) \
		if(n->prev == NULL) { \
			dlist_prepend_##i(l, value); \
			return; \
		} \
		struct s_dlist_node_##i* new_node = create_node_##i(value, n->prev, n); \
		n->prev->next = new_node; \
		l->size++; \
	} \
	void dlist_remove_first_##i(dlist_##i* l) { \
		if(l == NULL || l->size == 0) return; \
		struct s_dlist_node_##i* n = l->head;     \
		assert(n != NULL);                 \
		assert(n->prev == NULL);           \
        l->head = n->next; \
		l->size--; \
		if(l->head == NULL) {  \
            l->tail = NULL;          \
            assert(l->size == 0);   \
        } else {         \
            l->head->prev = NULL;                 \
        } \
		free(n);               \
		assert(l->size != 0  || l->head == NULL); \
        assert(l->head == NULL || l->head->prev == NULL); \
	} \
	void dlist_remove_last_##i(dlist_##i* l) { \
		if(l == NULL || l->size == 0) return; \
		struct s_dlist_node_##i* n = l->tail;     \
        assert(n != NULL);                 \
		assert(n->next == NULL);           \
		l->tail = n->prev;     \
		l->size--; \
		if(l->tail == NULL) {  \
            l->head = NULL; \
            assert(l->size == 0);   \
        } else {         \
            l->tail->next = NULL;        \
        } \
		free(n);               \
		assert(l->size != 0 || l->tail == NULL); \
        assert(l->tail == NULL || l->tail->next == NULL);            \
	} \
	void dlist_remove_##i(dlist_##i* l, struct s_dlist_node_##i* n) { \
		if(l == NULL || n == NULL) return; \
		CHECK_CONTAINMENT(i) \
		if(n->prev == NULL) { \
			l->head = n->next; \
		} else { \
			n->prev->next = n->next; \
		} \
		if(n->next == NULL) { \
			l->tail = n->prev; \
		} else { \
			n->next->prev = n->prev; \
		} \
		free(n); \
		l->size--; \
	} \
	void dlist_remove_all_of_##i(dlist_##i* l, T value) { \
		if(l == NULL) return; \
		struct s_dlist_node_##i* n = l->head; \
		while(n != NULL) { \
			struct s_dlist_node_##i* old = n; \
			n = n->next; \
			if(dj_eq_##i(old->data, value)) dlist_remove_##i(l, old); \
		} \
	} \
	dlistnode_##i* dlist_find_first_of_##i(dlist_##i* l, T value) { \
		if(l == NULL) return NULL; \
		struct s_dlist_node_##i* n = l->head; \
		while(n != NULL) { \
			if(dj_eq_##i(n->data, value)) return n; \
			n = n->next; \
		} \
		return NULL; \
	} \
	dlistnode_##i* dlist_find_last_of_##i(dlist_##i* l, T value) { \
		if(l == NULL) return NULL; \
		struct s_dlist_node_##i* n = l->tail; \
		while(n != NULL) { \
			if(dj_eq_##i(n->data, value)) return n; \
			n = n->prev; \
		}; \
		return NULL; \
	} \
	inline bool dlist_contains_##i(const dlist_##i* l, T value) { \
		return dlist_find_first_of_##i((dlist_##i*) l, value) != NULL; \
	} \
	inline dlistnode_##i* dlist_last_node_##i(dlist_##i* l){ \
		if(l == NULL) return NULL; \
		return l->tail; \
	} \
	T* dlist_last_##i(dlist_##i* l) { \
		dlistnode_##i* n = dlist_last_node_##i(l); \
		if(n == NULL) return NULL; \
		return &n->data; \
	} \
	inline dlistnode_##i* dlist_first_node_##i(dlist_##i* l) { \
		if(l == NULL) return NULL; \
		return l->head; \
	} \
	T* dlist_first_##i(dlist_##i* l) { \
		dlistnode_##i* n = dlist_first_node_##i(l); \
		if(n == NULL) return NULL; \
		return &n->data; \
	} \
	void dlist_foreach_##i(const dlist_##i* l, void(*func)(T*)) { \
		if(l == NULL  || func == NULL) return; \
		dlistnode_##i* n = l->head; \
		while(n) { \
			func(&n->data); \
			n = n->next; \
		} \
	}
