#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>
#include <float.h>

#include "png.h"
#include "logger.h"
#include "zlib.h"
#include "util.h"

static const unsigned char SIGNATURE[8] = {0x89, 0x50, 0x4e, 0x47, 0x0d, 0x0a, 0x1a, 0x0a};

#define big_endian(buf) (((buf)[0] << 24) | ((buf)[1] << 16) | ((buf)[2] << 8) | (buf)[3])

struct s_chunk {
	uint32_t length;
	uint32_t signature;
	uint32_t crc;
	unsigned char* data;
	// Some chunks are allowed multiple times, so we use a linked list here
	struct s_chunk* next;
};

enum e_compressions {DEFLATE};
enum e_chunktypes {IHDR, IDAT, IEND, tIME, tEXt, iTXt, gAMA, bKGD, iCCP, NUM_CHUNKS, /*, CGBI, iCCP, bKGD, pHYs, eXIf, iTXt*/};
uint32_t chunksignatures[NUM_CHUNKS] = {
	0x49484452, // IHDR
	0x49444154, // IDAT
	0x49454e44, // IEND
	0x74494d45, // tIme
	0x74455874, // tEXt
	0x69545874, // iTXt
	0x67414d41, // gAMA
	0x624b4744, // bKGD
	0x69434350, // iCCP
};
/*
0x43674249, // CGBI
0x70485973, // pHYs
0x65584966, // eXIf
*/

#define handler_asserts(chunk, id) do { \
	assert(chunk->signature == chunksignatures[id]); \
	assert(chunk->data != NULL); } while(0)

#define chunk_foreach(X) \
    X(IHDR)              \
    X(IDAT)              \
	X(tIME) \
	X(iCCP)                \
    X(tEXt)             \
    X(iTXt)             \
    X(gAMA)             \
    X(bKGD)              \

static void init_chunk(struct s_chunk* chunk, uint32_t length, uint32_t type, FILE* f);
static void init_buf_chunk(struct s_chunk* chunk, uint32_t length, uint32_t type, const unsigned char* buffer, size_t* ptr);

#define chunk_handle_sig(sig) static void handle_##sig(Image* img, struct s_chunk* chunk);
chunk_foreach(chunk_handle_sig)
#undef chunk_handle_sig

static void cleanup_chunks(struct s_chunk chunks[NUM_CHUNKS]);
void filter_0(Image* img, size_t decompressed_size, const unsigned char decompressed_data[decompressed_size]);

Image* parse_chunks(struct s_chunk chunks[NUM_CHUNKS])
{
	Image* img = calloc(1, sizeof(Image));
	img->gamma = DBL_MAX;
	img->bg_color.r = 0;
	img->bg_color.g = 0;
	img->bg_color.b = 0;
	img->iccp_profile.present = false;

    if(chunks[IHDR].signature != chunksignatures[IHDR]) {
        log_error("Invalid PNG. The IHDR chunk is missing");
        cleanup_chunks(chunks);
        free(img);
        return NULL;
    }
    if(chunks[IDAT].signature != chunksignatures[IDAT]) {
        log_error("Invalid PNG. The IDAT chunk is missing");
        cleanup_chunks(chunks);
        free(img);
        return NULL;
    }
#define handle_chunk(id) if(chunks[id].signature == chunksignatures[id]) { handle_##id(img, &chunks[id]); }
	chunk_foreach(handle_chunk)
#undef handle_chunk

	cleanup_chunks(chunks);

	return img;
}

static void init_chunks(struct s_chunk chunks[NUM_CHUNKS]) {
    for(int i=0; i<NUM_CHUNKS; i++) {
        chunks[i].signature = 0;
        chunks[i].length = 0;
        chunks[i].data = NULL;
        chunks[i].crc = 0;
        chunks[i].next = NULL;
    }
}

#define remaining(buf_len, ptr) ((buf_len)-(ptr))
Image* load_png_from_buf(size_t buf_len, const unsigned char buffer[buf_len])
{
    size_t ptr = 0;
    if(remaining(buf_len, ptr) < 8) {
        log_error("Corrupt file");
        return NULL;
    }
    if(strncmp((const char*) &buffer[ptr], (const char*) &SIGNATURE, 8)) {
        log_error("The PNG file in stream seems to be corrupted");
        return NULL;
    }
    ptr+= 8;

    struct s_chunk chunks[NUM_CHUNKS];
    init_chunks(chunks);

    while(chunks[IEND].signature != chunksignatures[IEND]) {
        uint32_t length = big_endian(&buffer[ptr]);
        ptr += 4;

        uint32_t sig = big_endian(&buffer[ptr]);
        ptr += 4;

        char sig_str[4] = {((const char*) &sig)[3], ((const char*) &sig)[2], ((const char*) &sig)[1], ((const char*) &sig)[0]};
        log_debug("Found chunk %.4s (%#04x) at %#lx", sig_str, sig, ptr);

        uint8_t i;
        for(i = 0; i<NUM_CHUNKS; i++) {
            if(chunksignatures[i] == sig ) {
                if(remaining(buf_len, ptr) < length) {
                    log_error("Not enough data left to read the chunk. %ld/%d left", remaining(buf_len, ptr), length);
                }
                init_buf_chunk(&chunks[i], length, sig, buffer, &ptr);
                break;
            }
        }
        if(i == NUM_CHUNKS) {
            char sig_str[4] = {((const char*) &sig)[3], ((const char*) &sig)[2], ((const char*) &sig)[1], ((const char*) &sig)[0]};
            log_warn("The chunk type \033[31m%.4s\033[0m (%#04x) is not supported", sig_str, sig);
            ptr += length += 4;
        }
    }

    return parse_chunks(chunks);
}

Image* load_image (const char* filepath) {

	FILE* f = fopen(filepath, "rb");
	if(!f) {
		log_error("Could not open file %s", filepath);
		return NULL;
	}
	unsigned char read_signature[8];
	size_t bytes_read UNUSED = fread(read_signature, 1, 8, f);
	assert(bytes_read == 8);
	if(strncmp((const char*) &read_signature, (const char*) &SIGNATURE, 8)) {
		log_error("The PNG file %s seems to be corrupted", filepath);
		fclose(f);
		return NULL;
	}

	struct s_chunk chunks[NUM_CHUNKS];
	init_chunks(chunks);

	while(chunks[IEND].signature != chunksignatures[IEND]) {
		unsigned char buf[4];
		
		fread(buf, 1, 4, f);
		uint32_t length = big_endian(buf);

		fread(buf, 1, 4, f);
		uint32_t sig = big_endian(buf);

		uint8_t i;
		for(i = 0; i<NUM_CHUNKS; i++) {
			if(chunksignatures[i] == sig ) {
				init_chunk(&chunks[i], length, sig, f);
				break;
			}
		}
		if(i == NUM_CHUNKS) {
			log_warn("The chunk type \033[31m%.4s\033[0m (%#04x) is not supported", buf, sig);
			fseek(f, length + 4, SEEK_CUR);
		}
	}

	fclose(f);
	return parse_chunks(chunks);
}

static void rek_del_texts(Textpair* t)
{
	if(t->next != NULL)
		rek_del_texts(t->next);
	free(t);
}

void free_image(Image *dest) {
	if(!dest) return;

	if(dest->data != NULL) {
		free(dest->data);
	}
	if(dest->texts != NULL) {
		rek_del_texts(dest->texts);
	}
	if(dest->iccp_profile.present) {
		free(dest->iccp_profile.data);
	}
	free(dest);
}

static void rek_del_chunk(struct s_chunk* t)
{
	if(t->next != NULL)
		rek_del_chunk(t->next);
	free(t);
}

static void cleanup_chunks(struct s_chunk chunks[NUM_CHUNKS]) {
	for(int i=0; i<NUM_CHUNKS; i++) {
		if(chunks[i].signature != 0) {
			if(chunks[i].next != NULL)
				rek_del_chunk(chunks[i].next);
			free(chunks[i].data);
		}
	}
}

static void init_chunk(struct s_chunk* chunk, uint32_t length, uint32_t sig, FILE* f) 
{
	// If the type is already set the chunk must have been accessed in the past before and therefore just want to append to it
	if(chunk->signature == chunksignatures[IDAT]) {
		chunk->data = realloc(chunk->data, chunk->length + length);
		if(fread(chunk->data+chunk->length, 1, length, f) != length) {
				log_error("Could not read the whole chunk size. Something must have gone wrong");
		}
		chunk->length += length;
	} else if(chunk->signature == chunksignatures[tEXt] || 
			  chunk->signature == chunksignatures[iTXt]) {
		struct s_chunk* last = chunk;
		while(last->next != NULL) last = last->next;
		struct s_chunk* chunk = malloc(sizeof(struct s_chunk) + length);
		last->next = chunk;

		chunk->signature = sig;
		chunk->length = length;
		chunk->data = (unsigned char*) &chunk[1];
		chunk->next = NULL;
		if(fread(chunk->data, 1, chunk->length, f) != chunk->length) {
				log_error("Could not read the whole chunk size. Something must have gone wrong");
		}
	} else if(chunk->signature != 0) {
		log_warn("Multiple chunks other than IDAT,tEXt and iTXt and of the same type are not supported. Skipping second chunk for %#04x", sig);
		fseek(f, length + 4, SEEK_CUR);
		return;
	} else {
		chunk->signature = sig;
		chunk->length = length;
		chunk->data = malloc(chunk->length);
		if(fread(chunk->data, 1, chunk->length, f) != chunk->length) {
				log_error("Could not read the whole chunk size. Something must have gone wrong");
		}
	}
	unsigned char buf[4];
	fread(buf, 1, 4, f);
	chunk->crc = big_endian(buf);
}

static void init_buf_chunk(struct s_chunk* chunk, uint32_t length, uint32_t sig, const unsigned char* buffer, size_t* ptr)
{
    // If the type is already set the chunk must have been accessed in the past before and therefore just want to append to it
    if(chunk->signature == chunksignatures[IDAT]) {
        chunk->data = realloc(chunk->data, chunk->length + length);
        memcpy(chunk->data+chunk->length, &buffer[*ptr], length);
        *ptr += length;
        chunk->length += length;
    } else if(chunk->signature == chunksignatures[tEXt] ||
              chunk->signature == chunksignatures[iTXt]) {
        struct s_chunk* last = chunk;
        while(last->next != NULL) last = last->next;
        struct s_chunk* chunk = malloc(sizeof(struct s_chunk) + length);
        last->next = chunk;

        chunk->signature = sig;
        chunk->length = length;
        chunk->data = (unsigned char*) &chunk[1];
        chunk->next = NULL;
        memcpy(chunk->data, &buffer[*ptr], chunk->length);
        *ptr += chunk->length;
    } else if(chunk->signature != 0) {
        log_warn("Multiple chunks other than IDAT,tEXt and iTXt and of the same type are not supported. Skipping second chunk for %#04x", sig);
        *ptr += length + 4;
        return;
    } else {
        chunk->signature = sig;
        chunk->length = length;
        chunk->data = malloc(chunk->length);

        memcpy(chunk->data, &buffer[*ptr], chunk->length);
        *ptr += chunk->length;
    }
    chunk->crc = big_endian(&buffer[*ptr]);
    *ptr += 4;
}

static void handle_IHDR(Image* img, struct s_chunk* chunk) {
	assert(chunk->signature == chunksignatures[IHDR]);
	assert(chunk->length == 13);
	img->width = big_endian(&chunk->data[0]);
	img->height = big_endian(&chunk->data[4]);
	img->bits_per_pixel = chunk->data[8];
	// TODO: Actually handle that because it has a weird format
	img->color_type = chunk->data[9];
	unsigned char compression_method = chunk->data[10];
	img->filter_method = chunk->data[11];
	unsigned char interlaced = chunk->data[12];
	if(compression_method != 0) {
		log_error("Received invalid compression value must be 0");
	}
	assert(img->filter_method == 0 && "No other filter method is currently supported");
	if(interlaced != 0) {
		log_warn("Interlacing of PNGs is not yet supported");
	}
	switch(img->color_type) {
		case(0): // Grayscale
			img->nr_channels = 1;
			break;
		case(2):
			img->nr_channels = 3;
			break;
		case(3):
			log_error("Indexed images are not yet supported");
			img->nr_channels = 0;
			break;
		case(4):
			img->nr_channels = 2;
			break;
		case(6):
			img->nr_channels = 4;
			break;
		default:
			log_error("Unknown IHDR color type %d", img->color_type);
			break;
	}

	log_debug("W: %d H: %d Channels: %d", img->width, img->height, img->nr_channels);
	img->data = calloc(img->width*img->height, img->nr_channels);
}

static void handle_tIME(Image* img, struct s_chunk* chunk) {
	assert(chunk->signature == chunksignatures[tIME]);
	assert(chunk->data != NULL);
	assert(chunk->length == 7);
	struct tm stamp;
	stamp.tm_year = chunk->data[0] * 100 + chunk->data[1];
	stamp.tm_mon = chunk->data[2];
	stamp.tm_mday = chunk->data[3];
	stamp.tm_hour = chunk->data[4];
	stamp.tm_min = chunk->data[5];
	stamp.tm_sec = chunk->data[6];
	img->modification_time = mktime(&stamp);
}

static void handle_IDAT(Image* img, struct s_chunk* chunk) {
	handler_asserts(chunk, IDAT);

	size_t decompressed_size = 0;
	unsigned char* decompressed = zlib_decode(chunk->data, chunk->length, &decompressed_size);

	//size_t allocated_size = img->width * img->height * img->nr_channels;
	//log_debug("Decompressed: %zu Allocated: %zu", decompressed_size, allocated_size);
	assert(decompressed_size <= allocated_size);

	if(img->filter_method != 0) {
		log_error("Only the filter method 0 is currently defined and implemented");
	} else {
		filter_0(img, decompressed_size, decompressed);
	}
	free(decompressed);
}

static void handle_tEXt(Image* img, struct s_chunk* chunk) 
{
	handler_asserts(chunk, tEXt);
	Textpair* txt = malloc(sizeof(Textpair) + chunk->length + 1);
    ASSERT_NONNULL(txt);
	txt->next = NULL;
	txt->key = (char*) &txt[1];
	strcpy((char*) txt->key, (char*) chunk->data);
	size_t keylen = strlen(txt->key);
	size_t datalen = chunk->length - keylen - 1;
	txt->data = (char*) txt->key + keylen + 1;
	memcpy((char*) txt->data, (char*) chunk->data + keylen +1, datalen);
	((char*) txt->data)[datalen] = '\0';

	if(img->texts == NULL) {
		img->texts = txt;
	} else {
		Textpair* p = img->texts;
		while(p->next != NULL) p = p->next;
		p->next = txt;
	}
	if(chunk->next != NULL)
		handle_tEXt(img, chunk->next);
}

static void handle_iTXt(Image* img, struct s_chunk* chunk)
{
	handler_asserts(chunk, iTXt);

	unsigned char* stream_ptr = chunk->data;
	char* kw_begin = (char*) stream_ptr;
	size_t kw_len = 0;
	while(*stream_ptr != '\0') {
		kw_len++;
		stream_ptr++;
	} stream_ptr++;
	unsigned char compression_flag = *stream_ptr;
	stream_ptr++;
	unsigned char compression_method = *stream_ptr;

	stream_ptr++;
	char* lang = (char*) stream_ptr;
	while(*stream_ptr != '\0') { // Read until the end of language tag
		stream_ptr++;
	} stream_ptr++;
	char* translated_kw = (char*) stream_ptr;
	while(*stream_ptr != '\0') { // Read until the end of translated keyword
		stream_ptr++;
	} stream_ptr++;
	char* txt_begin = (char*) stream_ptr;
	size_t txt_len = (chunk->data + chunk->length - (unsigned char*) txt_begin);

	if(compression_flag == 1) {
		log_warn("Cannot decompress compressed iTXt chunks skipping...");
		return;
	}
	(void) compression_method;
	(void) lang;
	(void) translated_kw;

	Textpair* txt = malloc(sizeof(Textpair) + kw_len + txt_len + 2);
    ASSERT_NONNULL(txt);
	txt->next = NULL;
	txt->key = (char*) &txt[1];
	txt->data = txt->key + kw_len + 1;
	memcpy((char*) txt->key, kw_begin, kw_len);
	memcpy((char*) txt->data, txt_begin, txt_len);
	((char*) txt->key)[kw_len] = '\0';
	((char*) txt->data)[txt_len] = '\0';

	if(img->texts == NULL) {
		img->texts = txt;
	} else {
		Textpair* p = img->texts;
		while(p->next != NULL) p = p->next;
		p->next = txt;
	}
	if(chunk->next != NULL)
		handle_iTXt(img, chunk->next);
}

void handle_gAMA(Image* img, struct s_chunk* chunk)
{
	handler_asserts(chunk, gAMA);
	assert(chunk->length == 4);
	uint32_t g = ((uint32_t*) chunk->data)[0];
	img->gamma = g;
	img->gamma /= 100000.0f;
}

void handle_iCCP(Image* img, struct s_chunk* chunk)
{
    handler_asserts(chunk, iCCP);
    img->iccp_profile.present = true;
    img->iccp_profile.profile_name[79] = '\0';
    strcpy(img->iccp_profile.profile_name, (const char*) chunk->data);
    size_t name_len = strlen(img->iccp_profile.profile_name);
#ifndef NDEBUG
    char compression_mode = *(chunk->data + name_len + 1);
    assert(compression_mode == 0 && "Only compression mode 0 is in spec");
#endif
    size_t profile_len;
    img->iccp_profile.data = zlib_decode(chunk->data+name_len+2, chunk->length-(name_len+2), &profile_len);
    log_debug("Using ICC Profile %s with length %lu", img->iccp_profile.profile_name, profile_len);
}

void handle_bKGD(Image* img, struct s_chunk* chunk)
{
	handler_asserts(chunk, bKGD);
	switch(img->color_type) {
		case(3): {
			assert(chunk->length == 1);
			uint8_t c = ((uint8_t*) chunk)[0];
			img->bg_color.r = c;
			img->bg_color.g = c;
			img->bg_color.b = c; }
		case(0):
		case(4): {
			assert(chunk->length == 2);
			uint16_t c = ((uint16_t*) chunk)[0];
			img->bg_color.r = c;
			img->bg_color.g = c;
			img->bg_color.b = c;
			break; }
		case(2):
		case(6):
			assert(chunk->length == 6);
			uint16_t* c = (uint16_t*) chunk;
			img->bg_color.r = c[0];
			img->bg_color.g = c[1];
			img->bg_color.b = c[2];
			break;
		default:
			ASSERT_NOT_REACHED
	}
	log_debug("The background color is [%hu, %hu, %hu]", img->bg_color.r, img->bg_color.g, img->bg_color.b);
}

ImageColor get_color(Image* img, uint32_t x, uint32_t y)
{
	if(x > img->width || y > img->height) return (ImageColor) {0,0,0};
	ImageColor c;
	assert(img->bits_per_pixel == 8);
	assert(img->nr_channels == 3);

	uint64_t start = (x + y * img->height) * img->nr_channels;
	if(img->nr_channels == 3) {
		c.r = img->data[start];
		c.g = img->data[start+1];
		c.b = img->data[start+2];
		c.a = img->data[start+3];
	} else {
		c.r = c.g = c.b = c.a = 0;
	}

	return c;
}
