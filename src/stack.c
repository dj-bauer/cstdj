#include "stack.h"

#define CREATE_STACK_IMPL(i, T, fmt) \
	inline Stack_##i* create_stack_##i(void) {return create_vector_##i(); } \
	inline void init_stack_##i(Stack_##i* s) { init_vector_##i(s); } \
	inline void deinit_stack_##i(Stack_##i* s) { deinit_vector_##i(s); } \
	inline void destroy_stack_##i(Stack_##i** s_ptr) { destroy_vector_##i(*s_ptr); } \
	inline void stack_push_##i(Stack_##i* s, T value) { vector_append_##i(s, value); } \
	inline void stack_pop_##i(Stack_##i* s) { vector_remove_last_##i(s); } \
	inline T* stack_top_##i(Stack_##i* s) { return vector_last_##i(s); } \
	inline size_t stack_size_##i(const Stack_##i* stack){ return vector_size_##i(stack); } \
	inline bool stack_empty_##i(const Stack_##i* stack) { return stack_size_##i(stack) == 0; }


CREATE_STACK_IMPL(i, int, "%d")
CREATE_STACK_IMPL(f, float, "%f")
CREATE_STACK_IMPL(d, double, "%f")
CREATE_STACK_IMPL(v, void*, "%p")
CREATE_STACK_IMPL(c, char, "%c")
CREATE_STACK_IMPL(s, char*, "%d")
CREATE_STACK_IMPL(cs, const char*, "%d")
