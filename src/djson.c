#include "util.h"
#include "logger.h"
#include "stack.h"
#include "internal/json_int.h"

#include <string.h>
#include <stdlib.h>

bool is_int(double x) {
	int n = x;
	return n-x == 0;
}

JType json_type(const JNode* node) {
	if(json_is_true_type(node, JRoot)) {
		node = node->data.root.first_node;
	}
	return json_true_type(node);
}

JType json_true_type(const JNode* node)
{
	if(node == NULL) 
		return JNull;
	return node->type;
}

bool json_is_true_type(const JNode* node, JType type)
{
	return json_true_type(node) == type;
}
bool json_is_type(const JNode* node, JType type)
{
	return json_type(node) == type;
}

size_t json_size(const JNode* node)
{
	if(json_is_true_type(node, JRoot))
		node = node->data.root.first_node;
	switch(json_type(node)) {
		case JObject:
			return vector_size_v(&node->data.obj.nodes);
		case JArray:
			return vector_size_v(&node->data.arr.elements);
		default: break;
	}
	return 0;
}

JNode* json_obj_get(JNode* obj, const char* key)
{
	if(json_is_true_type(obj, JRoot))
		obj = obj->data.root.first_node;
	assert(json_is_true_type(obj, JObject));
	Vector_s* names = &obj->data.obj.names;
	Vector_v* nodes = &obj->data.obj.nodes;
	size_t num_names = vector_size_s(names);
	for(size_t i=0; i<num_names; i++) {
		const char* name = *vector_at_s(names, i);
		if(!strcmp(name, key)) {
			JNode* node = (JNode*) *vector_at_v(nodes, i);
			return node;
		}
	}
	return NULL;
}

JNode* json_arr_get(JNode* obj, size_t index)
{
	if(json_is_true_type(obj, JRoot))
		obj = obj ->data.root.first_node;
	assert(json_is_true_type(obj, JArray));
	Vector_v* nodes = &obj->data.arr.elements;
	if(index >= vector_size_v(nodes))
		return NULL;
	return (JNode*) *vector_at_v(nodes, index);
}

const char* json_get_str(const JNode* str)
{
	if(!str) return NULL;
	if(json_is_true_type(str, JRoot))
		str = str->data.root.first_node;
	assert(json_is_true_type(str, JString));
	return str->data.str;
}
bool json_get_bool(const JNode* bo)
{
    if(!bo) return false;
    if(json_is_true_type(bo, JRoot))
        bo = bo->data.root.first_node;
    assert(json_is_true_type(bo, JBool));
    return bo->data.boolean;
}
double json_get_num(const JNode* num)
{
    if(!num) return 0;
	if(json_is_true_type(num, JRoot))
		num = num->data.root.first_node;
    assert(json_is_true_type(num, JNumber));
    return num->data.num;
}
int json_get_int(const JNode* num)
{
	return (int) json_get_num(num);
}
long json_get_long(const JNode* num)
{
	return (long) json_get_num(num);
}

float json_get_float(const JNode* num)
{
	return (float) json_get_num(num);
}

void node_destructor(size_t index, void** node_ptr)
{
	JNode* node = (JNode*) *node_ptr;
	JType type = json_true_type(node);
	if(type == JObject) {
		vector_foreach_v(&node->data.obj.nodes, node_destructor);
		deinit_vector_v(&node->data.obj.nodes);
		deinit_vector_s(&node->data.obj.names);
	} else if (type == JArray) {
		vector_foreach_v(&node->data.arr.elements, node_destructor);
		deinit_vector_v(&node->data.arr.elements);
	} else if (type == JRoot) {
		node_destructor(0, (void**) &node->data.root.first_node);
		deinit_memarena(&node->data.root.storage);
	}
}
void json_free(JNode* root)
{
	if(root == NULL)
		return;
	if(root->type != JRoot) {
		log_error("Tried to free a non-root node");
		return;
	}
	node_destructor(0, (void**) &root);
	free(root);
}

static void json_indent(int indent) {
    for(int i=0; i<indent; i++)
        printf("\t");
}
static void json_print_rek(FILE* out, JNode* root, int indent)
{
    switch(json_type(root)) {
        case JNull:
            fprintf(out, "null");
            break;
        case JNumber: {
			double x = json_get_num(root);
			if(is_int(x))
				fprintf(out, "%d", (int)x);
			else
				fprintf(out, "%f", x);
            break; }
        case JString:
            fprintf(out, "\"%s\"", json_get_str(root));
            break;
        case JBool:
            fprintf(out, "%s", json_get_bool(root) ? "true" : "false");
            break;
        case JArray: {
            fprintf(out, "[\n");
            size_t item_count = json_size(root);
            for (size_t i = 0; i < item_count; i++) {
                json_indent(indent+1);
                json_print_rek(out, json_arr_get(root, i), indent + 1);
                printf(",\n");
            }
			json_indent(indent);
            fprintf(out, "]");
            break;
        } case JObject: {
            fprintf(out, "{\n");
            size_t prop_count = json_size(root);
            for (size_t i = 0; i < prop_count; i++) {
                const char* key = json_obj_key(root, i);
                json_indent(indent+1);
                printf("%s: ", key);
                json_print_rek(out, json_obj_get(root, key), indent + 1);
                fprintf(out, ",\n");
            }
			json_indent(indent);
            fprintf(out, "}");
            break;
        } default:
            ASSERT_NOT_REACHED;
    }
}

void json_print(JNode* root)
{
    json_print_rek(stdout, root, 0);
}

const char* json_obj_key(const JNode* node, size_t i)
{
    if(json_is_true_type(node, JRoot))
        node = node->data.root.first_node;
    assert(json_is_true_type(node, JObject));
    return *vector_at_s((Vector_s*) &node->data.obj.names, i);
}

int json_obj_int(const JNode* obj, const char* key)
{
	const JNode* prop = json_obj_get((JNode*) obj, key);
	return json_get_int(prop);
}
