#include "osmtypes.h"

#define X(en, str) [en] = str,

const char* osmtypes_names[UnknownType+1] = {
	OSMTYPES_FOREACH(X)
	[EmptyType] = "empty",
	[UnknownType] = "unknown"
};
#undef X

const char* osmtype_str(enum e_osmtype t)
{
	if(t > UnknownType) return "Error";
	return osmtypes_names[t];
}

const char* osmnature_str(enum e_natural_subtypes t) 
{
	if(t > UnclassifiedNatural) return "Error";
	const char* str[UnclassifiedNatural+1] = {
#define X(en, str, c) [en] = str,
	OSMNATURE_FOREACH(X)
#undef X
	};
	return str[t];
}

const char* osmlanduse_str(enum e_landuse_subtypes t)
{
	if(t > UnknownLanduse) return "Error";
	if(t == UnknownLanduse) return "unknown";
	const char* str[UnknownLanduse+1] = {
#define X(en, str, c) [en] = str,
	OSMLANDUSE_FOREACH(X)
#undef X
	};
	return str[t];
}

const char* osmleisure_str(enum e_leisure_subtypes t)
{
	if(t > UnknownLeisure) return "Error";
	if(t == UnknownLeisure) return "unknown";
	const char* str[UnknownLeisure+1] = {
#define X(en, str, c) [en] = str,
	OSMLEISURE_FOREACH(X)
#undef X
	};
	return str[t];
}
