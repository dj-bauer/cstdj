/*
    Copyright (C) 2023 Daniel Bauer

    This program is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation, either version 3 of the License,
    or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty
    of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "graph.h"
#include "vector_impl.h"
#include "heap.h"
#include "union_find.h"
#include "util.h"
#include "dlist.h"
#include "logger.h"

#include <float.h>
#include <stdio.h>

VECTOR_IMPL(nodes, AdjNode)
VECTOR_IMPL(edges, AdjEdge)

bool _dfs_visit(const AdjGraph* g, AdjNode* n)
    NONNULL(1,2);

int graph_init(AdjGraph* g, bool is_directed)
{
	if(g == NULL) return 1;
	init_vector_nodes(&g->nodes);
	init_vector_edges(&g->edges);
	g->directed = is_directed;
	g->next_node_id = 0;
	return 0;
}

int graph_cleanup(AdjGraph* g)
{
	if(g == NULL) return 1;
	size_t num_nodes = graph_node_count(g);
	for(size_t i=0; i<num_nodes; i++) {
		AdjNode* n = vector_at_nodes(&g->nodes, i);
		deinit_vector_l(&n->edges);
	}
	deinit_vector_nodes(&g->nodes);
	deinit_vector_edges(&g->edges);

	return 0;
}
AdjNode* graph_add_node(AdjGraph* g, void* data)
{
	if(g == NULL) return NULL;
	AdjNode template = (AdjNode) {
		.id = g->next_node_id++,
		.index = vector_size_nodes(&g->nodes),
		.data = data,
		.highlight = false
	};
	init_vector_l(&template.edges);
	return vector_append_nodes(&g->nodes, template);
}

inline static uint64_t calc_edge_id(uint32_t a, uint32_t b, bool directed)
{
	if(a > b && !directed) {
		uint32_t t = a;
		a = b;
		b = t;
	}
	return (((uint64_t) a) << 32) | ((uint32_t) b);
}

bool graph_connection_exists(const AdjGraph* g, const AdjNode* a_c, const AdjNode* b)
{
	AdjNode* a = (AdjNode*) a_c;
	for(size_t i=0; i<graph_node_edge_count(a); i++) {
		const AdjEdge* e = graph_edge_c(g, *vector_at_l(&a->edges, i));
        ASSERT_NONNULL(e);
		if(e->start == a->id && e->end == b->id)
			return true;
	}
	return false;
	//uint64_t id = calc_edge_id(a->id, b->id, g->directed);
	//return graph_edge_c(g, id) != NULL;
}

AdjEdge* graph_add_edge(AdjGraph* g, AdjNode* a, AdjNode* b,
		double weight, void* data) {
	uint64_t id = calc_edge_id(a->id, b->id, g->directed);
	AdjEdge* new_edge;
	if(!g->directed) {
		assert(!graph_connection_exists(g, a, b));
		// TODO: Flip and and b if a > b
		AdjEdge template = (AdjEdge) {
			.id = id,
			.start = a->index,
			.end = b->index,
			.highlight = false,
			.weight = weight,
			.data = data,
			.index = vector_size_edges(&g->edges)
		};
		new_edge = vector_append_edges(&g->edges, template);
		vector_append_l(&a->edges, new_edge->index);
		vector_append_l(&b->edges, new_edge->index);
	} else{

		assert(!graph_connection_exists(g, a, b));
		AdjEdge template = (AdjEdge) {
			.id = id,
			.start = a->index,
			.end = b->index,
			.highlight = false,
			.weight = weight,
			.data = data,
			.index = vector_size_edges(&g->edges)
		};
		new_edge = vector_append_edges(&g->edges, template);
		vector_append_l(&a->edges, new_edge->index);
	}
	return new_edge;
}

void print_graph(const AdjGraph* g, 
		char* buf, size_t buf_len, gnode_print_func print_func)
{
	if(buf_len < 12)
		return;

	if(g->directed) {
		strcpy(buf, "digraph {\n");
	} else {
		strcpy(buf, "graph {\n");
	}
	size_t buf_used = strlen(buf);

	char content_str[128];
	char orig_str[32];
	char dest_str[32];
	for(size_t i=0; i<graph_node_count(g); i++) {
		const AdjNode* n = graph_node_c(g, i);
        ASSERT_NONNULL(n);
		print_func(n->data, orig_str);
		sprintf(content_str, "\t\"%s\" [color=%s]\n",
				orig_str, n->highlight ? "red" : "black");
		buf_used += strlen(content_str);
		if(buf_used >= buf_len) {
			log_error("Buffer overflow in printing graph. String buffer allocated to small");
			assert(buf_used < buf_len);
		}
		strcat(buf, content_str);
	}
	for(size_t i=0; i<graph_edge_count(g); i++) {
		const AdjEdge* edge = graph_edge_c(g, i);
        ASSERT_NONNULL(edge);
		const AdjNode* o = graph_node_c(g, edge->start);
		const AdjNode* d = graph_node_c(g, edge->end);
        ASSERT_NONNULL(o);
        ASSERT_NONNULL(d);
		print_func(o->data, orig_str);
		print_func(d->data, dest_str);

		if(g->directed) {
			sprintf(content_str, "\t\"%s\" -> \"%s\" [label=\"%g\", color=%s, direction=%s]\n",
					orig_str, dest_str, edge->weight,
					edge->highlight ? "blue" : "black",
					// This used to say double edge. We could run the edge case (haha pun not intended)
					// and check if the back and forth edge has the same weight
					false ? "none" : "forward");
		} else {
			sprintf(content_str, "\t\"%s\" -- \"%s\" [label=\"%g\", color=%s]\n",
					orig_str, dest_str, edge->weight,
					edge->highlight ? "blue" : "black");
		}
		buf_used += strlen(content_str);
		assert(buf_used < buf_len);
		strcat(buf, content_str);
	}
	strcat(buf, "}");
}

AdjNode* graph_get_node(AdjGraph* g, void* v) 
{
	for(size_t i=0; i<graph_node_count(g); i++) {
		AdjNode* n = vector_at_nodes(&g->nodes, i);
        assert(n != NULL);
		if(n->data == v) 
			return n;
	}
	return NULL;
}

inline AdjNode* graph_node(AdjGraph* g, long index)
{
	return vector_at_nodes(&g->nodes, index);
}
inline const AdjNode* graph_node_c(const AdjGraph* g, long node_id) 
{
	return vector_at_nodes((Vector_nodes*) &g->nodes, node_id);
}
inline AdjEdge* graph_edge(AdjGraph* g, long index)
{
	return vector_at_edges(&g->edges, index);
}
inline const AdjEdge* graph_edge_c(const AdjGraph* g, long edge_index)
{
	return vector_at_edges((Vector_edges*) &g->edges, edge_index);
}

inline AdjEdge* graph_add_edge_i(AdjGraph* g, long a, long b,
		double weight, void* data)
{
    AdjNode* an = graph_node(g, a);
    AdjNode* bn = graph_node(g, b);
    ASSERT_NONNULL(an);
    ASSERT_NONNULL(bn);
	return graph_add_edge(g, an, bn, weight, data);
}

static int cmp_edges_by_prim_cost(const void** a_ptr, const void** b_ptr) {
	const AdjEdge* a = (const AdjEdge*) *a_ptr;
	const AdjEdge* b = (const AdjEdge*) *b_ptr;

	double diff = a->weight - b->weight;
	if(diff > 0) return 1;
	else if(diff == 0) return 0;
	else return -1;
}

void _unhighlight_node(size_t index, AdjNode* node) {
    graph_highlight_node(node, false);
}
void _unhighlight_edge(size_t index, AdjEdge * edge) {
    graph_highlight_edge(edge, false);
}
void _mark_white(size_t index, AdjNode* node) {
    node->algorithms.dfs.color = DFS_WHITE;
}

int graph_prim_spanntree(AdjGraph* g)
{
	heap_v pq;
	heap_init_v(&pq, cmp_edges_by_prim_cost);

    vector_foreach_nodes(&g->nodes, _unhighlight_node);
    vector_foreach_edges(&g->edges, _unhighlight_edge);

    size_t components = 0;
    size_t nodes_highlighted = 0;
	while(nodes_highlighted < graph_node_count(g)) {
		// Add node for the first component
		components++;
		for(size_t i=0; i<graph_node_count(g); i++) {
			AdjNode* root = graph_node(g, i);
            ASSERT_NONNULL(root);
			if(!root->highlight) {
				nodes_highlighted++;
				root->highlight = true;
				root->algorithms.prim.parent = -1;
				root->algorithms.prim.cost = 0;
				for(int j=0; j<vector_size_l(&root->edges); j++) {
					AdjEdge* e = graph_edge(g, j);
					heap_insert_v(&pq, e);
				}
			}
		}

		while(!heap_is_empty(&pq)) {
			AdjEdge* edge = (AdjEdge*) *heap_front_v(&pq);
			heap_pop_v(&pq);

			AdjNode* dest = graph_node(g, edge->end);
            ASSERT_NONNULL(dest);
			if(dest->highlight == true) continue;

			AdjNode* orig = graph_node(g, edge->start);
            ASSERT_NONNULL(orig);
			assert(orig->highlight == true);
			dest->highlight = true;
			nodes_highlighted++;
			dest->algorithms.prim.cost = orig->algorithms.prim.cost + edge->weight;
			dest->algorithms.prim.parent = orig->index;

			assert(orig->algorithms.prim.parent != dest->index);
			graph_highlight_edge(edge, true);

			for(size_t i=0; i<vector_size_l(&dest->edges); i++) {
				AdjEdge* e = graph_edge(g, i);
                assert(e != NULL);
				AdjNode* new_node = graph_node(g, e->end);
                assert(new_node != NULL);
				if(!new_node->highlight) {
					heap_insert_v(&pq, e);
				}
			}
		}
	}
	assert(pq.size == 0);
	heap_cleanup_v(&pq);

	return components;
}

int graph_kruskal_spanntree(AdjGraph* g)
{
	assert(!g->directed);
	heap_v pq;
	heap_init_v(&pq, cmp_edges_by_prim_cost);
	// We know that the maximum number of nodes is the next node id
	UnionFind* uf = unionfind_create(g->next_node_id);
	size_t num_components = graph_node_count(g);

    vector_foreach_nodes(&g->nodes, _unhighlight_node);
    vector_foreach_edges(&g->edges, _unhighlight_edge);

    // Add all edges to priority queue
    for(size_t i=0; i<graph_edge_count(g); i++) {
        heap_insert_v(&pq, graph_edge(g, i));
    }

	while(!heap_is_empty(&pq)) {
		AdjEdge* e = (AdjEdge*) *heap_front_v(&pq);
		heap_pop_v(&pq);
		
		AdjNode* a = graph_node(g, e->start);
		AdjNode* b = graph_node(g, e->end);
        assert(a != NULL && b != NULL);
		if(!unionfind_check(uf, a->id, b->id)) {
			e->highlight = true;
			a->highlight = true;
			b->highlight = true;
			unionfind_connect(uf, a->id, b->id);
			num_components--;
			assert(num_components > 0);
		}
	}

	heap_cleanup_v(&pq);
	unionfind_destroy(uf);

	return num_components;
}

int dijkstra_cmp_nodes(const void** a_ptr, const void** b_ptr)
{
	const AdjNode* a = (const AdjNode*) *a_ptr;
	const AdjNode* b = (const AdjNode*) *b_ptr;
	double diff = a->algorithms.dijkstra.distance - b->algorithms.dijkstra.distance;
	if(diff > 0) {
		return 1;
	} else if (diff == 0) {
		return 0;
	} else {
		return -1;
	}
}

bool graph_dijkstra_to(AdjGraph* g, AdjNode* start, AdjNode* dest)
{
	assert(g != NULL);
	assert(start != NULL);
	heap_v V;
	heap_init_v(&V, dijkstra_cmp_nodes);
	for(size_t i=0; i<graph_node_count(g); i++) {
		AdjNode* n = vector_at_nodes(&g->nodes, i);
		n->algorithms.dijkstra.parent = NULL;
		n->algorithms.dijkstra.distance = DBL_MAX;
	}
	start->algorithms.dijkstra.distance = 0;
	heap_insert_v(&V, start);
	bool first_it = true;
	while(true) {
		const void** front = heap_front_v(&V);
		if(front == NULL) {
			heap_cleanup_v(&V);
			// The case where our queue empties out. 
			// This means we either calculate everything or didn't find a way
			return dest == NULL;
		}
		AdjNode* u = (AdjNode*) *front;
		heap_pop_v(&V);
		if(first_it) {
			assert(u == start);
			first_it = false;
		}
		// Either We don't have any more v∈ V\S (u = NULL)
		// or we are handling the node we are wanting
		if(u == dest) {
			// We found everything we needed to
			heap_cleanup_v(&V);
			return true;
		}

		for(size_t i=0; i<graph_node_edge_count(u); i++) {
			AdjEdge* e = graph_edge(g, *vector_at_l(&u->edges, i));
            assert(e != NULL);
			assert(g->directed);
			AdjNode* v = graph_node(g, e->end);
			double new_d = u->algorithms.dijkstra.distance + e->weight;
			double old_d = v->algorithms.dijkstra.distance;
			if(old_d > new_d) {
				v->algorithms.dijkstra.parent = u;
				v->algorithms.dijkstra.distance = new_d;
				// We can skip cycling through all the queued up nodes if the node hasn't been added yet
				if(old_d == DBL_MAX) {
					heap_insert_v(&V, v);
				} else {
					heap_change_v(&V, v, true);
				}
			}
		}
	}
	ASSERT_NOT_REACHED;
}
inline bool graph_dijkstra(AdjGraph* g, AdjNode* start)
{
	return graph_dijkstra_to(g, start, NULL);
}

void graph_retrace_dijkstra(dlist_l* edges, double* weight, const AdjGraph* g, const AdjNode* dest)
{
	assert(edges != NULL);
	if(weight != NULL)
		*weight = dest->algorithms.dijkstra.distance;
	dlist_clear_l(edges);
	while(dest->algorithms.dijkstra.parent != NULL) {
		AdjNode* parent = dest->algorithms.dijkstra.parent;
		const AdjEdge* e = NULL;
		for(size_t i=0; i<graph_node_edge_count(parent); i++) {
			e = graph_edge_c(g, *vector_at_l(&parent->edges, i));
            assert(e != NULL);
			if(e->end == dest->id)
				break;
		}
		assert(e != NULL && e->end == dest->id);
		dlist_prepend_l(edges, e->index);
		dest = parent;
	}
}

// Returns whether we detected a B-Edge
bool _dfs_visit(const AdjGraph* g, AdjNode* n)
{
	n->algorithms.dfs.color = DFS_GRAY;
	for(size_t i=0; i<graph_node_edge_count(n); i++) {
		const AdjEdge* e = graph_edge((AdjGraph*) g, *vector_at_l(&n->edges, i));
        assert(e != NULL);
		AdjNode* v = graph_node((AdjGraph*) g, e->end);
        assert(v != NULL);
		if (v->algorithms.dfs.color == DFS_WHITE) {
			if(_dfs_visit(g, v)) return true;
		} else if (v->algorithms.dfs.color == DFS_BLACK) {
			return true;
		}
	}
	n->algorithms.dfs.color = DFS_BLACK;
	return false;
}

bool graph_has_circle_undirected(const AdjGraph* g)
{
	assert(!g->directed);
    vector_foreach_nodes((Vector_nodes*) &g->nodes, _mark_white);
	
	for(size_t i=0; i<graph_node_count(g); i++) 
	{
		AdjNode* n = vector_at_nodes((Vector_nodes*) &g->nodes, i);
        assert(n != NULL && "We are just accessing the vector at its valid indices");
		if(n->algorithms.dfs.color == DFS_WHITE) {
			if(_dfs_visit(g, n)) return true;
		}
	}

	return false;
}
