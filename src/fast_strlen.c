#include "fast_strlen.h"

inline bool strlen_gt(const char* str, int cmp) {
	while(*str != '\0') {
		if(cmp == 0) return true;
		cmp--;
		str++;
	}
	return false;
}
inline bool strlen_ge(const char* str, int cmp) {
	while(*str != '\0') {
		if(cmp == 0) return true;
		cmp--;
		str++;
	}
	return cmp == 0;
}
