#include <inttypes.h>
#include <stdlib.h>
#include <stdio.h>
#include "queue.h"
#include "util.h"

#define QUEUE_IMPL(i, T)                              \
                                                      \
	void queue_init_##i(queue_##i* q) {               \
		q->head = NULL;                               \
	 	q->tail = NULL;                               \
	}                                                 \
	void queue_cleanup_##i(queue_##i* q) {            \
		struct s_queue_node_##i* node = q->head;      \
		while(node) {                                 \
			struct s_queue_node_##i* to_delete = node;\
			node = node->next;                        \
			free(to_delete);                          \
		}                                             \
    }                                                 \
                                                      \
	queue_##i* queue_create_##i(void) {               \
		queue_##i* q = malloc(sizeof(*q));            \
        ASSERT_NONNULL(q);                            \
		q->head = NULL;                               \
		q->tail = NULL;                               \
		return q;                                     \
	}                                                 \
                                                      \
	void queue_destroy_##i(queue_##i* q) {            \
		if(q == NULL) return;                         \
        queue_cleanup_##i(q);                         \
		free(q);                                      \
	}                                                 \
                                                      \
	void queue_enq_##i(queue_##i *q, T value) {       \
		struct s_queue_node_##i* node = malloc(sizeof(*node)); \
        ASSERT_NONNULL(node);                                              \
		node->value = value;                          \
		node->next = NULL;                            \
													  \
		if(q->head == NULL) {                         \
			q->head = node;                           \
		} else {                                      \
			q->tail->next = node;                     \
		}                                             \
		q->tail = node;                               \
	}                                                 \
													  \
	T* queue_front_##i(const queue_##i* q) {          \
		if(!q->head) return NULL; /* Empty queue */   \
		return &q->head->value;                       \
	}                                                 \
													  \
	void queue_deq_##i(queue_##i* q) {                \
		if(!q->head) return;                          \
													  \
		struct s_queue_node_##i* old_head = q->head;  \
		q->head = old_head->next;                     \
		/* Check if we're removing the last item */   \
		if(!q->head) {                                \
			q->tail = q->head;                        \
		}                                             \
		free(old_head);                               \
	}                                                 \
                                                      \
	bool queue_empty_##i(const queue_##i* q) {        \
		return q->head == NULL;                       \
	}

#define QUEUE_IMPL_PRINT(i, T, fmt)                   \
	void queue_print_##i(const queue_##i* q) {        \
		if(!q->head) {                                \
			printf("Empty queue\n");                  \
			return;                                   \
		}                                             \
		bool start = true;                            \
		struct s_queue_node_##i* node = q->head;      \
		while(node) {                                 \
			if(!start) {                              \
				printf("->");                         \
			}                                         \
			printf("["fmt"]", node->value);           \
			start = false;                            \
			node = node->next;                        \
		}                                             \
		printf("\n");                                 \
	}                                                 \

#define QUEUE_IMPL_FULL(i, T, fmt) \
	QUEUE_IMPL(i, T) \
	QUEUE_IMPL_PRINT(i, T, fmt)

