#include "audio.h"
#include <alsa/asoundlib.h>
#include <alsa/pcm.h>

struct s_audio_connection {
	snd_pcm_t* pcm;
};

AudioDevice* audio_open(unsigned int samplerate, unsigned int num_channels, enum e_signdness sign, enum e_samplesize bytes, enum e_endianess endian)
{
	AudioDevice* a = malloc(sizeof(AudioDevice));
	if(a == NULL) return NULL;

	snd_pcm_open(&a->pcm, "default", SND_PCM_STREAM_PLAYBACK, 0);

	snd_pcm_hw_params_t* hw_params;
	snd_pcm_hw_params_malloc(&hw_params);

	snd_pcm_format_t fmt = SND_PCM_FORMAT_U8;
	switch(sign) {
		case(Unsigned):
			switch(bytes) {
				case(Bits8):
					fmt = SND_PCM_FORMAT_U8; break;
				case(Bits16):
					switch(endian) {
						case(Littleendian):
							fmt = SND_PCM_FORMAT_U16_LE; break;
						case(Bigendian):
							fmt = SND_PCM_FORMAT_U16_BE; break;
						default: assert(0);
					} break;
				case(Bits32):
					switch(endian) {
						case(Littleendian):
							fmt = SND_PCM_FORMAT_U32_LE; break;
						case(Bigendian):
							fmt = SND_PCM_FORMAT_U32_BE; break;
						default: assert(0);
					} break;
				default: assert(0);
			} break;
		case(Signed): {
			switch(bytes) {
				case(Bits8):
					fmt = SND_PCM_FORMAT_S8; break;
				case(Bits16):
					switch(endian) {
						case(Littleendian):
							fmt = SND_PCM_FORMAT_S16_LE; break;
						case(Bigendian):
							fmt = SND_PCM_FORMAT_S16_BE; break;
						default: assert(0);
					} break;
				case(Bits32):
					switch(endian) {
						case(Littleendian):
							fmt = SND_PCM_FORMAT_S32_LE; break;
						case(Bigendian):
							fmt = SND_PCM_FORMAT_S32_BE; break;
						default: assert(0);
					} break;
				default: assert(0);
			} break;
		default: assert(0);
		}
	}

	snd_pcm_hw_params_any(a->pcm, hw_params);
	snd_pcm_hw_params_set_access(a->pcm, hw_params, SND_PCM_ACCESS_RW_INTERLEAVED);
	snd_pcm_hw_params_set_format(a->pcm, hw_params, fmt);
	snd_pcm_hw_params_set_channels(a->pcm, hw_params, num_channels);
	snd_pcm_hw_params_set_rate(a->pcm, hw_params, samplerate, 0);
	snd_pcm_hw_params_set_periods(a->pcm, hw_params, 10, 0);
	snd_pcm_hw_params_set_period_time(a->pcm, hw_params, 100000, 0);
	snd_pcm_hw_params(a->pcm, hw_params);

	snd_pcm_hw_params_free(hw_params);

	return a;

}

void audio_close(AudioDevice *dev) {

	snd_pcm_close(dev->pcm);
	free(dev);
}

void audio_block(AudioDevice *dev) {
	snd_pcm_drain(dev->pcm);
}

void audio_play(AudioDevice* dev, const unsigned char* data, size_t num_bytes)
{
	snd_pcm_writei(dev->pcm, data, num_bytes);
}
