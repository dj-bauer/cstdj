#include "wav.h"
#include "logger.h"
#include <stdio.h>
#include <assert.h>
#include <string.h>
#include <stdbool.h>
#include <stdlib.h>

static bool big_endian(void) {
	int i = 1;
	return !((char*)&i)[0];
}

union u_int {
	unsigned int value;
	char bytes[4];
};

union u_short {
	short value;
	char bytes[2];
};

static void reverse_int(union u_int* i)
{
	unsigned char tmp = i->bytes[0];
	i->bytes[0] = i->bytes[3];
	i->bytes[3] = tmp;
	tmp = i->bytes[1];
	i->bytes[1] = i->bytes[2];
	i->bytes[2] = tmp;
}

static void reverse_short(union u_short* s)
{
	unsigned char tmp = s->bytes[0];
	s->bytes[0] = s->bytes[1];
	s->bytes[1] = tmp;
}

struct s_wav_header {
	char chunk_id[4];
	union u_int chunk_size;
	char format[4];
	char subchunk_id_1[4];
	union u_int subchunk_size_1;
	union u_short audio_format;
	union u_short num_channels;
	union u_int sample_rate;
	union u_int byte_rate;
	union u_short block_align;
	union u_short bits_per_sample;
	char subchunk_id_2[4];
	union u_int subchunk_size_2;
};

union u_wav_header {
	unsigned char data[sizeof(struct s_wav_header)];
	struct s_wav_header header;
};


static void reverse_endianess(struct s_wav_header* header)
{
	reverse_int(&header->chunk_size);
	reverse_int(&header->subchunk_size_1);
	reverse_short(&header->audio_format);
	reverse_short(&header->num_channels);
	reverse_int(&header->sample_rate);
	reverse_int(&header->byte_rate);
	reverse_short(&header->block_align);
	reverse_short(&header->bits_per_sample);
	reverse_int(&header->subchunk_size_2);
}

char* load_wav_file(const char* filename, int* num_channels, int* num_samples, int* bits_per_sample, 
		int* buffer_size, SoundFormat* format) 
{
	bool is_big_endian = big_endian();
	assert(sizeof(int) == 4 && "The implementation requires integers to be 32-bit");

	// Buffer to be quickly read into a 32-bit integer
	FILE* f = fopen(filename, "rb");
	if(!f) {
		log_error("Could not open file %s", filename);
		return NULL;
	}
	
	struct s_wav_header header;
	int bytes_read = fread(&header, 1, sizeof(struct s_wav_header), f);
	if(bytes_read != sizeof(struct s_wav_header)) {
		fclose(f);
		log_error("Could not read the wav header. Incomplete file");
		return NULL;
	}
	if(is_big_endian) reverse_endianess(&header);

    // An arbitrary upper bound check
    if(header.subchunk_size_2.value > (unsigned int) 1024*1024*1024*2) {
        log_error("Only allow for files less than 2G");
        fclose(f);
        return NULL;
    }

	char* buf = malloc(header.subchunk_size_2.value);
	if(!buf) {
		fclose(f);
		return NULL;
	}

	bytes_read = fread(buf, 1, header.subchunk_size_2.value, f);
	assert(bytes_read == header.subchunk_size_2.value);
	fclose(f);

	return buf;
}

