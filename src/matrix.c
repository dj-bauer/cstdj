#include "matrix.h"
#include "util.h"
#include "cas.h"
#include "logger.h"

#include <assert.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


const struct s_stackmat_4_4 id_4x4_obj = {
	.cols=4, .rows=4,
	.data_ptr = (mat_data*) &id_4x4_obj.data_storage,
	.data_storage = {1, 0, 0, 0, 
		             0, 1, 0, 0,
					 0, 0, 1, 0,
					 0, 0, 0, 1},
};
const dj_mat* id_4x4 = (dj_mat*) &id_4x4_obj;

dj_mat* dj_mat_new(mat_dim rows, mat_dim cols)
{
	assert(rows > 0);
	assert(cols > 0);
	dj_mat* m = calloc(sizeof(dj_mat) + rows * cols * sizeof(mat_data), 1);
	assert(m != NULL);
	m->rows = rows;
	m->cols = cols;
	m->data = (mat_data*) &m[1];

	return m;
}

void dj_mat_delete(dj_mat** mat)
{
	free(*mat);
	*mat = NULL;
}

bool dj_mat_eq_tol(const dj_mat* a, const dj_mat* b, mat_data tol)
{
	if(!dj_mat_eq_dims(a, b))
		return false;
	mat_dim n = a->cols * a->rows;
	for(mat_dim i=0; i<n; i++) {
		if (fabs(a->data[i] - b->data[i]) > tol)
			return false;
	}
	return true;
}

void dj_mat_print(const dj_mat* A)
{
	assert(A->cols * 12+ 10 < 512);
	char buf[512] = "╭";
	mat_dim w = A->cols*12-2;
	for(mat_dim i=0; i<w; i++) {
		buf[i+3] = ' ';
	}
	buf[w+4] = '\0';
	printf("%s╮\n", buf);
	for(mat_dim r=0; r<A->rows; r++) {
		printf("│");
		for(mat_dim c = 0; c<A->cols; c++) {
			printf("%10.4f%s", dj_mat_get(A, r, c),
					c == A->cols - 1 ? "" : "  ");
		}
		printf("│\n");
	}
	buf[2] = '\260'; // Only the third byte needs to be changed to change the top left corner into a bottom left corner
	printf("%s╯\n", buf);
}

inline bool dj_mat_eq(const dj_mat* a, const dj_mat* b)
{
	return dj_mat_eq_tol(a, b, 0);
}
inline bool dj_mat_eq_dims(const dj_mat* a, const dj_mat* b)
{
	return (a->cols == b->cols) && (a->rows == b->rows);
}
inline bool dj_mat_is_sq(const dj_mat* A)
{
	return A->cols == A->rows;
}
bool dj_mat_is_lo_trig(const dj_mat* A)
{
	for(mat_dim i=0; i<A->rows; i++) {
		for(mat_dim j=i+1; j<A->cols; j++) {
			if(dj_mat_get(A, i, j) != 0) {
				return false;
			}
		}
	}
	return true;
}
bool dj_mat_is_up_trig(const dj_mat* A)
{
	for(mat_dim i=0; i<A->rows; i++) {
		for(mat_dim j=0; j<A->cols && j<i; j++) {
			if(dj_mat_get(A, i, j) != 0)
				return false;
		}
	}
	return true;
}
mat_data dj_mat_det(const dj_mat* A)
{
	assert(dj_mat_is_sq(A));
	mat_dim n = A->cols;
	mat_data res = 0;
	if(n == 1) {
		res = A->data[0];
	} else if (n == 2) {
		res  = dj_mat_get(A, 0, 0) * dj_mat_get(A, 1, 1);
		res -= dj_mat_get(A, 0, 1) * dj_mat_get(A, 1, 0);
	} else if (n == 3) {
		res  = dj_mat_get(A, 0, 0) * dj_mat_get(A, 1, 1) * dj_mat_get(A, 2, 2);
		res += dj_mat_get(A, 0, 1) * dj_mat_get(A, 1, 2) * dj_mat_get(A, 2, 0);
		res += dj_mat_get(A, 0, 2) * dj_mat_get(A, 1, 0) * dj_mat_get(A, 2, 1);
		res -= dj_mat_get(A, 0, 0) * dj_mat_get(A, 1, 2) * dj_mat_get(A, 2, 1);
		res -= dj_mat_get(A, 0, 1) * dj_mat_get(A, 1, 0) * dj_mat_get(A, 2, 2);
		res -= dj_mat_get(A, 0, 2) * dj_mat_get(A, 1, 1) * dj_mat_get(A, 2, 0);
	} else {
		mat_dim row = 0;
		dj_mat* submat = dj_mat_new(n-1, n-1);
		bool negative = false;
		for(mat_dim col = 0; col < n; col++) {
			for(mat_dim sub_col = (col+1) % n; sub_col != col; sub_col=((sub_col+1) % n)) {
				mat_dim target_col = sub_col - (sub_col > col ? 1 : 0);
				for(mat_dim sub_row = 1; sub_row<n; sub_row++) {
					dj_mat_set(submat, sub_row-1, target_col, dj_mat_get(A, sub_row, sub_col));
				}
			}
			res += (negative ? -1 : 1) * dj_mat_get(A, row, col) * dj_mat_det(submat);
			negative = !negative;
		}
		dj_mat_delete(&submat);
	}
	return res;
}

mat_data dj_mat_get(const dj_mat* A, mat_dim row, mat_dim col)
{
	assert(A->rows > row);
	assert(A->cols > col);
	return A->data[row * A->cols + col];
}
inline void dj_mat_set(dj_mat* A, mat_dim row, mat_dim col, mat_data value)
{
	assert(A->rows > row);
	assert(A->cols > col);
	A->data[row * A->cols + col] = value;
}

void dj_mat_set_rand(dj_mat* A, mat_dim min, mat_dim max)
{
	mat_dim n = A->cols * A->rows;
	for(mat_dim i=0; i<n; i++) {
		A->data[i] = rand() % 100; //random_double(min, max);
	}
}
void dj_mat_set_all(dj_mat* mat, mat_data value)
{
	mat_dim n = mat->cols * mat->rows;
	for(mat_dim i=0; i<n; i++) {
		mat->data[i] = value;
	}
}
void dj_mat_set_diag(dj_mat* mat, mat_data value)
{
	mat_dim n = MIN(mat->rows, mat->cols);
	for(mat_dim i=0; i<n; i++) {
		dj_mat_set(mat, i, i, value);
	}
}
void dj_mat_set_id(dj_mat* mat)
{
	assert(dj_mat_is_sq(mat));
	for(mat_dim i=0; i<mat->rows; i++) {
		for(mat_dim j=0; j<mat->cols; j++) {
			dj_mat_set(mat, i, j, i == j ? 1 : 0);
		}
	}
}
void dj_mat_set_col(dj_mat* A, const dj_mat* vec, mat_dim col)
{
	assert(dj_vec_is(vec));
	mat_dim n = MAX(A->rows, A->cols);
	assert(n == A->rows);
	assert(col < A->cols);
	for(mat_dim i=0; i<n; i++) {
		dj_mat_set(A, i, col, vec->data[i]);
	}
}

void dj_mat_cp(dj_mat* restrict dest, const dj_mat* restrict orig)
{
	assert(dj_mat_eq_dims(dest, orig));
	memcpy(dest->data, orig->data, sizeof(mat_data) * orig->cols * orig->rows);
}
void dj_mat_cp_col(dj_mat* dest, const dj_mat* A, mat_dim col)
{
	assert(dj_vec_is(dest));
	mat_dim n = MAX(dest->rows, dest->cols);
	assert(n == A->rows);
	assert(col < A->cols);
	for(mat_dim i=0; i<n; i++) {
		dest->data[i] = dj_mat_get(A, i, col);
	}
}

void dj_mat_trans(dj_mat* restrict T, const dj_mat* A)
{
	assert(T->cols == A->rows);
	assert(T->rows == A->cols);
	for(mat_dim r = 0; r<T->rows; r++) {
		for(mat_dim c = 0; c<T->cols; c++) {
			dj_mat_set(T, r, c, dj_mat_get(A, c, r));
		}
	}
}
void dj_mat_inv(dj_mat* restrict A_i, const dj_mat* A_const)
{
	// TODO: assert(dj_mat_det(A_const) != 0)
	assert(dj_mat_is_sq(A_const));
	assert(dj_mat_eq_dims(A_i, A_const));

	dj_mat* A = dj_mat_new(A_const->rows, A_const->cols);
	mat_dim n = A->cols;

	dj_mat_cp(A, A_const);
	dj_mat_set_id(A_i);

	for(mat_dim k=0; k<n; k++) {
		mat_data tmp = dj_mat_get(A, k, k);
		for(mat_dim i=0; i<n; i++) {
			dj_mat_set(A, i, k, dj_mat_get(A, i, k) / tmp);
			dj_mat_set(A_i, i, k, dj_mat_get(A_i, i, k) / tmp);
		}
		for(mat_dim i=0; i<n; i++) {
			if (i == k)
				continue;
			mat_data tmp = dj_mat_get(A, k, i);
			for(mat_dim j=0; j<n; j++) {
				dj_mat_set(A, j, i, dj_mat_get(A, j, i) - dj_mat_get(A, j, k) * tmp);
				dj_mat_set(A_i, j, i, dj_mat_get(A_i, j, i) - dj_mat_get(A_i, j, k) * tmp);
			}
		}
	}
	dj_mat_delete(&A);
}

void dj_mat_add(dj_mat* dest, const dj_mat* A, const dj_mat* B)
{
	assert(dj_mat_eq_dims(dest, A));
	assert(dj_mat_eq_dims(A, B));
	mat_dim n = A->cols * A->rows;
	for(mat_dim i=0; i<n; i++) {
		dest->data[i] = A->data[i] + B->data[i];
	}
}
void dj_mat_sub(dj_mat* dest, const dj_mat* A, const dj_mat* B)
{
	assert(dj_mat_eq_dims(dest, A));
	assert(dj_mat_eq_dims(A, B));
	mat_dim n = A->cols * A->rows;
	for(mat_dim i=0; i<n; i++) {
		dest->data[i] = A->data[i] - B->data[i];
	}
}
void dj_mat_mult_sca(dj_mat* dest, const dj_mat* A, mat_data scalar)
{
	assert(dj_mat_eq_dims(dest, A));
	mat_dim n = A->cols * A->rows;
	for(mat_dim i=0; i<n; i++) {
		dest->data[i] = A->data[i] * scalar;
	}
}
void dj_mat_mult(dj_mat* dest, const dj_mat* A, const dj_mat* B)
{
	assert(A->cols == B->rows);
	assert(dest->cols == B->cols);
	assert(dest->rows == A->rows);
	dj_mat_set_all(dest, 0);
	for(mat_dim i=0; i<dest->rows; i++) {
		for(mat_dim j=0; j<dest->cols; j++) {
			for(mat_dim k=0; k<A->cols; k++) {
				dj_mat_set(dest, i, j, dj_mat_get(dest, i, j) +
						dj_mat_get(A, i, k) * dj_mat_get(B, k, j));
			}
		}
	}
}

/* ==== Nice linear algebra algorithms ==== */

void dj_mat_cholesky(dj_mat* L, const dj_mat* A)
{
	assert(dj_mat_is_sq(A));
	assert(dj_mat_eq_dims(L, A));

	mat_dim n = A->cols;
	dj_mat_set_all(L, 0);

	for(mat_dim i=0; i<n; i++) {
		for(mat_dim j=0; j <= i; j++) {
			mat_data sum = 0;
			for(mat_dim k=0; k<j; k++) {
				sum += dj_mat_get(L, i, k) * dj_mat_get(L, j, k);
			}
			if(i == j)
				dj_mat_set(L, i, j, sqrt(dj_mat_get(A, i, i) - sum));
			else
				dj_mat_set(L, i, j, 1 / dj_mat_get(L, j, j) * (dj_mat_get(A, i, j) - sum));
		}
	}
}

void dj_mat_gram_schmidt(dj_mat* Q, const dj_mat* A)
{
	assert(dj_mat_is_sq(A));
	assert(dj_mat_eq_dims(Q, A));

	dj_mat* a_i = dj_mat_new(A->rows, 1);
	dj_mat* q_j = dj_mat_new(A->rows, 1);

	for(mat_dim i=0; i<A->cols; i++) {
		dj_mat_cp_col(a_i, A, i);
		for(mat_dim j=0; j<i; j++) {
			dj_mat_cp_col(q_j, Q, j);
			mat_data sca_prod = dj_vec_sca_prod(a_i, q_j);
			dj_mat_mult_sca(q_j, q_j, sca_prod);
			dj_mat_sub(a_i, a_i, q_j);
		}
		dj_vec_norm(a_i, a_i);
		dj_mat_set_col(Q, a_i, i);
	}

	dj_mat_delete(&a_i);
	dj_mat_delete(&q_j);
}

void dj_mat_mod_gram_schmidt(dj_mat* Q, const dj_mat* A)
{
	assert(dj_mat_is_sq(A));
	assert(dj_mat_eq_dims(Q, A));
	dj_mat* V = dj_mat_new(A->rows, A->cols);
	dj_mat_cp(V, A);
	dj_mat* v_j = dj_mat_new(A->rows, 1);
	dj_mat* q_i = dj_mat_new(A->rows, 1);
	dj_mat* rij_qi = dj_mat_new(A->rows, 1);

	for(mat_dim i=0; i<A->cols; i++) {
		dj_mat_cp_col(q_i, V, i);
		dj_vec_norm(q_i, q_i);
		dj_mat_set_col(Q, q_i, i);

		for(mat_dim j=i+1; j<A->rows; j++) {
			dj_mat_cp_col(v_j, V, j);
			mat_data r_ij = dj_vec_sca_prod(q_i, v_j);
			dj_mat_mult_sca(rij_qi, q_i, r_ij);

			dj_mat_sub(v_j, v_j, rij_qi);
			dj_mat_set_col(V, v_j, j);
		}
	}
	dj_mat_delete(&V);
	dj_mat_delete(&v_j);
	dj_mat_delete(&q_i);
	dj_mat_delete(&rij_qi);
}

void dj_mat_fw_subst(dj_mat* x, const dj_mat* L, const dj_mat* b_const)
{
	assert(dj_mat_is_sq(L));
	assert(dj_mat_is_lo_trig(L));
	assert(dj_mat_eq_dims(x, b_const));
	assert(dj_vec_is(x));
	mat_dim n = MAX(x->rows, x->cols);
	assert(n == L->rows);

	dj_mat* b = dj_mat_new(b_const->rows, b_const->cols);
	dj_mat_cp(b, b_const);
	for(mat_dim i=0; i<n; i++) {
		x->data[i] = b->data[i] / dj_mat_get(L, i, i);
		for(mat_dim j=i+1; j<n; j++) {
			b->data[j] -= dj_mat_get(L, j, i) * x->data[i];
		}
	}
	dj_mat_delete(&b);
}

void dj_mat_bw_subst(dj_mat* x, const dj_mat* U, const dj_mat* b_const)
{
	assert(dj_mat_is_sq(U));
	assert(dj_mat_is_up_trig(U));
	assert(dj_mat_eq_dims(x, b_const));
	assert(dj_vec_is(x));
	mat_dim n = MAX(x->rows, x->cols);
	assert(n == U->rows);

	dj_mat* b = dj_mat_new(b_const->rows, b_const->cols);
	dj_mat_cp(b, b_const);
	for(mat_dim i_p1 = n; i_p1 > 0; i_p1--) {
		mat_dim i = i_p1-1;
		x->data[i] = b->data[i] / dj_mat_get(U, i, i);
		for(mat_dim j_p1=1; j_p1<=i_p1-1; j_p1++) {
			mat_dim j = j_p1 - 1;
			b->data[j] -= dj_mat_get(U, j, i) * x->data[i];
		}
	}
	dj_mat_delete(&b);
}

void dj_mat_qr(dj_mat* restrict Q, dj_mat* restrict R, const dj_mat* A)
{
	// A = QR => Q*A = R
	dj_mat_mod_gram_schmidt(Q, A);
	dj_mat* Q_t = dj_mat_new(Q->cols, Q->rows);
	dj_mat_trans(Q_t, Q);
	dj_mat_mult(R, Q_t, A);
	dj_mat_delete(&Q_t);
}

/* Vector utilities */

inline bool dj_vec_is(const dj_mat* A)
{
	return A->cols == 1 || A->rows == 1;
}
mat_data dj_vec_sca_prod(const dj_mat* a, const dj_mat* b)
{
	assert(dj_vec_is(a));
	assert(dj_vec_is(b));

	mat_dim n = MAX(a->cols, a->rows);
	assert(n == MAX(b->cols, b->rows));

	mat_data sum = 0;
	for(mat_dim i=0; i<n; i++) {
		sum += a->data[i] * b->data[i];
	}
	return sum;
}
void dj_vec_norm(dj_mat* dest, const dj_mat* src)
{
	assert(dj_vec_is(dest));
	assert(dj_vec_is(src));
	mat_dim n = MAX(dest->cols, dest->rows);
	assert(n == MAX(src->cols, src->cols));
	mat_data len = dj_vec_length(src);

	for(mat_dim i=0; i<n; i++) {
		dest->data[i] = src->data[i] / len;
	}
}
mat_data dj_vec_length(const dj_mat* vec)
{
	assert(dj_vec_is(vec));
	mat_dim n = MAX(vec->cols, vec->rows);
	mat_data sum = 0;
	for(mat_dim i=0; i<n; i++) {
		sum += vec->data[i] * vec->data[i];
	}
	return sqrt(sum);
}
void dj_vec_cross_prod(dj_mat* c, const dj_mat* a, const dj_mat* b)
{
	assert(dj_mat_eq_dims(a, b));
	assert(dj_mat_eq_dims(a, c));
	assert(dj_vec_is(a));
	assert(MAX(a->cols, a->rows) == 3);
	c->data[0] = a->data[1] * b->data[2]
			   - a->data[2] * b->data[1];
	c->data[1] = a->data[2] * b->data[0]
		       - a->data[0] * b->data[2];
	c->data[2] = a->data[0] * b->data[1]
		       - a->data[1] * b->data[0];
}

void dj_mat_svd(dj_mat* restrict U, dj_mat* restrict sigma, dj_mat* restrict V_t, const dj_mat* A)
{
	mat_dim m = A->rows;
	mat_dim n = A->cols;
	mat_dim grade = MIN(m, n);
	assert(dj_mat_is_sq(U) && U->cols == m);
	assert(sigma->rows == m && sigma->cols == n);
	assert(dj_mat_is_sq(V_t) && V_t->cols == n);

	dj_mat* A_t = dj_mat_new(A->cols, A->rows);
	dj_mat_trans(A_t, A);

	dj_mat* B = dj_mat_new(grade, grade);
	dj_mat_mult(B, A_t, A);

	// TODO: Step 2
	// Eigenwerte von B berechnen

	// TODO: Step 3
	// Eigenvektoren v₁-vₙ berechnen
	// V = [v₁,...,vₙ]
	
	// TODO: Step 4
	// Sigma = Diagonalmatrix mit √(λᵢ) an ∑ ᵢᵢ
	
	// TODO: Step 5
	// U Berechnen uᵢ = 1/√(λᵢ) * Avᵢ Diese Vektoren zu einer Basis ergänzen

	dj_mat_delete(&A_t);
}

void dj_mat_eigenvalues(mat_data* restrict dest, const dj_mat* mat)
{
	if(dj_mat_is_sq(mat) && mat->cols == 2) {
		// Simple 2x2 case
		mat_data a = 1;
		mat_data b = -(dj_mat_get(mat, 0, 0)+dj_mat_get(mat, 1,1));
		mat_data c = dj_mat_get(mat, 0, 0)*dj_mat_get(mat, 1, 1) - dj_mat_get(mat, 0, 1)*dj_mat_get(mat, 1, 0);
		cas_solve_quadratic(dest, a, b, c);
	} else if (dj_mat_is_sq(mat) && mat->cols == 3) {
		mat_data a = dj_mat_get(mat, 0, 0);
		mat_data b = dj_mat_get(mat, 0, 1);
		mat_data c = dj_mat_get(mat, 0, 2);
		mat_data d = dj_mat_get(mat, 1, 0);
		mat_data e = dj_mat_get(mat, 1, 1);
		mat_data f = dj_mat_get(mat, 1, 2);
		mat_data g = dj_mat_get(mat, 2, 0);
		mat_data h = dj_mat_get(mat, 2, 1);
		mat_data i = dj_mat_get(mat, 2, 2);
		mat_data A = -1;
		mat_data B = a + e + i;
		mat_data C = f*h + b*d + c*g - a*i - e*i - a*e;
		mat_data D = a*e*i + b*f*g + c*d*h - a*f*h - b*d*i - c*e*g;
		cas_solve_cubic(dest, A, B, C, D);
	} else {
		log_trace("Using QR-Method for calculating matrix");
		assert(dj_mat_is_sq(mat));
		dj_mat* Q = dj_mat_new(mat->rows, mat->cols);
		dj_mat* R = dj_mat_new(mat->rows, mat->cols);
		dj_mat* A = dj_mat_new(mat->rows, mat->cols);
		dj_mat_cp(A, mat);

		for(int k=0; k<100; k++) {
			dj_mat_qr(Q, R, A);

			log_debug("A: ");
			dj_mat_print(A);
			log_debug("Q: ");
			dj_mat_print(Q);
			log_debug("R: ");
			dj_mat_print(R);
			dj_mat_mult(A, R, Q);
		}
		for(mat_dim i=0; i<mat->rows; i++) {
			dest[i] = dj_mat_get(A, i, i);
		}

		dj_mat_delete(&A);
		dj_mat_delete(&R);
		dj_mat_delete(&Q);
	}
}
