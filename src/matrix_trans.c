#include "matrix_trans.h"
#include <assert.h>

void dj_trans_scale(dj_mat* restrict dest, const dj_mat* orig, mat_data scale)
{
	assert(dest->cols == 4);
	assert(dj_mat_is_sq(orig));
	assert(dj_mat_eq_dims(dest, orig));
	dj_stackmat_new(id, 4, 4);
	dj_mat_set_diag(id, scale);
	dj_mat_set(id, 3, 3, 1);

	dj_mat_mult(dest, orig, id);
}

void dj_trans_translate(dj_mat* dest, const dj_mat* orig, const dj_mat* vec)
{
	assert(dest->cols == 4);
	assert(dj_mat_is_sq(orig));
	assert(dj_mat_eq_dims(dest, orig));
	assert(dj_vec_is(vec));

	dj_stackmat_new(trans, 4, 4);
	dj_mat_set_diag(trans, 1);
	for(mat_dim i=0; i<3; i++) {
		dj_mat_set(trans, i, 3, vec->data[i]);
	}
	dj_mat_mult(dest, orig, trans);
}
