#ifndef DJ_CMP_H
#define DJ_CMP_H

#include "stdbool.h"

#define SIMPLE_CMP(i, T) \
	bool dj_eq_##i(T a, T b); \
	bool dj_lt_##i(T a, T b);
SIMPLE_CMP(i, int)
SIMPLE_CMP(d, double)
SIMPLE_CMP(f, float)
SIMPLE_CMP(v, void*)
SIMPLE_CMP(cs, const char*)
SIMPLE_CMP(l, long)

#define dj_eq_s dj_eq_cs
#define dj_lt_s dj_lt_cs

#endif
