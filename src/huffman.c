#include "huffman.h"
#include "util.h"

void huffman_add_node(size_t tree_size, HuffmanNode tree[tree_size], size_t* next_node_index, uint16_t symbol, uint16_t code, uint8_t num_bits)
{
	assert(tree_size > 0);
	size_t i = *next_node_index;
	if(i == 0) {
		// Initialize root node
		tree[i].zero_child = 0;
		tree[i].one_child = 0;
		tree[i].symbol = 0xDEAD;
		tree[i].has_symbol = false;
		i++;
	}

	// We shift the code up to the left side
	uint8_t unused_bits = 16 - num_bits;
	uint16_t shifted_code = code << unused_bits;

	size_t cur_node = 0;
	for(int j=0; j<num_bits; j++) {
		if(shifted_code & 0x8000) {
			if(tree[cur_node].one_child == 0) {
				assert(i < tree_size);
				tree[i].zero_child = 0;
				tree[i].one_child = 0;
				tree[i].symbol = 0xDEAD;
				tree[i].has_symbol = false;
				tree[cur_node].one_child = i;
				i++;
			}
			cur_node = tree[cur_node].one_child;
		} else {
			if(tree[cur_node].zero_child == 0) {
				assert(i < tree_size);
				tree[i].zero_child = 0;
				tree[i].one_child = 0;
				tree[i].symbol = 0xDEAD;
				tree[i].has_symbol = false;
				tree[cur_node].zero_child = i;
				i++;
			}
			cur_node = tree[cur_node].zero_child;
		}
		shifted_code <<= 1;
	}
	assert(shifted_code == 0);
	assert(cur_node < tree_size);
	assert(tree[cur_node].one_child == 0);
	assert(tree[cur_node].zero_child == 0);
	assert(tree[cur_node].has_symbol == false);
	assert(tree[cur_node].symbol == 0xDEAD);
	tree[cur_node].symbol = symbol;
	tree[cur_node].has_symbol = true;

	*next_node_index = i;
}

void huffman_print_tree(FILE* f, size_t dim, const HuffmanNode tree[dim], size_t max_node)
{
	fprintf(f, "graph {\n");
	size_t n = MIN(dim, max_node);
	for(size_t i=0; i<n; i++) {
		if(tree[i].has_symbol)
			fprintf(f, "\t%zu [label=%d, color=red]\n", i, tree[i].symbol);
	}
	for(size_t i=0; i<n; i++) {
		if(tree[i].zero_child != 0)
			fprintf(f, "\t%zu -- %d [label=0]\n", i, tree[i].zero_child);
		if(tree[i].one_child != 0)
			fprintf(f, "\t%zu -- %d [label=1]\n", i, tree[i].one_child);
	}
	fprintf(f, "}\n");
}
