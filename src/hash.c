#include <string.h>
#include <math.h>

#include "hash.h"

size_t hash_str(const char* str)
{
	// Taken from: https://cp-algorithms.com/string/string-hashing.html
	size_t len = strlen(str);
	const size_t p = 59;
	const size_t m = 1e9 + 9;
	size_t hash = 0;
	size_t exp = 1;
	for(size_t i=0; i<len; i++) {
		hash += str[i] * pow(p, exp);
		hash = (hash + (str[i] - 'A' + 1) * exp) % m;
		exp = (exp * p) % m;
	}
	return m;
}
