#include "union_find.h"
#include <stdlib.h>
#include <stdio.h>

UnionFind* unionfind_create(long num_indices)
{
    UnionFind* uf = malloc(sizeof(UnionFind) + sizeof(long) * num_indices);
	if(uf == NULL) return NULL;
    uf->num_indices = num_indices;
    for(long i=0; i<num_indices; i++) {
        uf->represents[i] = i;
    }
    return uf;
}

void unionfind_destroy(UnionFind* uf)
{
    free(uf);
}
inline int unionfind_check(UnionFind* uf, long a, long b)
{
	return (unionfind_representative(uf, a) == unionfind_representative(uf, b))
		? 1 : 0;
}

long unionfind_representative(UnionFind* uf, long node)
{
    if(uf == NULL || node < 0 || node >= uf->num_indices) return -1;
    if(uf->represents[node] == node) return node;
    long parent = uf->represents[node];
    while(uf->represents[parent] != parent) {
        parent = uf->represents[parent];
    }
	// Adjust all representatives
	while(uf->represents[node] != parent) {
		long new_node = uf->represents[node];
		uf->represents[node] = parent;
		node = new_node;
	}
    return parent;
}

long unionfind_connect(UnionFind* uf, long a, long b)
{
    if(uf == NULL || a < 0 || b < 0) return -1;

    struct s_uf_string {
        unsigned long size;
        long repr;
    } a_str, b_str;
    a_str = (struct s_uf_string) {.size = 1, .repr = a};
    b_str = (struct s_uf_string) {.size = 1, .repr = b};

    while(a_str.repr != uf->represents[a_str.repr]) {
        a_str.size++;
        a_str.repr = uf->represents[a_str.repr];
    }

    while(b_str.repr != uf->represents[b_str.repr]) {
        b_str.size++;
        b_str.repr = uf->represents[b_str.repr];
    }

    if(a_str.size > b_str.size) {
        uf->represents[b_str.repr] = a_str.repr;
        return a_str.repr;
    } else {
        uf->represents[a_str.repr] = b_str.repr;
        return b_str.repr;
    }
}
