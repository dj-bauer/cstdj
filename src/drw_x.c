#include "color.h"
#include "djwindow.h"
#include "drw.h"
#include "x_window.h"
#include "logger.h"
#include <X11/X.h>
#include <X11/Xlib.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>

unsigned long rgb(Color c) {
	int r = c.r * 255.0;
	int g = c.g * 255.0;
	int b = c.b * 255.0;
	return b + (g << 8) + (r << 16);
}
struct s_rgb color_to_rgb(Color in) {
    return (struct s_rgb) {in.b * 255.0, in.g * 255.0, in.r * 255.0};
}

inline static void fg(DJWindow* win, Color c) {
	XSetForeground(win->disp, win->gc, rgb(c));
}

void clear_screen(DJWindow *win) {
	XClearWindow(win->disp, win->win);
}

void draw_rect(DJWindow *win, int x, int y, int w, int h, Color c)
{
	fg(win, c);
	XFillRectangle(win->disp, win->win, win->gc, x, y, w, h);
}

void draw_circle(DJWindow* win, int x, int y, int radius, Color color)
{
	fg(win, color);
	XFillArc(win->disp, win->win, win->gc, x-radius, y-radius, 2*radius, 2*radius, 0, 360*64);
}

void draw_line(DJWindow *win, int x0, int y0, int x1, int y1, Color c)
{
	fg(win, c);
	XDrawLine(win->disp, win->win, win->gc, x0, y0, x1, y1);
}

void fill(DJWindow* win, Color c) {
	draw_rect(win, 0, 0, win->size.x, win->size.y, c);
}

void set_bg(DJWindow *win, Color c) {
	XSetBackground(win->disp, win->gc, rgb(c));
}

void print_text(DJWindow* win, int x, int y, const char* string, Color c)
{
	fg(win, c);
	XDrawString(win->disp, win->win, win->gc, x, y, string, strlen(string));
}

struct s_djpixmap {
	int width;
	int height;
	int bytes_per_pixel_channel;
	int nr_channels;
	char* data;
	XImage* img;
};

DJimg* create_image(DJWindow* win, int width, int height)
{
	DJimg * pix = malloc(sizeof(DJimg));
	if(pix == NULL) {
		log_error("Could not create Dj image (malloc returned NULL)");
		return NULL;
	}
	pix->width = width;
	pix->height = height;
	pix->nr_channels = 0;
	pix->bytes_per_pixel_channel = 0;
	pix->data = calloc(4, width * height);
	Visual* visual = DefaultVisual(win->disp, win->screen);
	pix->img = XCreateImage(win->disp, visual, 24, ZPixmap, 0, pix->data, pix->width, pix->height, 32, 0);
	if(pix->img == NULL) {
		log_error("Could not create Ximage");
	}
	assert(pix->img != NULL);
	return pix;
}
void destroy_image(DJimg* pix)
{
	XDestroyImage(pix->img);
}
void draw_image(DJWindow* win, DJimg* pix, int x, int y)
{
	XPutImage(win->disp, win->win, win->gc, pix->img, 0, 0, x, y, pix->width, pix->height);
}
char* image_raw_data(DJimg* img) {
	return img->data;
}

void img_pixel(DJimg* pix, int x, int y, gbra c) {
	struct s_rgb* buf = (struct s_rgb*) pix->data;
	buf[x * pix->width + y] = c;
}
void draw_rect_outline(DJWindow* win, int x, int y, int w, int h, Color c)
{
	fg(win, c);
	XDrawRectangle(win->disp, win->win, win->gc, x, y, w, h);
}
