#ifndef BST_CMP_H
#define BST_CMP_H

#include <stdbool.h>
#include "bstree.h"

#define LT_MACRO(i, T) \
	struct s_bstnode_##i* node_insert_##i(struct s_bstnode_##i* n, T value); \
	struct s_bstnode_##i* create_node_##i(T value, struct s_bstnode_##i* parent); \
	struct s_bstnode_##i* node_remove_##i(BSTree_##i* t, struct s_bstnode_##i* n, T value); \
	struct s_bstnode_##i* node_search_##i(struct s_bstnode_##i* n, T value); \

LT_MACRO(i, int)
LT_MACRO(f, float)
LT_MACRO(d, double)
LT_MACRO(v, void*)
LT_MACRO(s, char*)
LT_MACRO(cs, const char*)

#undef LT_MACRO

#endif
