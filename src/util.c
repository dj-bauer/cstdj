#include "util.h"
#include <string.h>
#include <stdlib.h>
#include <limits.h>
#include <stdio.h>

char* strdup(const char* s) {
	size_t n = strlen(s);
	char* r = malloc(n+1);
	if(r) {
		strcpy(r, s);
	}
	return r;
}

bool str_startswith(const char* s, const char* prefix)
{
	while(*prefix != '\0') {
		if(*prefix != *s) return false;
		prefix++;
		s++;
	}
	return true;
}

bool str_endswith(const char* s, const char* suffix)
{
	size_t l1 = strlen(s);
	size_t l2 = strlen(suffix);

	return ( l2 == 0 ) || ( ( l1 >= l2) && memcmp( s + l1 - l2, suffix, l2 ) == 0 );
}

double random_double(double min, double max)
{
	double x = rand();
	double range = max - min;
	return min + range * (x / (double) INT_MAX);
}

size_t fprint(FILE* f, const char* str)
{
	return fwrite(str, sizeof(const char) , strlen(str), f);
}
