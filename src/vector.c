#include "vector_impl.h"

VECTOR_IMPL(i, int)
VECTOR_IMPL(f, float)
VECTOR_IMPL(d, double)
VECTOR_IMPL(v, void*)
VECTOR_IMPL(c, char)
VECTOR_IMPL(s, char*)
VECTOR_IMPL(cs, const char*)
VECTOR_IMPL(l, long)
VECTOR_IMPL(ull, unsigned long long)
