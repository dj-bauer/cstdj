/*
    Copyright (C) 2024 Daniel Bauer

    This program is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation, either version 3 of the License,
    or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty
    of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "vector.h"
#include <stdlib.h>
#include <errno.h>
#include <assert.h>
#include <string.h>

#ifdef NDEBUG
#define DBG_ONNULL_RET_ZERO(x)
#define DBG_ONNULL_RET_NULL(x)
#else
#define DBG_ONNULL_RET_ZERO(x) if((x) == NULL) return 0;
#define DBG_ONNULL_RET_NULL(x) if((x) == NULL) return NULL;
#endif

#define VECTOR_IMPL(i, T) \
	struct s_vector_##i* create_vector_##i(void) { \
		struct s_vector_##i* v = malloc(sizeof(struct s_vector_##i)); \
		init_vector_##i(v); \
		return v; \
	} \
	void init_vector_##i(struct s_vector_##i* v) { \
		if(v == NULL) return; \
		v->size = 0; \
		v->capacity = 0; \
		v->data = NULL; \
	} \
	void destroy_vector_##i(struct s_vector_##i *v_ptr) { \
		if(v_ptr == NULL) return; \
		deinit_vector_##i(v_ptr); \
		free(v_ptr); \
	} \
	void deinit_vector_##i(struct s_vector_##i* v) { \
		if(v == NULL) return; \
		free(v->data); \
		v->data = NULL; \
		v->size = 0; \
		v->capacity = 0; \
	} \
	T* vector_at_##i(struct s_vector_##i* v, size_t index) { \
		if(index >= v->size) { \
			errno = ERANGE; \
			return NULL; \
		} \
		return &v->data[index]; \
	} \
	T* vector_first_##i(struct s_vector_##i* v) { \
		if(v == NULL) return NULL; \
		if(v->size == 0) { \
			errno = ERANGE; \
			return NULL; \
		} \
		return &v->data[0]; \
	} \
	T* vector_last_##i(struct s_vector_##i* v) { \
		if(v == NULL) return NULL; \
		if(v->size == 0) { \
			errno = ERANGE; \
			return NULL; \
		} \
		return &v->data[v->size-1]; \
	} \
	static inline void size_up_##i(struct s_vector_##i* v) {  \
		assert(v != NULL); \
		if(v->capacity == 0) { \
			v->capacity = 1; \
		} else { \
			v->capacity *= 2; \
		} \
		T* new_data = realloc(v->data, v->capacity * sizeof(T)); \
		assert(new_data != NULL); \
		if(new_data != NULL) \
			v->data = new_data; \
	} \
	static inline void size_down_##i(struct s_vector_##i* v) { \
		assert(v != NULL); \
		if(v->capacity == 0) return; \
		v->capacity /= 2; \
		T* new_data = realloc(v->data, v->capacity * sizeof(T)); \
		assert(new_data != NULL); \
		if(new_data != NULL) \
			v->data = new_data; \
	} \
	T* vector_insert_##i(struct s_vector_##i* vec, size_t index, T value) { \
		if(vec == NULL) return NULL; \
		if(vec->size == vec->capacity) size_up_##i(vec); \
		assert(vec->capacity > index); \
		for(size_t i=vec->size; i>index; i--) { \
			vec->data[i] = vec->data[i-1]; \
		} \
		vec->data[index] = value; \
		vec->size++; \
		return &vec->data[index]; \
	} \
	inline T* vector_append_##i(struct s_vector_##i* v, T value) { \
		return vector_insert_##i(v, v->size, value); \
	} \
	inline T* vector_prepend_##i(struct s_vector_##i* v, T value) { \
		return vector_insert_##i(v, 0, value); \
	} \
	void vector_concat_##i(struct s_vector_##i* dest, const struct s_vector_##i* src) { \
		if(dest == NULL || src == NULL) return; \
		if(dest->size + src->size > dest->capacity) { \
			dest->capacity = dest->size + src->size; \
			T* new_data = realloc(dest->data, dest->capacity * sizeof(T)); \
			assert(new_data != NULL); \
			if(new_data != NULL) \
				dest->data = new_data; \
		} \
		memcpy(&dest->data[dest->size], src->data, src->size * sizeof(T)); \
		dest->size += src->size; \
	} \
	void vector_remove_last_##i(struct s_vector_##i* v) { \
		if(v == NULL) return; \
		v->size--; \
		if(v->size < v->capacity / 2) size_down_##i(v); \
	} \
	inline size_t vector_size_##i(const struct s_vector_##i* v) { \
		DBG_ONNULL_RET_ZERO(v) \
		return v->size; \
	} \
	void vector_foreach_##i(struct s_vector_##i* v, void (*foreach_func)(size_t, T*)) { \
		for (size_t index=0; index<v->size; index++) \
			foreach_func(index, &v->data[index]); \
	} \
	bool vector_contains_##i(struct s_vector_##i* v, T* item, bool (*eq_func)(T* a, T* b)) { \
		for (size_t index = 0; index<v->size; index++) \
			if(eq_func(item, &v->data[index])) return true; \
		return false; \
	}
