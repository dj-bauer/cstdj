#include "fbvideo.h"

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/fcntl.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <sys/kd.h>

int fb_open(Video* video)
{
	video->fd = open("/dev/fb0", O_RDWR);
	if(video->fd == -1) {
		perror("Count not open framebuffer device /dev/fb0");
		return 1;
	}
	if(ioctl(video->fd, FBIOGET_FSCREENINFO, &video->finfo) == -1) {
		close(video->fd);
		perror("Could not read fixed information");
		return 2;
	}
	if(ioctl(video->fd, FBIOGET_VSCREENINFO, &video->vinfo) == -1) {
		close(video->fd);
		perror("Could not read variable information");
		return 3;
	}
	video->memory_size = video->finfo.line_length * video->vinfo.yres * video->vinfo.bits_per_pixel / 8;
	video->width = video->vinfo.xres;
	video->height = video->vinfo.yres;
	printf("Framebuffer is %dx%d\n", video->width, video->height);

	video->memory = (char*) mmap(0, video->memory_size, PROT_READ | PROT_WRITE, MAP_SHARED, video->fd, 0);
	if(video->memory == MAP_FAILED) {
		close(video->fd);
		perror("Failed mapping framebuffer device to memory");
		return 4;
	}
	if(video->vinfo.bits_per_pixel != 32) {
		fprintf(stderr, "I currently only support 32-bit color depth");
		fb_close(video);
		return 5;
	}
ioctl(0, KDSETMODE, KD_GRAPHICS);
	return 0;
}
int fb_close(Video* video)
{
	ioctl(0, KDSETMODE, KD_TEXT);
	munmap(video->memory, video->memory_size);
	close(video->fd);
	return 0;
}
void write_pixel(Video* video, int x, int y, 
		unsigned char r, unsigned char g, unsigned char b)
{
#ifndef NDEBUG
	if(x < 0 || x >= video->width || y < 0 || y >= video->height) {
		return;
	}
#endif
	// We are sure that the bits per pixel = 32
	size_t loc = (x + video->vinfo.xoffset) * 4 +
		         (y + video->vinfo.yoffset) * video->finfo.line_length;

	video->memory[loc + 0] = b;
	video->memory[loc + 1] = g;
	video->memory[loc + 2] = r;
	video->memory[loc + 3] = 0;
}
