#include "heap.h"
#include "util.h"
#include <inttypes.h>
#include <stdlib.h>
#include <assert.h>
#include <stdio.h>

#define last_index(heap) (heap->size - 1)
#define get_parent(index) (((index)-1)/2)
#define get_left(index) (2 * (index) + 1)
#define get_right(index) (2 * (index) + 2)

/* Whether a binary heap entry has a parent */
#define has_parent(index) (index != 0)
/* Whether a binary heap entry has a left child */
#define has_left_child(heap, index) ((heap)->size > get_left(index))
/* Whether a binary heap entry has a right child */
#define has_right_child(heap, index) ((heap)->size > get_right(index))

#define is_higher(heap, a, b, T) (heap->cmp_func((const T*)&heap->data[a], (const T*)&heap->data[b]) < 0)
#define higher_than_parent(heap, index, T) is_higher(heap, index, get_parent(index), T) 
#define higher_or_equal(heap, a, b, T) (heap->cmp_func((const T*)&(heap)->data[a], (const T*)&(heap)->data[b]) <= 0)

#define swap_with_index(heap, a, b, T) \
			T tmp = h->data[a];\
			h->data[a] = h->data[b]; \
			h->data[b] = tmp;

#define swap_with_parent(heap, index, T) \
			size_t parent = get_parent(index); \
			swap_with_index(heap, index, parent, T)

// We should scale down if the array is maximum a quarter filled
#define should_size_down(heap) (heap->size * 4 < heap->capacity)
#define remove_from_root(heap, value, id) remove_rek_##id(heap, value, 0)

/** 
 * @brief Implements the heap functions for a given type
 * @param i suffix for all the functions
 * @param T datatype
 **/
#define HEAP_IMPL(i, T) \
	static void bubble_up_##i(heap_##i* h, size_t index); \
	static void bubble_down_##i(heap_##i* h, size_t index); \
	static bool remove_rek_##i(heap_##i* h, T value, size_t start); \
	void heap_init_##i(heap_##i* h, int (*cmp_func)(const T* a, const T* b)) { \
		h->size = 0; \
		h->capacity = 0; \
		h->data = NULL; \
		assert(cmp_func != NULL); \
		h->cmp_func = cmp_func; \
	} \
	void heap_cleanup_##i(heap_##i* h) { \
		if(h == NULL) return; \
		h->capacity = 0; \
		h->size = 0; \
		h->cmp_func = NULL; \
		free(h->data); \
		h->data = NULL; \
	} \
	heap_##i* heap_create_##i(int (*cmp_func)(const T* a, const T* b)) { \
		heap_##i* h = malloc(sizeof(*h)); \
		heap_init_##i(h, cmp_func); \
		return h; \
	} \
	void heap_destroy_##i(heap_##i** h) { \
		if(*h == NULL) return; \
		heap_cleanup_##i(*h); \
		free(*h); \
		*h = NULL; \
	} \
	const T* heap_front_##i(const heap_##i* h) {\
		if(heap_is_empty(h)) return NULL; \
		return (const T*) &h->data[0]; \
	} \
	void heap_insert_##i(heap_##i* h, T value) { \
		if(h == NULL) return; \
		if(h->size == h->capacity) { \
			if(h->capacity == 0) { \
				h->capacity = 1; \
			} else { \
				h->capacity *= 2; \
			} \
			h->data = realloc(h->data, sizeof(T) * h->capacity); \
			assert(h->data != NULL && "Reallocation failed"); \
		} \
		assert(h->size < h->capacity && "Resizing of heap failed"); \
		size_t index = h->size; \
		h->data[h->size] = value; \
		h->size++; \
		bubble_up_##i(h, index); \
	} \
	void heap_pop_##i(heap_##i* h) { \
		if(h == NULL) return; \
		if(heap_is_empty(h)) return; \
		remove_from_root(h, h->data[0], i); \
	} \
	static void bubble_up_##i(heap_##i* h, size_t index) {\
		while(has_parent(index) && higher_than_parent(h, index, T)) { \
			swap_with_parent(h, index, T); \
			index = parent; \
		} \
	} \
	static void bubble_down_##i(heap_##i* h, size_t index) { \
		/* Wenn wir größer als das parent sind, sind wir auch größer als die Kinder*/ \
		if(has_parent(index) && higher_than_parent(h, index, T)) { \
			swap_with_parent(h, index, T); \
			bubble_up_##i(h, parent); \
			return; \
		} \
		size_t right = get_right(index); \
		size_t left = get_left(index); \
		/* Wenn wir kein linkes kind haben, haben wir auch kein rechtes*/ \
		if(has_left_child(h, index)) { \
			/* Wenn wir ein rechtes kind haben, welches größer ist */ \
			if(has_right_child(h, index) && is_higher(h, right, left, T)) { \
				/* Wenn dieses größeres Kind tatsächlich höher als das parent ist */ \
				if(higher_than_parent(h, right, T)) { \
					swap_with_parent(h, right, T); \
					bubble_down_##i(h, right); \
					return; \
				} \
				/* Wenn wir entweder nur das linked kind haben oder größer als das rechte Kind sind */ \
			} else { \
				/* UND größer als das Parent sind*/ \
				if(higher_than_parent(h, left, T)) { \
					swap_with_parent(h, left, T); \
					bubble_down_##i(h, left); \
					return; \
				} \
			} \
		} \
	} \
	static bool remove_rek_##i(heap_##i* h, T value, size_t index) { \
		if(index + 1 > h->size) return false; \
		if(h->data[index] == value) { \
			size_t last = last_index(h); \
			swap_with_index(heap, index, last, T); \
			h->size -= 1; \
			bubble_down_##i(h, index); \
			if(h->capacity > 2 && should_size_down(h)) { \
				h->capacity /= 2; \
				h->data = realloc(h->data, sizeof(T) * h->capacity); \
			} \
			return true; \
		} \
		bool deleted = false; \
		if(has_left_child(h, index) && higher_or_equal(h, get_left(index), index, T)) { \
			deleted = remove_rek_##i(h, value, get_left(index)); \
		} \
		if(!deleted && has_right_child(h, index) && higher_or_equal(h, get_right(index), index, T)) { \
			deleted = remove_rek_##i(h, value, get_right(index)); \
		} \
		if(h->capacity > 2 && should_size_down(h)) { \
			h->capacity /= 2; \
			h->data = realloc(h->data, sizeof(T) * h->capacity); \
		} \
		return deleted; \
	} \
	void heap_reserve_##i(heap_##i* h, size_t n) { \
		h->capacity = n; \
		h->data = realloc(h->data, sizeof(T) * h->capacity); \
		assert(h->data != NULL && "Reallocation failed"); \
		assert(h->size < h->capacity && "Resizing of heap failed"); \
	} \
	bool heap_change_##i(heap_##i* heap, T item, bool higher) \
	{ \
		for(size_t i=0; i<heap->size; i++) { \
			if(heap->data[i] == item) { \
				if(higher) \
					bubble_up_##i(heap, i); \
				else \
					bubble_down_##i(heap, i); \
				return true; \
			} \
		} \
		return false; \
	}

#define HEAP_IMPL_PRINT(i, T, fmt) \
	void heap_print_##i(const heap_##i* h) { \
		if(h == NULL) return; \
		if(heap_is_empty(h)) { \
			printf("Empty Heap\n"); \
			return; \
		} \
		bool start = true; \
		printf("Heap["); \
		for(size_t i=0; i<h->size; i++) { \
			if(!start) printf(", "); \
			printf(fmt, h->data[i]); \
			start = false; \
		} \
		printf("]\n"); \
	} \

#define HEAP_IMPL_FULL(i, T, fmt) \
	HEAP_IMPL(i, T) \
	HEAP_IMPL_PRINT(i, T, fmt)
