#include "arena_allocator.h"
#include "dlist_impl.h"
#include "util.h"
#include <unistd.h>
// TODO: On which systems can I enable this?
//#define USE_MMAP

#ifdef USE_MMAP
#include <sys/mman.h>
#endif

bool dj_eq_memzone(struct s_memzone a, struct s_memzone b) {
	return a.allocated == b.allocated && a.mem == b.mem && a.capacity == b.capacity;
}
DLIST_IMPL(memzone, struct s_memzone)

void init_memarena(MemoryArena* a) {
	init_dlist_memzone(&a->zones);
	a->capacity = 0;
	a->allocated = 0;
	a->minimum_bs = sysconf(_SC_PAGESIZE);
}

static void free_zone(struct s_memzone* zone) {
#ifdef USE_MMAP
	munmap(zone->mem, zone->capacity);
#else
	free(zone->mem);
#endif
}

void deinit_memarena(MemoryArena* a) {
	dlist_foreach_memzone(&a->zones, free_zone);
	dlist_clear_memzone(&a->zones);
	a->capacity = 0;
	a->allocated = 0;
}

#define fits_in_zone(zone, requested) (zone->capacity - zone->allocated >= requested)

static inline void malloc_impl(struct s_memzone* current_zone) {
#ifdef USE_MMAP
	current_zone->mem = mmap(NULL, current_zone->capacity, PROT_READ|PROT_WRITE, MAP_PRIVATE|MAP_ANONYMOUS, 0, 0);
#else
	current_zone->mem = malloc(current_zone->capacity);
#endif
}

char* memarena_alloc(MemoryArena* a, size_t n) 
{
	struct s_memzone* current_zone = dlist_last_memzone(&a->zones);
	if(current_zone == NULL || !fits_in_zone(current_zone, n)) {
		struct s_memzone new_zone = {.capacity = MAX(n, a->minimum_bs), 
			                         .allocated = 0, 
									 .mem = NULL};
		dlist_append_memzone(&a->zones, new_zone);
		current_zone = dlist_last_memzone(&a->zones);
		if(current_zone == NULL) return NULL;
		malloc_impl(current_zone);
		a->capacity += current_zone->capacity;
	}
	assert(current_zone != NULL);
	char* ptr = &current_zone->mem[current_zone->allocated];
	current_zone->allocated += n;
	a->allocated += n;

	return ptr;
}

#define VECTORIZE_ZERO(T) \

char* memarena_calloc(MemoryArena* a, const size_t n)
{
	char* b = memarena_alloc(a, n);
	if(b == NULL) return NULL;
	memset(b, 0, n);
	/*if(n % sizeof(long) == 0) {
		long* c = (long*) b;
		for(size_t i=0; i<n/sizeof(long); i++)
			c[i] = 0;
	} else {
		for(size_t i=0; i<n; i++)
			b[i] = 0;
	}*/
	return b;
}
