#define _POSIX_C_SOURCE 200112L
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/mman.h>
#include <errno.h>
#include <fcntl.h>
#include <time.h>

#include <wayland-client-core.h>
#include <wayland-client-protocol.h>
#include <xkbcommon/xkbcommon.h>

#include "wayland/wayland_window.h"
#include "wayland/djwindow_keyboard_wayland.h"
#include "wayland/djwindow_toplevel_wayland.h"
#include "wayland/djwindow_pointer_wayland.h"
#include "logger.h"
#include "wayland/xdg-shell-client-protocol.h"
#ifndef DJWINDOW_NO_DECORATION
#include "wayland/xdg-decoration-client-protocol.h"
#endif
#ifndef DJWINDOW_NO_ICON
#include "wayland/xdg-toplevel-icon-client-protocol.h"
#endif

// EGL test

static void wl_buffer_release(void* data, struct wl_buffer* buffer)
{
	wl_buffer_destroy(buffer);
}
static const struct wl_buffer_listener wl_buffer_listener = {
	.release = wl_buffer_release,
};

static void randname(char* buf)
{
	struct timespec ts;
	clock_gettime(CLOCK_REALTIME, &ts);
	long r = ts.tv_nsec;
	for(int i=0; i<6; i++) {
		buf[i] = 'A' + (r&15) + (r&16)*2;
		r >>= 5;
	}
}
static int create_shm_file(void)
{
	int retries = 100;
	do {
		char name[] = "/wl_shm-XXXXXX";
		randname(name +sizeof(name)-7);
		--retries;
		int fd=shm_open(name, O_RDWR | O_CREAT | O_EXCL, 0600);
		if(fd >=0) {
			shm_unlink(name);
			return fd;
		}
	} while(retries > 0 && errno == EEXIST);
	return -1;
}
static int allocate_shm_file(size_t size)
{
	int fd=create_shm_file();
	if (fd < 0)
		return -1;
	int ret;
	do {
		ret = ftruncate(fd, size);
	} while(ret < 0 && errno == EINTR);
	if(ret < 0) {
		close(fd);
		return -1;
	}
	return fd;
}
static struct wl_buffer* test_drw_frame(struct s_djwindow* win)
{
	const int width = dj_win_width(win);
	const int height = dj_win_height(win);
	int stride = width*4;
	int size = stride*height;

	int fd=allocate_shm_file(size);
	if(fd == -1) 
		return NULL;

	uint32_t* data = mmap(NULL, size, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
	if(data == MAP_FAILED) {
		close(fd);
		return NULL;
	}

	struct wl_shm_pool* pool = wl_shm_create_pool(win->wl_shm, fd, size);
	struct wl_buffer* buffer = wl_shm_pool_create_buffer(pool, 0, width, height, stride, WL_SHM_FORMAT_XRGB8888);
	wl_shm_pool_destroy(pool);
	close(fd);

    int offset = (int) win->offset % 8;
	for(int y=0; y<height; ++y) {
		for(int x=0; x<width; ++x) {
            if (((x + offset) + (y + offset) / 8 * 8) % 16 < 8)
				data[y * width + x] = 0xFF666666;
			else
				data[y * width + x] = 0xFFEEEEEE;
		}
	}
	munmap(data, size);
	wl_buffer_add_listener(buffer, &wl_buffer_listener, NULL);
	return buffer;
}
static void wl_seat_capabilities(void* data, struct wl_seat* seat, uint32_t caps)
{
	struct s_djwindow* win = data;

	if((caps & WL_SEAT_CAPABILITY_POINTER) && !win->input.pointer) {
		win->input.pointer = wl_seat_get_pointer(seat);
		wl_pointer_add_listener(win->input.pointer, &pointer_listener, win);
	}
	if((caps & WL_SEAT_CAPABILITY_KEYBOARD) && !win->input.keyboard) {
		win->input.keyboard = wl_seat_get_keyboard(seat);
		wl_keyboard_add_listener(win->input.keyboard, &keyboard_listener, win);
	}
}

static void wl_seat_name(void* data, struct wl_seat* seat, const char* name)
{
	log_info("seat name: %s", name);
}

static const struct wl_seat_listener wl_seat_listener = {
	.capabilities = wl_seat_capabilities,
	.name = wl_seat_name
};

static void xdg_wm_base_ping(void* data, struct xdg_wm_base* xdg_wm_base, uint32_t serial)
{
	xdg_wm_base_pong(xdg_wm_base, serial);
}

static const struct xdg_wm_base_listener xdg_wm_base_listener = {
	.ping = xdg_wm_base_ping,
};

// Implemented below
static const struct wl_callback_listener wl_surface_frame_listener;

static void wl_surface_done(void* data, struct wl_callback* cb, uint32_t time)
{
    DJWindow* window = (DJWindow*) data;
	// This is only relevant for direct mode rendering
	if(window->draw_type != DRW_X11)
		return;

    /* Destroy this callback */
    wl_callback_destroy(cb);

    /* Request another frame */
    cb = wl_surface_frame(window->surface);
    wl_callback_add_listener(cb, &wl_surface_frame_listener, window);

    /* Update scroll amount at 24 pixels per second */
    if (window->last_frame != 0) {
        int elapsed = time - window->last_frame;
        window->offset += elapsed / 1000.0 * 24;
    }

    /* Submit a frame for this event */
    struct wl_buffer* buffer = test_drw_frame(window);
    wl_surface_attach(window->surface, buffer, 0, 0);
    wl_surface_damage_buffer(window->surface, 0, 0, INT32_MAX, INT32_MAX);
    wl_surface_commit(window->surface);
    window->last_frame = time;
}

static const struct wl_callback_listener wl_surface_frame_listener = {
        .done = wl_surface_done
};

static void xdg_surface_configure(void* data, struct xdg_surface* xdg_surface, uint32_t serial)
{
	DJWindow* win = data;
	xdg_surface_ack_configure(xdg_surface, serial);

	// TODO: We will no longer do this in the future with opengl
	if(win->draw_type == DRW_X11) {
		struct wl_buffer* buffer = test_drw_frame(win);
		wl_surface_attach(win->surface, buffer, 0, 0);
		wl_surface_commit(win->surface);
	}
}
const struct xdg_surface_listener xdg_surface_listener = {
	.configure = xdg_surface_configure,
};

void wl_output_scale(void* data, struct wl_output* output, int factor) {
}
void wl_output_geometry(void* data, struct wl_output* wl_output, int32_t x, int32_t y,
		int32_t physical_width, int32_t physical_height, int32_t subpixel,
		const char* make, const char* model,
		int32_t transform)
{
}
void wl_output_mode(void* data, struct wl_output* output, uint32_t flags,
		int32_t width, int32_t height, int32_t refresh)
{
}
void wl_output_done(void* data, struct wl_output* output)
{
	// TODO: When this object is sent, all information was relayed and we should apply the changes
}
void wl_output_name(void* data, struct wl_output* output, const char* name)
{
	log_info("The output is called %s", name);
}
void wl_output_description(void* data, struct wl_output* output, const char* description)
{
	log_info("I'd describe the output as %s", description);
}
static struct wl_output_listener wl_output_listener = {
	.geometry = wl_output_geometry,
	.mode = wl_output_mode,
	.done = wl_output_done,
	.scale = wl_output_scale,
	.name = wl_output_name,
	.description = wl_output_description,
};

static void wl_surface_enter(void* data, struct wl_surface* wl_surface, struct wl_output* output)
{
}
static void wl_surface_leave(void* data, struct wl_surface* wl_surface, struct wl_output* output)
{
}
static void wl_surface_preferred_scale(void* data, struct wl_surface* wl_surface, int32_t factor)
{
	DJWindow* win = (DJWindow*) data;
	win->scaling_factor = factor;
	wl_surface_set_buffer_scale(win->surface, win->scaling_factor);
	wl_egl_window_resize(win->egl.window, win->scaling_factor*dj_win_width(win), win->scaling_factor*dj_win_height(win), 0, 0);
}
static void wl_surface_preferred_transform(void* data, struct wl_surface* wl_surfae, uint32_t transform)
{
}
static const struct wl_surface_listener surface_listener = {
	.enter = wl_surface_enter,
	.leave = wl_surface_leave,
	.preferred_buffer_scale = wl_surface_preferred_scale,
	.preferred_buffer_transform = wl_surface_preferred_transform
};

static void registry_handle_global(void* data, struct wl_registry* registry,
		uint32_t name, const char* interface, uint32_t version)
{
	struct s_djwindow* win = data;
	if(!strcmp(interface, wl_shm_interface.name)) {
		// Shared memory
		win->wl_shm = wl_registry_bind(registry, name, &wl_shm_interface, 1);
	} else if(!strcmp(interface, wl_compositor_interface.name)) {
		// Compositor
		win->compositor = wl_registry_bind(registry, name, &wl_compositor_interface, 6);
	} else if (!strcmp(interface, wl_seat_interface.name)) {
		// Input
		win->input.seat = wl_registry_bind(registry, name, &wl_seat_interface, 7);
		wl_seat_add_listener(win->input.seat, &wl_seat_listener, win);
	} else if(!strcmp(interface, xdg_wm_base_interface.name)) {
		// XDG
		win->xdg.wm_base = wl_registry_bind(registry, name, &xdg_wm_base_interface, 1);
		xdg_wm_base_add_listener(win->xdg.wm_base, &xdg_wm_base_listener, win);
	} else if(!strcmp(interface, zxdg_decoration_manager_v1_interface.name)) {
		// Decoration
		win->xdg.decoration_manager = wl_registry_bind(registry, name, &zxdg_decoration_manager_v1_interface, 1);
	} else if(!strcmp(interface, xdg_toplevel_icon_manager_v1_interface.name)) {
		// Icon manager
		win->xdg.icon_manager = wl_registry_bind(registry, name, &xdg_toplevel_icon_manager_v1_interface, 1);
	} else if(!strcmp(interface, wl_output_interface.name)) {
		win->wl_output = wl_registry_bind(registry, name, &wl_output_interface, 2);
		wl_output_add_listener(win->wl_output, &wl_output_listener, win);
	}
}
static void registry_handle_global_remove(void* data, struct wl_registry* registry,
		uint32_t name)
{
}

const struct wl_registry_listener registry_listener = {
	.global = registry_handle_global,
	.global_remove = registry_handle_global_remove
};

DJWindow* dj_win_init(DrawType drawtype)
{
    switch(drawtype) {
        case(DRW_X11): {
            log_error("How dare you use X11 drawing on wayland. 1990 calls and wants their drawing back");
            // TODO: actually run this when we support actually other drawing APIs
            //return NULL;
			break;
        } case(DRW_GL): {
			break;
        } case(DRW_VULKAN): {
            log_error("EGL is not yet implemented for Vulkan :/");
            return NULL;
        }
    }

	DJWindow* win = calloc(1, sizeof(DJWindow));
	if(!win) return NULL;
	win->draw_type = drawtype;
	win->dims = (struct s_dims) {.x = 640, .y=480};

	if(connect_wayland(win)) {
		free(win);
		return NULL;
	}

	// TODO: make this more modifiable
	if(win->draw_type == DRW_GL && egl_init(win, 3, 0)) {
		// TODO: Unreference all the wayland stuff created
		free(win);
		return NULL;
	}

	if(win->draw_type == DRW_X11 && !win->wl_shm) {
		log_error("Failed to create wl_shm global");
	}


	win->xkb.context = xkb_context_new(XKB_CONTEXT_NO_FLAGS);

	wl_surface_add_listener(win->surface, &surface_listener, win);

#ifndef DJWINDOW_NO_DECORATION
	if(!win->xdg.decoration_manager) {
		log_warn("Could not find a decoration manager");
	} else {
		// We get pretty server side decoration :3
		struct zxdg_toplevel_decoration_v1* deco = zxdg_decoration_manager_v1_get_toplevel_decoration(win->xdg.decoration_manager, win->xdg.toplevel); 
		zxdg_toplevel_decoration_v1_set_mode(deco, ZXDG_TOPLEVEL_DECORATION_V1_MODE_SERVER_SIDE);
	}
#endif
#ifndef DJWINDOW_NO_ICON
	if(!win->xdg.icon_manager) {
		log_warn("Could not find a toplevel icon manager. No icon will be set");
	}
#endif

	if(win->draw_type == DRW_X11) {
		struct wl_callback* cb = wl_surface_frame(win->surface);
		wl_callback_add_listener(cb, &wl_surface_frame_listener, win);
	}

	DJEvent event = (DJEvent) {
		.resize.type = WinResizeEvent,
		.resize.width = win->dims.x,
		.resize.height = win->dims.y
	};
	dispatch_event(win, WinResizeEvent, &event);
	return win;
}

void dj_win_close(DJWindow** win)
{
	if(!(*win)) return;
	wl_surface_destroy((*win)->surface);
	wl_compositor_destroy((*win)->compositor);
	wl_registry_destroy((*win)->registry);
	wl_display_disconnect((*win)->display);
	free(*win);
	*win = NULL;
}

void dj_win_update(DJWindow *win)
{
	if(!win) return;
	wl_display_dispatch(win->display);
}

void dj_win_set_title(DJWindow* win, const char* str)
{
	xdg_toplevel_set_title(win->xdg.toplevel, str);
	wl_surface_commit(win->surface);
}

bool dj_win_get_fullscreen(const DJWindow* window) {
	return window->is_fullscreen;
}
void dj_win_set_fullscreen(DJWindow* window, bool value) {
	if(value) {
		xdg_toplevel_set_fullscreen(window->xdg.toplevel, NULL);
		window->is_fullscreen = true;
	} else {
		xdg_toplevel_unset_fullscreen(window->xdg.toplevel);
		window->is_fullscreen = false;
	}
}
void dj_win_set_icon(DJWindow* win, int width, int height, const unsigned char* icon_rgba, const char* icon_name)
{
	if(!win->xdg.icon_manager)
		return;
#ifndef DJWINDOW_NO_ICON
	struct xdg_toplevel_icon_v1* icon = xdg_toplevel_icon_manager_v1_create_icon(win->xdg.icon_manager);
	xdg_toplevel_icon_v1_set_name(icon, icon_name);

	xdg_toplevel_icon_manager_v1_set_icon(win->xdg.icon_manager, win->xdg.toplevel, icon);
	xdg_toplevel_icon_v1_destroy(icon);
#else
	log_warn("Window icon setting was disable at compile time with DJWINDOW_NO_ICON");
#endif
}

unsigned int dj_win_width(const DJWindow* window)
{
    return window->dims.x;
}
unsigned int dj_win_height(const DJWindow* window)
{
    return window->dims.y;
}

void dj_win_flip(DJWindow* win)
{
	if(win->draw_type != DRW_GL) {
		log_warn("Undefined action for flipping window with draw type %d", win->draw_type);
		return;
	}
	eglSwapBuffers(win->egl.display, win->egl.surface);
}

#include "../djwindow_common.h"
