// TODO: Linux header guard
#include "linux/input-event-codes.h"

#include "wayland/wayland_window.h"
#include "wayland/djwindow_pointer_wayland.h"
#include "keycodes.h"
#include "logger.h"

const struct wl_pointer_listener pointer_listener = {
        .enter = pointer_handle_enter,
        .leave = pointer_handle_leave,
        .motion = pointer_handle_motion,
        .button = pointer_handle_button,
        .axis = pointer_handle_axis,
        .frame = pointer_handle_frame,
		.axis_source = pointer_handle_axis_source,
		.axis_stop = pointer_handle_axis_stop,
		.axis_discrete = pointer_handle_axis_discrete,
		.axis_value120 = pointer_handle_axis_value120,
		.axis_relative_direction = pointer_handle_axis_relative_direction
};

static MouseButton wl_button_mapping(uint32_t btn) {
    switch(btn) {
        case(BTN_LEFT): return MouseLeft;
        case(BTN_RIGHT): return MouseRight;
        case(BTN_MIDDLE): return MouseMiddle;
    }
    return MouseUnknown;
}

void pointer_handle_enter(void *data, struct wl_pointer *pointer,
                                 uint32_t serial, struct wl_surface *surface,
                                 wl_fixed_t sx, wl_fixed_t sy) {
    log_debug("Entering screen");
}
void pointer_handle_leave(void *data, struct wl_pointer *pointer,
                                 uint32_t serial, struct wl_surface *surface) {
    log_debug("Left screen");
}
void pointer_handle_motion(void *data, struct wl_pointer *pointer,
                                  uint32_t time, wl_fixed_t sx, wl_fixed_t sy) {
    // TODO: Adjust the mouse position to relative to window and not absolute on the screen
    struct s_djwindow* win = data;
    DJEvent ev = {
            .mousemove = { .type = MouseMotionEvent, .x = sx, .y = sy }
    };
    dispatch_event(win, MouseMotionEvent, &ev);
}
void pointer_handle_button(void *data, struct wl_pointer *wl_pointer,
                                  uint32_t serial, uint32_t time, uint32_t button,
                                  uint32_t state) {
    DJWindow* win = data;
    MouseButton btn = wl_button_mapping(button);
    if(state) {
        DJEvent ev = { .buttonpress = {
                .type = MousePressEvent,
                .x=-1, .y=-1,
                .button=btn,
        }};
        dispatch_event(win, MousePressEvent, &ev);
    } else {
        DJEvent ev = { .buttonrelease = {
                .type = MouseReleaseEvent,
                .x=-1, .y=-1, // TODO: Actually know this
                .button=btn,
        }};
        dispatch_event(win, MouseReleaseEvent, &ev);
    }
}

typedef enum {Wheel, Finger, Continuus, WheelTilt} AxisSource; 
void pointer_handle_axis(void *data, struct wl_pointer *wl_pointer,
                                uint32_t time, uint32_t axis_int, wl_fixed_t value)
{
    //log_debug("Axis event. What is this?");
	AxisSource axis = axis_int;
	(void) axis;

}
void pointer_handle_frame(void* data, struct wl_pointer* wl_pointer) {
}
void pointer_handle_axis_relative_direction(void* data, struct wl_pointer* pointer,
		uint32_t axis_int, uint32_t direction)
{
	AxisSource axis = axis_int;
	(void) axis;
}
void pointer_handle_axis_value120(void* data, struct wl_pointer* pointer,
							uint32_t axis_int, int32_t value120)
{
	AxisSource axis = axis_int;
	(void) axis;
}
void pointer_handle_axis_discrete(void* data, struct wl_pointer* pointer,
							uint32_t axis_int, int32_t discrete)
{
	AxisSource axis = axis_int;
    DJWindow* win = data;
	if(axis == Wheel) {
		DJEvent ev = (DJEvent) { .mousescroll = {
			.type = MouseScrollEvent,
			.direction = discrete
		}};
		dispatch_event(win, MouseScrollEvent, &ev);
	}
}
void pointer_handle_axis_stop(void* data, struct wl_pointer* pointer,
							uint32_t time, uint32_t axis_int)
{
	AxisSource axis = axis_int;
	(void) axis;
}
void pointer_handle_axis_source(void* data, struct wl_pointer* pointer,
							uint32_t time)
{
}
