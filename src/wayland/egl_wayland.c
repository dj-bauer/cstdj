#include "wayland/wayland_window.h"
#include "logger.h"
#include <stdio.h>
#include <assert.h>

void print_config(DJWindow* window, EGLint id, EGLConfig cfg);

int egl_init(DJWindow* window, int gl_major, int gl_minor)
{
	EGLint egl_version[2];
	EGLint num_configs;
	EGLint fb_requirements[] = {
		EGL_SURFACE_TYPE, EGL_WINDOW_BIT,
		EGL_RENDERABLE_TYPE, EGL_OPENGL_BIT,
		EGL_DEPTH_SIZE, 24,
		EGL_RED_SIZE, 8,
		EGL_GREEN_SIZE, 8,
		EGL_BLUE_SIZE, 8,
		EGL_NONE
	};
	
	// We want desktop OpenGL
	// make changable if we want OpenGL ES in the future
	eglBindAPI(EGL_OPENGL_API);


	window->egl.window = wl_egl_window_create(window->surface, window->dims.x, window->dims.y);
	if(!window->egl.window) {
		log_error("Failed to create EGL window");
		return 1;
	}
	
	window->egl.display = eglGetDisplay((EGLNativeDisplayType) window->display);
	if(window->egl.display == EGL_NO_DISPLAY) {
		log_error("Couldn't get EGL display");
		return 1;
	}

	if(eglInitialize(window->egl.display, &egl_version[0], &egl_version[1]) != EGL_TRUE) {
		log_error("Couldn't initialize EGL");
		return 1;
	} else {
		log_debug("Initialized EGL v%d.%d", egl_version[0], egl_version[1]);
	}

	if(eglChooseConfig(window->egl.display, fb_requirements, &window->egl.fb_config, 1, &num_configs) != EGL_TRUE) {
		log_error("Couldn't find a suitable EGL framebuffer config");
		return 1;
	}
	print_config(window, 0, window->egl.fb_config);

	window->egl.surface = eglCreateWindowSurface(window->egl.display, window->egl.fb_config, (EGLNativeWindowType) window->egl.window, NULL);
	if(window->egl.surface == EGL_NO_SURFACE) {
		log_error("Couldn't create EGL surface");
		return 1;
	}

	EGLint ctx_attribs[] = {
		EGL_CONTEXT_MAJOR_VERSION, gl_major,
		EGL_CONTEXT_MINOR_VERSION, gl_minor,
		EGL_CONTEXT_OPENGL_FORWARD_COMPATIBLE, EGL_TRUE,
		EGL_CONTEXT_OPENGL_PROFILE_MASK, EGL_CONTEXT_OPENGL_CORE_PROFILE_BIT, 
		EGL_NONE
	};
	window->egl.context = eglCreateContext(window->egl.display, window->egl.fb_config, EGL_NO_CONTEXT, ctx_attribs);
	if(window->egl.context == EGL_NO_CONTEXT) {
		log_error("Failed to create EGL context");
		return 1;
	}

	if(!eglMakeCurrent(window->egl.display, window->egl.surface, window->egl.surface, window->egl.context)) {
		log_error("Failed to make this EGL context current");
		return 1;	
	}

	return 0;
}

static const char* int_to_bool(EGLint v) {
	if(v == EGL_TRUE)
		return "EGL_TRUE";
	else if(v == EGL_FALSE)
		return "EGL_FALSE";
	assert(false);
	return "";
}
void print_config(DJWindow* window, EGLint id, EGLConfig cfg)
{
	static int queries[] = {EGL_ALPHA_SIZE, EGL_ALPHA_MASK_SIZE, 
		EGL_BIND_TO_TEXTURE_RGB, EGL_BIND_TO_TEXTURE_RGBA, EGL_BLUE_SIZE, EGL_BUFFER_SIZE, 
		EGL_COLOR_BUFFER_TYPE, EGL_CONFIG_CAVEAT, EGL_CONFIG_ID, EGL_CONFORMANT, 
		EGL_DEPTH_SIZE, 
		EGL_GREEN_SIZE, 
		EGL_LEVEL, EGL_LUMINANCE_SIZE, 
		EGL_MAX_PBUFFER_WIDTH, EGL_MAX_PBUFFER_HEIGHT, EGL_MAX_PBUFFER_PIXELS, EGL_MAX_SWAP_INTERVAL, EGL_MIN_SWAP_INTERVAL, 
		EGL_NATIVE_RENDERABLE, EGL_NATIVE_VISUAL_ID, EGL_NATIVE_VISUAL_TYPE,
		EGL_RED_SIZE, EGL_RENDERABLE_TYPE,
		EGL_SAMPLE_BUFFERS, EGL_SAMPLES, EGL_STENCIL_SIZE, EGL_SURFACE_TYPE,
		EGL_TRANSPARENT_TYPE, EGL_TRANSPARENT_RED_VALUE, EGL_TRANSPARENT_GREEN_VALUE, EGL_TRANSPARENT_BLUE_VALUE};
	EGLint values[sizeof(queries)/sizeof(*queries)];
	for(int i=0; i<sizeof(queries)/sizeof(*queries); i++) {
		eglGetConfigAttrib(window->egl.display, cfg, queries[i], &values[i]);
	}
	printf("==== Config %d ==== \n", id);
	int i=0;
	printf("\t%s: %d\n", "EGL_ALPHA_SIZE", values[i++]);
	printf("\t%s: %d\n", "EGL_ALPHA_MASK_SIZE", values[i++]);
	printf("\t%s: %s\n", "EGL_BIND_TO_TEXTURE_RGB", int_to_bool(values[i++]));
	printf("\t%s: %s\n", "EGL_BIND_TO_TEXTURE_RGBA", int_to_bool(values[i++]));
	printf("\t%s: %d\n", "EGL_BLUE_SIZE", values[i++]);
	printf("\t%s: %d\n", "EGL_BUFFER_SIZE", values[i++]);
	printf("\t%s: %s\n", "EGL_COLOR_BUFFER_TYPE", values[i] == EGL_RGB_BUFFER ? "EGL_RGB_BUFFER" : (values[i] == EGL_LUMINANCE_BUFFER ? "EGL_LUMINANCE_BUFFER" : "INVALID")); i++;
	printf("\t%s: %s\n", "EGL_CONFIG_CAVEAT", values[i] == EGL_NONE ? "EGL_NONE" : (values[i] == EGL_SLOW_CONFIG ? "EGL_SLOW_CONFIG" : "INVALID")); i++;
	printf("\t%s: %d\n", "EGL_CONFIG_ID", values[i++]);
	printf("\t%s: %d\n", "EGL_CONFORMANT", values[i++]);
	printf("\t%s: %d\n", "EGL_DEPTH_SIZE", values[i++]);
	printf("\t%s: %d\n", "EGL_GREEN_SIZE", values[i++]);
	printf("\t%s: %d\n", "EGL_LEVEL", values[i++]);
	printf("\t%s: %d\n", "EGL_LUMINANCE_SIZE", values[i++]);
	printf("\t%s: %d\n", "EGL_MAX_PBUFFER_WIDTH", values[i++]);
	printf("\t%s: %d\n", "EGL_MAX_PBUFFER_HEIGHT", values[i++]);
	printf("\t%s: %d\n", "EGL_MAX_PBUFFER_PIXELS", values[i++]);
	printf("\t%s: %d\n", "EGL_MAX_SWAP_INTERVAL", values[i++]);
	printf("\t%s: %d\n", "EGL_MIN_SWAP_INTERVAL", values[i++]);
	printf("\t%s: %s\n", "EGL_NATIVE_RENDERABLE", int_to_bool(values[i++]));
	printf("\t%s: %d\n", "EGL_NATIVE_VISUAL_ID", values[i++]);
	printf("\t%s: %d\n", "EGL_NATIVE_VISUAL_TYPE", values[i++]);
	printf("\t%s: %d\n", "EGL_RED_SIZE", values[i++]);
	printf("\t%s: %d\n", "EGL_RENDERABLE_TYPE", values[i++]);
	printf("\t%s: %d\n", "EGL_SAMPLE_BUFFERS", values[i++]);
	printf("\t%s: %d\n", "EGL_SAMPLES", values[i++]);
	printf("\t%s: %d\n", "EGL_STENCIL_SIZE", values[i++]);
	printf("\t%s: %d\n", "EGL_SURFACE_TYPE", values[i++]);
	printf("\t%s: %s\n", "EGL_TRANSPARENT_TYPE", values[i] == EGL_NONE ? "EGL_NONE" : (values[i] == EGL_TRANSPARENT_RGB ? "EGL_TRANSPARENT_RGB" : "INVALID")); i++;
	printf("\t%s: %d\n", "EGL_TRANSPARENT_RED_VALUE", values[i++]);
	printf("\t%s: %d\n", "EGL_TRANSPARENT_GREEN_VALUE", values[i++]);
}
