#include "wayland/djwindow_toplevel_wayland.h"
#include "wayland/wayland_window.h"

void xdg_toplevel_configure(void* data, struct xdg_toplevel* xdg_toplevel,
		int32_t width, int32_t height, struct wl_array* states)
{
	DJWindow* win = (DJWindow*) data;
	if(width == 0 || height == 0) {
		/* Compoitor is deferring to us */
		return;
	}
	// New dimensions
	if(win->dims.x != width || win->dims.y != height) {
		win->dims.x = width;
		win->dims.y = height;

		if(win->draw_type == DRW_GL) {
			wl_egl_window_resize(win->egl.window, win->scaling_factor*width, win->scaling_factor*height, 0, 0);
			wl_surface_commit(win->surface);
		}
	}
	DJEvent event = (DJEvent) {
		.resize.type = WinResizeEvent,
		.resize.width = width,
		.resize.height = height
	};
	dispatch_event(win, WinResizeEvent, &event);
}
void xdg_toplevel_close(void* data, struct xdg_toplevel* xdg_toplevel)
{
	DJWindow* window = (DJWindow*) data;
	DJEvent ev = {.close.type = WinCloseEvent, .close.win = window};
	dispatch_event(window, WinCloseEvent, &ev);
}
void xdg_toplevel_configure_bounds(void* data, struct xdg_toplevel* xdg_toplevel,
		int32_t width, int32_t height)
{
}
void xdg_toplevel_wm_capabilities(void* data, struct xdg_toplevel* xdg_toplevel,
		struct wl_array* capabilities)
{
}

const struct xdg_toplevel_listener xdg_toplevel_listener = {
	.configure = xdg_toplevel_configure,
	.close = xdg_toplevel_close,
	.configure_bounds = xdg_toplevel_configure_bounds,
	.wm_capabilities = xdg_toplevel_wm_capabilities
};
