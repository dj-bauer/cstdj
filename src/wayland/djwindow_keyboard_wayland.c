#include <assert.h>
#include <sys/mman.h>
#include <unistd.h>
#include <xkbcommon/xkbcommon.h>

#include "wayland/djwindow_keyboard_wayland.h"
#include "wayland/wayland_window.h"
#include "logger.h"
#include "keycodes.h"

KeySymbol wl_key_to_dj(uint32_t wl, const char* descr);

const struct wl_keyboard_listener keyboard_listener = {
	.keymap 	 = keyboard_handle_keymap,
	.enter  	 = keyboard_handle_enter,
	.leave  	 = keyboard_handle_leave,
	.key    	 = keyboard_handle_key,
	.modifiers   = keyboard_handle_modifiers,
	.repeat_info = keyboard_handle_repeat,
};

void keyboard_handle_keymap(void* data, struct wl_keyboard* keyboard, uint32_t format, int32_t fd, uint32_t size)
{
	log_debug("Received new keymap");
	assert(format == WL_KEYBOARD_KEYMAP_FORMAT_XKB_V1);
	DJWindow* win = (DJWindow*) data;

	char* map_shm = mmap(NULL, size, PROT_READ, MAP_PRIVATE, fd, 0);
	assert(map_shm != MAP_FAILED);
	win->xkb.keymap = xkb_keymap_new_from_string(win->xkb.context, map_shm, XKB_KEYMAP_FORMAT_TEXT_V1, XKB_KEYMAP_COMPILE_NO_FLAGS);

	munmap(map_shm, size);
	close(fd);

	win->xkb.state = xkb_state_new(win->xkb.keymap);
}

void keyboard_handle_enter(void* data, struct wl_keyboard* keyboard, uint32_t serial, struct wl_surface* surface, struct wl_array* keys)
{
	// DJWindow* win = (DJWindow*) data;
	log_debug("Keyboard focused");
}

void keyboard_handle_leave(void* data, struct wl_keyboard* keyboard, uint32_t serial, struct wl_surface* surface)
{
	// DJWindow* win = (DJWindow*) data;
	log_debug("Keyboard unfocused");
}

void keyboard_handle_modifiers(void* data, struct wl_keyboard* keyboard, uint32_t serial, uint32_t mods_pressed, uint32_t mods_latched, uint32_t mods_locked, uint32_t group)
{
	DJWindow* win = (DJWindow*) data;
	xkb_state_update_mask(win->xkb.state, mods_pressed, mods_latched, mods_locked, 0, 0, group);
}

void keyboard_handle_key(void* data, struct wl_keyboard* keyboard, uint32_t serial, uint32_t time, uint32_t key, uint32_t state)
{
	DJWindow* win = (DJWindow*) data;
	xkb_keysym_t sym = xkb_state_key_get_one_sym(win->xkb.state, key + 8);

	char buf[128];
	xkb_keysym_get_name(sym, buf, sizeof(buf));

	char ch_buf[128];
	xkb_state_key_get_utf8(win->xkb.state, key + 8, ch_buf, sizeof(ch_buf));

	DJEventType type = state == WL_KEYBOARD_KEY_STATE_PRESSED ? KeyPressEvent : KeyReleaseEvent;
	DJEvent ev;
	KeySymbol dj_key = wl_key_to_dj(sym, buf);
	if(type == KeyPressEvent) {
		// TODO: Add my modifier
		ev = (DJEvent) {.keypress = {.type = type, .win = win, .key = dj_key, .utf8_ch = ch_buf, .utf8_name = buf}};
	} else {
		ev = (DJEvent) {.keyrelease = {.type = type, .key = dj_key}};
	}
	dispatch_event(win, type, &ev);
}

void keyboard_handle_repeat(void* data, struct wl_keyboard* keyboard, int32_t rate, int32_t delay)
{
}

KeySymbol wl_key_to_dj(uint32_t wl, const char* descr)
{
	switch(wl) {
		case 32: return SpaceKey;
		case 101: return E_Key;
		case 115: return S_Key;
		case 0xffe9: return Alt_L;
		case 65307: return EscapeKey;
		case 65293: return ReturnKey;
		case 65480: return F11;
		case 65505: return ShiftKey;
	}
	log_warn("Unknown key to map %d (%s)", wl, descr);
	return SpaceKey;
}
