#include "wayland/wayland_window.h"
#include "wayland/djwindow_toplevel_wayland.h"
#include "logger.h"

extern const struct wl_registry_listener registry_listener;
extern const struct xdg_surface_listener xdg_surface_listener;

int connect_wayland(DJWindow* win)
{
	win->display = wl_display_connect(NULL);
	if(!win->display) {
		log_error("Failed to connect to Wayland Display");
		return 1;
	}

	win->registry = wl_display_get_registry(win->display);
	if(!win->registry) {
		log_error("Failed to get display registry");
		wl_display_disconnect(win->display);
		return 1;
	}
	wl_registry_add_listener(win->registry, &registry_listener, win);
	wl_display_roundtrip(win->display);

	if(!win->compositor) {
		log_error("Could not find the requested compositor");
		wl_registry_destroy(win->registry);
		wl_display_disconnect(win->display);
		return 1;
	}

	if(!win->input.seat) {
		log_error("Could not find an input_seat");
		wl_compositor_destroy(win->compositor);
		wl_registry_destroy(win->registry);
		wl_display_disconnect(win->display);
		return 1;
	}

	win->surface = wl_compositor_create_surface(win->compositor);
	if(!win->surface) {
		log_error("Failed to create surface");
		wl_compositor_destroy(win->compositor);
		wl_registry_destroy(win->registry);
		wl_display_disconnect(win->display);
		return 1;
	}

	win->xdg.surface = xdg_wm_base_get_xdg_surface(win->xdg.wm_base, win->surface);
	xdg_surface_add_listener(win->xdg.surface, &xdg_surface_listener, win);

	win->xdg.toplevel = xdg_surface_get_toplevel(win->xdg.surface);
	xdg_toplevel_add_listener(win->xdg.toplevel, &xdg_toplevel_listener, win);

	wl_surface_commit(win->surface);

	return 0;
}
