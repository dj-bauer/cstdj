#include "bstree_impl.h"
#include "cmp.h"

BST_IMPL_ALL(i, int, "%d")
BST_IMPL_ALL(d, double, "%f")
BST_IMPL_ALL(f, float, "%f")
BST_IMPL_ALL(s, char*, "%s")
BST_IMPL_ALL(cs, const char*, "%s")
BST_IMPL_ALL(v, void*, "%p")
