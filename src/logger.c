#include "logger.h"
#include "termcolor.h"
#include <stdio.h>
#include <time.h>
#include <stdarg.h>

static const char* level_colors[] = {
	  ANSI_MAGENTA, ANSI_BLUE, ANSI_GREEN, ANSI_YELLOW, ANSI_RED
};
static const char* level_names[] = {
	"TRACE", "DEBUG", "INFO", "WARNING", "ERROR"
};

void log_func(logLevel lvl, const char* filename, unsigned int line, const char* format, ...) {
	time_t t;
	time(&t);
	struct tm* current_time = localtime(&t);
	char time[11];
	strftime(time, sizeof(time), "[%H:%M:%S]", current_time);

	char msg[256];
	va_list ap;
	va_start(ap, format);
	vsnprintf(msg, sizeof(msg), format, ap);
	va_end(ap);

	fprintf(lvl == LogError ? stderr : stdout, ANSI_LIGHT_GRAY"%s %s%s%s%s %s%s:%d%s %s\n", time,
			ANSI_BOLD, level_colors[lvl], level_names[lvl], ANSI_RESET, 
			ANSI_GRAY, filename, line, ANSI_RESET,
			msg);
}
