/* Copyright (c) 2019 John Schember <john@nachtimwald.com>

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the “Software”), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
#include "tpool.h"
#include <pthread.h>
#include <stdlib.h>

struct tpool_work {
	thread_func_t func;
	void* arg;
	struct tpool_work* next;
};
typedef struct tpool_work tpool_work_t;

struct tpool {
	tpool_work_t* work_first;
	tpool_work_t* work_last;
	pthread_mutex_t work_mutex;
	pthread_cond_t work_cond;
	pthread_cond_t working_cond;
	size_t working_count;
	size_t thread_count;
	bool stop;
};

static tpool_work_t* tpool_work_create(thread_func_t func, void* arg) {
	if(func == NULL) return NULL;

	tpool_work_t* work;
	work = malloc(sizeof(tpool_work_t));
	if(work == NULL) return NULL;
	work->func = func;
	work->arg = arg;
	work->next = NULL;
	return work;
}

static void tpool_work_destroy(tpool_work_t* work) {
	if(work == NULL) return;

	free(work);
}

static tpool_work_t* tpool_work_get(tpool_t* tp) {
	if(tp == NULL) return NULL;

	tpool_work_t* work = tp->work_first;
	if(work == NULL) return NULL;

	tp->work_first = work->next;
	if(work->next == NULL) {
		tp->work_last = NULL;
	}

	return work;
}

static void* tpool_worker(void* arg) {
	tpool_t* tp = arg;

	while(true) {
		pthread_mutex_lock(&tp->work_mutex);

		while(tp->work_first == NULL && !tp->stop)
			pthread_cond_wait(&tp->work_cond, &tp->work_mutex);

		if(tp->stop)
			break;

		tpool_work_t* work = tpool_work_get(tp);
		tp->working_count++;
		pthread_mutex_unlock(&tp->work_mutex);

		if(work != NULL) {
			work->func(work->arg);
			tpool_work_destroy(work);
		}

		pthread_mutex_lock(&tp->work_mutex);
		tp->working_count--;
		if(!tp->stop && tp->working_count == 0 && tp->work_first == NULL)
			pthread_cond_signal(&tp->working_cond);
		pthread_mutex_unlock(&tp->work_mutex);
	}
	tp->thread_count--;
	pthread_cond_signal(&tp->working_cond);
	pthread_mutex_unlock(&tp->work_mutex);

	return NULL;
}

tpool_t* tpool_create(size_t num) {
	if(num == 0) num = 2;

	tpool_t* tp = calloc(1, sizeof(tpool_t));
	if(tp == NULL) return NULL;
	tp->thread_count = num;

	pthread_mutex_init(&tp->work_mutex, NULL);
	pthread_cond_init(&tp->work_cond, NULL);
	pthread_cond_init(&tp->working_cond, NULL);

	tp->work_first = NULL;
	tp->work_last = NULL;

	for(int i=0; i<num; i++) {
		pthread_t t;
		pthread_create(&t, NULL, tpool_worker, tp); 
		pthread_detach(t);
	}

	return tp;
}

void tpool_destroy(tpool_t* tp)
{
	if(tp == NULL) return;

	pthread_mutex_lock(&tp->work_mutex);

	tpool_work_t* work = tp->work_first;
	tpool_work_t* next_work;
	while(work != NULL) {
		next_work = work->next;
		tpool_work_destroy(work);
		work = next_work;
	}
	tp->stop = true;
	pthread_cond_broadcast(&tp->work_cond);
	pthread_mutex_unlock(&tp->work_mutex);

	tpool_wait(tp);

	pthread_mutex_destroy(&tp->work_mutex);
	pthread_cond_destroy(&tp->work_cond);
	pthread_cond_destroy(&tp->working_cond);

	free(tp);
}

bool tpool_add_work(tpool_t* tp, thread_func_t func, void* arg)
{
	if(tp == NULL) return false;

	tpool_work_t* work = tpool_work_create(func, arg);
	if(work == NULL) return false;

	pthread_mutex_lock(&tp->work_mutex);
	if(tp->work_first == NULL) {
		tp->work_first = work;
		tp->work_last = work;
	} else {
		tp->work_last->next = work;
		tp->work_last = work;
	}
	pthread_cond_broadcast(&tp->work_cond);
	pthread_mutex_unlock(&tp->work_mutex);

	return true;
}

void tpool_wait(tpool_t* tp) {
	if(tp == NULL) return;

	pthread_mutex_lock(&tp->work_mutex);
	while(true) {
		if((!tp->stop && tp->working_count != 0) || (tp->stop && tp->thread_count != 0)) {
			pthread_cond_wait(&tp->working_cond, &tp->work_mutex);
		} else {
			break;
		}
	}
	pthread_mutex_unlock(&tp->work_mutex);
}
