#include "djtest.h"
#include "logger.h"

#include <dirent.h>
#include <assert.h>

bool cmp_mem_eq(const char* a, const char* b, size_t amount)
{
	for(size_t i=0; i<amount; i++) {
		if(*a != *b) {
			log_error("Memcmp failed");
			fwrite(a, sizeof(char), amount-i, stderr);
			fwrite("\n", 1, 1, stderr);
			fwrite(b, sizeof(char), amount-i, stderr);
			fwrite("\n", 1, 1, stderr);
			return false;
		}
		a++;
		b++;
	}
	return true;
}

int test_inout(const char* directory, int (*t_func)(const char*))
{
	DIR* d;
	struct dirent* dir;
	d = opendir(directory);
	if(d) {
		while((dir = readdir(d)) != NULL) {
			if(dir->d_name[0] == '.')
				continue;
			size_t l = strlen(dir->d_name);
			if(l <= 3) {
				log_error("Test file `%s` doesn't have file ending `.in`. Skipping", dir->d_name);
				continue;
			}
			if(strcmp(dir->d_name+l-3, ".in")) {
				log_error("Test file `%s` doesn't have file ending `.in`. Skipping", dir->d_name);
				continue;
			}

			char myname[l+1];
			strncpy(myname, dir->d_name, l);
			myname[l-3] = '\0';
			if(t_func(myname)) {
				closedir(d);
				return 1;
			}
		}
		closedir(d);
	} else {
		log_error("Cannot find test directory %s", directory);
	}
	return 0;
}
