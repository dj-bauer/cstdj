#include "heap_impl.h"

HEAP_IMPL_FULL(i, int, "%d")
HEAP_IMPL_FULL(f, float, "%f")
HEAP_IMPL_FULL(s, char*, "%s")
HEAP_IMPL_FULL(u64, uint64_t, "%"PRIu64)
HEAP_IMPL_FULL(v, void*, "%p")
