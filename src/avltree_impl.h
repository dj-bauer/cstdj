#include "avltree.h"
#include "./cmp.h"
#include "./bst_util.h"
#include <assert.h>

#define set_left(root, child) \
	root->left = child; \
	if(child != NULL) child->parent = root;
#define set_right(root, child) \
	root->right = child; \
	if(child != NULL) child->parent = root;
#define is_leaf(node) \
	((node)->left == NULL && (node)->right == NULL)

/** 
 * @brief Implements the avl functions for a given type
 * @param i suffix for all the functions
 * @param T datatype
 **/
#define AVL_IMPL(i, T) \
	/* Performs a left rotation around the root n and returns the new root */ \
	inline struct s_bstnode_##i* rotate_left_##i(struct s_bstnode_##i* x) { \
		assert(x != NULL); \
		struct s_bstnode_##i* z = x->right; \
		assert(z != NULL); \
		struct s_bstnode_##i* t23 = z->left; \
		set_right(x, t23); \
		z->parent = x->parent; \
		set_left(z, x); \
		if(x->balance == 0) { \
			x->balance += 1; \
			z->balance -= 1; \
		} else { \
			x->balance = 0; \
			z->balance = 0; \
		} \
		assert(-1 <= x->balance && x->balance <= 1); \
		assert(-1 <= z->balance && z->balance <= 1); \
		return z; \
	} \
	struct s_bstnode_##i* rotate_right_##i(struct s_bstnode_##i* x) { \
		assert(x != NULL); \
		struct s_bstnode_##i* z = x->left; \
		assert(z != NULL); \
		struct s_bstnode_##i* t23 = z->right; \
		set_left(x, t23); \
		z->parent = x->parent; \
		set_right(z, x); \
		if(x->balance == 0) { \
			x->balance -= 1; \
			z->balance += 1; \
		} else { \
			x->balance = 0; \
			z->balance = 0; \
		} \
		assert(-1 <= x->balance && x->balance <= 1); \
		assert(-1 <= z->balance && z->balance <= 1); \
		return z; \
	} \
	inline struct s_bstnode_##i* rotate_left_right_##i(struct s_bstnode_##i* z) { \
		struct s_bstnode_##i* new_y = rotate_left_##i(z->left); \
		z->left = new_y; \
		new_y->parent = z; \
		return rotate_right_##i(z); \
	} \
	inline struct s_bstnode_##i* rotate_right_left_##i(struct s_bstnode_##i* z) { \
		struct s_bstnode_##i* new_y = rotate_right_##i(z->right); \
		z->right = new_y; \
		new_y->parent = z; \
		return rotate_left_##i(z); \
	} \
	 /**
	   Can be seen here: https://www.geeksforgeeks.org/insertion-in-an-avl-tree/
	 **/ \
	static inline void replace_at_parent_##i(struct s_bstree_##i* t, \
			                                 struct s_bstnode_##i* parent, \
			                                 struct s_bstnode_##i* z, \
											 struct s_bstnode_##i* new_z) { \
		assert(t != NULL); \
		assert(z != NULL);  \
		assert(new_z != NULL); \
		if(parent != NULL) { \
			if(z == parent->left) { \
				parent->left = new_z; \
			} else { \
				assert(z == parent->right); \
				parent->right = new_z; \
			} \
		} else {\
			t->root = new_z; \
		} \
		new_z->parent = parent; \
	} \
	static inline void bal_on_insert_##i(struct s_bstree_##i* t, \
								  struct s_bstnode_##i* z, \
								  struct s_bstnode_##i* y, \
								  struct s_bstnode_##i* x) { \
		assert(z != NULL); \
		assert(y != NULL); \
		assert(x != NULL); \
		assert(z->balance < -1 || z->balance > 1); \
		struct s_bstnode_##i* parent = z->parent; \
		struct s_bstnode_##i* new_z = NULL; \
		if(z->left == y && y->left == x) { \
			/* left-left case*/ \
			new_z = rotate_right_##i(z); \
		} else if(z->right == y && y->right == x) {\
			/* right-right case*/ \
			new_z = rotate_left_##i(z); \
		} else if(z->left == y && y->right == x) { \
			/* left-right case*/ \
			new_z = rotate_left_right_##i(z); \
		} else if(z->right == y && y->left == x) {\
			/* right-left case*/ \
			new_z = rotate_right_left_##i(z); \
		} else { assert(0 && "Invalid case. No left-left, left-right, right-right, right-left"); } \
		replace_at_parent_##i(t, parent, z, new_z); \
	} \
	static void rek_increase_balance_factor_##i(struct s_bstree_##i* t, \
			                                    struct s_bstnode_##i* parent,  \
			                                    struct s_bstnode_##i* child,  \
												struct s_bstnode_##i* grandchild) { \
		assert(child != NULL); \
		if(parent == NULL) return; /* TODO: what if the root node is the parent? */ \
		if(child == parent->left) parent->balance--; \
		else {\
			assert(child == parent->right); \
			parent->balance++; \
		} \
		if(parent->balance < -1 || parent->balance > 1) { \
			bal_on_insert_##i(t, parent, child, grandchild); \
		} \
		if(parent->balance == 0) return; \
		rek_increase_balance_factor_##i(t, parent->parent, parent, child); \
	} \
	bool avl_insert_##i(BSTree_##i* tree, T value) { \
		bool success; \
		if(tree->root == NULL) { \
			assert(tree->size == 0); \
			tree->root = create_node_##i(value, NULL); \
			success = true; \
		} else { \
			struct s_bstnode_##i* n = node_insert_##i(tree->root, value); \
			success = n != NULL;\
            if(success) {        \
                /* Update parents balance factor */ \
                rek_increase_balance_factor_##i(tree, n->parent, n, NULL);              \
            }                           \
		} \
		if(success) tree->size++; \
		return success; \
	} \
	bool avl_remove_##i(BSTree_##i* t, T value) { \
		if(t == NULL) return false; \
		size_t old_size = t->size; \
		/*struct s_bstnode_##i* old_node = node_search_##i(t->root, value);*/ \
		node_remove_##i(t, t->root, value); \
		return old_size != t->size; \
	}
