#include "osm.h"
#include "svg.h"
#include <assert.h>

#define LINE_COLOR HEX_BLACK
#define BG_COLOR HEX_GRAY
#define ERROR_COLOR "#ff00ff"

const char* osm_natural_colors(enum e_natural_subtypes t) {
	if(t > UnclassifiedNatural) return ERROR_COLOR;
	const char* colors[UnclassifiedNatural+1] = {
#define X(id, str, c) [id] = c,
		OSMNATURE_FOREACH(X)
#undef X
	};
	return colors[t];
}

const char* osm_landuse_colors(enum e_landuse_subtypes t) {
	if(t >= UnknownLanduse) return ERROR_COLOR;
	const char* colors[UnknownLanduse+1] = {
#define X(id, str, c) [id] = c,
		OSMLANDUSE_FOREACH(X)
#undef X
	};
	return colors[t];
}

const char* osm_leisure_colors(enum e_leisure_subtypes t)
{
	if(t >= UnknownLeisure) return ERROR_COLOR;
	const char* colors[UnknownLeisure] = {
#define X(id, str, c) [id] = c,
		OSMLEISURE_FOREACH(X)
#undef X
	};
	return colors[t];
}

static inline const char* road_color(const struct s_osmway* way) {
	assert(way->data.type == Highway);
	assert(way->data.subtypes.highway <= UnclassifiedHighway);
#define X(id, str, c) [id] = c,
	static const char* tbl[UnclassifiedHighway+1] = {
		OSMROADS_FOREACH(X)
	};
#undef X
	return tbl[way->data.subtypes.highway];
}

const char* way_color(const struct s_osmway* way) {
	if(way->data.type > UnknownType) return ERROR_COLOR;
	if(way->data.type == CustomType)
		return way->data.subtypes.custom.color;
	if(way->data.type == Highway) {
		return road_color(way);
	} else if(way->data.type == Natural) {
		return osm_natural_colors(way->data.subtypes.natural);
	} else if(way->data.type == Landuse) {
		return osm_landuse_colors(way->data.subtypes.landuse);
	} else if(way->data.type == Leisure) {
		return osm_leisure_colors(way->data.subtypes.leisure);
	}
	static const char* colors[] = {
		[Tourism] = "#ff5555",
		[Waterway] = HEX_BLUE,
		[Leisure] = ERROR_COLOR,
		[Power] = HEX_BLUE,
		[Building] = "#3771c8",
		[Highway] = ERROR_COLOR,
		[Barrier] = HEX_RED,
		[Amenity] = "#aa8800",
		[ManMade] = "#9955ff",
		[Landuse] = ERROR_COLOR,
		[Landcover] = "#5aa02c",
		[Natural] = "#32a852",
		[Area] = HEX_GRAY,
		[Razed] = ERROR_COLOR,
		[Railway] = HEX_BLACK,
		[Wall] = "#3d3846",
		[Playground] = "#ffcc00",
		[Historic] = "#ff6600",
		[Attraction] = "#ff9966",
		[Seamark] = "#9999ff",
		[EmptyType] = ERROR_COLOR,
		[Roof] = ERROR_COLOR,
		[UnknownType] = ERROR_COLOR,
	};
	return colors[way->data.type];
}
