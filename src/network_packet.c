#include "network_packet.h"
#include <arpa/inet.h>
#include <stdlib.h>
#include <string.h>
#include "logger.h"

#ifndef __GNUC__
#error "this Implementation of ntohll is currently just a hotfix :/"
#endif
uint64_t ntohll(uint64_t x) {
    return __bswap_64(x);
}
uint64_t htonll(uint64_t x) {
    return __bswap_64(x);
}

NetworkBuf* netbuf_create(size_t size)
{
    NetworkBuf* b = malloc(sizeof(NetworkBuf) + size*sizeof(unsigned char));
    if(b == NULL)
        return NULL;
    b->size = size;
    b->ptr = 0;
    return b;
}

void netbuf_free(NetworkBuf* buf)
{
    free(buf);
}


#define write_number(name, T, convSuffix) \
int write_##name(NetworkBuf* buf, T var) { \
    if(buf->size < buf->ptr + sizeof(T)) \
        return BUFFER_EOF;                \
    ((T*) (&buf->data[buf->ptr]))[0] = hton##convSuffix(var);                                                      \
    buf->ptr += sizeof(T);               \
    return BUFFER_FINE;                            \
}

#define read_number(name, T, convSuffix) \
int read_##name(T* var, NetworkBuf* buf) { \
    if(buf->size < buf->ptr + sizeof(T)) \
        return BUFFER_EOF;               \
    T tmp = ((T*) (&buf->data[buf->ptr]))[0]; \
    *var = ntoh##convSuffix(tmp);        \
    buf->ptr += sizeof(T);               \
    return BUFFER_FINE;                  \
}

#define handle_number(name, T, suffix) \
write_number(name, T, suffix)          \
read_number(name, T, suffix)

handle_number(short, int16_t, s)
handle_number(int, int32_t, l)
handle_number(long, int64_t, ll)

int write_float(NetworkBuf* buf, float var) {
    union { int32_t i; float f; } u;
    u.f = var;
    return write_int(buf, u.i);
}
int read_float(float* var, NetworkBuf* buf) {
    return read_int((int32_t*) var, buf);
}

int write_double(NetworkBuf* buf, double var) {
    union { int64_t l; double d; } u;
    u.d = var;
    return write_long(buf, u.l);
}
int read_double(double* var, NetworkBuf* buf) {
    return read_long((int64_t*) var, buf);
}

int write_str(NetworkBuf* buf, const char* str)
{
    uint32_t num_bytes = 0;
    size_t len = strlen(str);
    if(len > INT32_MAX) {
        return BUFFER_STR_TOO_LARGE;
    }
    num_bytes = (uint32_t) len;
    int err = write_int(buf, num_bytes);
    if(err) return err;

    if(buf->size < buf->ptr + num_bytes)
        return BUFFER_EOF;
    memcpy(&buf->data[buf->ptr], str, num_bytes);
    buf->ptr += num_bytes;
    return BUFFER_FINE;
}
int read_str(char** str, NetworkBuf* buf)
{
    uint32_t num_bytes;
    int err = read_int((int32_t*) &num_bytes, buf);
    if(err) return 1;

    *str = malloc(num_bytes + 1);
    if(*str == NULL)
        return BUFFER_MALLOC_FAILED;
    (*str)[num_bytes] = '\0';
    memcpy(*str, &buf->data[buf->ptr], num_bytes);
    buf->ptr += num_bytes;
    return BUFFER_FINE;
}

#undef handle_number
#undef write_number
#undef read_number
