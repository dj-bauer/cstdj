#include "xml.h"
#include "logger.h"
#include "util.h"
#include <assert.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <ctype.h>
#include <alloca.h>
#include <errno.h>
#include "fast_strlen.h"

// We allocate maximum 100KB on the stack. Everything else is on the heap.
// This should be properly set if on limited hardware such as embedded systems
#define MAX_STACK_ALLOCATION 102400

enum HierarchyMove {MoveUp, MoveDown, MoveStay};

static xml_node* create_dom_node(MemoryArena* alloc, xml_dom* dom, xml_node* current_node)
	NONNULL(1, 2);
enum HierarchyMove process_tag(xml_dom* dom, xml_node** current_node, const char* tag_str)
	NONNULL(1, 2) NULL_TERMINATED(3);
xml_node* add_child(MemoryArena* alloc, xml_node* current_node)
	NONNULL(1, 2);
void attach_attribute(MemoryArena* alloc, xml_node* node, char* key, char* value)
	NONNULL(1, 2) PROPER_STRING(3) PROPER_STRING(4);
void process_attributes(MemoryArena* alloc, xml_node* node, const char* attr_str)
	NONNULL(1, 2) NULL_TERMINATED(3);
bool check_close_tag(xml_node** current_node, const char* tag_str)
	NONNULL(1, 2) NULL_TERMINATED(2);
void handle_node_content(MemoryArena* alloc, xml_node* current_node, const char* end_of_tag)
	NONNULL(1, 3);
static inline char* arena_strndup(MemoryArena* alloc, const char* src, size_t n) {
	char* value = memarena_alloc(alloc, n+1);
	strncpy(value, src, n);
	value[n] = '\0';
	return value;
}

/**
 * Adds a new childnode to the given node
 * @param current_node must not be null
 * @return a reference to the newly attached child node
 */
xml_node* add_child(MemoryArena* alloc, xml_node* current_node)
{
	xml_node* new_node;

	if(!current_node->children) {
		// If we don't have a child yet, we need to create one
		// This functions as the head of our linked list
		current_node->children = (xml_node*) memarena_calloc(alloc, sizeof(xml_node));
		new_node = current_node->children;
        current_node->last_child = new_node;
	} else {
		// We attach a child at the end of the linked list
		new_node = (xml_node*) memarena_calloc(alloc, sizeof(xml_node));
        current_node->last_child->next = new_node;
        current_node->last_child = new_node;
	}
	new_node->parent = current_node;
	return new_node;
}

void attach_attribute(MemoryArena* alloc, xml_node* node, char* key, char* value)
{
	if(node->last_attr == NULL) {
		node->attributes = (xml_attr*) memarena_alloc(alloc, sizeof(xml_attr));
        node->last_attr = node->attributes;
	} else {
		node->last_attr->next = (xml_attr*) memarena_alloc(alloc, sizeof(xml_attr));
        node->last_attr = node->last_attr->next;
	}
	node->last_attr->tag = key;
    node->last_attr->content = value;
}

void process_attributes(MemoryArena* alloc, xml_node* node, const char* attr_str)
{
	while(attr_str) {
		if(!(*attr_str)) return;

		// Skip whitespaces in the beginning
		while(isspace(*attr_str)) {
			attr_str+=1;
			if(!(*attr_str)) return;
		}
		const char* equal_sign = strchr(attr_str, '=');
		const char* first_quotes = strchr(attr_str, '\'');
		if(!first_quotes) first_quotes = strchr(attr_str, '\"');
		if(!equal_sign || first_quotes < equal_sign) { // Somebody forgot the equal sign for the attributes
			return;
		}

		// Remove whitespaces between key and =
		const char* end_of_key = equal_sign;
		while(isspace(*(end_of_key-1))) end_of_key--; // Strip whitespaces between key and =
		char* key = arena_strndup(alloc, attr_str, end_of_key - attr_str);

		// Obtain the value
		char quote_type;// What kind of quotes are we using
		const char*	value_begin = equal_sign;
		while(*value_begin != '\'' && *value_begin != '"') value_begin+=1;
		quote_type = *value_begin;
		value_begin+=1;// Get to the first char of value
		const char* value_end = strchr(value_begin, quote_type);
		assert(value_end); // We need to get a closing quote
		char* value = arena_strndup(alloc, value_begin, value_end - value_begin);
						
		attach_attribute(alloc, node, key, value);

		attr_str = value_end+1;
	}
}

/**
 * Checks if the tag needs to be closing and if updates the current_node
 */
bool check_close_tag(xml_node** current_node, const char* tag_str)
{
	if(tag_str[0] == '/') {
		tag_str = tag_str+1;
		assert(!strcmp((*current_node)->tag, tag_str));
		*current_node = (*current_node)->parent;
		return true;
	} 
	return false;
}

xml_node* create_dom_node(MemoryArena* alloc, xml_dom* dom, xml_node* current_node)
{
	if(!current_node) {
		dom->root = (xml_node*) memarena_calloc(alloc, sizeof(xml_node));
		if(dom->root == NULL)
			return NULL;
		return dom->root;
	}
	return add_child(alloc, current_node);
}

char* first_pointer(char* a, char* b) {
	if(!a) return b;
	if(!b) return a;
	return a < b ? a : b;
}

static size_t my_index=0;
/**
 * Processes the tag.
 * Generates new child elements
 * Could go back up a level if the node is nodes
 */
enum HierarchyMove process_tag(xml_dom* dom, xml_node** current_node, const char* tag_str) {
	// Check for comment
	if(tag_str[0] == '!' && tag_str[1] == '-' && tag_str[2] == '-') {
		size_t len = strlen(tag_str);
		if(*(tag_str + len-2) == '-' && *(tag_str+len-1) == '-') return MoveStay;
	}

	// We're done if the tag is just being closed
	if(check_close_tag(current_node, tag_str)) return MoveUp;

	*current_node = create_dom_node(&dom->memory, dom, *current_node);


	// The number of characters the node tag has (excluding attributes)
	size_t tag_len = strlen(tag_str);
	// See if we have a space in the tag, then we got to check for attributes
	char* space_indices[6] = {strchr(tag_str, ' '),
							  strchr(tag_str, '\n'),
							  strchr(tag_str, '\r'),
							  strchr(tag_str, '\v'),
							  strchr(tag_str, '\t'),
							  strchr(tag_str, '\f')};
	char* space_index = space_indices[0];
	for(size_t i=0; i<6; i++) {
		space_index = first_pointer(space_index, space_indices[i]);
	}
	if(space_index) {
		process_attributes(&dom->memory, *current_node, space_index+1);
		tag_len = (space_index - tag_str);
	}
	(*current_node)->tag = arena_strndup(&dom->memory, tag_str, tag_len);
	//log_debug("%zu Created new node called %s", my_index, (*current_node)->tag);
	my_index++;
	return MoveDown;
}

void handle_node_content(MemoryArena* alloc, xml_node* current_node, const char* end_of_tag)
{
	if(!current_node) return;
	const char* next_tag;
	if((next_tag = strchr(end_of_tag, '<'))) {
		const char* content_start = end_of_tag+1;
		while(isspace(*content_start)) {
			content_start+=1;
			if(content_start >= next_tag) break;
		}
		if(content_start < next_tag) {
			current_node->content = arena_strndup(alloc, content_start, next_tag-content_start);
		}
	}
}

xml_dom* xml_parse_string(const char* data)
{
	xml_dom* dom = calloc(1, sizeof(xml_dom));
	if(dom == NULL)
		return NULL;
	init_memarena(&dom->memory);
	xml_node* current_node = NULL;

	// Skip the eventual UTF-16 BOM
	if(((unsigned char) data[0] == 0xFE && (unsigned char) data[1] == 0xFF) || 
	   ((unsigned char) data[1] == 0xFE && (unsigned char) data[0] == 0xFF)) {
		log_error("We are currently only supporting utf-8 documents");
		free(dom);
		return NULL;
	}

	const char* token;
	while((token = strchr(data, '<'))) {
		bool instant_exit = false;
		
		bool is_prologue = strlen_gt(token, 2) && *(token+1) == '?'; // The <?...?> prologue
		bool is_dtd = strlen_ge(token, 9) && !strncmp(token+1, "!DOCTYPE", 8);
		if(is_dtd) {
			int dtd_bracket_count = 0;
			do {
				if(!*token) { 
					log_error("The brackets in the DTD don't match up. Encountered EOF");
					xml_free(dom);
					return NULL;
				}
				dtd_bracket_count += (*token == '<');
				dtd_bracket_count -= (*token == '>');
				token+=1;
			} while(dtd_bracket_count != 0);
			data = token;
			continue;
		}

		const char* end_of_tag = strchr(token, '>') - 1;
		
		// handle cases like <foo/> or <example attr="yes"/>
		if(*end_of_tag == '/') {
			end_of_tag -= 1;
			instant_exit = true;
		}

		int tag_len = end_of_tag - token;
		char* tag_str = strndup(token+1, tag_len);
		if(tag_str == NULL) // allocation of temporary tag string has failed
			return NULL;
		if(!(is_prologue)) {
			enum HierarchyMove action = process_tag(dom, &current_node, tag_str);

			if(instant_exit) {
				if(action == MoveUp) {
					log_error("Your are trying to close yourself and another tag at the same time like: </tag/>");
					xml_free(dom);
					return NULL;
				}
				current_node = current_node->parent; // <foo />
			} else {
				handle_node_content(&dom->memory, current_node, end_of_tag+1);
			}
		}
		free(tag_str);

		data = token + tag_len + 1;
	}
	// The current node must have been handed up back again
	if(current_node) {
		log_error("Not all tags have been closed");
		xml_free(dom);
		return NULL;
	}

	return dom;
}

xml_dom* xml_parse_file(FILE* f)
{
	fseek(f, 0, SEEK_END);
	size_t length = ftell(f);
	fseek(f, 0, SEEK_SET);
	char* data;
	if(length+1 > MAX_STACK_ALLOCATION) {
		data = malloc((length+1)*sizeof(char));
		if(data == NULL)
			return NULL;
	} else {
		data = alloca((length+1)*sizeof(char));
	}
	data[length] = '\0';
	fread(data, 1, length, f);

	xml_dom* d = xml_parse_string(data);
	if(length+1 > MAX_STACK_ALLOCATION) {
		free(data);
	}

	return d;
}

xml_dom* xml_read_file(const char* filename)
{
	FILE* f = fopen(filename, "r");
	if(f == NULL) {
		log_error("Could not open file %s", strerror(errno));
		return NULL;
	}

	xml_dom* dom = xml_parse_file(f);

	fclose(f);

	return dom;
}

void xml_free(xml_dom* dom)
{
	if(!dom) return;
	//free_node(dom->root);
	deinit_memarena(&dom->memory);
	free(dom);
}

#define MANY_TABS "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t"
/**
 * Recursively prints an xml node and all its content, attributes and children
 */
void print_node(const xml_node* node, int num_rek)
{
	if(!node) return;
	if(node->content)
		printf("%.*s%s [%s]", num_rek, MANY_TABS, node->tag, node->content);
	else
		printf("%.*s%s", num_rek, MANY_TABS, node->tag);

	xml_attr* attr = node->attributes;
	while(attr) {
		printf("\t%s=\"%s\"%s", attr->tag, attr->content, attr->next ? "" : "\n");
		attr = attr->next;
	}

	print_node(node->children, num_rek+1);
	print_node(node->next, num_rek);
}
void xml_print_dom(const xml_dom* dom)
{
	print_node(dom->root, 0);
}

