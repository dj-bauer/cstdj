#include "sort_fun.h"
#include <assert.h>

inline static size_t partition(int values[], size_t l, size_t r) 
{
	int t;
	int x = values[r]; // pivot wert
	size_t i=l; 
	for(size_t j=l; j<r; j++) {
		if(values[j] <= x) {
			t = values[i];
			values[i] = values[j];
			values[j] = t;
			i++;
		}
	}
	t = values[i];
	values[i] = values[r];
	values[r] = t;
	return i;
}

struct s_tuple {
	size_t x;
	size_t y;
};

struct s_tuple partition_dp(int a[], size_t l, size_t r)
{
	if(a[r-1] > a[r]) {
		int t = a[r-1];
		a[r-1] = a[r];
		a[r] = t;
	}
	assert(a[r-1] < a[r]);

	size_t p1 = partition(a, l, r-1);
	size_t p2 = partition(a, p1+1, r);

	return (struct s_tuple) {p1, p2};
}

void qsort_rek_i_dp(int values[], size_t l, size_t r)
{
	if(l >= r) return;
	struct s_tuple pivots = partition_dp(values, l, r);
	if(pivots.x > 0) 
		qsort_rek_i_dp(values, l, pivots.x-1);
	if(pivots.y > 0) 
		qsort_rek_i_dp(values, pivots.x+1, pivots.y-1);
	qsort_rek_i_dp(values, pivots.y+1, r);
}

inline void qsort_i_double_pivot(size_t n, int values[n])
{
	if(n < 1) return;
	qsort_rek_i_dp(values, 0, n-1);
}

void qsort_rek_i(int values[], size_t l, size_t r)
{
	if(l >= r) 
		return;
	size_t q = partition(values, l, r);

	if(q > 0) qsort_rek_i(values, l, q-1);
	qsort_rek_i(values, q+1, r);
}

inline void qsort_i(size_t n, int values[n])
{
	if(n < 1) return;
	qsort_rek_i(values, 0, n-1);
}
