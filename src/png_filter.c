#include <assert.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>

#include "png.h"
#include "logger.h"
#include "util.h"

void push_4_bits(Image* img, uint32_t num_bytes, uint8_t scanline[num_bytes], size_t line)
{
    // The width in bytes of one scanline of the buffered image
	uint32_t buf_width = img->width * img->nr_channels;

    unsigned char* buf_row = &img->data[line*buf_width];
    for(uint32_t byte = 0; byte<num_bytes; byte++) {
        uint8_t data = scanline[byte];
        buf_row[byte*2 + 0] = data >> 4;
        buf_row[byte*2 + 1] = data & 0xF; // (0b00001111)
    }
}
void push_8_bits(Image* img, uint8_t* scanline, size_t line)
{
	uint32_t bytes_per_scanline = img->width * img->nr_channels;
	memcpy(&img->data[line * bytes_per_scanline], scanline, bytes_per_scanline);
}
void push_16_bits(Image* img, uint8_t* scanline, size_t line)
{
	uint16_t* src = (uint16_t*) scanline;
	uint32_t numbers_per_scanline = img->width * img->nr_channels;
	for(uint32_t i=0; i<numbers_per_scanline; i++) {
		uint16_t v = src[i] / 2;
		img->data[line * numbers_per_scanline + i] = v;
	}
}

void filter_0(Image* img, size_t decompressed_size, const unsigned char decompressed_data[decompressed_size])
{
	uint8_t bpp = ceil(((double)img->bits_per_pixel) * ((double)img->nr_channels) / 8.0f);
	//log_debug("Bits per pixel = %d", img->bits_per_pixel);
	uint32_t bytes_per_scanline = img->width * img->nr_channels * (((double)img->bits_per_pixel) / 8.0);
	//log_debug("Bytes per scanline %d", bytes_per_scanline);
	const unsigned char* in_ptr = decompressed_data;

	uint8_t buffers[2][bytes_per_scanline];
	memset(buffers, 0, sizeof(uint8_t)*2*bytes_per_scanline);
	for(uint32_t line=0; line<img->height; line++) {
		uint8_t* scanline = buffers[line % 2];
		uint8_t* prev_scanline = buffers[(line+1) % 2];

		enum e_filters filter = *(in_ptr++);
		switch(filter) {
			case(FILTER_NONE):
				memcpy(scanline, in_ptr, bytes_per_scanline);
				break;
			case(FILTER_SUB):
				for(size_t i = 0; i<bytes_per_scanline; i++) {
					scanline[i] = in_ptr[i] + (i < bpp ? 0 : scanline[i-bpp]);
				}
				break;
			case(FILTER_UP):
				for(size_t i = 0; i < bytes_per_scanline; i++) {
					scanline[i] = in_ptr[i] + (line == 0 ? 0 : prev_scanline[i]);
				}
				break;
			case(FILTER_AVG):
				for(size_t i=0; i<bytes_per_scanline; i++) {
					uint16_t avg = (uint16_t) (i < bpp ? 0 : scanline[i-bpp]) + 
						           (uint16_t) (line == 0 ? 0 : prev_scanline[i]);
					avg /= 2;
					scanline[i] = in_ptr[i] + (uint8_t) avg;
				}
				break;
			case(FILTER_PAETH):
				for(size_t i=0; i<bytes_per_scanline; i++) {
					uint8_t a = (i < bpp )? 0 : scanline[i-bpp];
					uint8_t b = (line == 0) ? 0 : prev_scanline[i];
					uint8_t c = (line == 0 || i < bpp) ? 0 : prev_scanline[i-bpp];

					int16_t p = a + b - c;
					int16_t pa = abs(p - a);
					int16_t pb = abs(p - b);
					int16_t pc = abs(p - c);
					int16_t d = (pa <= pb && pa <= pc) ? a : (pb <= pc ? b : c);
					scanline[i] = in_ptr[i] + d;
				}
				break;
			default:
				log_error("Unknown filter type %d. Corrupt image?", filter);
				// TODO: Gracefully fail here instead of crashing
				ASSERT_NOT_REACHED
		}
		in_ptr += bytes_per_scanline;
		switch(img->bits_per_pixel) {
			case(4):
				push_4_bits(img, bytes_per_scanline, scanline, line);
				break;
			case(8):
				push_8_bits(img, scanline, line);
				break;
			case(16):
				//push_16_bits(img, scanline, line);
				//break;
			default:
				FAIL_NOT_IMPLEMENTED
		}
	}
	img->bits_per_pixel = 8;
}
