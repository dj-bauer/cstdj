#include "cmp.h"
#include "string.h"
#include <assert.h>

#define CMP_IMPL(i, T) \
	inline bool dj_eq_##i(T a, T b) {return a == b; } \
	inline bool dj_lt_##i(T a, T b) {return a < b; }
CMP_IMPL(i, int)
CMP_IMPL(f, float)
CMP_IMPL(d, double)
CMP_IMPL(v, void*)
CMP_IMPL(l, long)

inline bool dj_eq_cs(const char* a, const char* b) { 
	if(a == NULL || b == NULL) 
		return false; 
	return strcmp(a, b) == 0; 
}
inline bool dj_lt_cs(const char* a, const char* b) { 
	if(a == NULL|| b == NULL) 
		return false;
	return strcmp(a, b) < 0; 
}
