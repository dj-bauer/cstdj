#include "unicode/block_elements.h"
#include <stdint.h>

int u_to_utf8(char buf[5], uint32_t codepoint);
