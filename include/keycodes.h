/**
 * @file djtest.h
 * @brief Keyboard code abstraction to interface with multiple window systems
 * @author dbauer
 **/
#ifndef DJ_KEYCODES_H
#define DJ_KEYCODES_H

typedef enum {EscapeKey = 9, BackspaceKey = 22, W_Key = 25, ReturnKey = 36, 
	A_Key = 38, S_Key = 39, D_Key = 40, K_Key = 45, ShiftKey = 50, Alt_L = 64, SpaceKey = 65, F11 = 95} KeySymbol;
typedef enum {MouseLeft, MouseRight, MouseMiddle, MouseUnknown} MouseButton;

#endif
