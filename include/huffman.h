#ifndef DJ_HUFFMAN_H
#define DJ_HUFFMAN_H

#include <stdbool.h>
#include <stdint.h>
#include <stddef.h>
#include <stdio.h>

#include "attributes.h"

typedef struct s_huffmannode {
	uint16_t zero_child;
	uint16_t one_child;
	uint16_t symbol;
	bool has_symbol;
} HuffmanNode;

/** Adds a node symbol to a huffman tree with a given symbol and code and code length.
 *  The nodes are expected to be laid out in an array and reference each other through pointers.
 *  Nodes will be inserted one by one in the tree and therefore 
 *  not specifically layed out in relation to each other
 *  @param tree_size the number of nodes allocated for
 *  @param tree allocated list for nodes
 *  @param next_node_index a pointer to an index set to the index where a new node could be inserted into
 *  @param symbol to be referenced by the code
 *  @param code huffman code as a number
 *  @param code_length how many bits of the code should be used for the code generation
 **/
void huffman_add_node(size_t tree_size, HuffmanNode tree[tree_size], size_t* next_node_index, uint16_t symbol, uint16_t code, uint8_t code_length)
	NONNULL(3);
void huffman_print_tree(FILE* f, size_t n, const HuffmanNode tree[n], size_t max_node);

#endif
