#ifndef DJ_BITREADER_H
#define DJ_BITREADER_H
/**
 * @file bitreader.h
 * @author dbauer
 * @brief Implements a bitreader, which can be used to read individual bits from a stream.
 * @details for example this is used in the zlib implementation
 **/

#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>

/** @brief Simple cursor struct to store the offset in a stream**/
struct s_bitreader_cursor {
	/** @brief the number of bits offset in the current byte **/
	uint8_t bit_offset;
	/** @brief The number of bytes to skip **/
	size_t byte_offset;
};

/**
 * @brief A reader object which can read bit by bit from a given byte-stream
 **/
typedef struct s_bitreader {
	/** @brief A char stream to be read **/
	const uint8_t* stream;
	/** The total number of bytes in the stream **/
	size_t stream_length;
	/** @brief the number of bits offset in the current byte **/
	uint8_t bit_offset;
	/** @brief The number of bytes to skip **/
	size_t byte_offset;
} Bitreader;

/**
 * @brief Initializes a bitreader for a given stream
 * @param reader to be initialized
 * @param raw_stream bitstream
 * @param stream_length length of the bitstream
 **/
void init_bitreader(Bitreader* reader, const unsigned char* raw_stream, size_t stream_length);
/**
 * @brief Reads a single bit from the stream
 * @param reader stream to read from
 **/
uint8_t read_single_bit(Bitreader* reader);
/**
 * @brief Returns the bits read from the stream
 * @param reader stream to be read from
 * @param num_bits to be read
 * @param MSB flags sets whether the bytes should be interpreted in big endian or little endian
   if true the most significant byte will come first like in the huffman coding
 **/
uint8_t read_bit(Bitreader* reader, uint8_t num_bits, bool MSB);
/**
 * @brief Reads num_bits and interprets the result in MSB
 * @param reader stream read from
 * @param num_bits how many bits should be read
 **/
uint32_t read_most_significant_bit_first(Bitreader* reader, uint16_t num_bits);
/**
 * @brief Reads num_bits and interprets the result in LSB
 * @param reader stream read from
 * @param num_bits how many bits should be read
 **/
uint32_t read_least_significant_bit_first(Bitreader* reader, uint16_t num_bits);
/**
 * Reads a whole byte from the stream
 * Asserts, that the current bit offset is 0, so the reader is at the start of a new byte
 **/
uint8_t bitreader_read_byte(Bitreader* reader);
/**
 * Reads a uint32_t byte from the stream
 * Asserts, that the current bit offset is 0, so the reader is at the start of a new byte
 **/
uint32_t bitreader_read_quadbyte(Bitreader* reader, bool big_endian);
/**
 * Reads a uint32_t byte from the stream
 * Asserts, that the current bit offset is 0, so the reader is at the start of a new byte
 **/
uint16_t bitreader_read_doublebyte(Bitreader* reader, bool big_endian);
/**
 * @brief Checks whether the reader has data left to be read 
 * @param reader to be checked
 * **/
bool bitreader_has_data(const Bitreader* reader);
/**
 * Discards the rest of the bits in the current byte and jumps to the next byte
 **/
void bitreader_skip_bits(Bitreader* reader);
/**
 * Returns the current byte of the readers stream
 * Asserts, that the current bit offset is 0, so the reader is at the start of a new byte
 **/
const unsigned char* bitreader_stream(Bitreader* reader);
/**
 * Jumps the reader n bytes forward in the stream
 **/
void bitreader_skip_bytes(Bitreader* reader, size_t n);

#endif
