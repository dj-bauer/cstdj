#ifndef SORT_FUN_H
#define SORT_FUN_H
#include <stddef.h>

void qsort_i(size_t n, int values[n]);
void qsort_i_double_pivot(size_t n, int values[n]);

#endif
