/**
 * @file svg.h
 * @brief write svg structures to file
 * @details Cannot currently modify or parse an existing file
 * @author dbauer
 **/
#ifndef DJ_SVG_H
#define DJ_SVG_H
#include <stdio.h>
#include "polygon.h"

#define NONE "none"
#define HEX_DARKRED "#880000"
#define HEX_RED "#ff0000"
#define HEX_GREEN "#00ff00"
#define HEX_BLUE "#0000ff"
#define HEX_WHITE "#ffffff"
#define HEX_LIGHTGRAY "#adadad"
#define HEX_GRAY "#515151"
#define HEX_BLACK "#000000"
#define HEX_PURPLE "#993399"
#define HEX_DARKBLUE "#003399"

/** @brief An svg viewport.
 * Basically where to place the viewing window into the plane we're drawing into
 **/
typedef struct s_viewport{
	/** @brief upper left corner of the viewport **/
	vec2 min;
	/** @brief width and height of the viewport **/
	vec2 dims;
} viewport;

FILE* svg_open(const char* filename, viewport viewport, const char* bg_color);
void svg_rect(FILE* f, const char* color, const char* stroke_color, double x, double y, double width, double height, double stroke_width);
void svg_line(FILE* f, const char* color, double x0, double y0, double x1, double y1, double stroke_width);
void svg_line_id(FILE* f, const char* color, double x0, double y0, double x1, double y1, double stroke_width, const char* id);
void svg_line_class(FILE* f, const char* color, double x0, double y0, double x1, double y1, double stroke_width, const char* class);
void svg_circle_id(FILE* f, const char* stroke_color, 
		                    const char* fill_color, 
							double stroke_width, vec2 coords, double radius, 
							const char* id);
void svg_circle_class(FILE* f, const char* stroke_color, 
		                    const char* fill_color, 
							double stroke_width, vec2 coords, double radius, 
							const char* class);
void svg_circle_class_id(FILE* f, const char* stroke_color, 
		                    const char* fill_color, 
							double stroke_width, vec2 coords, double radius, 
							const char* class, const char* id);
void svg_polyline(FILE* f, const char* stroke_color, double stroke_width, size_t node_count, vec2[node_count]);
void svg_polyline_id(FILE* f, const char* stroke_color, double stroke_width, size_t node_count, vec2[node_count], const char* id);
void svg_polyline_class_id(FILE* f, const char* stroke_color, double stroke_width, size_t node_count, vec2[node_count], const char* class, const char* id);
void svg_polyline_style(FILE* f, const char* stroke_color, double stroke_width, size_t node_count, vec2 points[node_count], const char* style);
void svg_polyline_style_id(FILE* f, const char* stroke_color, double stroke_width, size_t node_count, vec2 points[node_count], const char* id, const char* style);
void svg_polyline_class_id_style(FILE* f, const char* stroke_color, double stroke_width, 
		size_t node_count, vec2 points[node_count], 
		const char* class, const char* id, 
		const char* style);

void svg_polygon_id_class(FILE* f, const char* stroke_color, const char* fill_color, double stroke_width, size_t node_count, const vec2 points[node_count], const char* id, const char* class);
void svg_polygon_id(FILE* f, const char* stroke_color, const char* fill_color, double stroke_width, size_t node_count, const vec2 points[node_count], const char* id);
void svg_polygon(FILE* f, const char* stroke_color, const char* fill_color, double stroke_width, size_t node_count, const vec2 points[node_count]);

void svg_path_id(FILE* f, const char* stroke_color, const char* fill_color, double stroke_width, size_t node_count, const vec2 points[node_count], const char* id);
void svg_path(FILE* f, const char* stroke_color, const char* fill_color, double stroke_width, size_t node_count, const vec2 points[node_count]);
void svg_begin_path(FILE* f);
void svg_begin_path_id(FILE* f, const char* id);
void svg_end_path(FILE* f, const char* fill_color, const char* stroke_color, double stroke_width);
void svg_subpath(FILE* f, size_t num_points, const vec2 points[num_points]);

void svg_close(FILE* f);
void svg_add_style(FILE* f, const char* def);

#endif
