#ifndef STACK_H
#define STACK_H
/**
 * @file stack.h
 * @author dbauer
 * @brief Generic stack implementation
 **/
#include "vector.h"

/**
 * @brief Generates the declaration for a stack for a given type and all its functions
 * @param i suffix for all its functions
 * @param T for the actual typename
 **/
#define STACK_DECL(i, T) \
	/** @brief The stack is implemented using the vector struct **/ \
	typedef struct s_vector_##i Stack_##i; \
	/**
	 * @brief Allocates and initializes a new stack
	 **/ \
	Stack_##i* create_stack_##i(void); \
	/**
	 * @brief Initializes an stack object
	 * @details This is already called in create_stack_##i()
	 **/ \
	void init_stack_##i(Stack_##i* s); \
	/**
	 * @brief Uninitializes a stack and destroys all objects is stored.
	 **/ \
	void deinit_stack_##i(Stack_##i* s); \
	/**
	 * @brief Uninitializes and frees a stack 
	 **/ \
	void destroy_stack_##i(Stack_##i** s_ptr); \
	/**
	   @brief Pushes a value to the stack
	   @param s stack to be pushed onto
	   @param item to be pushed 
	**/ \
	void stack_push_##i(Stack_##i* s, T item); \
	/**
	   @brief Pops the topmost element of the stack.
	   @details Will do nothing if the stack is already empty
	   @param s stack to be popped of.
	 **/ \
	void stack_pop_##i(Stack_##i* s); \
	/**
	   @brief Returns a pointer to the topmost element of the stack
	   @param stack to be accessed
	   @return either pointer to the topmost element or NULL if the stack is empty
	 **/ \
	T* stack_top_##i(Stack_##i* stack); \
	/**
	 * @brief Returns the number of elements remaining in the stack
	 * @param stack to be counted
	 * @return number of elements
	 **/ \
	size_t stack_size_##i(const Stack_##i* stack); \
	/**
	   @brief Checks whether the stack is empty or not
	   @param stack to be checked
	   @return true if the stack if empty
	 **/ \
	bool stack_empty_##i(const Stack_##i* stack);

STACK_DECL(i, int)
STACK_DECL(f, float)
STACK_DECL(d, double)
STACK_DECL(v, void*)
STACK_DECL(c, char)
STACK_DECL(s, char*)
STACK_DECL(cs, const char*)

#endif
