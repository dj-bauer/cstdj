/**
 * @file bstree.h
 * @brief Binary-Search-Tree implementation using backlinks to the parent
 * @author dbauer
 **/
#ifndef BSTREE_H
#define BSTREE_H

#include <stdbool.h>
#include <stddef.h>
#include "attributes.h"

/**
 * @brief Generates the header declaration for a binary search tree storing a given type
 * @param i The suffix for all the functions
 * @param T the type to be generated for
 * **/
#define BST_DECL(suffix, T) \
/** @brief A node of which a struct s_bstree_##suffix is made up from **/ \
struct s_bstnode_##suffix { \
	/** @brief The left child **/ \
	struct s_bstnode_##suffix* left; \
	/** @brief The right child **/ \
	struct s_bstnode_##suffix* right; \
	/** @brief The parent node. It's a root if this is NULL **/ \
	struct s_bstnode_##suffix* parent; \
	/**
	   @brief This data is not used by the generic BST, 
	   but rather used for other insertion and removal functions
	 **/ \
	char balance;\
	/** @brief The actual data help by the node **/ \
	T data; \
}; \
/**
 * @brief Binary search tree datastructure.
 * @details Mainly just storing the number of elements contained and the root node
   @details The bst functions create an unbalanced simple tree.
	Other functions such as the avl_* functions etc will output a tree with other properties.
 **/ \
typedef struct s_bstree_##suffix { \
	/** @brief the root node of the tree **/ \
	struct s_bstnode_##suffix* root; \
	/** @brief the total number of nodes in the tree **/ \
	size_t size; \
} BSTree_##suffix;          \
void destroy_bst_##suffix(BSTree_##suffix* tree); \
BSTree_##suffix* create_bst_##suffix(void) \
    MALLOC(destroy_bst_##suffix); \
void init_bst_##suffix(BSTree_##suffix* tree); \
void deinit_bst_##suffix(BSTree_##suffix* tree); \
bool bst_insert_##suffix(BSTree_##suffix* tree, T value); \
bool bst_remove_##suffix(BSTree_##suffix* tree, T value); \
bool bst_contains_##suffix(const BSTree_##suffix* tree, T value); \
size_t bst_size_##suffix(const BSTree_##suffix* tree); \
T * bst_search_##suffix(BSTree_##suffix* tree, T value); \
void bst_generate_graph_##suffix(const BSTree_##suffix* tree, const char* filename); \
typedef void(*bst_it_func_##suffix)(T*, void*); \
void bst_foreach_##suffix(BSTree_##suffix* tree, bst_it_func_##suffix func, void* payload);

BST_DECL(v, void*)
BST_DECL(s, char*)
BST_DECL(cs, const char*)
BST_DECL(i, int)
BST_DECL(f, float)
BST_DECL(d, double)
BST_DECL(l, long)

#endif
