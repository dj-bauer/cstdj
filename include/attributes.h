#ifndef DJ_ATTRIBUTES_H
#define DJ_ATTRIBUTES_H

#ifndef __GNUC__
    #define __attribute__(x)
#endif

#define UNUSED __attribute__((unused))
#define PRINTF(fmt, args) __attribute__((format(printf, fmt, args)))
#define NONNULL(...) __attribute__((nonnull(__VA_ARGS__)))
#if __GNUC__ >= 14
	#define NULL_TERMINATED(...) __attribute__((null_terminated_string_arg(__VA_ARGS__)))
	#define MALLOC(free_func) __attribute__((malloc(free_func)))
#else
	#define NULL_TERMINATED(...)
	#define MALLOC(free_func)
#endif

// If this attribute is set then the pointer must be not-null and null-terminated
#define PROPER_STRING(...) NONNULL(__VA_ARGS__) NULL_TERMINATED(__VA_ARGS__)

#endif
