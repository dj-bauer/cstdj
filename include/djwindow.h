#ifndef DJ_WINDOW_H
#define DJ_WINDOW_H
/**
 * @file djwindow.h
 * @author dbauer
 * @brief Declares the event structs which are emitted by a djwindow instance
 **/

#include "djevent.h"
#include <stdbool.h>

/**
 * @brief The stored callback representation by a window
 **/
typedef struct s_eventcallback{
	/** @brief A function pointer which will act as the callback **/
	void(*callback_func)(DJEvent* event, void* obj); 
	/** 
	 * @brief A generic object pointer which will be passed by the callback
	 * @details Can be used as some form of member function
	   Can also be NULL
	*/
	void* obj; 
	/** @brief The next linked list entry **/
	struct s_eventcallback* next;
} EventCallback;

/** @brief A generic window class. The implementation depends on with what compile flags will be compiled **/
typedef struct s_djwindow DJWindow;

/** @brief Specifies which drawing mechanism/implementation is used **/
typedef enum {DRW_X11, DRW_GL, DRW_VULKAN} DrawType;

/**
 * @brief Creates a new window
 * @return a new window object on success
 */
DJWindow *dj_win_init(DrawType drawing);

/**
 * @brief Closes a window and sets its handle to NULL
 * @param win_ptr pointer to the window object pointer
 **/
void dj_win_close(DJWindow** win_ptr);

/**
 * @brief Updates the window and processes all events
 * @param win window to be updated
 */
void dj_win_update(DJWindow* win);

/**
 * @brief Updates the window with the graphics context framebuffer
 * @details The function depends on the graphics implementation
   For OpenGL it simply swaps the framebuffers
 * @param win window to be flipped
 */
void dj_win_flip(DJWindow* win);

/**
 * @brief Sets the title of the window to a new string
 * @param window to be modified
 * @param str new title
 */
void dj_win_set_title(DJWindow* window, const char* str);

/**
 * Subscribe to a specific window event
 * Provide a callback function and optionally an object which should be passed to the function
 * <p>has a default implementation in djwindow_common.h
 * @param win window to be subscribed to
 * @param event which should be subscribed to
 * @param callback_func function pointer which acts as the callback
 * @param obj optional and can be NULL object to be passed to the callback
 */
void dj_win_sub_event(DJWindow* win, DJEventType event, void(*callback_func)(DJEvent* event, void* obj), void* obj);
/**
 * Unsubscribes the specifically given object-callback combo from the window if it was subscribed
 * Here the exact combination of callbac_func and obj is used to select the first matching combi to be removed
 * <p>has a default implementation in djwindow_common.h
 * @notice has a default implementation in djwindow_common.h
 * @param win window to be unsubscribed to
 * @param event which should be unsubbed
 * @param callback_func which was given in the subscription
 * @param obj which was given at subscription time
 */
void dj_win_unsub_event(DJWindow* win, DJEventType event, void(*callback_func)(DJEvent* event, void* obj), void* obj);

/**
 * @brief If one was previously created exists it binds the created openGL context;
 * @param win window which should bind its main framebuffer
 */
void dj_win_bind_gl(DJWindow* win);

/**
 * @brief Returns the current window width 
 * @param window from which the width should be queried
 **/
unsigned int dj_win_width(const DJWindow* window);
/**
 * @brief Returns the current window height
 * @param window from which the height should be queried
 **/
unsigned int dj_win_height(const DJWindow* window);

const char* dj_mousebutton_str(MouseButton btn);

/**
 * Toggles wether the window is in fullscreen or not.
 * <p>has a default implementation in djwindow_common.h
 * @param window which should be set to fullscreen or back
 **/
void dj_win_toggle_fullscreen(DJWindow* window);
void dj_win_set_fullscreen(DJWindow* window, bool value);
bool dj_win_get_fullscreen(const DJWindow* window);
void dj_win_set_icon(DJWindow* window, int width, int height, const unsigned char* icon_rgba, const char* icon_name);
void dispatch_event(DJWindow* win, DJEventType type, DJEvent* e);

#endif
