/**
 * @file hashmap.h
 * @brief A rudimentary hashmap
 * @author dbauer
 **/
#ifndef DJ_HASHMAP_H
#define DJ_HASHMAP_H

#include <stddef.h>
#include <stdbool.h>

/**
 * @brief Generates the header declaration for a hashmap storing a given type
 * @param i The suffix for all the functions
 * @param T the type to be generated for
 * **/
#define HASHMAP_DECL(suffix, T) \
	typedef struct s_hashmap_##suffix { \
		T* data; \
		bool* cell_probed; \
		bool* cell_filled; \
		bool (*cmp_func)(T*, T*); \
		size_t (*hash_func)(T*); \
		size_t num_elements; \
		size_t capacity; \
	} Hashmap_##suffix; \
	/*
	 * Initialize an hashmap object
	 * @param hm Hashmap object to be initialized
	 * @param cmp_func The function to compare wether two objects are identical. e.g. compare the key component of a tuple struct
	 * @param hash_func Hashing function
	 */ \
	void init_hashmap_##suffix(Hashmap_##suffix* hm, bool (*cmp_func)(T*,T*), size_t (*hash_func)(T*)); \
	/*
	 * Frees up all allocated objects and marks the hashmap as an invalid object
	 */ \
	void deinit_hashmap_##suffix(Hashmap_##suffix* hm); \
	/*
	 * Insert an item into the hashmap
	 */ \
	void hashmap_insert_##suffix(Hashmap_##suffix* hm, T element); \
	void hashmap_remove_##suffix(Hashmap_##suffix* hm, T* element); \
	bool hashmap_contains_##suffix(Hashmap_##suffix* hm, T* element); \
	void hashmap_print_##suffix(const Hashmap_##suffix* hm); \
	void hashmap_reserve_##suffix(Hashmap_##suffix* hm, size_t min_capacity); \
	T* hashmap_get_##suffix(Hashmap_##suffix* hm, T* cmp); \

#endif
