/**
 * @file wav.h
 * @brief Wav file interaction functions
 * @author dbauer
 **/
#ifndef DJ_WAV_H
#define DJ_WAV_H

typedef enum {Mono8, Mono16, Stereo8, Stereo16} SoundFormat;

char* load_wav_file(const char* filename, int* num_channels, int* num_samples, int* bits_per_sample, 
		int* buffer_size, SoundFormat* format);

#endif
