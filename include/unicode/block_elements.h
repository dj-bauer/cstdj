/*
    Copyright (C) 2024 Daniel Bauer

    This program is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation, either version 3 of the License,
    or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty
    of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef STDJ_UNICODE_BLOCK_ELEMENTS_H
#define STDJ_UNICODE_BLOCK_ELEMENTS_H

#define U_UPPER_HALF_BLOCK 0x2580
#define U_UPPER_ONE_EIGHTH_BLOCK 0x2594

#define U_LOWER_ONE_EIGHTH_BLOCK 0x2581
#define U_LOWER_ONE_QUARTER_BLOCK 0x2582
#define U_LOWER_THREE_EIGHTH_BLOCK 0x2583
#define U_LOWER_HALF_BLOCK 0x2584
#define U_LOWER_FIVE_EIGHTH_BLOCK 0x2585
#define U_LOWER_THREE_QUARTER_BLOCK 0x2586
#define U_LOWER_SEVEN_EIGHTH_BLOCK 0x2587
#define U_FULL_BLOCK 0x2588

#define U_LEFT_SEVEN_EIGHTH_BLOCK 0x2589
#define U_LEFT_THREE_QUARTER_BLOCK 0x258A
#define U_LEFT_FIVE_EIGHTH_BLOCK 0x258B
#define U_LEFT_HALF_BLOCK 0x258C
#define U_LEFT_THREE_EIGHTH_BLOCK 0x258D
#define U_LEFT_ONE_QUARTER_BLOCK 0x258E
#define U_LEFT_ONE_EIGHTH_BLOCK 0x258F

#define U_RIGHT_HALF_BLOCK 0x2590
#define U_RIGHT_ONE_EIGHTH_BLOCK 0x2595

#define U_LIGHT_SHADE 0x2591
#define U_MEDIUM_SHADE 0x2592
#define U_DARK_SHADE 0x2593

#define U_QUADRANT_LL 0x2596
#define U_QUADRANT_LR 0x2597
#define U_QUADRANT_UL 0x2598
#define U_QUADRANT_UR 0x259D
#define U_QUADRANT_UL_LL_LR 0x2599
#define U_QUADRANT_UL_LR 0x259A
#define U_QUADRANT_UL_UR_LL 0x259B
#define U_QUADRANT_UL_UR_LR 0x259C
#define U_QUADRANT_UR_LL 0x259E
#define U_QUADRANT_UR_LL_LR 0x259F

#endif // STDJ_UNICODE_BLOCK_ELEMENTS_H
