#ifndef FBVIDEO_H
#define FBVIDEO_H
#include <linux/fb.h>
#include <stddef.h>

typedef struct fb_handler {
	struct fb_var_screeninfo vinfo;
	struct fb_fix_screeninfo finfo;
	char* memory;
	size_t memory_size;
	int fd;
	int width;
	int height;
} Video;

int fb_open(Video* video);
int fb_close(Video*);
void write_pixel(Video* video, int x, int y, 
		unsigned char r, unsigned char g, unsigned char b);

#endif
