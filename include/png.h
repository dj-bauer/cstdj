/**
 * @file png.h
 * @brief My own implementation of PNG loading
 **/
#ifndef DJ_PNG_H
#define DJ_PNG_H
#include <stdint.h>
#include <time.h>
#include <stdbool.h>

#include "attributes.h"

/** @brief All available image filters in the PNG spec **/
enum e_filters {FILTER_NONE, FILTER_SUB, FILTER_UP, FILTER_AVG, FILTER_PAETH};

/**
 * @brief A text pair which consists of a keyword and a content both as null-terminated strings
 **/
typedef struct s_textpair {
	/** @brief key of the tuple **/
	const char* key;
	/** @brief value of the tuple **/
	const char* data;
	/** @brief next element in the linked list for the text attributed **/
	struct s_textpair* next;
} Textpair;

typedef struct s_rgb {
	uint8_t r;
	uint8_t g;
	uint8_t b;
	uint8_t a;
} ImageColor;

/**
 * @brief A (generic) struct for images.
 *
 * This should be used for all images, which will be able to be loaded.
 * Currently it is tailored for png images, but should be expanded to support more image formats.
 * @author dbauer
 **/
typedef struct s_image {
	/** @brief The pixel width of the image  **/
	uint32_t width;
	/** @brief The pixel height of the image **/
	uint32_t height;
	/** @brief The number of channels per pixel **/
	uint8_t nr_channels;
	/** @brief Currently PNG specific color information to distinguish between, grayscale, indexed etc. **/
	uint8_t color_type;
	/** @brief How many bits a single color channel takes per pixel **/
	uint8_t bits_per_pixel;
	/** @brief The decompressed image data **/
	unsigned char* data;
	/** @brief The epoch timestamp of the last modification of the image **/
	time_t modification_time;
	/** @brief The PNG filter method used **/
	enum e_filters filter_method;
	/** @brief a Linked list of keyword identified strings **/
	Textpair* texts;
	/** @brief A gamma value if provided is at DBL_MAX if unset **/
	double gamma;
	/** @brief The recomended background color for this image **/
	struct {
		uint16_t r;
		uint16_t g;
		uint16_t b;
	} bg_color;

	struct s_iccp {
		bool present;
        char profile_name[80];
		unsigned char* data;
	} iccp_profile;
} Image;

/**
 * @brief Destroys an existing image and deallocates all space
 **/
void free_image(Image* dest);
/**
 * @brief Loads up an image from the filesystem
 **/
Image* load_image(const char* filename)
    MALLOC(free_image)
	NULL_TERMINATED(1);
Image* load_png_from_buf(size_t buf_len, const unsigned char buffer[buf_len])
	MALLOC(free_image);

ImageColor get_color(Image* img, uint32_t x, uint32_t)
    NONNULL(1);

#endif
