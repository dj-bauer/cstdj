/**
 * @file vector.h
 * @brief A simple dynamic array implementation
 * @author dbauer
 **/
#ifndef VECTOR_H
#define VECTOR_H

#include <stddef.h>
#include <stdbool.h>
#include "attributes.h"

/**
 * @brief Generates the header declaration for a vector of a given type
 * @param i The suffix for all the functions
 * @param T the type to be generated for
 * **/
#define VECTOR_DECL(i, T) \
	/** @brief The vector implementation for data type T **/ \
	typedef struct s_vector_##i { \
		/** @brief The number of items currently space is allocated for **/ \
		size_t capacity;\
		/** @brief The number of items currently stored in the array **/ \
		size_t size; \
		/** @brief The raw data array containing everything.
		    It might be safer to access data using the vector_at_##i function for bound checking.
		 **/ \
		T* data; \
	} Vector_##i; \
	/**
	   @brief deallocates a vector and all its content
	 **/ \
	void destroy_vector_##i(struct s_vector_##i* v_ptr); \
	/**
	 * @brief Allocates and initializes a new vector object
	 **/ \
	struct s_vector_##i* create_vector_##i(void)             \
        MALLOC(destroy_vector_##i); \
	/** 
	 * @brief Initializes an uninitialized vector object 
	 * @details This can also be used to initialize a vector laying on the heap 
	   This is automatically called in create_vector_##i
	 * **/ \
	void init_vector_##i(struct s_vector_##i* v); \
	/**
	   @brief Uninitializes a vector, 
	   so that it's state is unknown and holds no more memory footprint other itself
	 **/ \
	void deinit_vector_##i(struct s_vector_##i* v); \
	/**
	   @brief Appends an element to the end of the vector
	   @param v vector to be appended to
	   @param value to be used
	 **/ \
	T* vector_append_##i(struct s_vector_##i* v, T value); \
	/**
	 * @brief Adds an item to the vector into the first position.
	 * @warning This takes O(n) time because all elements need to be shifted one back.
	 * @param v the vector to be prepended into
	 * @param value to be inserted into slot 0
	 **/ \
	T* vector_prepend_##i(struct s_vector_##i* v, T value); \
	/**
	   @brief Inserts an element at the given index.
	  
	   All elements after the given index will be shifted on item to the right.
	   @param v vector to be inserted into
	   @param index index that the element will have after insertion
	   @param value to be inserted
	   @return A reference to the inserted element
	 **/ \
	T* vector_insert_##i(struct s_vector_##i* v, size_t index, T value); \
	/**
	   @brief Attaches a vector to another
	   @param dest The vector to be modified and stores the concatinated result
	   @param src vector which will be appended to v
	 **/ \
	void vector_concat_##i(struct s_vector_##i* dest, const struct s_vector_##i* src); \
	/** 
	   @brief Returns a pointer to the element in the array 
	   @param v vector to be accessed
	   @param index of the item to be accessed
	   @exception ERANGE if the index is larger than the maximum accessible element
	 **/ \
	T* vector_at_##i(struct s_vector_##i* v, size_t index)   \
	    NONNULL(1); \
	/**
	   @brief Returns a pointer to the first element in the array
	   @param v vector to be accessed
	   @exception ERANGE if the vector is empty
	 **/ \
	T* vector_first_##i(struct s_vector_##i* v); \
	/**
	   @brief Returns a pointer to the last element in the array
	   @param v vector to be accessed
	   @exception ERANGE if the vector is empty
	 **/ \
	T* vector_last_##i(struct s_vector_##i* v); \
	/**
	   @brief Removes the last element of the array
	   @param v vector to be removed from
	 **/ \
	void vector_remove_last_##i(struct s_vector_##i* v); \
	size_t vector_size_##i(const struct s_vector_##i* v); \
	/**
	 * @brief Executes the given function on each entry of the vector with its index
	 * @param v vector to be iterated over
	 * @param foreach_func to be executed
	 **/ \
	void vector_foreach_##i(struct s_vector_##i* v, void (*foreach_func)(size_t, T*)) \
        NONNULL(1, 2); \
	/**
	 * @brief Searches the vector for an item
	 * @param v vector to be searched in
	 * @param item that will be searched for
	 * @param eq_func to compare whether two objects are equal
	 * @return true if the item was found in the vector
	 **/ \
	bool vector_contains_##i(struct s_vector_##i* v, T* item, bool (*eq_func)(T* a, T* b)) \
        NONNULL(1, 3);

VECTOR_DECL(i, int)
VECTOR_DECL(f, float)
VECTOR_DECL(d, double)
VECTOR_DECL(v, void*)
VECTOR_DECL(c, char)
VECTOR_DECL(s, char*)
VECTOR_DECL(cs, const char*)
VECTOR_DECL(l, long)
VECTOR_DECL(ull, unsigned long long)

#endif
