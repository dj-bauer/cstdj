/**
 * @file network_packet.h
 * @author dbauer
 * @brief marshal and unmarshal structs
 **/
#ifndef DJ_NETWORK_PACKET_H
#define DJ_NETWORK_PACKET_H
#include <stdint.h>
#include <stddef.h>

/**
 * A network buffer simply having a size and being able to be written to.
 * @brief it can be written to and read from using the parsing functions.
 * @details This allows it to be filled from a file descriptor and the parsed
 * back or the other way around to send data.
 **/
typedef struct s_netbuf {
	size_t size;
	size_t ptr;
	unsigned char data[];
} NetworkBuf;

// create a network buffer
NetworkBuf* netbuf_create(size_t size);
void netbuf_free(NetworkBuf* buf);

#define create_parse_func(name, T) \
int write_##name(NetworkBuf* buf, const T var);\
int read_##name(T* var, NetworkBuf* buf);

#define size_short sizeof(int16_t)
create_parse_func(short, int16_t)

#define size_int sizeof(int32_t)
create_parse_func(int, int32_t)

#define size_long (sizeof(int64_t))
create_parse_func(long, int64_t)

#define size_float (sizeof(float))
create_parse_func(float, float)

#define size_double (sizeof(double))
create_parse_func(double, double)

#define size_str(str) (strlen(str) + size_int)
create_parse_func(str, char*)

#define BUFFER_EOF 1
#define BUFFER_FINE 0
#define BUFFER_STR_TOO_LARGE 2
#define BUFFER_MALLOC_FAILED 3

#endif
