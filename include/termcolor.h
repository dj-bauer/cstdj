/*
    Copyright (C) 2024 Daniel Bauer

    This program is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation, either version 3 of the License,
    or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty
    of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef STDJ_TERMCOLOR_H
#define STDJ_TERMCOLOR_H

#define GRAPHIC_ESC "\033["

#define ANSI_BLACK GRAPHIC_ESC "30m"
#define ANSI_RED "\033[31m"
#define ANSI_GREEN "\033[32m"
#define ANSI_YELLOW "\033[33m"
#define ANSI_BLUE "\033[34m"
#define ANSI_MAGENTA "\033[35m"
#define ANSI_CYAN "\033[36m"
#define ANSI_LIGHT_GRAY "\033[37m"

#define ANSI_GRAY "\033[90m"
#define ANSI_LIGHT_RED "\033[91m"
#define ANSI_LIGHT_GREEN "\033[92m"
#define ANSI_LIGHT_YELLOW "\033[93m"
#define ANSI_LIGHT_BLUE "\033[94m"
#define ANSI_LIGHT_MAGENTA "\033[95m"
#define ANSI_LIGHT_CYAN "\033[96m"
#define ANSI_WHITE "\033[97m"

#define ANSI_BG_BLACK GRAPHIC_ESC "40m"
#define ANSI_BG_RED GRAPHIC_ESC "41m"
#define ANSI_BG_GREEN GRAPHIC_ESC "42m"
#define ANSI_BG_YELLOW GRAPHIC_ESC "43m"
#define ANSI_BG_BLUE GRAPHIC_ESC "44m"

#define ANSI_RESET GRAPHIC_ESC "0m"
#define ANSI_BOLD GRAPHIC_ESC "1m"

#endif
