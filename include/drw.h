/** 
 * @file drw.h
 * @author dbauer
 * @brief Drawing functions operating on compatible DJWindow objects 
 **/
#ifndef DJ_DRW_H
#define DJ_DRW_H
#include "color.h"
#include "djwindow.h"
#include <stdint.h>

/** @brief A generic image struct, which can be blitted or modified **/
typedef struct s_djpixmap DJimg;

/**
 * @brief A 4 byte BGRA value, which is used in X11 pixmaps
 **/
typedef struct s_rgb {
	/** Blue component **/
    uint8_t b;
	/** Green component **/
    uint8_t g;
	/** Red component **/
    uint8_t r;
	/** Alpha component. Often unused **/
    uint8_t a;
} gbra;

/**
 * @brief Draws a filled rectangle of a given color into a supported window 
 * @param window to be drawn into
 * @param x position of the rectangle
 * @param y position of the rect
 * @param w width of the rect
 * @param h height of the rect
 * @param color of the filling
 **/
void draw_rect(DJWindow* window, int x, int y, int w, int h, Color color);
/**
 * @brief Draws a filled circle of a given color into a supported window 
 * @param window to be drawn into
 * @param center x position of the circle
 * @param center y position of the circle
 * @param radius of the circle
 * @param color of the filling
 **/
void draw_circle(DJWindow* window, int x, int y, int radius, Color color);
/**
 * @brief Draws the outline rectangle of a given color into a supported window 
 * @param window to be drawn into
 * @param x position of the rectangle
 * @param y position of the rect
 * @param w width of the rect
 * @param h height of the rect
 * @param color of the outline to be drawn
 **/
void draw_rect_outline(DJWindow* window, int x, int y, int w, int h, Color color);
/**
 * Draw a line between two points
 * @param window to be drawn in. If the window is incompatible the behavior is undefined.
 * @param x0 x-coordinate of first point
 * @param y0 y-coordinate of first point
 * @param x1 x-coordinate of second point
 * @param y1 y-coordinate of second point
 * @param color of the line to be drawn
 **/
void draw_line(DJWindow* window, int x0, int y0, int x1, int y1, Color color);
/**
 * @brief Fills the window with a given color
 * @param win drw.h compatible window 
 * @param c color to be used to fill
 **/
void fill(DJWindow* win, Color c);
/**
 * @brief Revert the full window back to it's original color. Most likely black
 * @param window to be cleared
 **/
void clear_screen(DJWindow* window);
/**
 * @brief Sets the background color of a window to a given color
 * @param window to be modified
 * @param color to be set
 **/
void set_bg(DJWindow* window, Color color);
/**
 * Very simple text drawing function
 * @param window to be drawn into
 * @param x position of the text
 * @param y position
 * @param string text
 * @param color the text should be in
 **/
void print_text(DJWindow* window, int x, int y, const char* string, Color color);

/**
 * @brief Creates an image object which can be blit onto a window
 * @details The default color depth is four channels and 8 bit per channel
 * @param window to which the image belongs
 * @param width of the image
 * @param height of the image
 **/
DJimg* create_image(DJWindow* window, int width, int height);
/**
 * @brief Deallocates an image
 * @param pix to be destroyed
 **/
void destroy_image(DJimg* pix);
/**
 * @brief Blits an image onto the window
 *
 * There is no scaling involved and the correct size of the image is assumed
 * @param window to be blit onto
 * @param pix image to be used
 * @param x position of the top left corner of the image
 * @param y position of the top left corner of the image
 **/
void draw_image(DJWindow* window, DJimg* pix, int x, int y);
/**
 * @brief Returns a reference to the raw byte representation of the image
 * @param pix image whose pixels should be accessed
 **/
char* image_raw_data(DJimg* pix);
/**
 * @brief Sets an images pixel to a given gbra color
 * @param pix image to be modified
 * @param x coordinate
 * @param y coordinate
 * @param c color to be used
 **/
void img_pixel(DJimg* pix, int x, int y, gbra c);
/**
 * @brief Sets an images pixel to a given RGB color
 * @param pix image to be modified
 * @param x coordinate
 * @param y coordinate
 * @param c color to be used
 **/
void img_pixel_c(DJimg* pix, int x, int y, Color c);

/**
 * @brief Converts an RGB color into an gbra value
 * @param c color to be converted
 **/
struct s_rgb color_to_rgb(Color c);

#endif
