#ifndef CSTDJ_UTIL_H
#define CSTDJ_UTIL_H
#include <stddef.h>
#include <stdio.h>
#include <stdbool.h>
#include <assert.h>

#define MAX(a, b) ((a) > (b) ? (a) : (b))
#define MIN(a, b) ((a) < (b) ? (a) : (b))
#define CLAMP(value, min, max) MAX(MIN(value, max), min)
#ifndef M_PI
#define M_PI 3.1415926
#endif
#ifndef M_PI_4
#define M_PI_4 (M_PI/4)
#endif
#ifndef M_PI_3
#define M_PI_3 (M_PI/3)
#endif

#define ASSERT_NONNULL(x) assert((x) != NULL)
#define ASSERT_NOT_REACHED assert(0 && "Not Reachable ");
#define FAIL_NOT_IMPLEMENTED assert(0 && "Not Implemented ");

#define N_ELEMENTS(X) (sizeof(X) / sizeof(X[0]))

/**
 * Creates a new string object and copies the source string
 */
char* strdup(const char* s);
char* strndup(const char* s, size_t n);
bool str_startswith(const char* s, const char* prefix);
bool str_endswith(const char* s, const char* suffix);

long int getline(char** string, size_t* n, FILE* stream);

double random_double(double min, double max);

size_t fprint(FILE* f, const char* str);
#define print(str) fprintf(stdout, str)

#endif
