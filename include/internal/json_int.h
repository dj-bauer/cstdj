//
// Created by dbauer on 12/5/24.
//

#ifndef CSTDJ_JSON_INT_H
#define CSTDJ_JSON_INT_H
#include "djson.h"
#include "vector.h"
#include "arena_allocator.h"

typedef bool JBool_t;
typedef char* JString_t;
typedef double JNumber_t;
typedef enum {ParseOK, ParseFailed} ParseError;

typedef struct s_json_obj {
	// TODO: Use Trie instead of linear list
	Vector_s names;
	Vector_v nodes;
} JObject_t;

typedef struct s_json_arr {
	Vector_v elements;
} JArray_t;

typedef struct s_json_root {
	MemoryArena storage;
	struct s_json_node* first_node;
} JRoot_t;

//VECTOR_DECL(n, JNode)
struct s_json_node {
	JType type;
	union u_json_node_data {
		JNumber_t num;
		JBool_t boolean;
		JString_t str;
		JObject_t obj;
		JArray_t arr;
		JRoot_t root;
	} data;
};

// Parser and lexer stuff

typedef enum {TString, TNumber, TBool, TNull, TLeftBrace, TRightBrace, TLeftBracket, TRightBracket, TComma, TColon, TEof} TokenType;
extern const char* token_str[];

typedef struct s_json_token {
    TokenType type;
    union u_json_node_data value;
} JToken;

VECTOR_DECL(t, JToken)

struct s_json_lexer {
    const char* stream;
    size_t stream_len;
	// The first address not in stream
	const char* stream_end; 
    // Points to the position in the stream that the current token begins at
    const char* token_start;
    // Points to the current inspected character
    const char* current_ch;
    size_t line;
    Vector_t tokens;
    MemoryArena* storage;
	int token_ptr;
};


struct s_json_lexer lexer_init(MemoryArena* storage, const char* stream, size_t stream_len);
void lexer_cleanup(struct s_json_lexer* lexer);
ParseError lexer_scan(struct s_json_lexer* lexer);
JToken lexer_next_token(struct s_json_lexer* lexer);

JType json_true_type(const JNode* node);
bool json_is_true_type(const JNode* node, JType type);

// Returns the i'th key of a nodes property keys
const char* json_obj_key(const JNode* node, size_t i);


#endif //CSTDJ_JSON_INT_H
