#ifndef DJ_FAST_STRLEN_H
#define DJ_FAST_STRLEN_H
#include <stdbool.h>

bool strlen_gt(const char* str, int cmp);
bool strlen_ge(const char* str, int cmp);

#endif
