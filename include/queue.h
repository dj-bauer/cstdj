#ifndef DJ_QUEUE_H
#define DJ_QUEUE_H
/**
   @file queue.h
   @brief Implements a linked list implementation of a queue for diverse data types
   @author dbauer
 **/

#include <stdbool.h>
#include <stdint.h>
#include "attributes.h"

/**
  @brief Generates the declaration for a queue and all its functions for a given datatype
  @param i the suffix for the specific implementation. Must be unique from other types
  @param T data type
 **/
#define QUEUE_DECL(T, i)                            \
	/** @brief A node used for the linked list implementation of the queue **/ \
	struct s_queue_node_##i {                       \
		/** @brief Actual value held in the queue **/ \
		T value;                                    \
		/** @brief Linked next node **/ \
		struct s_queue_node_##i* next;              \
	};                                              \
	/** @brief A queue implementation for type i **/ \
	typedef struct s_queue_##i {                    \
		/** @brief the first item in the queue **/ \
		struct s_queue_node_##i* head;              \
		/** @brief the last item in the queue **/ \
		struct s_queue_node_##i* tail;              \
	} queue_##i;                                    \
	/**
	 * @brief Destroys a queue and its content
	 * @param q_ptr pointer to the queue object to be removed
	 **/ \
	void queue_destroy_##i(queue_##i* queue); \
	/** @brief Creates and initializes a new queue object **/ \
	queue_##i* queue_create_##i(void)                  \
        MALLOC(queue_destroy_##i);                  \
	/**
	 * @brief Initializes a queue object
	 * @param q to be initialized
	 **/ \
	void queue_init_##i(queue_##i* q)                  \
        NONNULL(1);              \
	/**
	 * @brief Uninitializes and empties a queue object
	 * @param q queue which should be uninitialized after operation
	 **/ \
	void queue_cleanup_##i(queue_##i* q)               \
        NONNULL(1);           \
	/**
	 * @brief Returns whether the queue is currently empty or not
	 * @param queue to be queried from 
	 **/ \
	bool queue_empty_##i(const queue_##i* queue)       \
        NONNULL(1);   \
	/**
	 * @brief Prints the current queues content to stdout
	 * @param queue to be printed
	 **/ \
	void queue_print_##i(const queue_##i* queue)       \
        NONNULL(1);   \
	/** 
	 * @brief Appends an element to the end of the queue
	 * @param queue to be appended to
	 * @param value to be appended
	 **/ \
	void queue_enq_##i(queue_##i* queue, T value)      \
        NONNULL(1);  \
	/** 
	 * @brief Removes the first element from the queue 
	 * @param queue to have the first element removed
	 * **/ \
	void queue_deq_##i(queue_##i* queue)               \
        NONNULL(1);           \
	/** 
	 * @brief Returns a reference to the first element in the Queue.
	 * @param queue to te referenced from
	 * @return Reference could be NULL
	 **/ \
	T* queue_front_##i(const queue_##i* queue)         \
        NONNULL(1);

QUEUE_DECL(int, i)
QUEUE_DECL(float, f)
QUEUE_DECL(char*, s)
QUEUE_DECL(uint64_t, u64)
QUEUE_DECL(void*, v)

#endif
