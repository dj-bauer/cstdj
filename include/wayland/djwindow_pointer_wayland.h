#ifndef DJ_WINDOW_POINTER_WAYLAND_H
#define DJ_WINDOW_POINTER_WAYLAND_H

#include <stdint.h>
#include <wayland-client-core.h>
#include <wayland-client-protocol.h>
#include "wayland_window.h"

void pointer_handle_axis(void *data, struct wl_pointer *wl_pointer,
                         uint32_t time, uint32_t axis, wl_fixed_t value);
void pointer_handle_frame(void* data, struct wl_pointer* wl_pointer);
void pointer_handle_button(void *data, struct wl_pointer *wl_pointer,
                                  uint32_t serial, uint32_t time, uint32_t button,
                                  uint32_t state);
void pointer_handle_enter(void *data, struct wl_pointer *pointer,
                          uint32_t serial, struct wl_surface *surface,
                          wl_fixed_t sx, wl_fixed_t sy);
void pointer_handle_leave(void *data, struct wl_pointer *pointer,
                          uint32_t serial, struct wl_surface *surface);
void pointer_handle_motion(void *data, struct wl_pointer *pointer,
                           uint32_t time, wl_fixed_t sx, wl_fixed_t sy);
void pointer_handle_axis_relative_direction(void* data, struct wl_pointer* pointer,
		uint32_t axis, uint32_t direction);
void pointer_handle_axis_value120(void* data, struct wl_pointer* pointer,
							uint32_t axis, int32_t value120);
void pointer_handle_axis_discrete(void* data, struct wl_pointer* pointer,
							uint32_t axis, int32_t discrete);
void pointer_handle_axis_stop(void* data, struct wl_pointer* pointer,
							uint32_t time, uint32_t axis);
void pointer_handle_axis_source(void* data, struct wl_pointer* pointer,
							uint32_t time);

extern const struct wl_pointer_listener pointer_listener;

#endif
