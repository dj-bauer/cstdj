#ifndef WAYLAND_WINDOW_H
#define WAYLAND_WINDOW_H

#include "djwindow.h"

#include <wayland-egl.h>
#include <EGL/egl.h>

struct s_dims {
    unsigned int x;
    unsigned int y;
};

struct s_djwindow {
	struct wl_display* display;
	struct wl_registry* registry;
	// Is this our abstraction of the compositor?
	struct wl_compositor* compositor;
	struct wl_surface* surface;
	struct wl_output* wl_output;
	// TODO: We need this for direct rendering, right?
	struct wl_shm* wl_shm;

	/** @brief Anything input related **/
	struct s_input_stuff {
		/** @brief Generator input manager **/
		struct wl_seat* seat;
		/** @brief Keyboard connection **/
		struct wl_keyboard* keyboard;
		/** @brief Mouse event connection **/
		struct wl_pointer* pointer;
		/** @brief Touch event connection
		 * @todo Currently unused
		 */
		struct wl_touch* touch;
	} input;

	/**@brief All our xkb stuff **/
	struct s_xkb_stuff {
		struct xkb_context* context;
		struct xkb_state*   state;
		struct xkb_keymap*  keymap;
	} xkb;

	struct s_xdg_stuff {
		struct xdg_wm_base* wm_base;
		struct xdg_toplevel* toplevel;
		struct xdg_surface* surface;
#ifndef DJWINDOW_NO_DECORATION
		struct zxdg_decoration_manager_v1* decoration_manager;
#endif
#ifndef DJWINDOW_NO_ICON
		struct xdg_toplevel_icon_manager_v1* icon_manager;
#endif
	} xdg;

	EventCallback* callbacks[EventCount]; // List which stores all the event subscribers
    struct s_dims dims;

	struct s_egl_stuff {
		// EGL stuff
		/** @brief Our Opengl connection kinda **/
		EGLDisplay display;
		/** @brief The framebuffer configuration that we chose based on our criteria **/
		EGLConfig fb_config;
		/** @brief Our OpenGL context **/
		EGLContext context;
		/** The surface that we draw to **/
		EGLSurface surface;
		/** Some wgl framebuffer kinda binding for resizing and stuff, maybe? **/
		struct wl_egl_window* window;
	} egl;

	DrawType draw_type;

	uint32_t scaling_factor;

    // Learning/Testing stuff
    float offset;
    uint32_t last_frame;
	bool is_fullscreen;
};

/**
 * Connects the display, registry wl_surface and xdg surface & toplevel
 * This function should be able to be used for any wayland draw type
 * @param window to be set the state of
 * @return 0 on success, nonzero 
 **/
int connect_wayland(DJWindow* window);
int egl_init(DJWindow* window, int gl_major, int gl_minor);

#endif
