#include <stdint.h>
#include <wayland-client-core.h>
#include <wayland-client-protocol.h>
void keyboard_handle_keymap(void* data, struct wl_keyboard* keyboard, uint32_t format, int32_t fd, uint32_t size);
void keyboard_handle_enter(void* data, struct wl_keyboard* keyboard, uint32_t serial, struct wl_surface* surface, struct wl_array* keys);
void keyboard_handle_leave(void* data, struct wl_keyboard* keyboard, uint32_t serial, struct wl_surface* surface);
void keyboard_handle_key(void* data, struct wl_keyboard* keyboard, uint32_t serial, uint32_t time, uint32_t key, uint32_t state);
void keyboard_handle_modifiers(void* data, struct wl_keyboard* keyboard, uint32_t serial, uint32_t mods_pressed, uint32_t mods_latched, uint32_t mods_locked, uint32_t group);
void keyboard_handle_repeat(void* data, struct wl_keyboard* keyboard, int32_t rate, int32_t delay);

extern const struct wl_keyboard_listener keyboard_listener;
