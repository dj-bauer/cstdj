#ifndef DJWINDOW_TOPLEVEL_WAYLAND_H
#define DJWINDOW_TOPLEVEL_WAYLAND_H
#include <stdint.h>
#include <wayland-client-core.h>
#include "wayland/xdg-shell-client-protocol.h"

void xdg_toplevel_configure(void* data, struct xdg_toplevel* xdg_toplevel,
		int32_t width, int32_t height, struct wl_array* states);
void xdg_toplevel_close(void* data, struct xdg_toplevel* xdg_toplevel);
void xdg_toplevel_configure_bounds(void* data, struct xdg_toplevel* xdg_toplevel,
		int32_t width, int32_t height);
void xdg_toplevel_wm_capabilities(void* data, struct xdg_toplevel* xdg_toplevel,
		struct wl_array* capabilities);

extern const struct xdg_toplevel_listener xdg_toplevel_listener;

#endif
