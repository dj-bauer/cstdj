#ifndef DJ_HEAP_H
#define DJ_HEAP_H
/**
 * @file heap.h
 * @author dbauer
 * @brief Efficient implementation of a heap for a given compare function
 **/

#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>

/**
 * @brief Generates the declaration for Heap and corresponding functions
 * @param i suffix for struct and functions
 * @param T the type
 **/
#define HEAP_DECL(i, T) \
	/** @brief The heap data structure object for type T**/ \
	typedef struct s_heap_##i { \
		/** 
		 * @brief The allocated array for capacity many elements of type T
		 * @details Due to the nature of the heap, it is always packed, meaning the heap is left-complete
		 **/ \
		T* data; \
		/** @brief The number of elements in the heap. **/ \
		size_t size; \
		/** @brief The number of elements the current array is free of **/ \
		size_t capacity; \
		/** 
		 * @brief Compare function to select which object is higher up in the heap
		 * @return <0 if a is higher up than b, 0 if a == 0, >0 if a lower than b
		 **/ \
		int (*cmp_func)(const T* a, const T* b); \
	} heap_##i; \
	/**
	 * @brief Creates and initializes a new heap object
	 * @param cmp_func which returns 0 on equal, and <0 on higher up in the tree
	 **/ \
	heap_##i* heap_create_##i(int (*cmp_func)(const T* a, const T* b)); \
	/**
	 * @brief Deallocates all memory of the heap and destroys the object
	 * @param h_ptr pointer to the heap address. This will be set to null after
	 **/ \
	void heap_destroy_##i(heap_##i** h_ptr); \
	/**
	 * @brief Initializes a heap structure
	 * @param heap reference
	 * @param cmp_func which returns 0 on equal, and <0 on higher up in the tree
	 **/ \
	void heap_init_##i(heap_##i* heap, int (*cmp_func)(const T* a, const T* b)); \
	/**
	 * @brief Removes all allocated memory besides the heap structure itself
	 * @param heap to be cleaned up
	 **/ \
	void heap_cleanup_##i(heap_##i* heap);\
	/**
	 * @brief Inserts an element into the heap
	 * @param heap to be inserted into
	 * @param value which should be inserted
	 **/ \
	void heap_insert_##i(heap_##i* heap, T value); \
	/**
	 * @brief Removes the top element of the heap.
	 * @param heap to be removed from.
	 **/ \
	void heap_pop_##i(heap_##i* heap); \
	/**
	 * @brief Returns a reference to the topmost element of the heap.
	 * @param heap to be referenced from
	 **/ \
	const T* heap_front_##i(const heap_##i* heap); \
	/** 
	 * @brief Prints the heap and all its content to stdout
	 * @param heap to be printed
	 * **/ \
	void heap_print_##i(const heap_##i* heap); \
	/**
	 * @brief Reserves at least n elements wide heap storage
	 * @details This can be used if the number of elements is known ahead of first insertion
	 * @param heap to be reserved for
	 * @param n number of elements to be reserved
	 **/ \
	void heap_reserve_##i(heap_##i* heap, size_t n); \
	/**
	 * @brief This has to be run when a value is modified.
	 * @param higher_than_prev Wether the item should be higher than is previous (true) or lower (false)
	 * @return wheter the item was found in the heap
	 **/ \
	bool heap_change_##i(heap_##i* heap, T item, bool higher_than_prev);

/** @brief Whether the heap is empty or not **/
#define heap_is_empty(heap) ((heap)->size == 0)

#define CREATE_HEAP_DEF CREATE_HEAP_HEADER

HEAP_DECL(i, int)
HEAP_DECL(f, float)
HEAP_DECL(s, char*)
HEAP_DECL(u64, uint64_t)
HEAP_DECL(v, void*)


#endif
