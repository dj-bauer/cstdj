/**
 * @file cas.h
 * @brief Trashy computer algebra system functions.
 * @details this implementations are more like algebruh
 * @author dbauer
 **/
#ifndef DJ_CAS_H
#define DJ_CAS_H

// Solves ax² + bx + c
void cas_solve_quadratic(double solutions[2], double a, double b, double c);
// Solve ax³ + bx² + cx + d
void cas_solve_cubic(double solutions[3], double a, double b, double c, double d);

#endif
