/**
 * @file audio.h
 * @brief Utility to open an audio channel and send data to it
 * @author dbauer
 **/
#include <stddef.h>

/**
 * @brief Abstract implementation for audio connectdion.
 * @details Concrete implementation depends on compile flags
 **/
typedef struct s_audio_connection AudioDevice;

/** @brief There are signed and unsigned audio **/
enum e_signdness {Unsigned, Signed};
/** @brief There are 1-byte, 2-byte and 4-byte samples **/
enum e_samplesize {Bits8, Bits16, Bits32};
/** @brief A system is either little or big endian **/
enum e_endianess {Littleendian, Bigendian};

/**
 * @brief Opens a new audio connection
 **/
AudioDevice* audio_open(unsigned int samplerate, unsigned int num_channels, enum e_signdness sign, enum e_samplesize bytes, enum e_endianess endian);
/** 
 * @brief Closes an audio connection
 * @param dev device to be uninitialized
 **/
void audio_close(AudioDevice* dev);

/**
 * @brief Enqueues another audio stream 
 * @param dev audio device to be used
 * @param data audio stream
 * @param num_bytes the length of data
 **/
void audio_play(AudioDevice* dev, const unsigned char* data, size_t num_bytes);
/**
 * @brief Wait's until the audio buffer is emptied
 **/
void audio_block(AudioDevice* dev);
