#ifndef ZLIB_H
#define ZLIB_H
#include <stdint.h>
#include <stddef.h>
#include <stdbool.h>

#include "bitreader.h"
#include "attributes.h"


unsigned char* zlib_decode(const unsigned char* data, size_t length, size_t* out_length)
	NONNULL(3);
/**
 * Reads one huffman block
 * @param dst The location of the pointer where the data is stored. This can be reallocated
 * @param reader A bitreader stream from where the bits can be fetched from
 * @param dst_size A pointer to the variable to be set to the new size of the unpacked data;
 * @return true if other blocks are following, false else
 */
bool read_deflate_block(unsigned char** dst, Bitreader* reader, size_t* dst_size)
NONNULL(1, 2, 3);

void generate_huffman_codes(uint16_t n, uint16_t codes[n], const uint8_t bit_lengths[n]);

#endif
