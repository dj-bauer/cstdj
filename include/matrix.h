/**
 * @file matrix.h
 * @author dbauer
 * @brief A generic matrix implementation for arbitrary size matrices
 * @details Both for ways for matrices on the heap but also existing on the stack if wanted
 **/
#ifndef DJ_MATRIX_H
#define DJ_MATRIX_H

// vi: ft=c


#include <stdbool.h>

typedef unsigned int mat_dim;
typedef double mat_data;

/**
 * @brief A matrix object
 * @details all operations are under the namespace dj_mat_*
 * Will be created using dj_mat_new
 * and destroyed using dj_mat_delete
 **/
typedef struct s_djmat {
	/* @brief the number of rows the matrix has */
	mat_dim rows;
	/* @brief the number of cols the matrix has */
	mat_dim cols;
	/* @brief The whole content of the matrix in row-major form */
	mat_data* data;
} dj_mat;

/**
 * Creates a new matrix
**/
dj_mat* dj_mat_new(mat_dim rows, mat_dim cols);

/**
 * Destroys a matrix object
 **/
void dj_mat_delete(dj_mat**);

/**
 * Copies the origin matrix into the dest matrix
 **/
void dj_mat_cp(dj_mat* restrict dest, const dj_mat* restrict orig);
/**
 * Copies a row of a Matrix into a vector
 **/
void dj_mat_cp_col(dj_mat* dest, const dj_mat* orig, mat_dim col);

mat_data dj_mat_get(const dj_mat* mat, mat_dim row, mat_dim col);
void dj_mat_set(dj_mat* mat, mat_dim row, mat_dim col, mat_data value);
void dj_mat_set_all(dj_mat* mat, mat_data value);
void dj_mat_set_rand(dj_mat* A, mat_dim min, mat_dim max);
void dj_mat_set_diag(dj_mat* mat, mat_data value);
void dj_mat_set_id(dj_mat* mat);
void dj_mat_set_col(dj_mat* A, const dj_mat* vec, mat_dim col);

bool dj_mat_eq_tol(const dj_mat* a, const dj_mat* b, mat_data tolerance);
bool dj_mat_eq(const dj_mat* a, const dj_mat* b);
bool dj_mat_eq_dims(const dj_mat* a, const dj_mat* b);
bool dj_mat_is_sq(const dj_mat* A);
bool dj_mat_is_lo_trig(const dj_mat* A);
bool dj_mat_is_up_trig(const dj_mat* A);
mat_data dj_mat_det(const dj_mat* A);

void dj_mat_print(const dj_mat* mat);

void dj_mat_mult(dj_mat* restrict dest, const dj_mat* A, const dj_mat* B);
void dj_mat_mult_sca(dj_mat* dest, const dj_mat* A, mat_data scalar);

/**
 * Performs dest = A + B
 **/
void dj_mat_add(dj_mat* dest, const dj_mat* A, const dj_mat* B);
/**
 * Performs dest = A - B
 **/
void dj_mat_sub(dj_mat* dest, const dj_mat* A, const dj_mat* B);

/**
 * Performs dest = transpose(A)
 * CANNOT be used inplace like dj_mat_trans(A, A)
 **/
void dj_mat_trans(dj_mat* restrict dest, const dj_mat* A);
/**
 * Inverses matrix
 * CANNOT be used inplace like dj_mat_inv(A, A)
 **/
void dj_mat_inv(dj_mat* restrict dest, const dj_mat* A);

/* ==== Nice linear algebra algorithms ==== */

/**
 * Calculates the lower left triangular matrix for the Cholesky decomposition.
 * This requires A to be symmetric and positive definite.
 **/
void dj_mat_cholesky(dj_mat* L, const dj_mat* A);
/**
 * Uses the numeric unstable Gram-Schmidt algorithm to calculate an orthonormalsystem,
 * which generates the same romo as the input matrix would
 **/
void dj_mat_gram_schmidt(dj_mat* Q, const dj_mat* A);
/**
 * Uses the modified gram Schmidt algorithm for better numerical stability than dj_mat_gram_schmidt
 **/
void dj_mat_mod_gram_schmidt(dj_mat* Q, const dj_mat* A);

void dj_mat_fw_subst(dj_mat* restrict x, const dj_mat* L, const dj_mat* b);
void dj_mat_bw_subst(dj_mat* restrict x, const dj_mat* U, const dj_mat* b);

void dj_mat_svd(dj_mat* restrict U, dj_mat* restrict sigma, dj_mat* restrict V_t, const dj_mat* A);
// Calculates the eigenvalues for the matrix A.
// In this first try the only implementation available will be for a 2x2 matrix
void dj_mat_eigenvalues(mat_data* restrict dest, const dj_mat* A);
void dj_mat_qr(dj_mat* restrict Q, dj_mat* restrict R, const dj_mat* A);

/* ==== Stackmat dark magic ==== */

#define DJ_DECL_STACKMAT(num_rows, num_cols) \
/**
 * @brief A matrix object that lives on the stack
 * @details Can be used the same way as dj_mat
 * Will be created using the Macro dj_stackmat_new
 * Since it's on the stack it doesn't need to be destroyed
 **/  \
struct s_stackmat_##num_rows##_##num_cols { \
	/** @brief number of rows of the stack matrix **/ \
	mat_dim rows; \
	/** @brief number of columns of the stack matrix **/ \
	mat_dim cols; \
	/** 
	 * @brief pointer to the data array below
	 * @details This is used so we can cast this struct to a matrix 
	 * and the pointers are at the same location in the struct
	 * Therefore we can simply point to the local data on the stack
	 * **/ \
	mat_data* data_ptr; \
	/**
	 * @brief The actual value of the matrix pointed to by the data_ptr
	 * @details Because the stackmat is allocated on the stack
	 * the actual data lies therefore also on the stack :^)
	 **/ \
	mat_data data_storage[num_rows*num_cols]; \
}

DJ_DECL_STACKMAT(1,1);
DJ_DECL_STACKMAT(2,2);
DJ_DECL_STACKMAT(3,3);
DJ_DECL_STACKMAT(4,4);
DJ_DECL_STACKMAT(4,1);

#define djmat_concat(a, b) a##b
/**
 * Easy macro to create a stackmat
 **/
#define dj_stackmat_new(name, num_rows, num_cols) \
struct s_stackmat_##num_rows##_##num_cols djmat_concat(name, __obj) = {.rows = num_rows, .cols = num_cols}; \
djmat_concat(name, __obj).data_ptr = (mat_data*) &djmat_concat(name, __obj).data_storage; \
dj_mat* name = (dj_mat*) &djmat_concat(name, __obj)

extern const dj_mat* id_4x4;

/* ==== A set of specific vector functions :) ==== */
bool dj_vec_is(const dj_mat* A);
mat_data dj_vec_sca_prod(const dj_mat* a, const dj_mat* b);
void dj_vec_norm(dj_mat* dest, const dj_mat* src);
mat_data dj_vec_length(const dj_mat* vec);
void dj_vec_cross_prod(dj_mat* c, const dj_mat* a, const dj_mat* b);

#endif
