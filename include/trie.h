/**
 * @file trie.h
 * @date 2024-04-09
 * @author dbauer
 * @brief A naive implementation of the Trie prefix tree
 * The current implementation uses nodes for individual characters 
 * instead of nodes for each unique character-string
 **/
#ifndef TRIE_H
#define TRIE_H

#include <stdbool.h>
#include <stddef.h>

#include "attributes.h"

struct s_trie_node;
typedef struct s_trie Trie;

typedef enum {TrieSuccess, TrieExists, TrieMalloc} TrieError;

void destroy_trie(Trie* trie_ptr);
Trie* create_trie(void) MALLOC(destroy_trie);
bool trie_contains_string(const Trie* t, const char* str)
	NONNULL(1, 2) NULL_TERMINATED(2);
bool trie_contains_string_n(const Trie* t, const char* str, size_t len)
	NONNULL(1, 2);
TrieError trie_insert(Trie* t, const char* str)
	NONNULL(1, 2) NULL_TERMINATED(2);
TrieError trie_insert_n(Trie* t, const char* str, size_t len)
	NONNULL(1, 2);
void trie_print(const Trie* t);

#endif
