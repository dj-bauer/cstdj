#ifndef DJ_MATRIX_TRANS_H
#define DJ_MATRIX_TRANS_H

/**
 * @file matrix_trans.h
 * @brief Provides typical transformation matrix operations
 * @author dbauer
 **/

#include "matrix.h"

/**
 * Calculates a scaled form of the transformation matrix
 * Requires 4x4 matrices
 **/
void dj_trans_scale(dj_mat* restrict dest, const dj_mat* matrix, mat_data scale);
/**
 * Translates the input matrix by the given vector
 * Requires 4x4 matrices and 4x1 vector(column)
 **/
void dj_trans_translate(dj_mat* restrict dest, const dj_mat* orig, const dj_mat* vec);

#endif
// vi: ft=c

