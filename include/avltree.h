/**
  @file avltree.h
  @author dbauer
  @brief Supplies an insert and remove function 
   for a BST to be used as an AVL tree.
  @details This requires the tree to be only 
   modified using these bst functions otherwise,
   it could end up in an invalid state.
   An AVL-Tree auto rotates the nodes on 
   insertion and removal in a way, 
   that the tree will keep itself balanced 
   and guarantee a lookup complexity of O(log n).
 **/
#ifndef AVL_H
#define AVL_H
#include "bstree.h"

/** 
 * @brief Declares the avl functions for a given type
 * @param i suffix for all the functions
 * @param T datatype
 **/
#define AVL_DECL(i, T) \
	/** 
	   @brief Inserts a value into a Binary-Search-Tree type T and rebalances it if necessary 
	   @param t tree to be modified
	   @param value to be inserted
	   @return if the value was actually inserted. If it returns false it most likely already contains the key
	 **/ \
	bool avl_insert_##i(BSTree_##i* t, T value) \
	    NONNULL(1); \
	/**
	   @brief Removes a value from a Binary-Search-Tree and rebalances it if necessary 
	   @param t to be removed from
	   @param value to be removed
	   @return if the value was actually removed. If false the tree doesn't contain value
	   @todo Actually implement this crap
	 **/ \
	bool avl_remove_##i(BSTree_##i* t, T value); \
	/**
	 * @brief Performs a left rotation around the given node
	 * @param x node to be rotated around
	 **/ \
	struct s_bstnode_##i* rotate_left_##i(struct s_bstnode_##i* x); \
	/**
	 * @brief Performs a right rotation around the given node
	 * @param x node to be rotated around
	 **/ \
	struct s_bstnode_##i* rotate_right_##i(struct s_bstnode_##i* x); \
	/**
	 * @brief Performs a left right rotation around the given node
	 * @param z node to be rotated around
	 **/ \
	struct s_bstnode_##i* rotate_left_right_##i(struct s_bstnode_##i* z); \
	/**
	 * @brief Performs a right left rotation around the given node
	 * @param z node to be rotated around
	 **/ \
	struct s_bstnode_##i* rotate_right_left_##i(struct s_bstnode_##i* z); \

AVL_DECL(i, int)
AVL_DECL(f, float)
AVL_DECL(d, double)
AVL_DECL(v, void*)
AVL_DECL(s, char*)
AVL_DECL(cs, const char*)

#endif
