#ifndef X_WINDOW_H
#define X_WINDOW_H
/**
 * @file x_window.h
 * @author dbauer
 * @brief Holds the declaration for the private implementation of the djwindow for the x-context
 * @details This is necessary to be shared between djwindow_x.c and drw_x.c
 **/

#include <X11/X.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <GL/glx.h>

#include "djwindow.h"

/** @brief A simple 2d vector **/
struct s_dims {
	/** @brief X-coordinate **/
	unsigned int x;
	/** @brief Y-coordinate **/
	unsigned int y;
};

/** 
 * @brief The x-implementation for a djwindow.
 * @details This is shared between djwindow_x.c and drw_x.c
 **/
struct s_djwindow{
	/** @brief The X display **/
	Display* disp;
	/** @brief The actual window created **/
	Window win;
	/** @brief The X screen (mostly 0) **/
	int screen;
	/** 
	 * @brief If created the X-Graphics context 
	 * **/
	GC gc;
	/**
	 * @brief The window pixel sizes
	 **/
	struct s_dims size;

	/**
	 * @brief xim and xic for keysymbol lookup
	 **/
	XIM xim;
	XIC xic;

	/** @brief Whether the window uses OpenGL or normal X-graphics **/
	DrawType drawtype;
	/** @brief If created the OpenGL context **/
	GLXContext gl_context;
	/**
	 * @brief Holds the window attributes like fullscreen minimized etc.
	 * @details Currently doesn't serve much function though
	 */
	XSetWindowAttributes attributes;
	/** Saves the current fullscreen state **/
	bool is_fullscreen;
	/**
	 * @brief A bunch of linked lists which hold the callbacks
	 */
	EventCallback* callbacks[EventCount]; // List which stores all the event subscribers
	/**
	 * The atoms of protocols that were interested in receiving
	 */
	Atom atom_protocols[1];
};

#endif
