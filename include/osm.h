/**
 * @file osm.h
 * @author dbauer
 * @brief Openstreetmap utilities
 * @details Currently supports loading up a map file and inspecting parts of its elements
 * The features can be seen in the osm test, which uses svg to render the map
 **/
#ifndef DJ_OSM_H
#define DJ_OSM_H
#include "xml.h"
#include "bstree.h"
#include "avltree.h"
#include "osmtypes.h"
#include "graph.h"

typedef struct s_coord {
	/** @brief Latitude coordinate of the location **/
	double lat;
	/** @brief Longitude coordinate of the location **/
	double lon;
} coord;
coord coord_minus(const coord a, const coord b);
coord coord_plus(const coord a, const coord b);
coord coord_sca_mult(const coord a, double scal);

struct s_osmnode {
	/** @brief The OSM internal id of the node **/
	long id;
	coord coords;
	struct s_wayattr data; 
};

/**
 * @brief Represents an openstreetmap way
 **/
struct s_osmway {
	/** @brief The OSM internal id of the way **/
	long id;
	/** @brief Dynamically allocated array of pointers to the nodes making up the way **/
	struct s_osmnode** nodes;
	/** @brief The number of nodes contained in nodes **/
	size_t nodecount;
	/** @brief All attributes the way contains **/
	struct s_wayattr data; 
	/**
	 * Wheter a way is filled or not.
	 * This will be initialized to all special rules
	 **/
	bool area;
	/**
	 * @brief The actual calculated area for the way if it is an area
	 **/
	long double area_value;
	/** @brief Stores an enum for the corresponding type description **/
	int type_specifier;
};

/** A member of a relation **/
struct s_osmrel_member {
	/** what kind of member it is **/
	enum e_relationmember type;
	/** 
	 * what special purpose the member has
	 * @details In a multipolygon this could be the inner or outer role
	 * Therefore parts could get cut out of an multipolygon relation
	 **/
	enum e_relationrole role;
	/** custom data associated with that role **/
	void* ptr;
};

struct s_osmrelation {
	long id;
	enum e_relation type;

	size_t membercount;
	struct s_osmrel_member* members;
	struct s_wayattr data;
};

BST_DECL(osmnodes, struct s_osmnode)
AVL_DECL(osmnodes, struct s_osmnode)
BST_DECL(osmways, struct s_osmway)
AVL_DECL(osmways, struct s_osmway)
BST_DECL(osmrel, struct s_osmrelation)
AVL_DECL(osmrel, struct s_osmrelation)

struct s_osm {
	/** @brief The XML-Object to be parsed **/
	xml_dom* dom;
	/** @brief Map of node ids to nodes **/
	BSTree_osmnodes nodes;
	/** @brief Map of way ids to ways **/
	BSTree_osmways ways;
	/** @brief Map or relation ids to relations **/
	BSTree_osmrel relations;
	coord min_coord;
	coord max_coord;
};

void osm_load(const char* mapfile, struct s_osm* map);
void osm_free(struct s_osm* map);
void osm_graph(const struct s_osm* map, const char* out_file, AdjGraph* g);

struct s_osmnode* osm_add_node(struct s_osm* map, long id, coord coords, struct s_wayattr attributes);
struct s_osmway* osm_add_way(struct s_osm* map, long id, size_t num_nodes, bool area, struct s_wayattr attributes);

const char* osm_natural_colors(enum e_natural_subtypes t);
const char* osm_landuse_colors(enum e_landuse_subtypes t);
const char* osm_leisure_colors(enum e_leisure_subtypes t);
const char* way_color(const struct s_osmway* way);

#endif
