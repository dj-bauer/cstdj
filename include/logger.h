/**
 * @file logger.h
 * @brief A general purpose logging facility
 * @author dbauer
 **/
#ifndef DJ_LOGGER_H
#define DJ_LOGGER_H

#include "attributes.h"

/** @brief All the available Loglevels **/
typedef enum {LogTrace, LogDebug, LogInfo, LogWarn, LogError} logLevel;

/** @brief Logs the formatted string with trace severity to stdout **/
#define log_trace(...) log_func(LogTrace, __FILE__, __LINE__, __VA_ARGS__)
/** @brief Logs the formatted string with debug severity to stdout **/
#define log_debug(...) log_func(LogDebug, __FILE__, __LINE__, __VA_ARGS__)
/** @brief Logs the formatted string with info severity to stdout **/
#define log_info(...) log_func(LogInfo, __FILE__, __LINE__, __VA_ARGS__)
/** @brief Logs the formatted string with warn severity to stdout **/
#define log_warn(...) log_func(LogWarn, __FILE__, __LINE__, __VA_ARGS__)
/** @brief Logs the formatted string with error severity to stderr **/
#define log_error(...) log_func(LogError, __FILE__, __LINE__, __VA_ARGS__)

/**
 * @brief Generic logging function.
 * @warning This should generally not be used and instead utilized the log_macros
 * @param lvl The severity
 * @param filename of the file currently issuing this log call
 * @param line in the file which calls the log function
 * @param format string for actuall content
 **/
void log_func(logLevel lvl, const char* filename, unsigned int line, const char* format, ...)
    PRINTF(4, 5);


#endif
