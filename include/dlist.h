#ifndef DLIST_H
#define DLIST_H

#include <stddef.h>
#include <stdbool.h>

#define DLIST_DECL(i, T) \
	/** @brief A node in the doubly linked list for type T **/ \
	typedef struct s_dlist_node_##i{ \
		/** @brief Previous member in the list**/ \
		struct s_dlist_node_##i* next; \
		/** @brief Next member in the list**/ \
		struct s_dlist_node_##i* prev; \
		/** @brief The actual data help in this list slot **/ \
		T data; \
	} dlistnode_##i; \
	/** @brief The doubly linked list for type T **/ \
	typedef struct s_dlist_##i{ \
		/** @brief The first item in the list **/ \
		struct s_dlist_node_##i* head; \
		/** @brief The last item in the list **/ \
		struct s_dlist_node_##i* tail; \
		/** @brief The current number of items in the list **/ \
		size_t size; \
	} dlist_##i; \
	dlist_##i* create_dlist_##i(void); \
	void init_dlist_##i(dlist_##i* l); \
	void deinit_dlist_##i(dlist_##i* l); \
	void destroy_dlist_##i(dlist_##i** lptr); \
	void dlist_append_##i(dlist_##i* l, T value); \
	void dlist_prepend_##i(dlist_##i* l, T value); \
	void dlist_insert_##i(dlist_##i* l, dlistnode_##i* n, T value); \
	void dlist_insert_before_##i(dlist_##i* l, dlistnode_##i* n, T value); \
	void dlist_remove_##i(dlist_##i* l, dlistnode_##i* n); \
	void dlist_remove_first_##i(dlist_##i* l); \
	void dlist_remove_last_##i(dlist_##i* l); \
	void dlist_remove_all_of_##i(dlist_##i* l, T value); \
	dlistnode_##i* dlist_find_first_of_##i(dlist_##i* l, T value); \
	dlistnode_##i* dlist_find_last_of_##i(dlist_##i* l, T value); \
	dlistnode_##i* dlist_first_node_##i(dlist_##i* l); \
	dlistnode_##i* dlist_last_node_##i(dlist_##i* l); \
	T* dlist_first_##i(dlist_##i* l); \
	T* dlist_last_##i(dlist_##i* l); \
	void dlist_clear_##i(dlist_##i* l); \
	size_t dlist_size_##i(const dlist_##i* l); \
	bool dlist_contains_##i(const dlist_##i* l, T value); \
	void dlist_foreach_##i(const dlist_##i* l, void(*func)(T*));

#define dlist_empty(dl) ((dl)->size == 0)

DLIST_DECL(i, int)
DLIST_DECL(f, float)
DLIST_DECL(d, double)
DLIST_DECL(s, char*)
DLIST_DECL(cs, const char*)
DLIST_DECL(v, void*)
DLIST_DECL(l, long)

#endif
