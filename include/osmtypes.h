#ifndef OSMTYPES_H
#define OSMTYPES_H

/*
 * The lower the type, the lower the priority */
#define OSMTYPES_FOREACH(O) \
	O(Tourism, "tourism") \
	O(Waterway, "waterway") \
	O(Leisure, "leisure") \
	O(Power, "power") \
	O(Building, "building") \
	O(Highway, "highway") \
	O(Barrier, "barrier") \
	O(Amenity, "amenity") \
	O(ManMade, "man_made") \
	O(Landuse, "landuse") \
	O(Landcover, "landcover") \
	O(Indoor, "indoor") \
	O(Natural, "natural") \
	O(Boundary, "boundary") \
	O(Area, "area:") \
	O(Razed, "razed") \
	O(Railway, "railway") \
	O(Wall, "wall") \
	O(Playground, "playground") \
	O(Historic, "historic") \
	O(Attraction, "attraction") \
	O(Seamark, "seamark") \
	O(Roof, "roof")

#define X(en, str) en, 
enum e_osmtype {
	OSMTYPES_FOREACH(X)
	EmptyType,
	/* This type is used when you want to render custom tracks or such*/
	CustomType,
	UnknownType,
};
#undef X
extern const char* osmtype_names[UnknownType+1];
const char* osmtype_str(enum e_osmtype t);

#define OSMROADS_FOREACH(X) \
	X(MotorwayHighway, "motorway", "#e990a0") \
	X(TrunkHighway, "trunk", "#fcc0ac") \
	X(PrimaryHighway, "primary", "#fdd7a1") \
	X(SecondaryHighway, "secondary", "#f6fabb") \
	X(TertiaryHighway, "tertiary", "#fefefe") \
	X(ResidentialHighway, "residential", "#ffffff") \
	X(MotorwayLinkHighway, "motorway_link", "#e990a0") \
	X(TrunkLinkHighway, "trunk_link", "#ffcc0ac") \
	X(PrimaryLinkHighway, "primary_link", "#fdd7a1") \
	X(SecondaryLinkHighway, "secondary_link", "#f6fabb") \
	X(TertiaryLinkHighway, "tertiary_link", "#fefefe") \
	X(LivingStreetHighway, "living_street", "#ededed") \
	X(ServiceHighway, "service", "#ffffff") \
	X(FootwayHighway, "footway", "#a895a5") \
	X(PedestrianHighway, "pedestrian", "#fefefe") \
	X(TrackHighway, "track", "#a48c00") \
	X(BuswayHighway, "busway", "#412cc5") \
	X(EscapeHighway, "escape", "#ff0000") \
	X(RacewayHighway, "raceway", "#f399e6") \
	X(BridlewayHighway, "bridleway", "#005100") \
	X(StepsHighway, "steps", "#515151") \
	X(CorridorHighway, "corridor", "#ff0000") \
	X(PathHighway, "path", "#c5a22c") \
	X(ViaFerrataHighway, "via_ferrata", "#412cc5") \
	X(CyclewayHighway, "cycleway", "#1772a5") \
	X(UnclassifiedHighway, "unclassified", "#ff0000")

#define ENUM_FOREACH(name, str, color) name, 
enum e_highway_subtypes {
	OSMROADS_FOREACH(ENUM_FOREACH)
};

#define NOCOLOR "#ff00ff"
#define OSMNATURE_FOREACH(X) \
	X(FellNatural, "fell", NOCOLOR) \
	X(GrasslandNatural, "grassland", "#88ff4d") \
	X(HeathNatural, "heath", NOCOLOR) \
	X(ScrubNatural, "scrub", "#99ff66") \
	X(ShrubNatural, "shrub", "#669900") \
	X(ShrubberyNatural, "shrubbery", "#669900") \
	X(TreeNatural, "tree", "#197332") \
	X(TreeRowNatural, "tree_row", "#197332") \
	X(TundraNatural, "tundra", NOCOLOR) \
	X(WoodNatural, "wood", " #c68c53") \
	X(BayNatural, "bay", NOCOLOR) \
	X(BeachNatural, "beach", "#ffff99") \
	X(BlowholeNatural, "blowhole", NOCOLOR) \
	X(CapeNatural, "cape", NOCOLOR) \
	X(CoastlineNatural, "coastline", NOCOLOR) \
	X(CrevasseNatural, "crevasse", NOCOLOR) \
	X(GeyserNatural, "geyser", NOCOLOR) \
	X(GlacierNatural, "glacier", NOCOLOR) \
	X(HotSpringNatural, "hot_spring", NOCOLOR) \
	X(IsthmusNatural, "isthmus", NOCOLOR) \
	X(MudNatural, "mud", NOCOLOR) \
	X(PeninsulaNatural, "peninsula", NOCOLOR) \
	X(ReefNatural, "reef", NOCOLOR) \
	X(ShingleNatural, "shingle", "#d1d1e0") \
	X(ShoalNatural, "shoal", NOCOLOR) \
	X(SpringNatural, "spring", NOCOLOR) \
	X(StraitNatural, "strait", NOCOLOR) \
	X(WaterNatural, "water", "#0000ff") \
	X(WetlandNatural, "wetland", NOCOLOR) \
	X(SandNatural, "sand", "#f9f06b") \
	X(UnclassifiedNatural, "unclassified", NOCOLOR)

enum e_natural_subtypes {
	OSMNATURE_FOREACH(ENUM_FOREACH)
};
const char* osmnature_str(enum e_natural_subtypes t);

#define OSMLANDUSE_FOREACH(X) \
	X(CommercialLanduse, "commercial", "#ffcccc") \
	X(ConstructionLanduse, "construction", "#999966") \
	X(BrownfieldLanduse, "brownfield", "#999966") \
	X(EducationLanduse, "education", "#ffcccc") \
	X(ResidentialLanduse, "residential", "#99ff99") \
	X(GrassLanduse, "grass", "#66ff66") \
	X(VillageGreenLanduse, "village_green", "#66ff66") \
	X(MeadowLanduse, "meadow", "#88ff4d") \
	X(ForestLanduse, "forest", "#1f7a1f") \
	X(RetailLanduse, "retail", "#b38f00") \
	X(CemetaryLanduse, "cemetery", "#d279d2") \
	X(RailwayLanduse, "railway", "#d2a679") \
	X(FlowerbedLanduse, "flowerbed", "#ff9999") \
	X(ReligiousLanduse, "religious", "#809fff") \
	X(RecreationGroundLanduse, "recreation_ground", "#ffcc00") \
	X(OrchardLanduse, "orchard", "#ff9966") \
	X(FarmlandLanduse, "farmland", "#d0ffa3") \
	X(FarmyardLanduse, "farmyard", "#ffa87b") \
	X(CivicAdminLanduse, "civic_admin", "#62a0ea") \
	X(AllotmentsLanduse, "allotments", "#b5835a") \
	X(CulturalLanduse, "cultural", "#3771c8")

enum e_landuse_subtypes {
	OSMLANDUSE_FOREACH(ENUM_FOREACH)
	UnknownLanduse
};
const char* osmlanduse_str(enum e_landuse_subtypes t);

#define OSMLEISURE_FOREACH(X) \
	X(PitchLeisure, "pitch", "#26a269") \
	X(SportCenterLeisure, "sports_centre", "#ffa348") \
	X(ParkLeisure, "park", "#8fff44") \
	X(PlaygroundLeisure, "playground", "#f9f06b") \
	X(TrackLeisure, "track", "#e66100") \
	X(StadiumLeisure, "stadium", "#c01c28") \
	X(GardenLeisure, "garden", "#33d17a") \
	X(PoolLeisure, "swimming_pool", "#99c1f1") \
	X(NatureReserveLeisure, "nature_reserve", "#107f47") \
	X(OutdoorSeatingLeisure, "outdoor_seating", "#f5c211")

enum e_leisure_subtypes {
	OSMLEISURE_FOREACH(ENUM_FOREACH)
	UnknownLeisure
};
#undef ENUM_FOREACH
const char* osmleisure_str(enum e_leisure_subtypes t);

struct s_waycustom {
	const char* color;
	const char* name;
};

/**
 * @brief All information that a way could store
 **/
struct s_wayattr {
	/** 
	 * @brief The current Type of the way 
	 * @details This is not as precise as it could since the osm data is very ambiguus
	 * **/
	enum e_osmtype type;
	/**
	 * @brief the more exact definition of the way
	 * @details The exact road type, exact foilage type or building type etc.
	 **/
	union {
		enum e_highway_subtypes highway;
		enum e_natural_subtypes natural;
		enum e_landuse_subtypes landuse;
		enum e_leisure_subtypes leisure;
		struct s_waycustom custom;
	} subtypes;
};

#define RELATION_FOREACH(X) \
	X(MultipolygonRel, "multipolygon") \
	X(RouteRel, "route") \
	X(RoutemasterRel, "route_master") \
	X(RestrictionRel, "restriction") \
	X(BoundaryRel, "boundary") \
	X(PublicTransportRel, "public_transport") \
	X(DestinationSignRel, "destination_sign") \
	X(WaterwayRel, "waterway") \
	X(EnforcementRel, "enforcement") \
	X(ConnectivityRel, "connectivity") \
	X(UnknownRel, "unknown")

#define X(id, str) id,
enum e_relation {
	RELATION_FOREACH(X)
};

#define RELATIONROLE_FOREACH(X) \
	X(LabelRole, "label") \
	X(InnerRole, "inner") \
	X(OuterRole, "outer")

enum e_relationrole {
	RELATIONROLE_FOREACH(X)
	UnknownRole
};

#undef X

#define RELATIONMEMBER_FOREACH(X) \
	X(NodeMember, "node", struct s_osmnode, osmnodes, nodes) \
	X(WayMember, "way", struct s_osmway, osmways, ways) \
	X(RelationMember, "relation", struct s_osmrelation, osmrel, relations)

#define X(id, str, T, suffix, avl_tree) id,
enum e_relationmember {
	RELATIONMEMBER_FOREACH(X)
};
#undef X

#endif
