/* Copyright (c) 2019 John Schember <john@nachtimwald.com>

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the “Software”), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
/**
 * @file tpool.h
 * @brief Relatively nice threadpool
 * @author John Schember
 **/
#ifndef TPOOL_H
#define TPOOL_H

// Mostly taken from https://nachtimwald.com/2019/04/12/thread-pool-in-c/
// Therefore I don't claim the copyright for this

#include <stddef.h>
#include <stdbool.h>

struct tpool;
typedef struct tpool tpool_t;

typedef void (*thread_func_t)(void* arg);
tpool_t* tpool_create(size_t num);
void tpool_destroy(tpool_t* tp);

bool tpool_add_work(tpool_t* tp, thread_func_t func, void* arg);
void tpool_wait(tpool_t* tp);

#endif
