#ifndef DJ_XML
#define DJ_XML
/**
   @file xml.h
   @brief Provides a simple and relatively stable way to parse and traverse (simple) xml files.
   @warning It does not pass all xml standard tests, yet most simple xml files should be read correctly
   @author dbauer
 **/
#include <stdio.h>
#include "arena_allocator.h"
#include "attributes.h"

/**
   @brief An xml node which is used in the xml-tree representation.
   @details It contains a linked list of its children 
   and is member of the linked list for its siblings
   It contains the string of which tag it belongs too 
   and its data in string representation
 **/
typedef struct s_xml_node {
	/** @brief Parent node **/
	struct s_xml_node* parent;
	/** @brief The first of all children nodes **/
	struct s_xml_node* children;
    /** @brief The last element of the children linked list **/
    struct s_xml_node* last_child;
	/** @brief Linked list for next nodes **/
	struct s_xml_node* next;
	/** @brief linked list of all attributes **/
	struct s_xml_attr* attributes;
    /** @brief The last element of the attribute linked list **/
    struct s_xml_attr* last_attr;
	/** @brief The tag (type of the node) like `<node>` **/
	char* tag;
	/** @brief The actual content of the node in string representation **/
	char* content;
} xml_node;

/** @brief An attribute held by an xml node **/
typedef struct s_xml_attr {
	/** @brief Next attribute **/
	struct s_xml_attr* next;
	/** 
	 * @brief The tag
	 * @details The attribute name
	 **/
	char* tag;
	/** @brief Actual content of the attribute in string representation **/
	char* content;
} xml_attr;

/**
   @brief The object returned when parsing an xml-file.
   @details It essentially just contains the root node of the dom
 **/
typedef struct{
	/** @brief The root node **/
	xml_node* root;
	/** @brief The MemoryArena object, which holds all the allocated memory **/
	MemoryArena memory;
} xml_dom;

/** 
 * @brief Destroys and frees an xml-dom object 
 * @param dom object which should be destroyed
 **/
void xml_free(xml_dom* dom);
/** 
 * @brief Parses an utf-8 xml string 
 * @param data string object holding the xml code
 **/
xml_dom* xml_parse_string(const char* data)
	PROPER_STRING(1) /*MALLOC(xml_free)*/;
/** 
 * @brief Parses an utf-8 encoded file 
 * @param f file handle to be read from
 **/
xml_dom* xml_parse_file(FILE* f)
	NONNULL(1) /*MALLOC(xml_free)*/;
/** 
 * @brief Opens a file and parses its content into a dom-tree 
 * @param filename of the file to be read from
 **/
xml_dom* xml_read_file(const char* filename)
	PROPER_STRING(1) /*MALLOC(xml_free)*/;
/** 
 * @brief Prints out the xml-dom 
 * @param dom object to be dumped
 **/
void xml_print_dom(const xml_dom* dom)
	NONNULL(1);

#endif
