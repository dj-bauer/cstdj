/**
 * @file graph.h
 * @brief Graph data structure implemented with adjecency lists
 * @author dbauer
 **/
#ifndef DJ_GRAPH_ADJ_H
#define DJ_GRAPH_ADJ_H
#include <stdint.h>
#include <stdbool.h>

#include "attributes.h"
#include "vector.h"
#include "dlist.h"

typedef struct s_adjnode {
	// The index into the array this element is sitting at
	long index;
	// The unique id to represent this edge
	uint32_t id;

	Vector_l edges;

	bool highlight;
	void* data;

	union u_algorithmata {
		struct s_primalgorithm {
			long parent;
			double cost;
		} prim;
		struct s_dijkstraalgorithm {
			struct s_adjnode* parent;
			double distance;
		} dijkstra;
		struct s_dfsalgorithm {
			enum e_dfscolor {DFS_WHITE, DFS_GRAY, DFS_BLACK} color;
		} dfs;
	} algorithms;
} AdjNode;

typedef struct s_adjedge {
	// The index into the array this element is sitting at
	long index;
	// The unique id to represent this edge
	uint64_t id;

	long start;
	long end;

	double weight;
	bool highlight;
	void* data;
} AdjEdge;

VECTOR_DECL(nodes, struct s_adjnode)
VECTOR_DECL(edges, struct s_adjedge)

typedef struct s_adjgraph {
	bool directed;
	Vector_nodes nodes;
	Vector_edges edges;
	long next_node_id;
} AdjGraph;

typedef void(*gnode_print_func)(void*, char[32]);

int graph_init(AdjGraph* g, bool is_directed);
int graph_cleanup(AdjGraph* g);
AdjNode* graph_add_node(AdjGraph* g, void* data);
/**
 * Creates an edge between a and b if it doesn't exist before.
 **/
AdjEdge* graph_add_edge(AdjGraph* g, AdjNode* a, AdjNode* b, 
		double weight, void* data)
        NONNULL(1, 2, 3);
/**
 * Wraps graph_add_edge to be used with indices 
 **/
AdjEdge* graph_add_edge_i(AdjGraph* g, long a, long b, 
		double weight, void* data);

void print_graph(const AdjGraph* g, 
		char* buf, size_t buf_len, gnode_print_func print_func)
        NONNULL(1, 2, 4);

#define graph_node_count(g) vector_size_nodes(&(g)->nodes)
#define graph_edge_count(g) vector_size_edges(&(g)->edges)
#define graph_node_edge_count(node) vector_size_l(&(node)->edges)

/**
 * Checks whether a connection exists between the node a and b
 **/
bool graph_connection_exists(const AdjGraph* g, const AdjNode* a, const AdjNode* b)
    NONNULL(1,2,3);

#define graph_highlight_edge(e, value) (e)->highlight = value
#define graph_highlight_node(n, value) (n)->highlight = value
#define graph_highlight_node_i(graph, n_id, value) graph_highlight_node(graph_node(graph, n_id), value)

AdjNode* graph_node(AdjGraph* g, long node_id)
    NONNULL(1);
const AdjNode* graph_node_c(const AdjGraph* g, long node_id)
    NONNULL(1);
AdjEdge* graph_edge(AdjGraph* g, long edge_id)
    NONNULL(1);
const AdjEdge* graph_edge_c(const AdjGraph* g, long edge_id)
    NONNULL(1);
/**
 * Searches through all the nodes and tries to find the first one
 * that contains data in its data field
 * Returns NULL if not found
 * @param g graph to be searched through
 * @param data search query
 * @return the first node the has the data field.
 */
AdjNode* graph_get_node(AdjGraph* g, void* data)
    NONNULL(1);

/**
 * @brief Generates the minimum spanning tree using prims algorithm
 * @details Highlights all edges, which are used in the spanning tree
 * @params g graph, which edges highlight attribute will be modified
 * @return The number of components the tree has
 **/
int graph_prim_spanntree(AdjGraph* g)
    NONNULL(1);
int graph_kruskal_spanntree(AdjGraph* g)
    NONNULL(1);

// Highlights the edges used
// stores the dijkstra predecessor for each node in its algorithm union
bool graph_dijkstra(AdjGraph* g, AdjNode* start);
// Performs dijkstra but only until a certain node is found.
// This only works with positive edge weights
bool graph_dijkstra_to(AdjGraph* g, AdjNode* start, AdjNode* dest);
// Returns wether a path was found or not
void graph_retrace_dijkstra(dlist_l* edges, double* weight, const AdjGraph* graph, const AdjNode* dest)
    NONNULL(1);
// Checks whether an undirected graph has a cycle in O(|V| + |E|)
bool graph_has_circle_undirected(const AdjGraph* g)
    NONNULL(1);

#endif
