/**
 * @file arena_allocator.h
 * @brief A simple linear allocator
 * @details Can be used nicely when you do many allocations, which will be deallocated at once
 * @author dbauer
 **/
#ifndef DJ_ARENA_ALLOCATOR_H
#define DJ_ARENA_ALLOCATOR_H

#include "dlist.h"

struct s_memzone{
	size_t capacity;
	size_t allocated;
	char* mem;
};

DLIST_DECL(memzone, struct s_memzone)

typedef struct s_memory_arena {
	dlist_memzone zones;
	size_t capacity;
	size_t allocated;
	size_t minimum_bs;
} MemoryArena;

void init_memarena(MemoryArena* a);
void deinit_memarena(MemoryArena* a);
char* memarena_alloc(MemoryArena* a, size_t n);
char* memarena_calloc(MemoryArena* a, size_t n);

#endif
