/**
 * @file union_find.h
 * @author dbauer
 * @brief Implements the union find data structure
 * @details This can be used to quickly check if two points are connected via a path and connect points.
 * Especially usefull for kruskal minimum spanning tree
 **/
#ifndef UNION_FIND_H
#define UNION_FIND_H

#include "attributes.h"

/**
 * @brief The union find data structure.
 * @details Implemented using an array
 **/
typedef struct s_unionfind {
	/** @brief the length of the array of representants **/
    long num_indices;
	/** @brief array storing what node is represented by what node **/
    long represents[];
} UnionFind;

void unionfind_destroy(UnionFind* uf);
UnionFind* unionfind_create(long num_indices)
	MALLOC(unionfind_destroy);
/**
 * Calculates the representative of a node and updates the datastructure for
 * faster requests
 * @param uf UnionFind datastructure to be queried from
 * @param node to be queried
 * @return the representative of the node
 */
long unionfind_representative(UnionFind* uf, long node);
long unionfind_connect(UnionFind* uf, long a, long b);
/**
 * @return 1 if a and b have the same representative, ergo they are in the same component. 0 Otherwise
 **/
int unionfind_check(UnionFind* uf, long a, long b);

#endif
