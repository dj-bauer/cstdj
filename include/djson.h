#ifndef DJ_JSON_H
#define DJ_JSON_H

#include <stdbool.h>
#include <stddef.h>
#include "attributes.h"

typedef enum {JNull, JNumber, JBool, JString, JObject, JArray, JRoot} JType;

typedef struct s_json_node JNode;

JType json_type(const JNode* node);
bool json_is_type(const JNode* node, JType);

size_t json_size(const JNode* obj);

JNode* json_obj_get(JNode* obj, const char* key);
JNode* json_arr_get(JNode* arr, size_t index);
const char* json_get_str(const JNode* str);
// The normal function
double json_get_num(const JNode* num);
// Some utility functions for easy casting
int json_get_int(const JNode* num);
long json_get_long(const JNode* num);
float json_get_float(const JNode* num);
bool json_get_bool(const JNode* num);

/**
 * Returns a property of an object directly as an integer
 */
int json_obj_int(const JNode* obj, const char* key);

void json_free(JNode* root);
JNode* json_parse(const char* json_buf, size_t buf_size) NONNULL(1);
void json_print(JNode* root);

#endif
