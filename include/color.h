/**
 * @file color.h
 * @author dbauer
 * @brief Holds many rgb-color constants
 **/
#ifndef STDJ_COLOR_H
#define STDJ_COLOR_H

/** 
 * @brief A rgb color value
 * @details It's values range between 0 and 1
 * **/
typedef struct s_color{
	/** @brief Red component **/
	float r;
	/** @brief Green component **/
	float g;
	/** @brief Blue component **/
	float b;
} Color;

/** @brief 256 different colors which can be used for various distinctive drawings **/
extern Color distincts[256];

extern Color RED; 
extern Color DARK_RED;
extern Color LIGHT_RED; 
extern Color GREEN;
extern Color DARK_GREEN;
extern Color BLUE;
extern Color LIGHT_BLUE;
extern Color YELLOW;
extern Color ORANGE;
extern Color PURPLE;

extern Color BLACK;
extern Color DARK_GRAY;
extern Color GRAY;
extern Color LIGHT_GRAY;
extern Color WHITE;

void color_2_hex(char dst[8], Color c);

#endif
