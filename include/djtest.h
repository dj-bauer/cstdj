/**
 * @file djtest.h
 * @brief Rudimentary Unittesting framework
 * @author dbauer
 **/
#ifndef DJ_TEST_H
#define DJ_TEST_H

#include "logger.h"
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <stdio.h>

#define dj_assert_op(a, b, operation, fmt, T) \
	do {                                      \
        if(!(a operation b)) { \
            log_error("Assertion failed " #a " " #operation " " #b " (" fmt " " #operation " " fmt ")", (T) a, (T) b); \
            return 1; \
        } else { \
            tests_passed += 1;\
        } \
    } while(0)
#define assert_true(a) \
	do {                  \
        if(!(a)) { \
		    log_error("Assertion failed " #a " == true"); \
		    return 1; \
	    } else { \
            tests_passed += 1; \
        } \
    } while(0)

#define assert_false(a) \
    do {                    \
	    if(a) { \
		    log_error("Assertion failed " #a " == false"); \
		    return 1; \
	    } else { \
		    tests_passed += 1; \
	    } \
    } while (0)

#define assert_s_op(a, b, op) \
	if(!(strcmp(a,b) op 0)) { \
		log_error("Assertion failed `" #a " == " #b "` (%s) == (%s)", a, b);\
		return 1; \
	} else { \
		tests_passed += 1; \
	}

#define assert_sorted_impl(T) \
	bool assert_sorted_##T(size_t n, T array[n]) { \
		if(n == 0) return true; \
		T last = array[0]; \
		for(size_t i=1; i<n; i++) { \
			if(array[i] < last) return false; \
			last = array[i]; \
		} \
		return true; \
	}
assert_sorted_impl(int)

#define assert_i_sorted(n, array) assert_true(assert_sorted_int(n, array))
#define assert_i_unsorted(n, array) assert_false(assert_sorted_int(n, array))

#define assert_f_approx(a, b, tolerance) \
	do { \
		if(fabs(a-b) > tolerance) { \
			log_error("Assertion failed " #a " ~= " #b " ( %f ~= %f )", (float) a, (float) b); \
			return 1; \
		} else { \
			tests_passed += 1;\
		} \
	} while(0);
#define assert_f_op(a, b, operation) dj_assert_op(a, b, operation, "%f", float)
#define assert_f_eq(a,b) assert_f_op(a,b, ==)
#define assert_f_ne(a,b) assert_f_op(a,b, !=)
#define assert_f_gt(a,b) assert_f_op(a,b, >)
#define assert_f_lt(a,b) assert_f_op(a,b, <)
#define assert_f_ge(a,b) assert_f_op(a,b, >=)
#define assert_f_le(a,b) assert_f_op(a,b, <=)

#define assert_i_op(a, b, operation) dj_assert_op(a, b, operation, "%d", int)
#define assert_i_eq(a,b) assert_i_op(a,b, ==)
#define assert_i_ne(a,b) assert_i_op(a,b, !=)
#define assert_i_gt(a,b) assert_i_op(a,b, >)
#define assert_i_lt(a,b) assert_i_op(a,b, <)
#define assert_i_ge(a,b) assert_i_op(a,b, >=)
#define assert_i_le(a,b) assert_i_op(a,b, <=)

#define assert_l_op(a, b, operation) dj_assert_op(a, b, operation, "%ld", long)
#define assert_l_eq(a,b) assert_l_op(a,b, ==)
#define assert_l_ne(a,b) assert_l_op(a,b, !=)
#define assert_l_gt(a,b) assert_l_op(a,b, >)
#define assert_l_lt(a,b) assert_l_op(a,b, <)
#define assert_l_ge(a,b) assert_l_op(a,b, >=)
#define assert_l_le(a,b) assert_l_op(a,b, <=)

//#define assert_p_op(a, b, operation) dj_assert_op(a, b, operation, "%p")
#define assert_p_op(a, b, operation) \
	do {                                      \
        if(!(a operation b)) { \
            log_error("Assertion failed " #a " " #operation " " #b " (%p " #operation " %p)", (void*) a, (void*) b); \
            return 1; \
        } else { \
            tests_passed += 1;\
        } \
    } while(0)
#define assert_p_eq(a,b) assert_p_op(a,b, ==)
#define assert_p_ne(a,b) assert_p_op(a,b, !=)
#define assert_p_gt(a,b) assert_p_op(a,b, >)
#define assert_p_lt(a,b) assert_p_op(a,b, <)
#define assert_p_ge(a,b) assert_p_op(a,b, >=)
#define assert_p_le(a,b) assert_p_op(a,b, <=)

#define assert_s_eq(a,b) assert_s_op(a,b, ==)
#define assert_s_ne(a,b) assert_s_op(a,b, !=)
#define assert_s_gt(a,b) assert_s_op(a,b, >)
#define assert_s_lt(a,b) assert_s_op(a,b, <)
#define assert_s_ge(a,b) assert_s_op(a,b, >=)
#define assert_s_le(a,b) assert_s_op(a,b, <=)


#define assert_null(a) assert_p_eq(a, (void*) NULL)
#define assert_nonnull(a) assert_p_ne(a, (void*) NULL)
#define assert_zero(a) assert_i_eq(a, 0)

#define test_begin(name)\
	int tests_passed = 0; \
	log_info("Testing "name)

#define test_end() \
	log_info("%d Tests passed", tests_passed); \
	return 0

#define run_test(func) \
	if(func()) return 1

bool cmp_mem_eq(const char* a, const char* b, size_t amount);

int test_inout(const char* directory, int (*t_func)(const char*));

#endif
