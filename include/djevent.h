#ifndef DJ_EVENT_H
#define DJ_EVENT_H
#include "keycodes.h"
/**
 * @file djevent.h
 * @author dbauer
 * @brief Declares the event types, which are sent to callback functions
 **/


/** @brief All the possible events that can be subscribed to **/
typedef enum {KeyPressEvent, KeyReleaseEvent, MousePressEvent,
	          MouseReleaseEvent, MouseMotionEvent, MouseScrollEvent, 
			  WinResizeEvent, WinFocusEvent, WinUnfocusEvent, WinCloseEvent, EventCount} DJEventType;

typedef enum {ShiftModifier = 0x1, CtrlModifier = 0x2, AltModifier = 0x4, AltGrModifier = 0x8, WindowsModifier = 0x10} DJKeyModifier;

#include "keycodes.h"

/** @brief The event which is passed by the window to a callback function **/
typedef union {
	/** @brief Supplies the pressed key **/
	struct {
		DJEventType type;
		struct s_djwindow* win;
		KeySymbol key;
		DJKeyModifier modifier;
		/**This contains the string the key would have resulted in from the current key combination **/
		const char* utf8_ch;
		/**A friendly name description of the key**/
		const char* utf8_name;
	} keypress;
	/** @brief supplies the released key **/
	struct {
		DJEventType type;
		KeySymbol key;
	} keyrelease;
	/** @brief Supplies the mouse position and button which has been pressed **/
	struct {
		DJEventType type;
		int x;
		int y;
		MouseButton button;
	} buttonpress;
	/** @brief Supplies the mouse position and button which has been released **/
	struct {
		DJEventType type;
		int x;
		int y;
		MouseButton button;
	} buttonrelease;
	/** @brief Supplies the new mouse position after move **/
	struct {
		DJEventType type;
		int x;
		int y;
	} mousemove;
	/** @brief Supplies the direction the mousewheel has scrolled **/
	struct {
		DJEventType type;
		int direction;
	} mousescroll;
	/** @brief Supplies the new window dimensions on a resize **/
	struct {
		DJEventType type;
		int width;
		int height;
	} resize;
	/** @brief Supplies the window to the close event **/
	struct {
		DJEventType type;
		struct s_djwindow* win;
	} close;
} DJEvent;

#endif
