/**
 * @file polygon.h
 * @author dbauer
 * @brief Simple polygon utilities
 **/
#ifndef POLYGON_H
#define POLYGON_H
#include <stddef.h>

/** @brief very rudimentary 2d vector used for polygon stuff **/
typedef struct s_vec2 {
	/** @brief x component **/
	long double x;
	/** @brief y component **/
	long double y;
} vec2;

vec2 vec2_add(vec2 a, vec2 b);
vec2 vec2_sub(vec2 a, vec2 b);
/** @brief Calculates the length of a vector **/
long double vec2_dist(vec2 a);
vec2 vec2_abs(vec2 a);
vec2 vec2_sca_mult(vec2 a, long double sca);
long double vec2_sca(vec2 a, vec2 b);
/** @brief Calculates the normalized vector **/
vec2 vec2_norm(vec2 v);
long double vec2_angle(vec2 a, vec2 b);

/**
 * @brief Checks the vertex winding order
 * @return different signed value depending on the winding order
 **/
long double vertex_winding(size_t num_points, const vec2 points[num_points]);
/**
 * @brief Flips the vertex winding order of a given polygon
 **/
void flip_winding_order(size_t num_points, vec2 points[num_points]);
/**
 * @brief Calculates the area of a polygon
 * @details Uses ineffitent O(n^2) Ear-Clipping technique
 * @todo Implement faster algorithm
 **/
long double polygon_area(size_t n, const vec2 points[n]);

#endif
