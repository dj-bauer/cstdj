/**
 * @file djtest.h
 * @brief Collection of usefull hash functions
 * @author dbauer
 **/
#ifndef DJ_HASH_H
#define DJ_HASH_H
#include <stddef.h>

size_t hash_str(const char* str);

#endif
