INCLUDES=-Iinclude

# We compile with address sanitizer and debug build by default
#SANITIZE=1
#DEBUG=1

LIB_NAME=stdj
SHARED_OBJECT=lib${LIB_NAME}.so
BUILD_DIR=build
# List of targets that should be built and ran on test target
AUTOTEST=heap matrix queue bitreader wav trie graph union_find dijkstra stack zlib dlist fast_strlen avl png vector jsonTree

DEBUG_FLAGS = -g -Werror -p
SANITIZER_FLAGS = -fsanitize=address
OPTIMIZE_FLAGS = -DNDEBUG -O3
flags=-std=c99 -Wall -pedantic
LDFLAGS:=-lasound -lm
TEST_LDFLAGS:=-L. -l${LIB_NAME} -lm

ifdef ANALYZE
	flags += -fanalyzer
endif

ifdef SANITIZE
	DEBUG_FLAGS += ${SANITIZER_FLAGS}
	LDFLAGS += ${SANITIZER_FLAGS}
	TEST_LDFLAGS += ${SANITIZER_FLAGS}
	DEBUG=1
	build_name="Debug with Sanitizer"
else
	build_name="Debug without Sanitizer"
endif

ifdef DEBUG
	flags += ${DEBUG_FLAGS}
else
	RELEASE=1
endif

ifdef RELEASE
	flags += ${OPTIMIZE_FLAGS}
	build_name="Release"
endif

ifndef WINDOW_PLATFORM
	WINDOW_PLATFORM:="X"
endif

SOURCES:=$(wildcard src/*.c src/wayland/*.c)

ifeq (${WINDOW_PLATFORM}, "X")
	SOURCES:=$(filter-out %_wayland.c, $(SOURCES))
	LDFLAGS += -lX11 -lGLX
else
	SOURCES:=$(filter-out %_x.c, ${SOURCES})
	LDFLAGS += -lwayland-client -lrt -lxkbcommon -lwayland-egl -lEGL
	WAYLAND_PROTOCOLS=shell decoration toplevel-icon
	EXTRA_DEPS += $(foreach p,${WAYLAND_PROTOCOLS}, include/wayland/xdg-$p-client-protocol.h)
	SOURCES += $(foreach p,${WAYLAND_PROTOCOLS}, src/wayland/xdg-$p-client-protocol_wayland.c)
endif
# We wanna remove duplicates
SOURCES:=$(sort ${SOURCES})

OBJECTS:=$(patsubst %.c,%.o, ${SOURCES})
DEPENDENCIES:=$(patsubst %.c, %.d, ${SOURCES})
ALL_TESTS:=$(patsubst tests/%.c,%, $(wildcard tests/*.c))

all: ${ALL_TESTS}
	@echo "Building ${build_name} for window ${WINDOW_PLATFORM}"

-include ${DEPENDENCIES}

%.o: %.c Makefile ${EXTRA_DEPS}
	${CC} ${flags} ${INCLUDES} -MMD -MP -c -fpic $< -o $@


%: tests/%.o Makefile ${SHARED_OBJECT}
	@mkdir -p ${BUILD_DIR}
	${CC} $< -o ${BUILD_DIR}/$@ ${TEST_LDFLAGS}

${LIB_NAME}: ${SHARED_OBJECT}
	
${SHARED_OBJECT}: ${OBJECTS} Makefile
	rm -f $@
	${CC} ${LDFLAGS} ${OBJECTS} -shared -o $@

install: ${SHARED_OBJECT}
	install -d $(DESTDIR)/usr/lib/
	install -m 644 ${SHARED_OBJECT} $(DESTDIR)/usr/lib/

RUN_AUTOTESTS:=$(patsubst %,run_test_%, ${AUTOTEST})
# This is the master test function, which runs all tests available
testall: ${RUN_AUTOTESTS} runjsontest

run_test_%: %
	./${BUILD_DIR}/$< | grep "[ERROR|WARNING]"
	@printf "\033[32mPassed $< :) \033[0m\n"

check:
	cppcheck --check-level=exhaustive --std=c99 src/*.c src/*.h include/*.h

clean:
	rm -f ${SHARED_OBJECT} ${OBJECTS} ${DEPENDENCIES} src/wayland/xdg-*-client-protocol_wayland.c include/wayland/xdg-*-client-protocol.h src/*.o src/*.d tests/*.o tests/*.d
	rm -rf ${BUILD_DIR} doc/doxygen/

doc: testjson
	doxygen config.dox
	rm -rf doc/doxygen/html/json_results
	cp -r tests/JSONTestSuite/results/ doc/doxygen/html/json_results

viewdoc:
	${BROWSER} ./doc/doxygen/html/index.html

testjson: json
	LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:. ./build/json || true
	LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:. ./tests/JSONTestSuite/run_tests.py
	#LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:. ./tests/JSONTestSuite/run_tests.py --filter tests/jsonTestSuite.json

JSON_TESTS_PASS=$(wildcard tests/JSONTestSuite/test_parsing/y_*.json)
JSON_TESTS_FAIL=$(wildcard tests/JSONTestSuite/test_parsing/n_*.json)
runjsontest: $(JSON_TESTS_PASS)  $(JSON_TEST_FAIL)
tests/JSONTestSuite/test_parsing/y_%.json: json FORCE
	./build/json $@
tests/JSONTestSuite/test_parsing/n_%.json: json FORCE
	./build/json $@ && sh -c 'exit 1' || sh -c 'exit 0'

WL_PROT_DIR=/usr/share/wayland-protocols/
WL_SHELL_PROT=${WL_PROT_DIR}stable/xdg-shell/xdg-shell.xml
WL_DECORATION_PROT=${WL_PROT_DIR}unstable/xdg-decoration/xdg-decoration-unstable-v1.xml
WL_ICON_PROT=${WL_PROT_DIR}staging/xdg-toplevel-icon/xdg-toplevel-icon-v1.xml

include/wayland/xdg-shell-client-protocol.h: ${WL_SHELL_PROT}
	wayland-scanner client-header < $< > $@
include/wayland/xdg-decoration-client-protocol.h: ${WL_DECORATION_PROT}
	wayland-scanner client-header < $< > $@
include/wayland/xdg-toplevel-icon-client-protocol.h: ${WL_ICON_PROT}
	wayland-scanner client-header < $< > $@

src/wayland/xdg-shell-client-protocol_wayland.c: ${WL_SHELL_PROT} include/wayland/xdg-shell-client-protocol.h
	wayland-scanner private-code < $< > $@
src/wayland/xdg-decoration-client-protocol_wayland.c: ${WL_DECORATION_PROT} include/wayland/xdg-decoration-client-protocol.h
	wayland-scanner private-code < $< > $@
src/wayland/xdg-toplevel-icon-client-protocol_wayland.c: ${WL_ICON_PROT} include/wayland/xdg-toplevel-icon-client-protocol.h
	wayland-scanner private-code < $< > $@

.PHONY: clean doc
FORCE:
