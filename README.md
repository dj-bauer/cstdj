# cstdj

My personal standard toolset.
This includes a bunch of datastructures and other stuff i get interested.
The code should be implemented in ISO-C99.

Specific systems are only working on linux like the framebuffer video.

The main doxygen documentation can be found [here](https://cstdj-dj-bauer-b2e271ebd7850636573f3fb0d97ae6ae587615ee85a52411.gitlab.io/)

# TODOs
  - [ ] AVL Remove
  - [ ] improve wayland window support

# Systems

  - Arena Allocator
  - Audio
  - AVL-Tree algorithms
    * Currently only insertion is implemented
    * Removal is a todo
  - Bitreader
    * Utility for the Zlib implementation
  - Binary Search Tree
  - CAS
    * very rudimentary attempt
  - Color
    * Simple header with color constants as RGB-float triplets
  - DJwindow
    * window abstraction system
    * Currently supported platforms
      - Good X support
      - Rudimentary wayland support
      - No windows support
  - DJtest
    * simple unittesting framework
  - dlist
    * Double linked list
  - Drw
    * Drawing utilities used in conjunction with the window system
  - Fast strlen
    * if you just wanna compare string lengths, this will do it quicker
  - Fbvideo
    * Framebuffer video. Very fast video outside of X
  - Graph datastructure
    * Not all edge and node operations are implemented yet
  - Hash
    * usefull hash functions
  - Hashmap
  - Heap
    * Binary heap using a custom compare function
  - Huffman tree
    * Used for zlib implementation
  - Logger so you never wanna do printf again
  - Matrix
    * Linear algebra written like a true noob
  - Network packet
    * A very simple network packet marshalling library
  - OSM
    * OpenStreetMap utility.
    * So far it can only open and store maps
  - PNG
    * Acceptable png parsing library
  - Polygon
    * Utilities for 2D-Polygons like calculating the area
  - Queue
  - Stack
  - Svg
    * write pretty svg graphics from code
  - Thread pool
    * Simply push tasks to a thread pool
  - Trie
    * Fast string search
  - Union Find
    * No faster way using Kruskal
  - Vector
    * Dynamic array
  - Wav
    * Rudimentary wave file
  - xml
    * Lightweight and not standard conforming xml parser
  - Zlib
    * Buggy, but working zlib parser implementation
  - json
    * WIP RFC8259 compatible json parser. Test raiting should be [here](json_results/parsing.html)


# Thanks
Big thanks to Mark Adler for his amazing documentation of the deflate format.
This was propably the biggest and most challenging implementation,
and a miracle it works :)

# Copyright

The following parts are not under my copyright and have their own license shipped with them

  - tpool.h,tpool.c Copyright (C) 2019 John Schember <john@nachtimwald.com>
  - Openstreetmap test-files in tests/\*.osm OpenStreetMap® is open data, licensed under the Open Data Commons Open Database License (ODbL) by the OpenStreetMap Foundation (OSMF). 
  - tests/dijkstra-data/ and tests/uf-data Copyright (C) Copyright 2024 Kohei Morita <https://github.com/yosupo06/library-checker-problems> (Apache License)
  - tests/pngs Copyright (C) 2011 Willem van Schaik <http://www.schaik.com/pngsuite/>
  - tests/xml-testfiles/XML-Test-Suite/ Copyright (C) W3C <https://www.w3.org/XML/Test/>

Copyright (C) 2024 Daniel Bauer <daniel.bauer@caserio.de>

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.
