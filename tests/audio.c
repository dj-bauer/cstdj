#include "audio.h"
#include <alsa/asoundlib.h>
#include <math.h>
#include "util.h"

AudioDevice* dev;

#define PI 3.1415926

#define seconds 2
#define sample_rate 44100
#define num_samples (seconds * sample_rate)

void sin_snd(unsigned char* samples, size_t n, double freq) {
	for(size_t i=0; i<n; i++) {
		double x = (double) i / (double) sample_rate;
		double y = sin(x * 2 * PI * 440);
		// transform y from [-1;1] to [0;1]
		y = (y+1.0) / 2.0;
		samples[i] = y * 255.0;
	}
}
void square_snd(unsigned char* samples, size_t n, double freq) {
	size_t full_cycle = (double) sample_rate / freq;
	size_t half_cycle = full_cycle / 2.0;
	size_t cycle_index = 0;
	for(size_t i=0; i<n; i++) {
		samples[i] = cycle_index < half_cycle ? 0xFF : 0x00;
		cycle_index = (cycle_index+1) % full_cycle;
	}
}

void sawtooth_snd(unsigned char* samples, size_t n, double freq) {
	for(size_t i=0; i<n; i++) {
		double x = (double) i / (double) sample_rate;
		x *= freq;
		double y = x - floor(x);
		samples[i] = y * 255.0;
	}
}

void triang_snd(unsigned char* samples, size_t n, double freq) {
	double p = 1 / freq;
	for(size_t i=0; i<n; i++) {
		double x = (double) i / (double) sample_rate;
		double y = (4.0/p) * fabs(fmod(x - p/4.0, p) - (p/2.0)) / 2.0;
		samples[i] = y * 255.0;
	}
}

void mix_snd(unsigned char* dst, const unsigned char* bufa, const unsigned char* bufb, size_t n) {
	for(size_t i=0; i<n; i++) {
		// Check for overflow
		unsigned short sum = (unsigned short) bufa[i] + (unsigned short) bufb[i];
		dst[i] = MIN(sum, 0xff);
	}
}

int main(int argc, const char* argv[]) {
	dev = audio_open(sample_rate, 1, Unsigned, Bits8, 0);

	unsigned char square[num_samples] = {0};
	square_snd(square, num_samples, 880);

	unsigned char sines[num_samples] = {0};
	sin_snd(sines, num_samples, 440);

	unsigned char samples[num_samples] = {0};
	//mix_snd(samples, square, sines, num_samples);
	
	triang_snd(samples, num_samples, 440);

	audio_play(dev, samples, num_samples);
	audio_block(dev);

	audio_close(dev);
	return 0;
}
