#include "termcolor.h"
#include "unicode.h"
#include <assert.h>
#include <stdio.h>

const char* colors[] = {ANSI_BLACK, ANSI_RED, ANSI_GREEN, ANSI_YELLOW, ANSI_BLUE};

#define WIDTH 6
#define HEIGHT 6
char pixels[HEIGHT][WIDTH] = {
	{2,3,2,3, 4, 4},
	{3,2,3,2, 5, 5},
	{2,3,2,3, 5, 5},
	{3,2,3,2, 4, 4},
	{6,1,1,6, 6, 1},
	{6,1,1,6, 6, 1}
};

int get_shape(int w, int h, char p[w][h], int x, int y, char colors[16])
{
	int tl=p[x][y];
	int tr=p[x+1][y];
	int bl=p[x][y+1];
	int br=p[x+1][y+1];
	colors[0] = '\0';
	if(tl == br && tr == bl && tr != br) {
		sprintf(colors, "%s%d;%dm", GRAPHIC_ESC, tl+30, tr+40);
		return U_QUADRANT_UL_LR;
	} else if (tl == tr && bl==br && tl!=bl) {
		sprintf(colors, "%s%d;%dm", GRAPHIC_ESC, tl+30, bl+40);
		return U_UPPER_HALF_BLOCK;
	} else if (tl == bl && tr==br && tl!=tr) {
		sprintf(colors, "%s%d;%dm", GRAPHIC_ESC, tl+30, tr+40);
		return U_LEFT_HALF_BLOCK;
	}
	return '?';
}

int main(void)
{
	printf("\u259A==\n");
	assert(WIDTH % 2 == 0);
	assert(HEIGHT % 2 == 0);
	for(int y=0; y<HEIGHT/2; y++) {
		for(int x=0; x<WIDTH/2; x++) {
			char fg[16];
			int codepoint = get_shape(WIDTH, HEIGHT, pixels, x*2, y*2, fg);
			char ch[5];
			u_to_utf8(ch, codepoint);
			printf("%s%s", fg, ch);
		}
		printf(ANSI_RESET"\n");
	}
}
