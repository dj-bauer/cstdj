#include "djtest.h"
#include "trie.h"
#include <stdlib.h>
#include <time.h>

static void gen_random_str(char* dst, size_t len) {
	dst[len] = '\0';
	for(size_t i=0; i<len; i++) {
		int v = 'A' + rand() % ('a'-'z');
		v += (rand() % 2) ? 32: 0;
		dst[i] = v;
	}
}

int trie_creation() {
	test_begin("create_trie");
	Trie* t = create_trie();
	assert_nonnull(t);

	char teststr[17];
	gen_random_str(teststr, 16);
	log_debug("Generated random string\n[%s]", teststr);
	assert_false(trie_contains_string(t, teststr));

	destroy_trie(t);
	test_end();
}
int first_chr() {
	test_begin("first_chr");
	Trie* t = create_trie();

	trie_insert(t, "A");
	trie_print(t);
	assert_true(trie_contains_string(t, "A"));
	assert_false(trie_contains_string(t, "B"));
	assert_false(trie_contains_string(t, "AA"));

	destroy_trie(t);
	test_end();
}
int single_random_str() {
	test_begin("single_random_str");
	Trie* t = create_trie();

	char str[17];
	gen_random_str(str, 16);
	trie_insert(t, str);
	trie_insert(t, "Hallo");
	trie_insert(t, "Hello");
	trie_insert(t, "Bruh");
	trie_print(t);
	assert_true(trie_contains_string(t, str));
	assert_true(trie_contains_string(t, "Hallo"));
	assert_false(trie_contains_string(t, "hallo"));
	assert_true(trie_contains_string(t, "Hello"));
	assert_false(trie_contains_string(t, "Hello!"));
	assert_false(trie_contains_string(t, ""));
	assert_false(trie_contains_string(t, "Hall"));

	destroy_trie(t);
	test_end();
}

int main() {
	srand(time(NULL));
	run_test(trie_creation);
	run_test(first_chr);
	run_test(single_random_str);
	return 0;
}
