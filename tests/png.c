#include "png.h"
#include "logger.h"
#include "djtest.h"

const char* descr_1 = "A compilation of a set of images created to test the\nvarious color-types of the PNG format. Included are\nblack&white, color, paletted, with alpha channel, with\ntransparency formats. All bit-depths allowed according\nto the spec are present.";

unsigned char* alloc_file(const char* filename, size_t* len) {
	FILE* f = fopen(filename, "r");
	if(!f) return NULL;
	fseek(f, 0, SEEK_END);
	*len = ftell(f);
	fseek(f, 0, SEEK_SET);
	unsigned char* mem = malloc(*len);
	fread(mem, 1, *len, f);
	fclose(f);
	return mem;
}

int t_iTXt(void) 
{
	test_begin("Good iTXt");

	Image* img = load_image("tests/pngs/good_itxt.png");
	assert_nonnull(img);

	Textpair* txt = img->texts;
	assert_nonnull(txt);
	assert_nonnull(txt->key);
	assert_nonnull(txt->data);
	assert_s_eq(txt->key, "Author");
	assert_s_eq(txt->data, "プロジェクト名雪");

	free_image(img);

	test_end();
}

int t_tEXt_multi(void)
{
	test_begin("Multiple tEXt chunks");

	Image* img = load_image("tests/pngs/ct1n0g04.png");
	assert_nonnull(img);
	
	Textpair* title = img->texts;
	assert_nonnull(title);
	assert_s_eq(title->key, "Title");
	assert_s_eq(title->data, "PngSuite");
	Textpair* author = title->next;
	assert_nonnull(author);
	assert_s_eq(author->key, "Author");
	assert_s_eq(author->data, "Willem A.J. van Schaik\n(willem@schaik.com)")
	Textpair* copyright = author->next;
	assert_nonnull(copyright);
	assert_s_eq(copyright->key, "Copyright");
	assert_s_eq(copyright->data, "Copyright Willem van Schaik, Singapore 1995-96");
	Textpair* descr = copyright->next;
	assert_nonnull(descr);
	assert_s_eq(descr->key, "Description");
	assert_s_eq(descr->data, descr_1);
	Textpair* software = descr->next;
	assert_nonnull(software);
	assert_s_eq(software->key, "Software");
	assert_s_eq(software->data, "Created on a NeXTstation color using \"pnmtopng\".");
	Textpair* disclaimer = software->next;
	assert_nonnull(disclaimer);
	assert_s_eq(disclaimer->key, "Disclaimer");
	assert_s_eq(disclaimer->data, "Freeware.");

	free_image(img);

	test_end();
}

int t_iTXt_jp(void)
{
	test_begin("Japanese iTXt chunks");

	Image* img = load_image("tests/pngs/ctjn0g04.png");
	assert_nonnull(img);
	
	Textpair* title = img->texts;
	assert_nonnull(title);
	assert_s_eq(title->key, "Title");
	assert_s_eq(title->data, "PngSuite");
	Textpair* author = title->next;
	assert_nonnull(author);
	assert_s_eq(author->key, "Author");
	assert_s_eq(author->data, "Willem van Schaik (willem@schaik.com)");
	Textpair* copyright = author->next;
	assert_nonnull(copyright);
	assert_s_eq(copyright->key, "Copyright");
	assert_s_eq(copyright->data, "著作権ウィレムヴァンシャイク、カナダ2011");
	Textpair* descr = author->next;
	assert_nonnull(descr);
	assert_s_eq(descr->key, "Description");
	assert_s_eq(descr->data, "PNG形式の様々な色の種類をテストするために作成されたイメージのセットのコンパイル。含まれているのは透明度のフォーマットで、アルファチャネルを持つ、白黒、カラー、パレットです。すべてのビット深度が存在している仕様に従ったことができました。");
	Textpair* software = descr->next;
	assert_nonnull(software);
	assert_s_eq(software->key, "Software");
	assert_s_eq(software->data, "\"pnmtopng\"を使用してNeXTstation色上に作成されます。");
	Textpair* disclaimer = software->next;
	assert_nonnull(disclaimer);
	assert_s_eq(disclaimer->key, "Disclaimer");
	assert_s_eq(disclaimer->data, "フリーウェア。");

	free_image(img);

	test_end();
}

int t_large(void) 
{
	test_begin("Large Image");
	Image* img = load_image("tests/pngs/meme.png");
	free_image(img);
	test_end();
}

int t_3x8(void) {
	test_begin("3 channel 8 bit rgb");
	Image* img = load_image("tests/pngs/basn2c08.png");
	free_image(img);
	test_end();
}

int t_3x16(void) {
	test_begin("3 channel 16 bit rgb");
	Image* img = load_image("tests/pngs/basn2c16.png");
	free_image(img);
	test_end();
}

int t_1x8(void) {
	test_begin("1 channel 8 bit rgb");
	Image* img = load_image("tests/pngs/basn0g08.png");
	free_image(img);
	test_end();
}

int t_1x4(void) {
	test_begin("1 channel 4 bit");
	Image* img = load_image("tests/pngs/basn0g04.png");
	assert_nonnull(img);

	size_t buf_len;
	unsigned char* buf = alloc_file("tests/pngs/basn0g04.png", &buf_len);
	assert_nonnull(buf);
	Image* img_buf = load_png_from_buf(buf_len, buf);
	free(buf);
	assert_nonnull(img_buf);
	assert_true(cmp_mem_eq((const char*) img->data, (const char*) img_buf->data, img->nr_channels * img->width * img->height));

	free_image(img_buf);
	free_image(img);
	test_end();
}


int main(void) {
	run_test(t_1x4);
	run_test(t_1x8);
	run_test(t_3x8);
	//run_test(t_3x16);
	run_test(t_iTXt);
	run_test(t_tEXt_multi);
	//run_test(t_iTXt_jp);
	//run_test(t_large);

	Image* img = load_image("tests/testpng.png");

	log_info("The image has the dimensions %dx%d", img->width, img->height);
	struct tm* local = localtime(&img->modification_time);
	const char* timestr = asctime(local);
	log_info("The image was edited on the %s", timestr);
	log_info("Color depth: %d Channel count: %d", img->bits_per_pixel, img->nr_channels);

	free_image(img);

	return 0;
}
