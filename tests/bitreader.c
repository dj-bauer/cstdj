#include "bitreader.h"
#include "logger.h"

/*
uint8_t stream[6] = {0b10100001, 0b10000111, 0b11111100, 
	                 0b11101101, 0b11010001, 0b11001010};
*/
uint8_t stream[6] = {161, 135, 252, 237, 209, 202};

int main(int argc, const char* argv[]) {
	Bitreader reader;
	init_bitreader(&reader, stream, 6);

	log_debug("First 6 bits: %d", read_bit(&reader, 6, false));
	log_debug("Next 5 bits: %d", read_bit(&reader, 5, false));
	log_debug("Next 7 bits: %d", read_bit(&reader, 7, false));
	log_debug("Next 2 bits: %d", read_bit(&reader, 2, false));

	return 0;
}
