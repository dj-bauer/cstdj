#include "svg.h"

int main(void) 
{
	viewport viewBox = {
		.min = (vec2) {.x = 150, .y=50},
		.dims = (vec2) {.x = 100, .y = 100}
	};
	FILE* f = svg_open("out.svg", viewBox, HEX_LIGHTGRAY);
	svg_rect(f, "#ff0000", "#00ff00", 10+150, 20+50, 50, 80, 3);
	svg_line(f, HEX_BLACK, 150, 50, 100+150, 50+100, 2);
	vec2 TRIANGLE[3] = {
		{.x = 160, .y = 60},
		{.x = 180, .y = 80},
		{.x = 170, .y = 90},
	};
	svg_polygon(f, NONE, HEX_BLUE, 0, 3, TRIANGLE);
	svg_close(f);
	return 0;
}
