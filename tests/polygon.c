#include "djtest.h"
#include "util.h"
#include "polygon.h"
#include "svg.h"
#include "../src/dlist_impl.h"
#include <math.h>
#include <color.h>

char cbuf[8];

bool dj_eq_p(vec2 a, vec2 b) {
	return a.x == b.x && a.y == b.y;
}
//#define SCALE (1.0f/100.0f)
#define SCALE 1

DLIST_DECL(p, vec2)
DLIST_IMPL(p, vec2)

void drw_step(FILE* svg, FILE* anim_f, dlist_p* points, size_t n, viewport vp, size_t it) {
	vec2 tmp_points[n];
	size_t i=0; 
	dlistnode_p* tmp_p = points->head;
	while(tmp_p != NULL) {
		tmp_points[i] = tmp_p->data;
		i++;
		tmp_p = tmp_p->next;
	}

	char id_str[64];
	sprintf(id_str, "step_%lu.svg", it+1);
	svg = svg_open(id_str, vp, HEX_BLACK);
	svg_path_id(svg, HEX_WHITE, NONE, 0.002*SCALE, i, tmp_points, id_str);
	svg_close(svg);
	fprintf(anim_f, "\t\t<img src=\"step_%lu.svg\"></img>\n", it+1);
}

void print_vec2(vec2* v) { printf("(%Lf,%Lf),", v->x, v->y); }
void print_l(const dlist_p* l) {
    printf("[");
    dlist_foreach_p(l, print_vec2);
    printf("]\n");
}

int t_scalarp(void) {
	test_begin("Scalar Product");

	vec2 a = {1, 0};
	vec2 b = {0, 1};
	assert_f_eq(vec2_sca(a, b), 0);

	test_end();
}

int t_cw_rect(void) {
	test_begin("Right rect");

	vec2 coords[5] = {
		{1, 1},
		{2, 1},
		{2, 2},
		{1, 2},
		{1, 1},
	};
	assert_i_lt(vertex_winding(5, coords), 0);

	flip_winding_order(5, coords);
	assert_i_gt(vertex_winding(5, coords), 0);

	test_end();
}

int t_triag(void) {
	test_begin("Triangulation");

	#define n 11
    vec2 coords[n] = {
        (vec2) {7082.259600, -50729.289500},
        (vec2) {7082.254600, -50729.285900},
        (vec2) {7082.305100, -50729.257600},
        (vec2) {7082.249000, -50729.217400},
        (vec2) {7082.233700, -50729.225900},
        (vec2) {7082.201600, -50729.202900},
        (vec2) {7082.398800, -50729.094600},
        (vec2) {7082.495000, -50729.192000},
        (vec2) {7082.238900, -50729.332800},
        (vec2) {7082.226700, -50729.323800},
        (vec2) {7082.214500, -50729.314800},
    };

	for(size_t i=0; i<n; i++) {
		coords[i] = vec2_sca_mult(coords[i], SCALE);
	}

    // Don't duplicate the first and last point
    dlist_p points;
	init_dlist_p(&points);
	for(size_t i=0; i<n; i++) {
		dlist_append_p(&points, coords[i]);
	}
	if(dj_eq_p(*dlist_first_p(&points), *dlist_last_p(&points))) {
		dlist_remove_last_p(&points);
	}

    viewport vp = {.min = {7082.2*SCALE, -50729.35*SCALE}, .dims = {0.3*SCALE, 0.3*SCALE}};
    FILE* svg = svg_open("step_0.svg", vp, HEX_BLACK);
    svg_polyline(svg, HEX_WHITE, 0.005*SCALE, n, coords);
    svg_close(svg);

    FILE* anim_f = fopen("out.html", "w");
    assert(anim_f != NULL);
    const char* anim_pre = "<html>\n\t<head>\n\t\t<style>\n\t\timg {\n\t\t\twidth: 45%\n\t\t}\n\t\t</style>\n\t</head>\n\t<body>\n";
    fwrite(anim_pre, strlen(anim_pre), sizeof(char), anim_f);

	double area = 0;
	size_t it  = 0;
	while(dlist_size_p(&points) > 2) {
		bool trimmed = false;
		dlistnode_p* p = points.head;
		while(p != NULL && !trimmed) {
			dlistnode_p* left_p = p->prev == NULL ? points.tail : p->prev;
			dlistnode_p* right_p = p->next == NULL ? points.head : p->next;

			assert_nonnull(left_p);
			assert_nonnull(right_p);

			vec2 a = vec2_sub(left_p->data, p->data);
			vec2 b = vec2_sub(right_p->data, p->data);
			long double angle = vec2_angle(a, b);
			if(0 <= angle && angle <= M_PI*2) {
				// TODO calc area
				// (1/2) |x1(y2 − y3) + x2(y3 − y1) + x3(y1 − y2)|
				vec2 p1 = right_p->data;
				vec2 p2 = p->data;
				vec2 p3 = left_p->data;
				double A = 0.5f * fabsl(p1.x*(p2.y-p3.y) + p2.x*(p3.y-p1.y) + p3.x*(p1.y-p2.y));
				assert_f_ge(A, 0);
				area += A;
				trimmed = true;
				dlist_remove_p(&points, p);
				
				drw_step(svg, anim_f, &points, n, vp, it);
			} else {
				p = p->next;
			}
		}
		it++;
		if(!trimmed) {
			fclose(anim_f);
			print_l(&points);
		}
		assert(trimmed);
	}
	dlist_clear_p(&points);
	assert_f_gt(area, 0);
	log_debug("The calculated area is %f", area);
	assert_f_eq(area, polygon_area(n, coords));

    const char* anim_post = "\t</body>\n</html>";
    fwrite(anim_post, strlen(anim_post), 1, anim_f);
    fclose(anim_f);

	test_end();
}
#undef n

int main(void) {
	run_test(t_cw_rect);
	run_test(t_triag);
	run_test(t_scalarp);
}
