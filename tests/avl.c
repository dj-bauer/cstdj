#include "avltree.h"
#include "bstree.h"
#include "djtest.h"
#include "attributes.h"
#include <stdio.h>

int t_left_rot() {
	test_begin("Left rotation"); 
	BSTree_i* t = create_bst_i();
	bst_insert_i(t, 1);
	bst_insert_i(t, 2);
	bst_insert_i(t, 3);
	t->root->balance = 2;
	t->root->right->balance = 1;
	assert_i_eq(t->root->data, 1);
	assert_i_eq(t->root->right->data, 2);
	assert_i_eq(t->root->right->right->data, 3);
	assert_null(t->root->left);
	assert_null(t->root->right->left);
	assert_null(t->root->parent);
	assert_p_eq(t->root, t->root->right->parent);
	assert_p_eq(t->root->right, t->root->right->right->parent);
	t->root = rotate_left_i(t->root);
	assert_zero(t->root->balance);
	assert_zero(t->root->left->balance);
	assert_zero(t->root->right->balance);
	assert_i_eq(t->root->data, 2);
	assert_i_eq(t->root->left->data, 1);
	assert_i_eq(t->root->right->data, 3);
	assert_null(t->root->left->left);
	assert_null(t->root->left->right);
	assert_null(t->root->right->left);
	assert_null(t->root->right->right);
	assert_null(t->root->parent);
	assert_p_eq(t->root->left->parent, t->root);
	assert_p_eq(t->root->right->parent, t->root);
	destroy_bst_i(t);
	test_end();
}
int t_right_rot() {
	test_begin("Right rotation"); 
	BSTree_i* t = create_bst_i();
	bst_insert_i(t, 3);
	bst_insert_i(t, 2);
	bst_insert_i(t, 1);
	t->root->balance = -2;
	t->root->left->balance = -1;
	assert_i_eq(t->root->data, 3);
	assert_i_eq(t->root->left->data, 2);
	assert_i_eq(t->root->left->left->data, 1);
	assert_null(t->root->right);
	assert_null(t->root->left->right);
	assert_null(t->root->parent);
	assert_p_eq(t->root, t->root->left->parent);
	assert_p_eq(t->root->left, t->root->left->left->parent);
	t->root = rotate_right_i(t->root);
	assert_i_eq(t->root->data, 2);
	assert_i_eq(t->root->left->data, 1);
	assert_i_eq(t->root->right->data, 3);
	assert_null(t->root->left->left);
	assert_null(t->root->left->right);
	assert_null(t->root->right->left);
	assert_null(t->root->right->right);
	assert_null(t->root->parent);
	assert_p_eq(t->root->left->parent, t->root);
	assert_p_eq(t->root->right->parent, t->root);
	assert_zero(t->root->balance);
	assert_zero(t->root->left->balance);
	assert_zero(t->root->right->balance);
	destroy_bst_i(t);
	test_end();
}

int t_adv_right_rot() {
	test_begin("Advanced right rotation");
	BSTree_cs* t = create_bst_cs();
	assert_true(bst_insert_cs(t, "z1"));
	assert_true(bst_insert_cs(t, "y1"));
	assert_true(bst_insert_cs(t, "x1"));
	assert_true(bst_insert_cs(t, "x0"));
	assert_true(bst_insert_cs(t, "x2"));
	assert_true(bst_insert_cs(t, "y2"));
	assert_true(bst_insert_cs(t, "z2"));
	t->root = rotate_right_cs(t->root); 
	destroy_bst_cs(t);
	test_end();
}

int t_lr() {
	test_begin("Left right rotation");
	BSTree_cs* t = create_bst_cs();
	assert_true(bst_insert_cs(t, "6Z"));
	assert_true(bst_insert_cs(t, "7T4"));
	assert_true(bst_insert_cs(t, "2Y"));
	assert_true(bst_insert_cs(t, "1T1"));
	assert_true(bst_insert_cs(t, "4X"));
	assert_true(bst_insert_cs(t, "3T2"));
	assert_true(bst_insert_cs(t, "5T3"));
	t->root->balance = -2;
	t->root->left->balance = 1;
	t->root = rotate_left_right_cs(t->root);
	assert_s_eq(t->root->data, "4X");
	assert_s_eq(t->root->left->data, "2Y");
	assert_s_eq(t->root->right->data, "6Z");
	assert_s_eq(t->root->left->left->data, "1T1");
	assert_s_eq(t->root->left->right->data, "3T2");
	assert_s_eq(t->root->right->left->data, "5T3");
	assert_s_eq(t->root->right->right->data, "7T4");
	assert_zero(t->root->balance);
	assert_zero(t->root->left->balance);
	assert_zero(t->root->right->balance);
	assert_zero(t->root->left->left->balance);
	assert_zero(t->root->left->right->balance);
	assert_zero(t->root->right->left->balance);
	assert_zero(t->root->right->right->balance);
	destroy_bst_cs(t);
	test_end();
}

int t_rl() {
	test_begin("Right left rotation");
	BSTree_cs* t = create_bst_cs();
	assert_true(bst_insert_cs(t, "2Z"));
	assert_true(bst_insert_cs(t, "1T1"));
	assert_true(bst_insert_cs(t, "6Y"));
	assert_true(bst_insert_cs(t, "4X"));
	assert_true(bst_insert_cs(t, "7T4"));
	assert_true(bst_insert_cs(t, "3T2"));
	assert_true(bst_insert_cs(t, "5T3"));
	t->root->balance = 2;
	t->root->right->balance = -1;
	t->root = rotate_right_left_cs(t->root);
	assert_s_eq(t->root->data, "4X");
	assert_s_eq(t->root->left->data, "2Z");
	assert_s_eq(t->root->right->data, "6Y");
	assert_s_eq(t->root->left->left->data, "1T1");
	assert_s_eq(t->root->left->right->data, "3T2");
	assert_s_eq(t->root->right->left->data, "5T3");
	assert_s_eq(t->root->right->right->data, "7T4");
	assert_zero(t->root->balance);
	assert_zero(t->root->left->balance);
	assert_zero(t->root->right->balance);
	assert_zero(t->root->left->left->balance);
	assert_zero(t->root->left->right->balance);
	assert_zero(t->root->right->left->balance);
	assert_zero(t->root->right->right->balance);
	destroy_bst_cs(t);
	test_end();
}

#ifdef LOG_THIS
#define insert_step(number) \
	assert_true(avl_insert_i(t, number)); \
	sprintf(fname, "anim_%d.dot", frame++); \
	bst_generate_graph_i(t, fname)
#else
#define insert_step(number) assert_true(avl_insert_i(t, number));
#endif


int t_fixed_insert_1() {
	test_begin("Fixed insertion 1");
	BSTree_i* t = create_bst_i();

	UNUSED int frame = 0;
	UNUSED char fname[32];

	insert_step(13);
	insert_step(10);
	insert_step(15);
	insert_step(5);
	insert_step(11);
	insert_step(16);
	insert_step(4);
	insert_step(8);
	insert_step(3);

	destroy_bst_i(t);
	test_end();
}

int t_fixed_insert_2() {
	test_begin("Fixed insert 2");
	BSTree_i* t = create_bst_i();
	UNUSED int frame = 0;
	UNUSED char fname[30];
	insert_step(30);
	insert_step(5);
	insert_step(35);
	insert_step(32);
	insert_step(40);
	insert_step(45);
	destroy_bst_i(t);
	test_end();
}

int t_fixed_insert_3() { 
	test_begin("Fixed insert 3");
	BSTree_i* t = create_bst_i();
	UNUSED int frame = 0;
	UNUSED char fname[30];
	insert_step(13);
	insert_step(10);
	insert_step(15);
	insert_step(5);
	insert_step(11);
	insert_step(16);
	insert_step(4);
	insert_step(6);
	insert_step(7);
	destroy_bst_i(t);
	test_end();
}

int t_fixed_insert_4() {
	test_begin("Fixed Insert 4");
	BSTree_i* t = create_bst_i();
	UNUSED int frame = 0;
	UNUSED char fname[30];
	insert_step(5);
	insert_step(2);
	insert_step(7);
	insert_step(1);
	insert_step(4);
	insert_step(6);
	insert_step(9);
	insert_step(3);
	insert_step(16);
	insert_step(15);
	/* TODO: Create a bunch of tests instead of blindly trusting no errors and the visuals */
	destroy_bst_i(t);
	test_end();
}

int t_rem_1() {
	test_begin("Remove 1");
	BSTree_i* t = create_bst_i();

	avl_insert_i(t, 44);
	avl_insert_i(t, 17);
	avl_insert_i(t, 62);
	avl_insert_i(t, 16);
	avl_insert_i(t, 15);
	avl_insert_i(t, 35);
	avl_insert_i(t, 50);
	avl_insert_i(t, 78);
	avl_insert_i(t, 32);
	avl_insert_i(t, 48);
	avl_insert_i(t, 54);
	avl_insert_i(t, 70);
	avl_insert_i(t, 88);
	avl_insert_i(t, 20);
	//avl_insert_i(t, 34);
	avl_insert_i(t, 47);
	avl_insert_i(t, 49);
	avl_insert_i(t, 53);
	avl_insert_i(t, 55);
	avl_insert_i(t, 87);
	avl_insert_i(t, 89);
	bst_generate_graph_i(t, "rem_1.dot");

	destroy_bst_i(t);
	test_end();
}

int main() {
	run_test(t_left_rot);
	run_test(t_right_rot);
	run_test(t_lr);
	run_test(t_rl);
	run_test(t_adv_right_rot);
	run_test(t_fixed_insert_1);
	run_test(t_fixed_insert_2);
	run_test(t_fixed_insert_3);
	run_test(t_fixed_insert_4);
	run_test(t_rem_1);
}
