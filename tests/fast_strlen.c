#include "djtest.h"
#include "fast_strlen.h"

int test_gt(void) {
	test_begin("Greater than");
	assert_true(strlen_gt("ABC", 2));
	assert_true(strlen_gt("ABC", 0));
	assert_false(strlen_gt("", 0));
	assert_true(strlen_gt("A", 0));
	assert_false(strlen_gt("ABC", 3));
	test_end();
}

int test_ge(void) {
	test_begin("Greater or Equal");
	assert_true(strlen_ge("ABC", 2));
	assert_true(strlen_ge("ABC", 3));
	assert_false(strlen_ge("ABC", 4));
	assert_false(strlen_ge("", 1));
	assert_true(strlen_ge("", 0));
	test_end();
}

int main(void) {
	run_test(test_gt);
	run_test(test_ge);
}
