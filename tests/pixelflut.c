#include "fbvideo.h"
#include "logger.h"

Video video;

void worker(void* data)
{
}

int main(void)
{
	if(fb_open(&video))
	{
		log_error("Couldn't open framebuffer");
		return 1;
	}

	fb_close(&video);
	return 0;
}
