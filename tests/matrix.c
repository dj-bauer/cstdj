#include <time.h>
#include <math.h>
#include "matrix_trans.h"
#include "util.h"
#include "matrix.h"
#include "djtest.h"

#define BOLD "\033[1m"
#define RESET "\033[0m"

#define assert_mat_eq(a, b) assert_true(dj_mat_eq(a,b))
#define assert_mat_ne(a, b) assert_false(dj_mat_eq(a,b))
#define assert_mat_approx_eq(a, b, tol) assert_true(dj_mat_eq_tol(a,b, tol))
#define assert_mat_approx_ne(a, b, factor) assert_false(dj_mat_eq_tol(a,b, factor))

DJ_DECL_STACKMAT(1, 2);
DJ_DECL_STACKMAT(1, 4);
DJ_DECL_STACKMAT(1, 3);

int t_transforms(void)
{
	test_begin("Transforms");
	
	dj_stackmat_new(id, 4, 4);
	dj_mat_set_id(id);
	dj_stackmat_new(v, 4, 1);
	v->data[0] = 1;
	v->data[1] = 2;
	v->data[2] = 3;

	dj_stackmat_new(scaled, 4, 4);
	dj_stackmat_new(trans, 4, 4);
	dj_stackmat_new(comp, 4, 4);
	dj_stackmat_new(comp2, 4, 4);

	dj_trans_scale(scaled, id, 2);

	dj_trans_translate(trans, id, v);

	dj_mat_mult(comp, trans, scaled);

	dj_trans_scale(comp2, trans, 2);

	assert_mat_eq(comp, comp2);
	test_end();
}

int t_stackmat(void) {
	test_begin("Stackmat");
	dj_stackmat_new(A, 4, 4);
	mat_data t = random_double(-100, 100);
	dj_mat_set_all(A, t);

	dj_mat* m = dj_mat_new(4, 4);
	dj_mat_set_all(m, t);
	assert_mat_eq(m, A);
	dj_mat_delete(&m);
	test_end();
}
int t_trans(void) {
	test_begin("Transpose");
	dj_stackmat_new(a, 3, 3);
	dj_stackmat_new(b, 3, 3);
	a->data[0] = 1;
	a->data[1] = 2;
	a->data[2] = 3;
	a->data[3] = 4;
	a->data[4] = 5;
	a->data[5] = 6;
	a->data[6] = 7;
	a->data[7] = 8;
	a->data[8] = 9;

	assert_f_eq(dj_mat_get(a, 2, 0), 7);

	dj_mat_trans(b, a);

	for(mat_dim i=0; i<3; i++) {
		for(mat_dim j=0; j<3; j++) {
			assert_i_eq(dj_mat_get(a, i, j), dj_mat_get(b, j, i));
		}
	}

	dj_stackmat_new(c, 1, 4);
	dj_stackmat_new(d, 4, 1);
	c->data[0] = 1;
	c->data[1] = 2;
	c->data[2] = 3;
	c->data[3] = 4;
	dj_mat_trans(d, c);
	for(mat_dim i=0; i<4; i++) {
		assert_i_eq(dj_mat_get(c, 0, i), dj_mat_get(d, i, 0));
	}

	test_end();
}

int t_mat_vec_mult(void) {
	test_begin("Vector Matrix Multiplication");
	dj_stackmat_new(id, 4, 4);
	dj_mat_set_id(id);
	
	dj_stackmat_new(vec, 4, 1);
	vec->data[0] = 1; 
	vec->data[1] = 2; 
	vec->data[2] = 3; 
	vec->data[3] = 4;

	dj_stackmat_new(res, 4, 1);
	dj_mat_mult(res, id, vec);

	assert_mat_eq(res, vec);

	dj_mat_mult_sca(id, id, 2.0);

	test_end();
}

int t_cross_prod(void) {
    test_begin("Cross Product");
	dj_stackmat_new(expected_z, 1, 3);
	dj_stackmat_new(x, 1, 3);
	dj_stackmat_new(y, 1, 3);
	dj_stackmat_new(z, 1, 3);

	expected_z->data[0] = 0; 
	expected_z->data[1] = 0; 
	expected_z->data[2] = 1;
	x->data[0] = 1; x->data[1] = 0; x->data[2] = 0;
	y->data[0] = 0; y->data[1] = 1; y->data[2] = 0;

	dj_vec_cross_prod(z, x, y);

	assert_mat_eq(z, expected_z);

	test_end();
}

int t_qr(void) {
	test_begin("QR Zerlegung");

	dj_stackmat_new(A, 3, 3);
	dj_stackmat_new(Q1, 3, 3);
	dj_stackmat_new(Q2, 3, 3);

	dj_mat_set(A, 0, 0, -2);
	dj_mat_set(A, 0, 1, 2);
	dj_mat_set(A, 0, 2, 3);
	dj_mat_set(A, 1, 0, 2);
	dj_mat_set(A, 1, 1, 3);
	dj_mat_set(A, 1, 2, 1);
	dj_mat_set(A, 2, 0, 1);
	dj_mat_set(A, 2, 1, 4);
	dj_mat_set(A, 2, 2, -2);

	dj_mat_set_rand(Q1, -100, 100);
	dj_mat_set_rand(Q2, -100, 100);

	dj_mat_gram_schmidt(Q1, A);

	dj_mat_mod_gram_schmidt(Q2, A);
	assert_mat_approx_eq(Q1, Q2, 0.0001);

	test_end();
}

int t_mod_gram_schmidt(void) {
	test_begin("Modified Gram Schmidt");

	dj_stackmat_new(A, 4, 4);
	dj_stackmat_new(Q, 4, 4);
	dj_stackmat_new(expected, 4, 4);
	dj_mat_set(expected, 0, 0, 0.0602);
	dj_mat_set(expected, 0, 1, 0.8345);
	dj_mat_set(expected, 0, 2, -0.8111);
	dj_mat_set(expected, 0, 3, -0.8111);
	dj_mat_set(expected, 1, 0, 0.3010);
	dj_mat_set(expected, 1, 1, 0.4575);
	dj_mat_set(expected, 1, 2, -0.4857);
	dj_mat_set(expected, 1, 3, -0.4867);
	dj_mat_set(expected, 2, 0, 0.4517);
	dj_mat_set(expected, 2, 1, 0.0808);
	dj_mat_set(expected, 3, 0, 0.7825);
	dj_mat_set(expected, 3, 1, -0.2961);
	dj_mat_set(expected, 3, 2, 0.3244);
	dj_mat_set(expected, 3, 3, 0.3244);
	for(int i=0; i<16; i++) {
		A->data[i] = i+1;
	}
	dj_mat_mod_gram_schmidt(Q, A);
	log_info("Expected:");
	dj_mat_print(expected);
	log_info("Calculated:");
	dj_mat_print(Q);
	assert_mat_approx_eq(Q, expected, 0.0001);

	test_end();
}

int t_fw_subst(void) {
	test_begin("Vorwärts-Substitution");
	int n = 16;

	dj_mat* L = dj_mat_new(n, n);
	dj_mat* b = dj_mat_new(n, 1);
	dj_mat* x = dj_mat_new(n, 1);
	dj_mat* expected = dj_mat_new(n, 1);

	for(size_t it=0; it<256; it++) {
		for(unsigned int i=0; i<n; i++) {
			for(unsigned int j=0; j<=i; j++) {
				dj_mat_set(L, i, j, random_double(0, 100));
			}
			expected->data[i] = random_double(0, 100);
		}
		dj_mat_mult(b, L, expected);
		dj_mat_fw_subst(x, L, b);
		assert_mat_approx_eq(x, expected, 0.001);
	}
	dj_mat_delete(&L);
	dj_mat_delete(&b);
	dj_mat_delete(&x);
	dj_mat_delete(&expected);
	test_end();
}

int t_bw_subst(void) {
	test_begin("Rückwärts -Substitution");
	int n = 16;

	dj_mat* U = dj_mat_new(n, n);
	dj_mat* b = dj_mat_new(n, 1);
	dj_mat* x = dj_mat_new(n, 1);
	dj_mat* expected = dj_mat_new(n, 1);

	for(size_t it=0; it<256; it++) {
		for(unsigned int i=0; i<n; i++) {
			for(unsigned int j=i; j<n; j++) {
				dj_mat_set(U, i, j, random_double(0, 100));
			}
			expected->data[i] = random_double(0, 100);
		}
		dj_mat_mult(b, U, expected);
		dj_mat_bw_subst(x, U, b);
		assert_mat_approx_eq(x, expected, 0.001);
	}
	dj_mat_delete(&U);
	dj_mat_delete(&b);
	dj_mat_delete(&x);
	dj_mat_delete(&expected);
	test_end();
}

int t_inv(void)
{
	test_begin("Matrix Invertierung");

	int n = 32 ;
	dj_mat* expected = dj_mat_new(n, n);
	dj_mat_set_diag(expected, 1);
	dj_mat* A = dj_mat_new(n, n);
	dj_mat* A_inv = dj_mat_new(n, n);
	dj_mat* id = dj_mat_new(n, n);

	for(int i=0; i<128; i++) {
		dj_mat_set_rand(A, 0, 100);

		dj_mat_inv(A_inv, A);
		dj_mat_mult(id, A, A_inv);
		assert_mat_approx_eq(id, expected, 0.0001);
	}

	dj_mat_delete(&expected);
	dj_mat_delete(&A);
	dj_mat_delete(&A_inv);
	dj_mat_delete(&id);

	test_end();
}

int t_det(void)
{
	test_begin("Determinant Calculation");

	dj_stackmat_new(mat1x1, 1, 1);
	mat1x1->data[0] = 42.6;
	assert_f_eq(dj_mat_det(mat1x1), 42.6);

	dj_stackmat_new(mat2x2, 2, 2);
	mat2x2->data[0] = 1;
	mat2x2->data[1] = 2;
	mat2x2->data[2] = 3;
	mat2x2->data[3] = 4;
	assert_f_eq(dj_mat_det(mat2x2), -2);

	dj_stackmat_new(mat3x3, 3, 3);
	for(mat_dim i=0; i<9; i++)
		mat3x3->data[i] = i+1;
	mat3x3->data[0] = 10;
	assert_f_eq(dj_mat_det(mat3x3), -27);

	dj_mat* mat6x6 = dj_mat_new(6, 6);
	mat_data test_data[36] = {
		62, 9, 6,88, 4, 5,
		27,28,90,68,34,30,
		66,52,38,52, 8,78,
		69,20,55,45,18,50,
		58,52,38,42,25,86,
		85,87,96,91,75, 0
	};
	for(mat_dim i=0; i<36; i++) {
		mat6x6->data[i] = test_data[i];
	}
	mat_data det = dj_mat_det(mat6x6);
	assert_f_eq(det, -33213681360);
	dj_mat_delete(&mat6x6);

	test_end();
}

int t_cholesky(void)
{
	test_begin("Cholesky Zerlegung");
	dj_stackmat_new(A, 3, 3);
	dj_stackmat_new(L, 3, 3);
	dj_stackmat_new(R, 3, 3);
	dj_stackmat_new(LR, 3, 3);

	dj_mat_set(A, 0, 0, 4);
	dj_mat_set(A, 0, 1, 12);
	dj_mat_set(A, 0, 2, -16);
	dj_mat_set(A, 1, 0, 12);
	dj_mat_set(A, 1, 1, 37);
	dj_mat_set(A, 1, 2, -43);
	dj_mat_set(A, 2, 0, -16);
	dj_mat_set(A, 2, 1, -43);
	dj_mat_set(A, 2, 2, 98);

	dj_mat_cholesky(L, A);

	dj_mat_trans(R, L);

	dj_mat_mult(LR, L, R);

	assert_mat_eq(A, LR);

	test_end();
}

int t_layout(void)
{
	test_begin("Layout");
	dj_stackmat_new(m, 3, 3);
	for(mat_dim i=0; i<9; i++) {
		m->data[i] = i;
	}
	for(mat_dim i=0; i<3; i++) {
		for(mat_dim j=0; j<3; j++) {
			assert_f_eq(dj_mat_get(m, i, j), i*3 + j);
		}
	}
	test_end();
}

int t_eigenvalues(void)
{
	test_begin("Eigenvalues");
	dj_stackmat_new(A, 2, 2);
	for(int i=0; i<4; i++)
		A->data[i] = i+1;
	mat_data values[4] = {NAN, NAN, NAN, NAN};
	dj_mat_eigenvalues(values, A);

	log_info("λ₁ = %f, λ₂ = %f", values[0], values[1]);
	assert_f_approx(values[0], 5.372281, 0.000001);
	assert_f_approx(values[1], -0.372281, 0.000001);

	dj_stackmat_new(B, 3, 3);
	for(int i=0; i<9; i++)
		B->data[i] = i+1;
	dj_mat_eigenvalues(values, B);

	log_info("λ₁ = %g, λ₂ = %g, λ₃ = %g", values[0], values[1], values[2]);
	assert_f_approx(values[0], 16.1168, 0.00006);
	assert_f_approx(values[1], 0, 0.00006);
	assert_f_approx(values[2], -1.1168, 0.00006);

	dj_stackmat_new(C, 4, 4);
	for(int i=0; i<16; i++)
		C->data[i] = i+1;
	dj_mat_print(C);
	dj_mat_eigenvalues(values, C);
	for(int i=0; i<4; i++) {
		log_info("λ_%d = %g", i, values[i]);
	}

	test_end();
}

int t_custom(void)
{
	test_begin("Custom");
	dj_stackmat_new(A, 3, 3);
	for(int i=0; i<9; i++) A->data[i] = 0;
	A->data[0] = 3; A->data[1] = -10; A->data[2] = -10;
	A->data[4] = 3;
	A->data[7] = -5; A->data[8] = -2;
	mat_data values[3] = {NAN, NAN, NAN};
	dj_mat_eigenvalues(values, A);
	log_info("λ₁ = %f, λ₂ = %f λ₃ = %f", values[0], values[1], values[2]);

	dj_stackmat_new(S, 3, 3);
	int s[] = {1, 0, 2, 0, 1, 0, 0, -1, 1};
	for(int i=0; i<9; i++) S->data[i] = s[i];
	dj_stackmat_new(S_, 3, 3);
	dj_mat_inv(S_, S);
	log_info("S");
	dj_mat_print(S);
	log_info("S'");
	dj_mat_print(S_);

	dj_stackmat_new(D1, 3, 3);
	dj_mat_mult(D1, S_, A);

	dj_stackmat_new(D, 3, 3);
	dj_mat_mult(D, D1, S);

	dj_mat_print(D);

	test_end();
}

int main(void) 
{
	srand(time(NULL));

	run_test(t_custom);
    run_test(t_transforms);
	run_test(t_mat_vec_mult);
	run_test(t_stackmat);
	run_test(t_layout);
	run_test(t_cross_prod);
	run_test(t_trans);
	run_test(t_det);
	run_test(t_inv);
	run_test(t_bw_subst);
	run_test(t_fw_subst);
	run_test(t_qr);
	run_test(t_cholesky);
	//run_test(t_eigenvalues);
	run_test(t_mod_gram_schmidt);

	return 0;
}
