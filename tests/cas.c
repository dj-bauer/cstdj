#include "cas.h"
#include "djtest.h"

void dbg(double sol[3], double a, double b, double c, double d)
{
	cas_solve_cubic(sol, a, b, c, d);
	log_info("The solutions for [f(x) = %gx³ %+gx² %+gx %+g] are %f, %f, %f", 
			a, b, c, d,
			sol[0], sol[1], sol[2]);
}

int t_cubic_solve(void) {
	test_begin("Cubic solving for Δ=0");

	double sol[3];
	dbg(sol, 1, 0, 0.23, 0);
	dbg(sol, 1, 1, -1.74, -2);
	dbg(sol, 1, 1, -1.74, -1.1);

	test_end();
}

int main(void) {
	run_test(t_cubic_solve);
}
