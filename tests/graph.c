#include "graph.h"
#include "djtest.h"
#include <stdlib.h>
#include <time.h>

void char_print_func(void* data, char dst[32]) {
	unsigned long data_address = (unsigned long) data;
	dst[0] = (char) data_address;
	dst[1] = '\0';
}
void print_long(void* data, char dst[32]) {
	unsigned long d = (unsigned long) data;
	sprintf(dst, "%lu", d);
}

int t_creation(void) {
	test_begin("Creation");
	AdjGraph g;
	graph_init(&g, false); 
	char ch[128];
	print_graph(&g, ch, 128, char_print_func);
	graph_cleanup(&g);
	test_end();
}

int t_print(void) {
	test_begin("Printing");
	AdjGraph g;
	graph_init(&g, true);
	long a = graph_add_node(&g, (void*) 'A')->index;
	long b = graph_add_node(&g, (void*) 'B')->index;
	long c = graph_add_node(&g, (void*) 'C')->index;
	long d = graph_add_node(&g, (void*) 'D')->index;
	long e1_i = graph_add_edge_i(&g, a, b, 1, NULL)->index;
	long e2_i = graph_add_edge_i(&g, a, c, 2, NULL)->index;
	AdjEdge* e1 = graph_edge(&g, e1_i);
	AdjEdge* e2 = graph_edge(&g, e2_i);
	assert_i_eq(e1->start, a);
	assert_i_eq(e1->end, b);
	assert_i_eq(e2->start, a);
	assert_i_eq(e2->end, c);
	graph_add_edge_i(&g, d, c, 1.5, NULL);
	graph_add_edge_i(&g, b, d, 2.74, NULL);
	graph_highlight_node_i(&g, c, true);
	const size_t buf_len = 512;
	char ch[buf_len];
	print_graph(&g, ch, buf_len, char_print_func);
    printf("%s\n", ch);
	graph_cleanup(&g);
	test_end();
}
int t_kruski(void) {
	test_begin("Kruskals Spanning Tree");
	AdjGraph graph;
	graph_init(&graph, false);
	long a = graph_add_node(&graph, (void*) 'A')->index;
	long b = graph_add_node(&graph, (void*) 'B')->index;
	long c = graph_add_node(&graph, (void*) 'C')->index;
	long d = graph_add_node(&graph, (void*) 'D')->index;
	long e = graph_add_node(&graph, (void*) 'E')->index;
	long f = graph_add_node(&graph, (void*) 'F')->index;
	long g = graph_add_node(&graph, (void*) 'G')->index;

	AdjEdge* edge = graph_add_edge_i(&graph, a, b, 7, NULL);
	assert_i_eq(edge->start, a);
	assert_i_eq(edge->end, b);
	edge = graph_add_edge_i(&graph, a, d, 5, NULL);
	assert_i_eq(edge->start, a);
	assert_i_eq(edge->end, d);
	graph_add_edge_i(&graph, b, d, 9, NULL);
	graph_add_edge_i(&graph, b, c, 8, NULL);
	graph_add_edge_i(&graph, b, e, 7, NULL);
	graph_add_edge_i(&graph, c, e, 5, NULL);
	graph_add_edge_i(&graph, d, e, 15, NULL);
	graph_add_edge_i(&graph, d, f, 6, NULL);
	graph_add_edge_i(&graph, f, e, 8, NULL);
	graph_add_edge_i(&graph, f, g, 11, NULL);
	edge = graph_add_edge_i(&graph, e, g, 9, NULL);
	assert_i_eq(edge->start, e);
	assert_i_eq(edge->end, g);

	graph_kruskal_spanntree(&graph);

	for(size_t i=0; i<graph_node_count(&graph); i++) {
		AdjNode* n = vector_at_nodes(&graph.nodes, i);
		assert_true(n->highlight);
	}

	graph_cleanup(&graph);
	test_end();
}

int t_prim(void) {
	test_begin("Prims Spanning Tree");
	AdjGraph graph;
	graph_init(&graph, false);
	long a = graph_add_node(&graph, (void*) 'A')->index;
	long b = graph_add_node(&graph, (void*) 'B')->index;
	long c = graph_add_node(&graph, (void*) 'C')->index;
	long d = graph_add_node(&graph, (void*) 'D')->index;
	long e = graph_add_node(&graph, (void*) 'E')->index;
	long f = graph_add_node(&graph, (void*) 'F')->index;
	long g = graph_add_node(&graph, (void*) 'G')->index;

	AdjEdge* edge = graph_add_edge_i(&graph, a, b, 7, NULL);
	assert_i_eq(edge->start, a);
	assert_i_eq(edge->end, b);
	edge = graph_add_edge_i(&graph, a, d, 5, NULL);
	assert_i_eq(edge->start, a);
	assert_i_eq(edge->end, d);
	graph_add_edge_i(&graph, b, d, 9, NULL);
	graph_add_edge_i(&graph, b, c, 8, NULL);
	graph_add_edge_i(&graph, b, e, 7, NULL);
	graph_add_edge_i(&graph, c, e, 5, NULL);
	graph_add_edge_i(&graph, d, e, 15, NULL);
	graph_add_edge_i(&graph, d, f, 6, NULL);
	graph_add_edge_i(&graph, f, e, 8, NULL);
	graph_add_edge_i(&graph, f, g, 11, NULL);
	edge = graph_add_edge_i(&graph, e, g, 9, NULL);
	assert_i_eq(edge->start, e);
	assert_i_eq(edge->end, g);

	graph_prim_spanntree(&graph);

	for(size_t i=0; i<graph_node_count(&graph); i++) {
		AdjNode* n = vector_at_nodes(&graph.nodes, i);
		assert_true(n->highlight);
	}

	graph_cleanup(&graph);
	test_end();
}


int t_prim_rand(void) {
	test_begin("Prim random graphs");
	AdjGraph g;
	graph_init(&g, false);

	const size_t n = 32;
	for(size_t i=0; i<n; i++) {
		graph_add_node(&g, (void*) i);
	}
	for(size_t i=0; i<n*2; i++) {
		AdjNode* a = graph_node(&g, rand() % n);
        AdjNode* b = graph_node(&g, rand() % n);
		double w = ((float)rand()) / ((float)RAND_MAX) * 100.0f;
        graph_add_edge(&g, a, b, w, NULL);
	}

	graph_prim_spanntree(&g);

	for(size_t i=0; i<graph_node_count(&g); i++) {
		AdjNode* n = vector_at_nodes(&g.nodes, i);
		assert_true(n->highlight);
	}

	graph_cleanup(&g);
	test_end();
}
int t_kruskal_rand(void) {
	test_begin("Kruskal random graphs");
	AdjGraph g;
	graph_init(&g, false);

	const size_t n = 32;
	for(size_t i=0; i<n; i++) {
		graph_add_node(&g, (void*) i);
	}
	for(size_t i=0; i<n*2; i++) {
		AdjNode* a = graph_node(&g, rand() % n);
        AdjNode* b = graph_node(&g, rand() % n);
		double w = ((float)rand()) / ((float)RAND_MAX) * 100.0f;
        graph_add_edge(&g, a, b, w, NULL);
	}

	graph_kruskal_spanntree(&g);

	for(size_t i=0; i<graph_node_count(&g); i++) {
		AdjNode* n = vector_at_nodes(&g.nodes, i);
		assert_true(n->highlight);
	}

	graph_cleanup(&g);
	test_end();
}


int main(void) {
	srand(time(NULL));
	run_test(t_creation);
	run_test(t_print);
	run_test(t_prim);
	run_test(t_kruski);
	//run_test(t_prim_rand);
	//run_test(t_kruskal_rand);
	return 0;
}
