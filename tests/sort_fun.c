#include "sort_fun.h"
#include "djtest.h"
#include <stdlib.h>
#include <time.h>

int t_qsort(void) {
	test_begin("Qsort");

	size_t n = 4096;
	int a[n];
	for(size_t i=0; i<n; i++) {
		a[i] = rand();
	}
	qsort_i(n, a);
	assert_i_sorted(n, a);

	test_end();
}

int t_qsort_double(void) {
	test_begin("Qsort double pivot element");

	size_t n = 4096;
	int a[n];
	for(size_t i=0; i<n; i++) {
		a[i] = rand();
	}
	qsort_i_double_pivot(n, a);
	assert_i_sorted(n, a);

	test_end();
}

int main(void) {
	srand(time(NULL));

	run_test(t_qsort);
	run_test(t_qsort_double);
}
