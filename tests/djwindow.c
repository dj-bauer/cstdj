#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <assert.h>

#include "logger.h"
#include "keycodes.h"
#include "djwindow.h"
#include "djevent.h"

// Temporary until i figure out loading my own bitmaps
#include <X11/Xlib.h>
#include <X11/Xutil.h>

int page_index = 0;
DJWindow* win;
bool stay_open = true;

void mouse_callback(DJEvent* ev, void* a) {
	log_info("The Mouse button %s was clicked", dj_mousebutton_str(ev->buttonpress.button));
	dj_win_unsub_event(win, MousePressEvent, mouse_callback, NULL);
}
void mouse_r_c(DJEvent* ev, void* a) {
	log_info("The mouse button was released");
}
void key_c(DJEvent* ev, void* a) {
	if(ev->keypress.key == EscapeKey) {
		log_info("Escape key was pressed. Bye");
		stay_open = false;
	} else if(ev->keypress.key == F11) {
		dj_win_toggle_fullscreen(ev->keypress.win);
		log_info("F11 was pressed. Toggling fullscreen");
	}
	log_info("The key `%s` (%d) '%s' was clicked", ev->keypress.utf8_name, ev->keypress.key, ev->keypress.utf8_ch);
}
void key_r_c(DJEvent* ev, void* a) {
	log_info("The key was released");
}
void on_resize(DJEvent* ev, void* a) {
	log_info("The window was resized to %d %d", ev->resize.width, ev->resize.height);
}
void focus(DJEvent* ev, void* a) {
	log_trace("Focus");
}
void unfocus(DJEvent* ev, void* a) {
	log_trace("Unfocus");
}
void on_close(DJEvent* ev, void* a) {
	log_info("Bye from close event");
	stay_open = false;
}

void scroll(DJEvent* ev, void* a) {
	page_index+=ev->mousescroll.direction;
	printf("Page index: %d\n", page_index);
}

int main() {
	win = dj_win_init(DRW_X11);
	if(!win) return 1;

	dj_win_set_title(win, "Window test suite");

	dj_win_sub_event(win, MousePressEvent, mouse_callback, NULL);
	dj_win_sub_event(win, MouseReleaseEvent, mouse_r_c, NULL);
	dj_win_sub_event(win, KeyPressEvent, key_c, NULL);
	dj_win_sub_event(win, KeyReleaseEvent, key_r_c, NULL);
	dj_win_sub_event(win, MouseScrollEvent, scroll, NULL);
	dj_win_sub_event(win, WinResizeEvent, on_resize, NULL);
	dj_win_sub_event(win, WinFocusEvent, focus, NULL);
	dj_win_sub_event(win, WinUnfocusEvent, unfocus, NULL);
	dj_win_sub_event(win, WinCloseEvent, on_close, NULL);

	#include "icn.xbm"
	dj_win_set_icon(win, icn_width, icn_height, icn_bits, "DJWindow :)");

	while(stay_open) {
		dj_win_update(win);
	}
	dj_win_close(&win);
	return 0;
}
