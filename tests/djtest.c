#include "djtest.h"

int t_sorting(void) {
	test_begin("Sorting");
	assert_i_sorted(0, NULL);
	int a = 0;
	assert_i_sorted(1, &a);

	int t[7] = {1,2,3,4,5,7,7};
	assert_i_sorted(7, t);

	int b[2] = {1,1};
	assert_i_sorted(2, b);

	int c[2] = {1,0};
	assert_i_unsorted(2, c);

	test_end();
}

int t_a(void) {
	int one = 1;
	int anotherone = 1;
	int two = 2;
	test_begin("Test");
	assert_i_eq(one, anotherone);
	assert_i_eq(one, one);
	assert_i_ne(one, two);
	assert_i_gt(two, one);
	assert_i_lt(one, two);
	assert_i_ge(one, one);
	assert_i_le(two, two);
	assert_true(1);
	assert_false(0);
	assert_f_eq(1.3, 1.3);
	const char* foo_var = "foo";
	assert_s_eq(foo_var, "foo");
	test_end();
}

int main(void) {
	run_test(t_a);
	run_test(t_sorting);
}
