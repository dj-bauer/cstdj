#include "vector.h"
#include "djtest.h"

int t_creation() {
	test_begin("Creation");
	Vector_i* v = create_vector_i();
	assert_nonnull(v); 
	assert_i_eq(v->size, 0);
	assert_i_eq(v->capacity, 0);

	destroy_vector_i(v);
	test_end();
}

int main() {
	run_test(t_creation);
}
