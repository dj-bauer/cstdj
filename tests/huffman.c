#include "djtest.h"
#include "huffman.h"
#include "zlib.h"
#include "util.h"

int t_treebuild(void)
{
	test_begin("Tree building");
	const char* symbols[] = {"A", "B", "C", "D", "E", "F", "G", "H"};
	const uint8_t bit_lengths[] = {3, 3, 3, 3, 3, 2, 4, 4};
	//const uint16_t expected_codes[] = {2, 3, 4, 5, 6, 0, 14, 15};

	uint16_t calc_codes[N_ELEMENTS(symbols)];
	generate_huffman_codes(N_ELEMENTS(symbols), calc_codes, bit_lengths);

	int tree_size = 2*N_ELEMENTS(symbols)-1;
	HuffmanNode tree[tree_size];

	size_t next_node = 0;
	for(int i=0; i<N_ELEMENTS(symbols); i++) {
		log_info("Inserting %s with code %d and length %d", symbols[i], calc_codes[i], bit_lengths[i]);
		huffman_add_node(tree_size, tree, &next_node, i, calc_codes[i], bit_lengths[i]);
	}
	huffman_print_tree(stdout, tree_size, tree, tree_size);

	test_end();
}

int main(void) {
	run_test(t_treebuild);
}
