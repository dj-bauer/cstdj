#include "djson.h"
#include "djtest.h"

int t_simple_int(void) {
    test_begin("Simple integer");
    const char* json = "45.3e+2";
    JNode* root = json_parse(json, strlen(json));
    assert_nonnull(root);

    assert_i_eq(json_size(root), 0);
    assert_true(json_is_type(root, JNumber));
    assert_f_eq(json_get_num(root), 45.3e2);

    json_free(root);
    test_end();
}

int t_simple_array(void) {
    test_begin("Simple array");
    const char* json = "[null, 1, \"bla\", null, 0]";
	JNode* root = json_parse(json, strlen(json));
    assert_nonnull(root);

    assert_true(json_is_type(root, JArray));
    size_t array_len = json_size(root);
    assert_i_eq(array_len, 5);
    JType types[] = {JNull, JNumber, JString, JNull, JNumber};
    for(size_t i=0; i<array_len; i++) {
        JNode* el = json_arr_get(root, i);
        assert_nonnull(el);
        assert_true(json_is_type(el, types[i]));
    }
    const char* str = json_get_str(json_arr_get(root, 2));
    assert_nonnull(str);
    assert_s_eq(str, "bla");
    assert_i_eq(json_get_int(json_arr_get(root, 1)), 1);
    assert_i_eq(json_get_int(json_arr_get(root, 4)), 0);

    json_free(root);
    test_end();
}

int t_simple_dict(void) {
    test_begin("Simple object");
    const char* json = "{\"foo\": 1, \"bar\": \"hi!\", \"zoo\": null}";
    JNode* root = json_parse(json, strlen(json));
	assert_nonnull(root);

    assert_true(json_is_type(root, JObject));
    assert_i_eq(json_size(root), 3);

    JNode* foo = json_obj_get(root, "foo");
    assert_nonnull(foo);
    assert_true(json_is_type(foo, JNumber));
    assert_i_eq(json_get_int(foo), 1);

    JNode* bar = json_obj_get(root, "bar");
    assert_nonnull(bar);
    assert_true(json_is_type(foo, JNumber));
    assert_nonnull(json_get_str(bar));
    assert_s_eq(json_get_str(bar), "hi!");

    JNode* zoo = json_obj_get(root, "zoo");
    assert_nonnull(zoo);
    assert_true(json_is_type(zoo, JNull));

    JNode* nonexistend = json_obj_get(root, "nonexistend");
    assert_null(nonexistend);
    assert_true(json_is_type(zoo, JNull));

    json_print(root);
    json_free(root);
    test_end();
}

int main(void) {
    run_test(t_simple_int);
    run_test(t_simple_array);
    run_test(t_simple_dict);
}
