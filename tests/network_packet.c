//
// Created by dbauer on 3/2/24.
//
#include "djtest.h"
#include "network_packet.h"
#include "util.h"

int buf_creation(void) {
    test_begin("Network buffer creation");

    NetworkBuf* buf = netbuf_create(16);
    assert_nonnull(buf);
    assert_nonnull(buf->data);
    assert_zero(buf->ptr);
    assert_i_eq(buf->size, 16);

    netbuf_free(buf);
    test_end();
}

int t1(void) {
    test_begin("Simple type test");

    NetworkBuf* buf = netbuf_create(4*size_int);

    for(int i=0; i<4; i++) {
        log_debug("I: %d", i);
        assert_zero(write_int(buf, i));
    }

    buf->ptr = 0;

    for(int i=0; i<4; i++) {
        int x;
        assert_zero(read_int(&x, buf));
        assert_i_eq(x, i);
    }

    netbuf_free(buf);

    test_end();
}

int t_str(void) {
    test_begin("Strings");

    const char* a = "Hallo Welt";
    const char* b = "Foo Bar lol!";
    size_t len = 3*size_int + size_str(a) + size_str(b);
    NetworkBuf* buf = netbuf_create(len);

    assert_zero(write_int(buf, 1));
    assert_zero(write_str(buf, a));
    assert_zero(write_int(buf, 2));
    assert_zero(write_str(buf, b));
    assert_zero(write_int(buf, 3));

    buf->ptr = 0;

    int x;
    char* y;
    assert_zero(read_int(&x, buf));
    assert_i_eq(x, 1);
    assert_zero(read_str(&y, buf));
    assert_s_eq(y, a);
    free(y);
    assert_zero(read_int(&x, buf));
    assert_i_eq(x, 2);
    assert_zero(read_str(&y, buf));
    assert_s_eq(y, b);
    free(y);
    assert_zero(read_int(&x, buf));
    assert_i_eq(x, 3);

    netbuf_free(buf);

    test_end();
}

int t_float(void) {
    test_begin("Float");

    double foo[6];

    NetworkBuf* buf = netbuf_create(size_double * 6);
    for(int i=0; i<6; i++) {
        foo[i] = random_double(-100, 100);
        assert_zero(write_double(buf, foo[i]));
    }

    buf->ptr = 0;

    for(int i=0; i<6; i++) {
        double x;
        assert_zero(read_double(&x, buf));
        assert_f_eq(foo[i], x);
    }

    test_end();
}

int main(void) {
    run_test(buf_creation);
    run_test(t1);
    run_test(t_str);
    run_test(t_float);
}