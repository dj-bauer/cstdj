#include "osm.h"
#include "logger.h"
#include "osmtypes.h"
#include "graph.h"
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#define DEFAULT_MAP "tests/unicampus.osm"

struct s_osm map;

void print_way(struct s_osmway* way, void* a) {
	printf("Way %lu of type %s\n", way->id, osmtype_str(way->data.type));
	for(size_t i=0; i<way->nodecount; i++) {
		printf("\tNode %lu\n", way->nodes[i]->id);
	}
}

static const char* osmrel_str(enum e_relation type) {
	if(type >= UnknownRel) return "unknown";
	if(0) {}
#define X(id, str) else if(type == id) return str;
	RELATION_FOREACH(X)
#undef X
	return "!!!ERROR Relation!!!";
}

static const char* member_type(enum e_relationmember type) {
#define X(id, str, a, b, c) else if(type == id) return str;
	if(0){} RELATIONMEMBER_FOREACH(X)
#undef X
		return "Unknown";
}

static const char* member_role(enum e_relationrole type) {
#define X(id, str) else if(type == id) return str;
	if(0){} RELATIONROLE_FOREACH(X)
#undef X
		return "Unknown";
}

void print_rel(struct s_osmrelation* rel, void* a) {
	printf("\033[31mRelation %lu of type %s members: %lu\n\033[0m", rel->id, osmrel_str(rel->type), rel->membercount);
	for(size_t i=0; i<rel->membercount; i++) {
		struct s_osmrel_member member = rel->members[i];
		printf("\t%8s %9lu role: %s \n", member_type(member.type), ((struct s_osmnode*)member.ptr)->id, member_role(member.role));
	}
}

void g_add_node(struct s_osmnode* node, void* a) {
	AdjGraph* g = (AdjGraph*) a;
	graph_add_node(g, node);
	assert(graph_node_count(g) > 0);
}
void g_add_way(struct s_osmway* way, void* a) {
	if(way->data.type != Highway) return;
	AdjGraph* g = (AdjGraph*) a;
	(void) g;
	for(size_t i=0; i<way->nodecount-1; i++) {
		AdjNode* a = graph_get_node(g, way->nodes[i]);
		AdjNode* b = graph_get_node(g, way->nodes[i+1]);
		if(a == NULL) {
			a = graph_add_node(g, way->nodes[i]);
		}
		if(b == NULL) {
			b = graph_add_node(g, way->nodes[i+1]);
		}
		assert(a != NULL);
		assert(b != NULL);
		coord diff = coord_minus(way->nodes[i]->coords, way->nodes[i+1]->coords);
		double dist = sqrt(diff.lat*diff.lat + diff.lon*diff.lon);
		graph_add_edge(g, a, b, dist, way);
	}
}

void print_nodes(void* data, char* dst)
{
	struct s_osmnode* node = (struct s_osmnode*) data;
	sprintf(dst, "%lu", node->id);
}

int main(int argc, const char* argv[]) {
	
	const char* map_name = DEFAULT_MAP;
	if(argc == 2)
		map_name = argv[1];


	osm_load(map_name, &map);

	size_t index_start = 9999420690;
	struct s_osmnode* a = osm_add_node(&map, index_start++, (coord){50.72694, 7.0842}, (struct s_wayattr) {.type = CustomType, .subtypes.custom = {.color = "#ff0000", .name = "trackpoint"}});
	struct s_osmnode* b = osm_add_node(&map, index_start++, (coord){50.72749, 7.08712}, (struct s_wayattr) {.type = CustomType, .subtypes.custom = {.color = "#ff0000", .name = "trackpoint"}});
	struct s_osmway* testtrack = osm_add_way(&map, index_start++, 2, false, (struct s_wayattr) {.type = CustomType, .subtypes.custom = {.color="#ff0000", .name="track"}});
	testtrack->nodes[0] = a;
	testtrack->nodes[1] = b;

	AdjGraph g;
	graph_init(&g, true);
	//bst_foreach_osmnodes(&map.nodes, g_add_node, &g);
	//bst_foreach_osmways(&map.ways, g_add_way, &g);
	//graph_prim_spanntree(&g);

	char* buf = malloc(1048576);
	if(!buf) {
		log_error("could not allocate enough memory to print the graph");
	} else {
		FILE* file = fopen("out.dot", "w");
		if(!file) {
			log_error("Could not open file out.dot with write permissions");
		} else {
			print_graph(&g, buf, 1048576, print_nodes);
			fwrite(buf, strlen(buf), 1, file);
			fclose(file);
		}
	}
	free(buf);

	osm_graph(&map, "map.svg", &g);
	graph_cleanup(&g);
	osm_free(&map);
	return 0;
}
