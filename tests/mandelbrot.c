#include "tpool.h"
#include "logger.h"
#include "util.h"
#include <complex.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

#define k 8
#define w 1920 * k
#define h 1080 * k
#define max_steps 400
#define min_x -2
#define max_x 2
#define min_y -2
#define max_y 2
#define BYTES_PER_PIXEL 3
#define THREADS 24

const int FILE_HEADER_SIZE = 14;
const int INFO_HEADER_SIZE = 40;

void worker(void* data);
void generateBitmapImage(unsigned char* image, int height, int width, char* imageFileName);
unsigned char* createBitmapFileHeader(int height, int stride);
unsigned char* createBitmapInfoHeader(int height, int width);

typedef long double ld;
typedef unsigned long ul;

struct pixel {
	unsigned long x;
	unsigned long y;
	long double complex c;
	unsigned char r;
	unsigned char g;
	unsigned char b;
};

struct pixel* pixels;
unsigned char* bitmap;

int main() {
	pixels = malloc(sizeof(struct pixel) * w * h);
	bitmap = malloc(sizeof(unsigned char) * h * w * BYTES_PER_PIXEL);

	tpool_t* pool = tpool_create(THREADS);

	ld step_x = (max_x - min_x) / ((ld) w);
	ld step_y = (max_y - min_y) / ((ld) h);
	ld min_real = 1000;
	ld max_real = -1000;
	for(ul x = 0; x<w; x++) {
		for(ul y = 0; y<h; y++) {
			ul i = x + y * w;
			pixels[i].x = x;
			pixels[i].y = y;
			pixels[i].c = (((ld) min_x) + step_x * ((ld) x)) 
				+ (((ld) min_y) + step_y * ((ld) y)) * I;
			min_real = MIN(creal(pixels[i].c), min_real);
			max_real = MAX(creal(pixels[i].c), max_real);
			tpool_add_work(pool, worker, &pixels[i]);
		}
	}
	log_info("Step size is %Lf, %Lf(i)", step_x, step_y);
	log_info("Real range is [%Lf, %Lf]", min_real, max_real);
	tpool_wait(pool);
	log_info("Set calculated");
	#define OUT_FILE "mandelbrot.bmp"
	generateBitmapImage((unsigned char*) bitmap, h, w, OUT_FILE);
	log_info("Wrote bitmap to "OUT_FILE);
	tpool_destroy(pool);
}




void generateBitmapImage (unsigned char* image, int height, int width, char* imageFileName)
{
    int widthInBytes = width * BYTES_PER_PIXEL;

    unsigned char padding[3] = {0, 0, 0};
    int paddingSize = (4 - (widthInBytes) % 4) % 4;

    int stride = (widthInBytes) + paddingSize;

    FILE* imageFile = fopen(imageFileName, "wb");

    unsigned char* fileHeader = createBitmapFileHeader(height, stride);
    fwrite(fileHeader, 1, FILE_HEADER_SIZE, imageFile);

    unsigned char* infoHeader = createBitmapInfoHeader(height, width);
    fwrite(infoHeader, 1, INFO_HEADER_SIZE, imageFile);

    int i;
    for (i = 0; i < height; i++) {
        fwrite(image + (i*widthInBytes), BYTES_PER_PIXEL, width, imageFile);
        fwrite(padding, 1, paddingSize, imageFile);
    }

    fclose(imageFile);
}

unsigned char* createBitmapFileHeader (int height, int stride)
{
    int fileSize = FILE_HEADER_SIZE + INFO_HEADER_SIZE + (stride * height);

    static unsigned char fileHeader[] = {
        0,0,     /// signature
        0,0,0,0, /// image file size in bytes
        0,0,0,0, /// reserved
        0,0,0,0, /// start of pixel array
    };

    fileHeader[ 0] = (unsigned char)('B');
    fileHeader[ 1] = (unsigned char)('M');
    fileHeader[ 2] = (unsigned char)(fileSize      );
    fileHeader[ 3] = (unsigned char)(fileSize >>  8);
    fileHeader[ 4] = (unsigned char)(fileSize >> 16);
    fileHeader[ 5] = (unsigned char)(fileSize >> 24);
    fileHeader[10] = (unsigned char)(FILE_HEADER_SIZE + INFO_HEADER_SIZE);

    return fileHeader;
}

unsigned char* createBitmapInfoHeader (int height, int width)
{
    static unsigned char infoHeader[] = {
        0,0,0,0, /// header size
        0,0,0,0, /// image width
        0,0,0,0, /// image height
        0,0,     /// number of color planes
        0,0,     /// bits per pixel
        0,0,0,0, /// compression
        0,0,0,0, /// image size
        0,0,0,0, /// horizontal resolution
        0,0,0,0, /// vertical resolution
        0,0,0,0, /// colors in color table
        0,0,0,0, /// important color count
    };

    infoHeader[ 0] = (unsigned char)(INFO_HEADER_SIZE);
    infoHeader[ 4] = (unsigned char)(width      );
    infoHeader[ 5] = (unsigned char)(width >>  8);
    infoHeader[ 6] = (unsigned char)(width >> 16);
    infoHeader[ 7] = (unsigned char)(width >> 24);
    infoHeader[ 8] = (unsigned char)(height      );
    infoHeader[ 9] = (unsigned char)(height >>  8);
    infoHeader[10] = (unsigned char)(height >> 16);
    infoHeader[11] = (unsigned char)(height >> 24);
    infoHeader[12] = (unsigned char)(1);
    infoHeader[14] = (unsigned char)(BYTES_PER_PIXEL*8);

    return infoHeader;
}

unsigned char interpolate(unsigned char a, unsigned char b, ld x) {
	return a * x + b * (1-x);
}

void worker(void* data)
{
	struct pixel* px = (struct pixel*) data;
	double complex z = 0;
	bool divergent = false;
	ld steps_survived;
	for(unsigned long i=0; i<max_steps && !divergent; i++) {
		z = z * z + px->c;
		if(cabs(z) > 4) {
			divergent = true;
			steps_survived = ((ld) i) / ((ld) max_steps);
		}
	}
	(void) steps_survived;
	if(divergent) {
		px->r = 0x00;
		px->g = 0x00;
		px->b = 0xFF;
	} else {
		px->r = 0xFF;
		px->g = 0x00;
		px->b = 0x00;
	}
	bitmap[(px->y * w + px->x) * BYTES_PER_PIXEL + 0] = px->r;
	bitmap[(px->y * w + px->x) * BYTES_PER_PIXEL + 1] = px->g;
	bitmap[(px->y * w + px->x) * BYTES_PER_PIXEL + 2] = px->b;
}
