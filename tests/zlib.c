#include <stdio.h>
#include <stdlib.h>

#include "zlib.h"
#include "logger.h"
#include "djtest.h"
#include "util.h"

const char* expected_static = 
"Hallo blah.\n"
"Hallo hallo blah hallo.\n"
"Hallo hallo hallo hallo hallo.\n"
"blah blah hallo blah.\n\n";

const char* expected_dynamic = 
"cc -L . -lasound -lm -lX11 -lGLX src/arena_allocator.o src/audio.o src/avltree.o src/bitreader.o src/bstree.o src/cmp.o src/color.o src/djwindow_x.o src/dlist.o src/drw_x.o src/fast_strlen.o src/graph.o src/heap.o src/logger.o src/matrix.o src/matrix_trans.o src/osm.o src/osm_colors.o src/osm_drw.o src/osmtypes.o src/png.o src/polygon.o\n"
"src/queue.o src/sort_fun.o src/stack.o src/svg.o src/tpool.o src/trie.o src/union_find.o src/util.o src/vector.o\n"
"src/wav.o src/xml.o src/zlib.o -shared -o libstdj.so\n"
"cc -std=c99 -Wall -pedantic -g -Werror -fsanitize=address -Iinclude -MMD -MP -c -fpic tests/zlib.c -o tests/zlib.o\n\n";

static const char* filename = "tests/fixed-testdata.bin";
static const char* f2 = "tests/zlib-dynamic.bin";

unsigned char* decomp_file(const char* fname, size_t* new_length)
{
	FILE* f = fopen(fname, "rb");
	if(!f) {
		log_error("Could not open file %s", filename);
		return NULL;
	}
	fseek(f, 0, SEEK_END);
	size_t size = ftell(f);
	fseek(f, 0, SEEK_SET);

	unsigned char buf[size];
	size_t bytes_read = fread(buf, 1, size, f);
	if(bytes_read != size) {
		log_error("Read %lu Size: %lu", bytes_read, size);
		log_error("Could not read the full file. Something gone wrong");
		fclose(f);
		return NULL;
	}
	fclose(f);

	unsigned char* uncompressed = zlib_decode(buf, size, new_length);
	return uncompressed;
}

int t_fixed(void)
{
	test_begin("Fixed Testdata");
	size_t len;
	unsigned char* uncompressed = decomp_file(filename, &len);
	assert_nonnull(uncompressed);
	assert_i_eq(len, strlen(expected_static)-1);
	assert_false(memcmp(uncompressed, expected_static, len));
	free(uncompressed);
	test_end();
}

int t_dynamic(void)
{
	test_begin("Dynamic Huffman tree");
	size_t len;
	unsigned char* uncompressed = decomp_file(f2, &len);
	assert_nonnull(uncompressed);
	assert_i_eq(len, strlen(expected_dynamic));
	assert_false(memcmp(uncompressed, expected_dynamic, len));
	free(uncompressed);
	test_end();
}

int t_huffman(void)
{
	test_begin("Testing huffman tree generation");
	const char* symbols[] = {"A", "B", "C", "D", "E", "F", "G", "H"};
	const uint8_t bit_lengths[] = {3, 3, 3, 3, 3, 2, 4, 4};
	const uint16_t expected_codes[] = {2, 3, 4, 5, 6, 0, 14, 15};
	assert_i_eq(N_ELEMENTS(symbols), N_ELEMENTS(bit_lengths));
	assert_i_eq(N_ELEMENTS(symbols), N_ELEMENTS(expected_codes));

	uint16_t calc_codes[N_ELEMENTS(symbols)];
	generate_huffman_codes(N_ELEMENTS(symbols), calc_codes, bit_lengths);

	for(int i=0; i<N_ELEMENTS(symbols); i++) {
		assert_i_eq(calc_codes[i], expected_codes[i]);
	}

	test_end();
}

int t_examples(void)
{
	test_begin("Test assortment of binaries");
	const char* examples[] = {"test3", "test2", "test1"};
	for(int i=0; i<sizeof(examples)/sizeof(examples[0]); i++) {
		char name[512];
		sprintf(name, "tests/zlib/%s_input.bin", examples[i]);
		FILE* f = fopen(name, "r");
		fseek(f, 0, SEEK_END);
		size_t length = ftell(f);
		fseek(f, 0, SEEK_SET);
		unsigned char* input = malloc(length);
		fread(input, 1, length, f);
		fclose(f);
		sprintf(name, "tests/zlib/%s_should.bin", examples[i]);
		f = fopen(name, "r");
		fseek(f, 0, SEEK_END);
		size_t should_length = ftell(f);
		fseek(f, 0, SEEK_SET);
		unsigned char* should = malloc(should_length);
		fread(should, 1, should_length, f);
		fclose(f);
	
		size_t is_length;
		log_debug("Testing %s", examples[i]);
		unsigned char* is = zlib_decode(input, length, &is_length);
		free(input);
		assert_nonnull(is);
		for(size_t i=0; i<MIN(is_length, should_length); i++) {
			if(is[i] != should[i]) {
				log_error("Binary mismatch at %#lx", i);
				free(is);
				free(should);
				assert_true(false);
			}
		}
		assert_l_eq(is_length, should_length);

		free(is);
		free(should);
	}
	test_end();
}

int main(void) {
	run_test(t_huffman);
	run_test(t_fixed);
	run_test(t_dynamic);
	run_test(t_examples);

	return 0;
}
