#include "png.h"
#include "svg.h"
#include "logger.h"

Image* img = NULL;
FILE* svg;

static void drw_pixel(int x, int y) {
	ImageColor c = get_color(img, x, y);
	char hex_c[12];
	sprintf(hex_c, "#%02x%02x%02x", c.r, c.g, c.b);
	svg_rect(svg, hex_c, "none", x, y, 1.001, 1.001, 0);
}

int main(int argc, const char* argv[]) 
{
	if(argc < 3) {
		log_error("Please provide a filename for the input_image and for the output image");
		return 1;
	}
	const char* in_img = argv[1];
	const char* out_img = argv[2];

	img = load_image(in_img);
	svg = svg_open(out_img, (viewport){.min = {0,0}, .dims={img->width, img->height}}, "none");
	for(int y=0; y<img->height; y++) {
		for(int x=0; x<img->width; x++) {
			drw_pixel(x, y);
		}
	}
	svg_close(svg);

	free_image(img);
}
