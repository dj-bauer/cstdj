#include "xml.h"
#include "logger.h"
#include <assert.h>
#include "attributes.h"

int main(void) {
	xml_dom* d = xml_read_file("tests/xml-testfiles/attr.xml");
	assert(d != NULL);

	xml_node* r = d->root;
	assert(r != NULL);
	log_debug("Root Attributes: %p", (void*) r->attributes);

	UNUSED xml_attr* a = d->root->attributes;
	//log_debug("Attributes parent: %p", a->parent);

	xml_free(d);
}
