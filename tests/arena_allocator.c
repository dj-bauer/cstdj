#include "arena_allocator.h"
#include "djtest.h"
#include "avltree.h"
#include "../src/vector_impl.h"

#include <time.h>
#include <stdlib.h>

int test1(void) {
	test_begin("[1]");
	MemoryArena a;
	init_memarena(&a);
	a.minimum_bs = 4*sizeof(int);
	int* first = (int*) memarena_alloc(&a, sizeof(int));
	*first = 4;
	assert_true(a.zones.head->data.allocated == sizeof(int));
	assert_true(a.zones.head->data.capacity == sizeof(int)*4);
	int* second = (int*) memarena_alloc(&a, sizeof(int));
	*second = 2;
	assert_i_eq(*first, 4);
	assert_i_eq(*(first+1), 2);
	int* third = (int*) memarena_alloc(&a, sizeof(int));
	int* fourth = (int*) memarena_alloc(&a, sizeof(int));
	assert_l_eq(a.capacity, a.allocated);
	assert_l_eq(a.allocated, sizeof(int)*4);
	*third = 3;
	*fourth = 42;
	deinit_memarena(&a);
	test_end();
}

typedef struct s_memwatcher {
	int* addr;
	int value;
} MemContent;
static inline bool dj_eq_mem(MemContent a, MemContent b) { return a.addr == b.addr; }
static inline bool dj_lt_mem(MemContent a, MemContent b) { return a.addr < b.addr; }

VECTOR_DECL(mem, struct s_memwatcher)
VECTOR_IMPL(mem, struct s_memwatcher)

int random_data(void) {
	test_begin("Random Access");
	Vector_mem* storage = create_vector_mem();
	MemoryArena alloc;
	init_memarena(&alloc);
	for(size_t i=0; i<1000; i++) {
		int* new_addr = (int*) memarena_alloc(&alloc, sizeof(int));
		*new_addr = rand();
		MemContent content = {.addr = new_addr, .value = *new_addr };
		vector_append_mem(storage, content);

		for(size_t j=0; j<i; j++) {
			for(size_t index = 0; index < vector_size_mem(storage); index++) {
				MemContent truth = *vector_at_mem(storage, index);
				assert_i_eq(truth.value, *truth.addr);
			}
		}
	}
	destroy_vector_mem(storage);
	deinit_memarena(&alloc);
	test_end();
}

int t_calloc(void) {
	test_begin("Calloc");
	MemoryArena alloc;
	init_memarena(&alloc);

	for(size_t i=0; i<1000; i++) {
		size_t len = rand() % 256;
		char* c = memarena_calloc(&alloc, len);
		for(size_t i=0; i<len; i++) {
			assert_zero(c[i]);
		}
	}

	deinit_memarena(&alloc);
	test_end();
}


int main(void) {
	srand(time(NULL));
	run_test(test1);
	run_test(random_data);
	run_test(t_calloc);
}
