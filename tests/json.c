#include "djson.h"
#include <stdio.h>
#include <stdlib.h>

typedef enum {ERROR, PASS, FAIL} TestStatus;

TestStatus testparse(const char* json_str, size_t json_len, int debug_out)
{
	JNode* root = json_parse(json_str, json_len);
    if(root != NULL)
	    json_free(root);
	return root != NULL ? PASS : FAIL;
}

TestStatus test(const char* path, int debug_out)
{
	FILE* f = fopen(path, "ro");
	if(f == NULL) return ERROR;
	fseek(f, 0, SEEK_END);
	long len = ftell(f);
	fseek(f, 0, SEEK_SET);
	char* data = malloc(len+1);
	fread(data, 1, len, f);
	data[len] = '\0';
	fclose(f);
	
	TestStatus status = testparse(data, len, debug_out);
	free(data);

	return status;
}

int main(int argc, const char* argv[])
{
	const char* path = argv[1];
	int debug_out = 0;
	TestStatus result = test(path, debug_out);
	return result == PASS ? 0 : 1;
}
