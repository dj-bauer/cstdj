#include "bstree.h"
#include "djtest.h"
#include <stdlib.h>
#include <time.h>
#include <stdio.h>

static void gen_random_str(char* dst, size_t len) {
	dst[len] = '\0';
	for(size_t i=0; i<len; i++) {
		int v = 'A' + rand() % ('a'-'z');
		v += (rand() % 2) ? 32: 0;
		dst[i] = v;
	}
}

#define LT_MACRO_ADV(suffix, type) \
	bool lt_##suffix(type a, type b) { return a < b; } \
	bool gt_##suffix(type a, type b) { return a > b; } \
	bool eq_##suffix(type a, type b) { return a == b; }
#define LT_MACRO(type) LT_MACRO_ADV(type, type)
bool lt_s(const char* a, const char* b) { return strcmp(a, b) < 0; }
bool gt_s(const char* a, const char* b) { return strcmp(a, b) > 0; }
bool eq_s(const char* a, const char* b) { return strcmp(a, b) == 0; }
#define lt_cs lt_s
#define gt_cs gt_s
#define eq_cs eq_s
LT_MACRO_ADV(i, int)
LT_MACRO_ADV(d, double)
LT_MACRO_ADV(f, float)
LT_MACRO_ADV(v, void*)

#define CREATE_TESTS_ADV(suffix, type, fmt) \
	static size_t bst_count_nodes_##suffix(const struct s_bstnode_##suffix* n) { \
		if(n == NULL) return 0; \
		size_t count = 1; \
		count += bst_count_nodes_##suffix(n->left); \
		count += bst_count_nodes_##suffix(n->right); \
		return count; \
	} \
	static bool bst_intact_rek_##suffix(const struct s_bstnode_##suffix* n) { \
		if(n == NULL) return true; \
		if(n->left != NULL) { \
			if(!lt_##suffix(n->left->data, n->data)) return false;\
		} \
		if(n->right != NULL) { \
			if(!gt_##suffix(n->right->data, n->data)) return false;\
		} \
		if(n->parent != NULL) { \
			if(n->parent->left != n && n->parent->right != n)  return false; \
		} \
		return bst_intact_rek_##suffix(n->left) && bst_intact_rek_##suffix(n->right); \
	} \
	static inline bool bst_intact_##suffix(const BSTree_##suffix* t) { \
		return bst_intact_rek_##suffix(t->root); \
	} \
	static void shuffle_##suffix(type* array, size_t n) { \
		if (n > 1) { \
			for (size_t i = 0; i < n - 1; i++) { \
			  size_t j = i + rand() / (RAND_MAX / (n - i) + 1); \
			  type t = array[j]; \
			  array[j] = array[i]; \
			  array[i] = t; \
			} \
        } \
    } \
	static type random_values_##suffix[512]; \
	int test_creation_##suffix() {\
		test_begin("Creating "#suffix); \
		BSTree_##suffix* t = create_bst_##suffix(); \
		assert_l_eq(bst_size_##suffix(t), 0); \
		assert_null(t->root); \
		destroy_bst_##suffix(t); \
		test_end(); \
	}  \
	int test_insertion_##suffix() { \
		test_begin("Insertion "#suffix); \
		BSTree_##suffix* t = create_bst_##suffix(); \
		assert_true(bst_insert_##suffix(t, fixed_values_##suffix[0])); \
		assert_l_eq(bst_size_##suffix(t), 1); \
		assert_false(bst_insert_##suffix(t, fixed_values_##suffix[0])); \
		assert_l_eq(bst_size_##suffix(t), 1); \
		assert_true(bst_contains_##suffix(t, fixed_values_##suffix[0])); \
		assert_true(bst_insert_##suffix(t, fixed_values_##suffix[1])); \
		assert_true(bst_insert_##suffix(t, fixed_values_##suffix[2])); \
		assert_true(bst_insert_##suffix(t, fixed_values_##suffix[3])); \
		assert_true(bst_insert_##suffix(t, fixed_values_##suffix[4])); \
		assert_true(bst_insert_##suffix(t, fixed_values_##suffix[5])); \
		destroy_bst_##suffix(t); \
		test_end(); \
	} \
	int test_removal_##suffix() { \
		test_begin("Removal "#suffix); \
		BSTree_##suffix* t = create_bst_##suffix(); \
		bst_insert_##suffix(t, random_values_##suffix[0]); \
		assert_true(bst_remove_##suffix(t, random_values_##suffix[0])); \
		assert_false(bst_remove_##suffix(t, random_values_##suffix[0])); \
		destroy_bst_##suffix(t); \
		t = create_bst_##suffix(); \
		bst_insert_##suffix(t, fixed_values_##suffix[2]); /* 3*/ \
		bst_insert_##suffix(t, fixed_values_##suffix[6]); /* 7*/ \
		bst_insert_##suffix(t, fixed_values_##suffix[0]); /* 1 */ \
		bst_insert_##suffix(t, fixed_values_##suffix[3]); /* -2 */ \
		bst_insert_##suffix(t, fixed_values_##suffix[5]); /* 4 */ \
		bst_insert_##suffix(t, fixed_values_##suffix[1]); /* 5 */ \
		bst_insert_##suffix(t, fixed_values_##suffix[7]); /* 10 */ \
		assert_true(bst_remove_##suffix(t, fixed_values_##suffix[6])); \
		destroy_bst_##suffix(t); \
		test_end(); \
	} \
	int test_random_##suffix() { \
		test_begin("Random insertion"#suffix); \
		BSTree_##suffix* t = create_bst_##suffix(); \
		for(int i=0; i<30; i++) { \
			type v = random_values_##suffix[i]; \
			if(!bst_contains_##suffix(t, v)) { \
				assert_true(bst_insert_##suffix(t, v)); \
				assert_true(bst_contains_##suffix(t, v)); \
			} else { \
				assert_false(bst_insert_##suffix(t, v)); \
			} \
		} \
		destroy_bst_##suffix(t); \
		test_end(); \
	} \
	int test_random_removal_##suffix() { \
		test_begin("Random Removal "#suffix); \
		BSTree_##suffix* t = create_bst_##suffix(); \
		for(int i=0; i<512; i++) { \
			bst_insert_##suffix(t, random_values_##suffix[i]); \
			if(!bst_intact_##suffix(t)) { \
				bst_generate_graph_##suffix(t, "graph_"#suffix".dot"); \
				assert_true(false && "Graph broken"); \
			} \
			assert_l_eq(bst_size_##suffix(t), bst_count_nodes_##suffix(t->root)); \
		} \
		shuffle_##suffix(random_values_##suffix, 512); \
		for(int i=0; i<200; i++) {\
			type v = random_values_##suffix[i]; \
			if(!bst_intact_##suffix(t)) { \
				bst_generate_graph_##suffix(t, "graph_"#suffix".dot"); \
				assert_true(false && "Graph broken"); \
			} else { \
				/*bst_generate_graph_##suffix(t, "graph_"#suffix"_working.dot");*/ \
			} \
			size_t old_size = bst_size_##suffix(t); \
			if(bst_contains_##suffix(t, v)) { \
				assert_true(bst_remove_##suffix(t, v)); \
				assert_l_eq(bst_size_##suffix(t), old_size-1); \
				if(bst_size_##suffix(t) != bst_count_nodes_##suffix(t->root)) { \
					bst_generate_graph_##suffix(t, "graph_"#suffix".dot"); \
				} \
				assert_l_eq(bst_size_##suffix(t), bst_count_nodes_##suffix(t->root)); \
			} else { \
				assert_false(bst_remove_##suffix(t, v)); \
				assert_l_eq(bst_size_##suffix(t), old_size); \
				assert_l_eq(bst_size_##suffix(t), bst_count_nodes_##suffix(t->root)); \
			} \
			assert_false(bst_contains_##suffix(t, v)); \
		} \
		destroy_bst_##suffix(t); \
		test_end(); \
	}
#define RUN_TESTS(type) \
	run_test(test_creation_##type); \
	run_test(test_insertion_##type); \
	run_test(test_removal_##type); \
	run_test(test_random_##type); \
	run_test(test_random_removal_##type);
#define CREATE_TESTS(t, s) CREATE_TESTS_ADV(t, t, s)

int fixed_values_i[12] = {1, 5, 3, -2, -1, 4, 7, 10};
float fixed_values_f[12] = {1, 5, 3, -2, -1, 4, 7, 10};
double fixed_values_d[12] = {1, 5, 3, -2, -1, 4, 7, 10};
const char* fixed_values_cs[12] = {"a", "W", "R", "B", "A", "D", "F", "G"};

CREATE_TESTS_ADV(i, int, "%d")
CREATE_TESTS_ADV(f, float, "%f")
CREATE_TESTS_ADV(d, double, "%f")
CREATE_TESTS_ADV(cs, const char*, "%s")
char random_strings[512][24];

int main() {
	srand(time(NULL));
	for(int i=0; i<512; i++) {
		random_values_i[i] = rand() % 1024;
		random_values_d[i] = (double) rand() / (double) RAND_MAX;
		random_values_f[i] = random_values_d[i];
		gen_random_str(random_strings[i], 23);
		random_values_cs[i] = random_strings[i];
	}
	RUN_TESTS(i);
	RUN_TESTS(f);
	RUN_TESTS(d);
	RUN_TESTS(cs);
}
