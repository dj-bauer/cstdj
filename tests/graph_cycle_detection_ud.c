#include "djtest.h"
#include "graph.h"

int t_automated(const char* name)
{
	log_info("Testing Automated %s", name);
	int tests_passed = 0;

	char fname_in[128];
	char fname_out[128];
	snprintf(fname_in, 128, "tests/cycle_detect_ud-data/in/%s.in", name);
	snprintf(fname_out, 128, "tests/cycle_detect_ud-data/out/%s.out", name);
	FILE* fin = fopen(fname_in, "r");
	FILE* fout = fopen(fname_out, "r");

	AdjGraph g;
	graph_init(&g, false);

	long N, M;
	size_t numbers_read = fscanf(fin, "%ld %ld\n", &N, &M);
	assert_i_eq(numbers_read, 1);

	for(size_t i=0; i<N; i++) {
		graph_add_node(&g, NULL);
	}
	for(size_t i=0; i<M; i++) {
		long a, b;
		numbers_read = fscanf(fin, "%ld %ld\n", &a, &b);
		graph_add_edge_i(&g, a, b, 1, NULL);
	}
	long expected;
	fscanf(fout, "%ld", &expected);
	if(graph_has_circle_undirected(&g)) {
		assert_i_eq(expected, -1);
	} else {
		assert_i_ne(expected, -1);
	}

	graph_cleanup(&g);
	fclose(fin);
	fclose(fout);
	test_end();
}

int main(void)
{
	const char* filedir = "tests/cycle_detect_ud-data/in/";
	test_inout(filedir, t_automated);
	return 0;
}
