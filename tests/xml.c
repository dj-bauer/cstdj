#include "xml.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "djtest.h"
#include "util.h"

#define TESTDIR "tests/xml-testfiles/"

void trim_trailing_newline(char* str)
{
	char* i = strrchr(str, '\n');
	if(i) {
		if(i - str == strlen(str)-1) {
			*i = 0;
		}
	}
}

int main(void)
{
	char* buf = NULL;
	size_t len = 0;
	int nr = 0;
	while(getline(&buf, &len, stdin) > 1) {
		trim_trailing_newline(buf);
		log_info("Attempting to parse %s", buf);
		xml_dom* dom = xml_read_file(buf);
		if(!dom) {
			log_error("Could not parse %s", buf);
			continue;
		}
		xml_free(dom);
		nr += 1;
		log_info("Nr. %d Parsed %s\n", nr, buf);
	}
	free(buf);

	return 0;
}
