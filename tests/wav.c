#include "wav.h"
#include <stdlib.h>

int main() 
{
	SoundFormat format;
	int num_channels;
	int num_samples;
	int bits_per_sample;
	int buffer_size;
	char* data = load_wav_file("tests/test.wav", 
			&num_channels, &num_samples, &bits_per_sample, 
			&buffer_size, &format);

	free(data);
}
