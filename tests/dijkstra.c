#include "djtest.h"
#include "graph.h"

void print_func(void* node, char buf[32])
{
	snprintf(buf, 32, "%ld", (long) node);
}

int t_automated(const char* name) {
	log_info("Testing Automated \033[1m%s\033[0m", name);
	int tests_passed = 0;

	char fname_in[128];
	char fname_out[128];
	snprintf(fname_in, 128, "tests/dijkstra-data/in/%s.in", name);
	snprintf(fname_out, 128, "tests/dijkstra-data/out/%s.out", name);
	FILE* fin = fopen(fname_in, "r");
	FILE* fout = fopen(fname_out, "r");

	AdjGraph g;
	graph_init(&g, true);

	long N, M, s, t;
	size_t numbers_read = fscanf(fin, "%ld %ld %ld %ld\n", &N, &M, &s, &t);
	assert_i_eq(numbers_read, 4);

	for(long i=0; i<N; i++) {
		graph_add_node(&g, (void*) i);
	}
	AdjNode* start = graph_node(&g, s);
	AdjNode* dest = graph_node(&g, t);
	for(long i=0; i<M; i++) {
		long a, b, c;
		numbers_read = fscanf(fin, "%ld %ld %ld\n", &a, &b, &c);
		assert_i_eq(numbers_read, 3);
		graph_add_edge_i(&g, a, b, c, NULL);
	}
	log_trace("Setup Graph... Running dijkstra...");
	bool found_it = graph_dijkstra_to(&g, start, dest);
	log_trace("Dijkstra done. Retracing and checking");

	long dist, edgecount;
	numbers_read = fscanf(fout, "%ld %ld\n", &dist, &edgecount);
	if(numbers_read == 1) {
		assert_i_eq(dist, -1);
		assert_false(found_it);
	} else {
		assert_i_eq(numbers_read, 2);
		assert_true(found_it);

		dlist_l path;
		init_dlist_l(&path);
		double total_weight;
		graph_retrace_dijkstra(&path, &total_weight, &g, dest);

		assert_i_eq(dist, total_weight);
		if(edgecount == dlist_size_l(&path)) {
			assert_i_eq(edgecount, dlist_size_l(&path));

			dlistnode_l* e_node = path.head;
			while(e_node) {
				long u, v;
				numbers_read = fscanf(fout, "%ld %ld\n", &u, &v);
				assert_i_eq(numbers_read, 2);

				AdjEdge* e = graph_edge(&g, e_node->data);

				assert_i_eq(e->start, u);
				assert_i_eq(e->end, v);
				e_node = e_node->next;
			}
		} else {
			log_warn("Found a different path with the same distance");
		}

		deinit_dlist_l(&path);
	}
	graph_cleanup(&g);

	fclose(fin);
	fclose(fout);

	test_end();
}

int main(void) {
	const char* filedir = "tests/dijkstra-data/in/";
	test_inout(filedir, t_automated);
}
