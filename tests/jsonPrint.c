#include "djson.h"
#include <stdio.h>
#include <stdlib.h>

int main(int argc, const char* argv[])
{
    if(argc == 1) {
        fprintf(stderr, "usage: %s <filename>\n", argv[0]);
        return 1;
    }
    FILE* f = fopen(argv[1], "r");
    size_t buf_len = ftell(f);
    char* buf = malloc(buf_len);

    JNode* root = json_parse(buf, buf_len);
    if(root == NULL)
        return 1;

    free(buf);
    json_print(root);
    json_free(root);
    return 0;
}
