#include "stack.h"
#include "djtest.h"

int t_creation() {
	test_begin("Creation");
	Stack_i* s = create_stack_i();
	assert_nonnull(s);
	assert_i_eq(s->size, 0);
	destroy_stack_i(&s);
	test_end();
}
int t_push() {
	test_begin("push");
	Stack_i* s = create_stack_i();
	stack_push_i(s, 1);
	assert_i_eq(*stack_top_i(s), 1); 
	stack_push_i(s, 2);
	assert_i_eq(*stack_top_i(s), 2); 
	stack_push_i(s, 3);
	assert_i_eq(*stack_top_i(s), 3); 
	assert_i_eq(s->size, 3);
	destroy_stack_i(&s);
	test_end();
}
int t_pop() {
	test_begin("pop");
	Stack_i* s = create_stack_i();
	stack_push_i(s, 1);
	stack_push_i(s, 2);
	stack_push_i(s, 3);
	stack_pop_i(s);
	assert_i_eq(*stack_top_i(s), 2); 
	stack_pop_i(s);
	assert_i_eq(*stack_top_i(s), 1); 
	stack_pop_i(s);
	assert_null(stack_top_i(s));
	assert_i_eq(s->size, 0);
	destroy_stack_i(&s);
	test_end();
}

int main() {
	run_test(t_creation);
	run_test(t_push);
	run_test(t_pop);
}
