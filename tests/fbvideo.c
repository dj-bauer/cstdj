#include "fbvideo.h"
#include <stdio.h>

struct s_rgb {
	unsigned char r;
	unsigned char g;
	unsigned char b;
};

struct s_rgb screen_color(const Video* v, int x, int y) {
	double xp = ((double) x) / ((double) v->width);
	double yp = ((double) y) / ((double) v->height);

	return (struct s_rgb) {xp * 0xFF, yp * 0xFF, (1.0-yp) * 0xFF};
}

int main(void) {
	Video v;
	if(fb_open(&v)) {
		fprintf(stderr, "Error open video");
	}
	for(int y=0; y<v.height; y++) {
		for(int x=0; x<v.width; x++) {
			struct s_rgb c = screen_color(&v, x, y);
			write_pixel(&v, x, y, c.r, c.g, c.b);
		}
	}
	/*for(int y=0; y<0xFF; y++) {
		for(int x=0; x<0xFF; x++) {
			write_pixel(&v, x, y, x, y, 0xFF - x);
			write_pixel(&v, v.width-1-x, y, x, y, 0xFF - x);
			write_pixel(&v, x, v.height-1-y, x, y, 0xFF - x);
			write_pixel(&v, v.width-x, v.height-1-y, x, y, 0xFF - x);
		}
	}*/
	getchar();
	fb_close(&v);
}
