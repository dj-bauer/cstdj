#include "union_find.h"
#include "djtest.h"
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

int t_create(void)
{
	test_begin("Union Find Creation");
    UnionFind* uf = unionfind_create(12);

    unionfind_destroy(uf);
	test_end();
}

static void print_uf(const UnionFind* uf, const char* filename)
{
    FILE* f = fopen(filename, "w");
    fprintf(f, "digraph {\n");
    for(long i=0; i<uf->num_indices; i++) {
        fprintf(f, "\t%lu -> %lu\n", i, uf->represents[i]);
    }
    fprintf(f, "}\n");
    fclose(f);
}

int t_print(void)
{
    test_begin("Union Find Print");
    UnionFind* uf = unionfind_create(12);

    unionfind_connect(uf, 4, 6);
    unionfind_connect(uf, 2, 7);
    unionfind_connect(uf, 4, 7);
    unionfind_connect(uf, 3, 2);

    print_uf(uf, "graph_1.dot");

    unionfind_representative(uf, 2);

    print_uf(uf, "graph_2.dot");

    unionfind_destroy(uf);
    test_end();
}

int t_automated(const char* testname) 
{
	char testtitle[64] = "Testing automated "; 
	strncat(testtitle, testname, 48);
	int tests_passed = 0;
	log_info(testtitle);

	char intitle[128];
	snprintf(intitle, 128, "tests/uf-data/in/%s.in", testname);
	FILE* infile = fopen(intitle, "r");
	char outtitle[128];
	snprintf(outtitle, 128, "tests/uf-data/out/%s.out", testname);
	FILE* outfile = fopen(outtitle, "r");

	size_t num_nodes, num_operations;
	int numbers_read = fscanf(infile, "%lu %lu\n", &num_nodes, &num_operations);
	assert_i_eq(numbers_read, 2);
	UnionFind* uf = unionfind_create(num_nodes);

	for(size_t i=0; i<num_operations; i++) {
		int t1, u1, u2;
		numbers_read = fscanf(infile, "%d %d %d\n", &t1, &u1, &u2);
		assert_i_eq(numbers_read, 3);
		if(t1 == 0) {
			unionfind_connect(uf, u1, u2);
		} else if (t1 == 1) {
			int is_connected = unionfind_check(uf, u1, u2);
			int should_connect;
			numbers_read = fscanf(outfile, "%d\n", &should_connect);
			assert_i_eq(numbers_read, 1);
			assert_i_eq(should_connect, is_connected);
		} else assert_true(false);
	}
	fclose(infile);
	fclose(outfile);

	unionfind_destroy(uf);

	test_end();
}

int main(void) 
{
    run_test(t_create);
    run_test(t_print);
	test_inout("tests/uf-data/in/", t_automated);
	return 0;
}
