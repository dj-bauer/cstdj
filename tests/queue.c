#include "djtest.h"
#include "queue.h"
#include <stdio.h>

int empty_queue() {
	test_begin("Empty Queue");
	queue_i* q = queue_create_i();

	assert_true(queue_empty_i(q));
	assert_null(queue_front_i(q));

	queue_destroy_i(q);
	test_end();
}

int single_element() {
	test_begin("Single Element");
	queue_i* q = queue_create_i();

	queue_enq_i(q, 1);
	assert_false(queue_empty_i(q));
	assert_nonnull(queue_front_i(q));

	assert_i_eq(*queue_front_i(q), 1);
	queue_deq_i(q);
	assert_true(queue_empty_i(q));
	assert_null(queue_front_i(q));

	queue_destroy_i(q);
	test_end();
}

int two_elements() {
	test_begin("Two Elements");
	queue_i* q = queue_create_i();

	queue_enq_i(q, 1);
	queue_enq_i(q, 2);
	assert_nonnull(queue_front_i(q));
	assert_false(queue_empty_i(q));

	assert_i_eq(*queue_front_i(q), 1);
	queue_deq_i(q);
	assert_i_eq(*queue_front_i(q), 2);
	assert_false(queue_empty_i(q));

	queue_destroy_i(q);
	test_end();
}

int main() {
	empty_queue();
	single_element();
	two_elements();
	return 0;
}
