#include "dlist.h"
#include "djtest.h"

int t_create_list(void) {
	test_begin("List Creation");
	dlist_i* l = create_dlist_i();
	assert_nonnull(l);
	assert_i_eq(dlist_size_i(l), 0);

	destroy_dlist_i(&l);
	assert_null(l);
	test_end();
}
int t_append(void) {
	test_begin("Appending");
	dlist_i* l = create_dlist_i();

	dlist_append_i(l, 1);
	assert_i_eq(dlist_size_i(l), 1);
	assert_nonnull(l->head);
	assert_nonnull(l->tail);
	assert_i_eq(l->head->data, 1);
	assert_null(l->head->prev);
	assert_null(l->head->next);
	assert_p_eq(l->head, l->tail);

	dlist_append_i(l, 2);
	assert_i_eq(dlist_size_i(l), 2);
	assert_nonnull(l->head);
	assert_nonnull(l->tail);
	assert_i_eq(l->head->data, 1);
	assert_null(l->head->prev);
	assert_nonnull(l->head->next);
	assert_p_ne(l->head, l->tail);
	assert_i_eq(l->tail->data, 2);
	assert_p_eq(l->tail->prev, l->head);
	assert_p_eq(l->head->next, l->tail);
	assert_null(l->tail->next);
	assert_nonnull(l->tail->prev);

	destroy_dlist_i(&l);
	test_end();
}
int t_prepend(void) {
	test_begin("Appending");
	dlist_i* l = create_dlist_i();

	dlist_prepend_i(l, 1);
	assert_i_eq(dlist_size_i(l), 1);
	assert_nonnull(l->head);
	assert_nonnull(l->tail);
	assert_i_eq(l->head->data, 1);
	assert_null(l->head->prev);
	assert_null(l->head->next);
	assert_p_eq(l->head, l->tail);

	dlist_prepend_i(l, 2);
	assert_i_eq(dlist_size_i(l), 2);
	assert_nonnull(l->head);
	assert_nonnull(l->tail);
	assert_i_eq(l->head->data, 2);
	assert_null(l->head->prev);
	assert_nonnull(l->head->next);
	assert_p_ne(l->head, l->tail);
	assert_i_eq(l->tail->data, 1);
	assert_p_eq(l->tail->prev, l->head);
	assert_p_eq(l->head->next, l->tail);
	assert_null(l->tail->next);
	assert_nonnull(l->tail->prev);

	destroy_dlist_i(&l);
	test_end();
}

int t_removal(void) {
    test_begin("Remove Node");

    dlist_i* l = create_dlist_i();
    dlist_append_i(l, 1);
    assert_i_eq(*dlist_first_i(l), 1);
    assert_i_eq(*dlist_last_i(l), 1);

    dlist_append_i(l, 2);
    assert_i_eq(*dlist_last_i(l), 2);
    assert_i_eq(dlist_size_i(l), 2);

    dlist_remove_last_i(l);
    assert_i_eq(*dlist_last_i(l), 1);
    assert_i_eq(dlist_size_i(l), 1);

    destroy_dlist_i(&l);
    test_end();
}
int t_remove_last_and_add(void) {
	test_begin("Remove last node and add new one");
	dlist_i* l = create_dlist_i();
	
	dlist_append_i(l, 1);
	dlist_remove_last_i(l);
	assert_null(dlist_last_i(l));
	assert_null(dlist_first_i(l));
	dlist_append_i(l, 2);
	assert_nonnull(dlist_first_i(l));
	assert_nonnull(dlist_last_i(l));
	assert_i_eq(*dlist_first_i(l), 2);
	assert_i_eq(*dlist_last_i(l), 2);

	destroy_dlist_i(&l);
	test_end();
}

int main(void) {
	run_test(t_create_list);
	run_test(t_append);
	run_test(t_prepend);
	run_test(t_remove_last_and_add);
    run_test(t_removal);
}
