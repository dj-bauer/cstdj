#include "logger.h"

int main()
{
	log_trace("Test %s", "Foo");
	log_debug("Debug");
	log_info("Info %d", 4);
	log_warn("Achtung achtung");
	log_error("Kaputt im Kopf");
}
