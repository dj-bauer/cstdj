#include "djtest.h"
#include "heap.h"
#include <string.h>
#include <float.h>

int lt(const int* a, const int* b) {
	if(*a == *b) return 0;
	if(*a < *b) return -1;
	return 1;
}

int heap_creation(void) {
	test_begin("Heap creation");

	heap_i stackheap;
	heap_init_i(&stackheap, lt);
	assert_null(stackheap.data);
	assert_true(heap_is_empty(&stackheap));
	heap_cleanup_i(&stackheap);
	assert_null(stackheap.data);

	heap_i* dyn_heap = heap_create_i(lt);
	assert_nonnull(dyn_heap);
	assert_null(dyn_heap->data);
	assert_true(heap_is_empty(dyn_heap));

	heap_destroy_i(&dyn_heap);
	assert_null(dyn_heap);

	test_end();
}

int heap_insert(void) {
	test_begin("Insert");

	heap_i* h = heap_create_i(lt);
	
	heap_insert_i(h, 2);
	heap_insert_i(h, 9);
	heap_insert_i(h, 3);
	heap_insert_i(h, 0);
	heap_insert_i(h, 1);
	assert_i_eq(h->size, 5);

	// Check if the heap property is still intact
	for(size_t i=1; i<h->size; i++) {
		size_t parent = (i-1) / 2;
		assert_i_ge(h->data[i], h->data[parent]);
	}

	heap_destroy_i(&h);

	test_end();
}

int heap_remove() {
	test_begin("Remove");
	heap_i* h = heap_create_i(lt);

	heap_insert_i(h, 2);
	heap_insert_i(h, 9);
	heap_insert_i(h, 3);
	heap_insert_i(h, 0);
	heap_insert_i(h, 1);

	// Check if the heap property is still intact
	for(size_t i=1; i<h->size; i++) {
		size_t parent = (i-1) / 2;
		assert_i_ge(h->data[i], h->data[parent]);
	}
	assert_i_eq(*heap_front_i(h), 0);

	heap_pop_i(h);
	// Check if the heap property is still intact
	for(size_t i=1; i<h->size; i++) {
		size_t parent = (i-1) / 2;
		assert_i_ge(h->data[i], h->data[parent]);
	}
	assert_i_eq(*heap_front_i(h), 1);

	heap_pop_i(h);

	// Check if the heap property is still intact
	for(size_t i=1; i<h->size; i++) {
		size_t parent = (i-1) / 2;
		assert_i_ge(h->data[i], h->data[parent]);
	}
	assert_i_eq(*heap_front_i(h), 2);

	heap_pop_i(h);

	// Check if the heap property is still intact
	for(size_t i=1; i<h->size; i++) {
		size_t parent = (i-1) / 2;
		assert_i_ge(h->data[i], h->data[parent]);
	}
	assert_i_eq(*heap_front_i(h), 3);

	heap_pop_i(h);
	// Check if the heap property is still intact
	for(size_t i=1; i<h->size; i++) {
		size_t parent = (i-1) / 2;
		assert_i_ge(h->data[i], h->data[parent]);
	}
	assert_i_eq(*heap_front_i(h), 9);

	// Check if the heap property is still intact
	for(size_t i=1; i<h->size; i++) {
		size_t parent = (i-1) / 2;
		assert_i_ge(h->data[i], h->data[parent]);
	}

	heap_destroy_i(&h);
	test_end();
}

int cmp(const char** a, const char** b) {
	return strcmp(*a, *b);
}

int heap_string(void) {
	test_begin("Sorting strings");
	heap_s* h = heap_create_s(cmp);

	heap_insert_s(h, "Hallo");
	heap_insert_s(h, "Welt");
	heap_insert_s(h, "a");
	heap_insert_s(h, "b");
	heap_insert_s(h, "ab");
	heap_insert_s(h, "AB");
	heap_insert_s(h, "A");

	heap_print_s(h);

	heap_destroy_s(&h);

	test_end();
}

struct s_dbl_cmp {
	double w;
};
int t_dbl_cmp(const void** a_ptr, const void** b_ptr) 
{
	const struct s_dbl_cmp* a = (const struct s_dbl_cmp*) *a_ptr;
	const struct s_dbl_cmp* b = (const struct s_dbl_cmp*) *b_ptr;
	double diff = a->w - b->w;
	if(diff > 0)
		return 1;
	else if(diff == 0)
		return 0;
	else
		return -1;
}
int t_dbl_max(void)
{
	test_begin("Max double comparison");
	heap_v h;
	heap_init_v(&h, t_dbl_cmp);
	struct s_dbl_cmp a = {DBL_MAX};
	struct s_dbl_cmp b = {DBL_MAX};
	struct s_dbl_cmp c = {0};
	struct s_dbl_cmp d = {DBL_MAX};
	struct s_dbl_cmp* ap = &a;
	struct s_dbl_cmp* cp = &c;
	assert_i_eq(t_dbl_cmp((const void**) &ap, (const void**) &cp), 1);
	heap_insert_v(&h, &a);
	heap_insert_v(&h, &b);
	heap_insert_v(&h, &c);
	heap_insert_v(&h, &d);
	assert_p_eq(&c, *heap_front_v(&h));
	heap_cleanup_v(&h);
	test_end();
}

int main(void) {
	heap_creation();
	heap_insert();
	heap_remove();
	heap_string();

	run_test(t_dbl_max);

	return 0;
}
